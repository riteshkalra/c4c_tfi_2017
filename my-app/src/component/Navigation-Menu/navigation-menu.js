import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import avatar3 from '../../../node_modules/admin-lte/dist/img/avatar3.png';

class NavigationMenu extends Component {
            render(){
                return (
                    <aside className="main-sidebar">
                        {/* sidebar: style can be found in sidebar.less */}
                        <section className="sidebar" >
                            {/* Sidebar user panel */}
                            <div className="user-panel">
                                <div className="pull-left image">
                                    <img src= {avatar3}  className="img-circle" alt="User " />
                                </div>
                                <div className="pull-left info">
                                    <p>Albina Akhlaq</p>
                                  
                                </div>
                            </div>
                            
                            {/* sidebar menu: : style can be found in sidebar.less */}
                            <ul className="sidebar-menu">
                                <li><Link to ="/dashboard"><i className="fa fa-laptop"></i> <span>DashBoard</span></Link> </li>
                                <li><Link to ="/classes"><i className="fa fa-laptop"></i> <span>ClassRooms</span></Link> </li>
                                <li><Link to ="/studentPage"><i className="fa fa-book"></i> <span>Students</span></Link> </li> 
                                <li><Link to ="/Analytics"><i className="fa fa-table"></i> <span>Analytics</span></Link> </li>  
                                <li><Link to ="/School"><i className="fa fa-edit"></i> <span>School</span></Link> </li>   
                                <li><Link to ="/template"><i className="fa fa-book"></i> <span>template</span></Link> </li>
                              </ul>
                        </section>
                        {/* /.sidebar */}
                    </aside>
                );
              }

            }

            export default NavigationMenu
