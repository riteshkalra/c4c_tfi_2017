import React from 'react';
import StudentListRow from './StudentListRow';

const StudentList = ({students}) => {
  return (
    <table id="example2" class="table table-bordered table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>First Name</th>
          <th>LastName</th>
          <th>Gender</th>                     
        </tr>
      </thead>
      <tbody>
      {students.map(student =>
        <StudentListRow key={student.id} student={student}/>
      )}
      </tbody>
    </table>
  );
};




export default StudentList;
