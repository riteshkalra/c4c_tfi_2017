import React  from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as studentActions from '../../actions/studentActions';
import StudentForm from './StudentForm';
import toastr from 'toastr';

export class ManageStudentPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      student: Object.assign({}, props.student),
      errors: {},
      saving: false
    };

    this.updateStudentState = this.updateStudentState.bind(this);
    this.saveStudent = this.saveStudent.bind(this);
  }

  componentWillReceiveProps(nextProps) {
     if (this.props.student.id !== nextProps.student.id) {
  //  // Necessary to populate form when existing student is loaded directly.
       this.setState({student: Object.assign({}, nextProps.student)});
     }
   }

  updateStudentState(event) {
    const field = event.target.name;
      let student = Object.assign({}, this.state.student);
    student[field] = event.target.value;
    return this.setState({student: student});
  }

  studentFormIsValid() {
    let formIsValid = true;
    let errors = {};

    if (this.state.student.firstName.length < 1) {
      errors.firstName = 'First Name must not be blank.';
      formIsValid = false;
    }

    if (this.state.student.lastName.length < 1) {
      errors.lastName = 'Last Name must not be blank.';
      formIsValid = false;
    }

    if (this.state.student.gender.length < 1) {
      errors.gender = 'Gender Name must not be blank.';
      formIsValid = false;
    }

    this.setState({errors: errors});
    return formIsValid;
  }


  saveStudent(event) {
    event.preventDefault();

    if (!this.studentFormIsValid()) {
      return;
    }

    this.setState({saving: true})    
    this.props.actions.saveStudent(this.state.student)
      .then(() => this.redirect())
      .catch(error => {
        toastr.error(error);
        this.setState({saving: false});
      });
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('Student saved');
    this.context.router.push('/studentPage');
  }

  render() {
    return (
      <StudentForm
        allStudents={this.props.students}
        onChange={this.updateStudentState}
        onSave={this.saveStudent}
        student={this.state.student}
        errors={this.state.errors}
        saving={this.state.saving}
      />
    );
  }
}

ManageStudentPage.propTypes = {
  student: PropTypes.object.isRequired,
 /// students: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

//Pull in the React Router context so router is available on this.context.router.
ManageStudentPage.contextTypes = {
  router: PropTypes.object
};

// function getStudentById(students, id) {
//   const student = students.filter(student => student.id === id);
//   if (student) return student[0]; //since filter returns an array, have to grab the first.
//   return null;
// }

function mapStateToProps(state, ownProps) {
 // const studentId = ownProps.params.id; // from the path `/student/:id`
  let student = {id:'',firstName: '', lastName: '', gender: '' };

  // if (studentId && state.students.length > 0) {
  //   student = getStudentById(state.students, studentId);
  // }

  return {
    student: student
  //  authors: authorsFormattedForDropdown(state.authors)
  };
}



function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(studentActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageStudentPage);
