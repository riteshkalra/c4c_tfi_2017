import React from 'react';

const StudentListRow = ({student}) => {
  return (
    <tr>     
      <td>{student.id}</td>
      <td>{student.firstName}</td>
      <td>{student.lastName}</td>
      <td>{student.gender}</td>
    </tr>
  );
};



export default StudentListRow;
