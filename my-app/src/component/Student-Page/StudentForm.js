import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import PropTypes from 'prop-types';

const StudentForm = ({student, allStudents, onSave, onChange, saving, errors}) => {
  return (
      
    <form>
      <h1>Manage Student</h1>
      
        <TextInput
          name="firstName"
          label="First Name"
          value={student.firstName}
          onChange={onChange}
          error={errors.firstName}/>
    
      <TextInput
        name="lastName"
        label="Last Name"
        value={student.lastName}
        onChange={onChange}
        error={errors.lastName}/>
      <TextInput
        name="gender"
        label="Gender"
        value={student.gender}
        onChange={onChange}
        error={errors.gender}/>     
      <input
        type="submit"
        disabled={saving}
        value={saving ? 'Saving...' : 'Add Student'}
        class="btn btn-info pull-right"
        onClick={onSave}/>    
    </form>
  );
};

StudentForm.propTypes = {
  student: PropTypes.object.isRequired,
  allStudents: PropTypes.array,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default StudentForm;


