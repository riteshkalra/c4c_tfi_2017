import React, { Component } from 'react';
import StudentList from './StudentList'
import * as studentActions from '../../actions/studentActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ManageStudentPage from './ManageStudentPage';



import {BrowserRouter } from 'react-router-dom'
class StudentPage extends Component {

  constructor(props, context) {
    super(props, context);
     this.redirectToAddStudentPage = this.redirectToAddStudentPage.bind(this);
    }

  redirectToAddStudentPage() {
   BrowserRouter.route('/studentPage');
  }

   render(){
    const {students} = this.props;
    return (
      <div>
        <section class="content">
          <div class="row">
            <div class="col-md-4">
              <div class="box">           
                <div class="box-body">
                <StudentList students={students}/>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="box">           
                <div class="box-body">               
                  <ManageStudentPage />
                </div>
              </div>
            </div>            
          </div>    
        </section>
      </div>
  

         
    );
  }
}


function mapStateToProps(state, ownProps) {
  return {
    students: state.students
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(studentActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(StudentPage);