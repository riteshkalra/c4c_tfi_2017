import { Form, Text } from 'react-form';

import React, { Component } from 'react';
class StudentFormReact extends Component {

	render()
	{
		return(
				 <Form
      				validateWarning={warningValidator}
      				validateSuccess={successValidator}
      				validateError={errorValidator} >
      				{ formApi => (
        				<form onSubmit={formApi.submitForm} id="form1" className="mb-4">
      					<label htmlFor="hello">Hello World</label>
      					<Text field="hello" id="hello" />
          				<button type="submit" className="btn btn-primary">Submit</button>
        				</form>
      				)}
    			</Form>
			);
	}
}


 errorValidator(values){
  return {
    hello: !values.hello ||
           !values.hello.match( /Hello World/ ) ? "Input must contain 'Hello World'" : null
  };
};

warningValidator (values){
  return {
    hello: !values.hello ||
           !values.hello.match( /^Hello World$/ ) ? "Input should equal 'Hello World'" : null
  };
};

successValidator(values) {
  return {
    hello: values.hello &&
           values.hello.match( /Hello World/ ) ? "Thanks for entering 'Hello World'!" : null
  };
};

export default StudentFormReact;
