import React, { Component } from 'react';
import classImage from '../../../node_modules/admin-lte/dist/img/photo2.png';
class ClassRoomInfoBox extends Component {

  render(){
    return (
      
      <section class="content">
      <div class="row">
         <div class="col-md-3">
          <div class="box box-primary">
             <div class="box-body box-profile">
               <img class="profile-user-img img-responsive img-circle" src= {classImage} alt="User profile picture"/>
               <h3 class="profile-username text-center">ABC-1A</h3>
               <p class="text-muted text-center">Year 2017-18</p>
               <ul class="list-group list-group-unbordered">
                 <li class="list-group-item">
                   <b>Place</b> <a class="pull-right">Mumbai</a>
                 </li>
                 <li class="list-group-item">
                   <b>Number of Student</b> <a class="pull-right">25</a>
                 </li>
               </ul>
               <a href="#" class="btn btn-primary btn-block"><b>Details</b></a>
           </div>
         </div>      
      </div>
       </div>
       </section>
      
      
    );
  }
}

export default ClassRoomInfoBox;
