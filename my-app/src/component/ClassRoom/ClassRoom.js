import React, { Component } from 'react';
import ClassRoomInfoBox from './ClassRoomInfoBox'

class ClassRoom extends Component {  render() {
    return (
      <div>      
        <ClassRoomInfoBox />      
      </div>
    );
  }
}

export default ClassRoom;
