import React, { Component } from 'react';
import avatar3 from '../../../node_modules/admin-lte/dist/img/avatar3.png';


class HeaderBarMain extends Component {
  render() {
    return (
      <header className="main-header">
          {/* Logo */}
          <div className="logo">
              {/* mini logo for sidebar mini 50x50 pixels */}
              <span className="logo-mini"><b>A</b>BC</span>
              {/* logo for regular state and mobile devices */}
              <span className="logo-lg"><b>Abacus</b></span>
          </div>
          {/* Header Navbar: style can be found in header.less */}
          <nav className="navbar navbar-static-top" >
              {/* Sidebar toggle button*/}
              <a href="/" className="sidebar-toggle" data-toggle="offcanvas" role="button" >
                  <span className="sr-only">Toggle navigation</span>
              </a>
              <div className="navbar-custom-menu">
                  <ul className="nav navbar-nav">
                   
                      <li className="dropdown user user-menu">
                          <a href="/" className="dropdown-toggle" data-toggle="dropdown">
                              <img src={avatar3}  className="user-image" alt="User " />
                              <span className="hidden-xs">Albina Akhlaq</span>
                          </a>
                         
                      </li>
                      { /* ontrol Sidebar Toggle Button */}
                      <li>
                          <a href="/" data-toggle="control-sidebar"><i className="fa fa-gears"></i></a>
                      </li>
                  </ul>
              </div>
          </nav>
      </header>
    );
  }
}

export default HeaderBarMain;
