import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import App from './App';
import {BrowserRouter } from 'react-router-dom'

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/admin-lte/dist/css/AdminLTE.min.css';
import '../node_modules/admin-lte/dist/css/skins/skin-blue.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import '../node_modules/ionicons/dist/css/ionicons.min.css';
import {loadStudents} from './actions/studentActions';



const store = configureStore();
store.dispatch(loadStudents());

render(
  <Provider store={store}>
  	<BrowserRouter>
    	<App/>
  	</BrowserRouter>  	
  </Provider>,
  document.getElementById('dashboard')
);



 