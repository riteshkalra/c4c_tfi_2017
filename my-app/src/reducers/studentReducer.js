import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function studentReducer(state = initialState.students, action) {
  switch (action.type) {
    case types.LOAD_STUDENTS_SUCCESS:
      return action.students;  
    case types.CREATE_STUDENT_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.student)
      ];


    default:
      return state;
  }
}
