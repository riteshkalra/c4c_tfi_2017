import {combineReducers} from 'redux';
import students from './studentReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';


const rootReducer = combineReducers({
  students , ajaxCallsInProgress
});

export default rootReducer;
