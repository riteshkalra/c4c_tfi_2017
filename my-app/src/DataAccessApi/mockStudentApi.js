import FireService from './firestore/firestore.js'


class StudentApi {
  static getAllStudents() {
    return FireService.getStudents();
  }

  static getStudent(id) {
    return FireService.getStudent(id);
  }

  static saveStudent(student) {
    function getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    }
    var sid = getRandomInt(1,1000);
    console.log("added student with id", sid);
    student.id = sid;
    return FireService.addStudent(sid, student);
  }

  }

export default StudentApi;
