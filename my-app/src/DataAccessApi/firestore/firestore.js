import {setDocument, deleteDocument, getDocument, getCollection} from './credentials.js';
import Student from '../../Model/Student'

var STUDENTS = "/students";
var SCHOOLS = "/schools";
var CLASSES = "/classes";
var TEACHERS = "/teachers";
var ACADEMICYEARS = "/academicyears";
var STUDENT_MAPPINGS = "/student_mappings";
  
class FireService {
  
  static addStudent(studentId, json) {
    // returns the promise with the student added
  	return setDocument(STUDENTS, studentId, json);
  }
  static deleteStudent(studentId) {
    // returns the promise with the student deleted
  	deleteDocument(STUDENTS, studentId);
  }
  static getStudent(studentId) {
    // returns the promise with the student
  	return getDocument(STUDENTS, studentId);
  }
  static getStudents() {
    // returns the promise with the list of students
    return getCollection(STUDENTS)
    .then(students => { 
      var studentlist = [];
      students.forEach(student => {
        console.log(student.data());
        studentlist.push(new Student(student.data()));
        console.log((new Student(student.data())).id);
        })
      return studentlist; 
    })
    .catch(error => { console.log("could not get student list"); });
  }

  static addSchool(schoolId, json) {
  	setDocument(SCHOOLS, schoolId, json);
  }
  static deleteSchool(schoolId) {
  	deleteDocument(SCHOOLS, schoolId);
  }
  static getSchool(schoolId) {
  	return getDocument(SCHOOLS, schoolId);
  }
  static getSchools() {
  	return getCollection(SCHOOLS);
  }

  static addClass(classId, json) {
  	setDocument(CLASSES, classId, json);
  }
  static deleteClass(classId) {
  	deleteDocument(CLASSES, classId);
  }
  static getClass(classId) {
  	return getDocument(CLASSES, classId);
  }
  static getClasses() {
  	return getCollection(CLASSES);
  }

  static addTeacher(teacherId, json) {
  	setDocument(TEACHERS, teacherId, json);
  }
  static deleteTeacher(teacherId) {
  	deleteDocument(TEACHERS, teacherId);
  }
  static getTeacher(teacherId) {
  	return getDocument(TEACHERS, teacherId);
  }
  static getTeachers() {
  	return getCollection(TEACHERS);
  }

  static addAcademicYear(academicYearId, json) {
  	setDocument(ACADEMICYEARS, academicYearId, json);
  }
  static deleteAcademicYear(academicYearId) {
  	deleteDocument(ACADEMICYEARS, academicYearId);
  }
  static getAcademicYear(academicYearId) {
  	return getDocument(ACADEMICYEARS, academicYearId);
  }
  static getAcademicYears() {
  	return getCollection(ACADEMICYEARS);
  }

  static addStudentMapping(studentMappingId, json) {
  	setDocument(STUDENT_MAPPINGS, studentMappingId, json);
  }
  static deleteStudentMapping(studentMappingId) {
  	deleteDocument(STUDENT_MAPPINGS, studentMappingId);
  }
  static getStudentMapping(studentMappingId) {
  	return getDocument(STUDENT_MAPPINGS, studentMappingId);
  }
  static getStudentMappings() {
  	return getCollection(STUDENT_MAPPINGS);
  }
}

export default FireService;
