import firebase from 'firebase'
require('firebase/firestore')

// Initialize Firebase
var config = {
  apiKey: "AIzaSyBdnrTwFa9PahMtp6YLIVWymvyvtN8yioY",
  authDomain: "c4ct4i2017.firebaseapp.com",
  databaseURL: "https://c4ct4i2017.firebaseio.com",
  projectId: "c4ct4i2017",
  storageBucket: "c4ct4i2017.appspot.com",
  messagingSenderId: "360800042690"
};
firebase.initializeApp(config);

// Initialize Cloud Firestore through Firebase
var db;
firebase.firestore().enablePersistence()
.then(function() {
  // Initialize Cloud Firestore through firebase
  db = firebase.firestore();
})
.catch(function(err) {
  if (err.code === 'failed-precondition') {
      // Multiple tabs open, persistence can only be enabled
      // in one tab at a a time.
      // ...
      console.log("Multiple tabs open");
  } else if (err.code === 'unimplemented') {
      // The current browser does not support all of the
      // features required to enable persistence
      console.log("The current browser does not support");
  }
});

console.log("before db initialised: " + db);

while(!db) {
  console.log("db is undefined");
  db = firebase.firestore();
}

console.log("after db initialised: " + db);

export function setDocument(collection, document, json) {
  /* sets the document and returns its ref */
  return db.collection(collection.toString()).doc(document.toString()).set(json, { merge: true })
  .then(() => { return getDocument(collection, document); })
  .catch(e => { console.log("error while setting the document"); })
}

export function deleteDocument(collection, document) {
  /* deletes the document and returns its ref */
  return getDocument(collection, document)
  .then(ref => { db.collection(collection.toString()).doc(document.toString()).delete(); return ref; })
  .catch(error => { console.log(collection, document, "does not exist. not deleted") })
}

export function getDocument(collection, document) {
  return db.collection(collection.toString()).doc(document.toString()).get();
}

export function getCollection(collection) {
  return db.collection(collection.toString()).get();
}
