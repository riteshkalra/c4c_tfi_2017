import StudentApi from '../DataAccessApi/mockStudentApi';
import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

export function loadStudentsSuccess(students) {
  return {type: types.LOAD_STUDENTS_SUCCESS, students};
}

export function createStudentSuccess(student) {
  return {type: types.CREATE_STUDENT_SUCCESS, student};
}

export function updateStudentSuccess(student) {
  return {type: types.UPDATE_STUDENT_SUCCESS, student};
}

export function loadStudents() {
  return dispatch => {
    dispatch(beginAjaxCall());
    return StudentApi.getAllStudents()
    .then(students => {
      dispatch(loadStudentsSuccess(students));
    })
    .catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
};

export function saveStudent(student) {
  return function(dispatch, getState) {
     dispatch(beginAjaxCall());
     return StudentApi.saveStudent(student)     
     .then((st) => {
        // set the id from the student
        student.id = st.id;
        dispatch(createStudentSuccess(student));
     })
     .catch(function(e) {
        dispatch(ajaxCallError(e));
        throw(e);
        console.log("error");
     })
  }
}


