class Student {
    constructor(data){
        this.id = data.id;
        this.firstName = data.firstName;
        this.lastName = data.lastName;
        
        // For backward-compatible, to be removed
        if (this.firstName == null) {
            this.firstName = data.first_name;
        }
        if (this.lastName == null) {
            this.lastName = data.last_name;
        }
        
        this.rollNo = data.rollNo;
        this.gender = data.gender;
        this.dob = data.dob;
        this.address = data.address;
        this.contacts = data.contacts;
        this.image = data.image;
        this.acadYear = data.acadYear;
        this.cls = data.cls;
        this.subjects = data.subjects;
    }

    get FirstName(){
        return this.firstName;
    }
    
    get LastName(){
        return this.lastName;
    }

    get RollNumber(){
        return this.rollNo;
    }

    get Gender() {
        return this.gender;
    }

    get DOB(){
        return this.dob;
    }

    get Address(){
        return this.address;
    }

    get Contacts(){
        return this.contacts;
    }

    get Image(){
        return this.image;
    }

    get AcademicYear(){
        return this.acadYear;
    }

    get Class(){
        return this.cls;
    }

    get Subjects(){
        return this.subjects;
    }
}

export default Student;
