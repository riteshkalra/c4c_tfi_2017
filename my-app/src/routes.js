import React, { Component } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import classRoom from './component/ClassRoom/ClassRoom'
import StudentPage from './component/Student-Page/StudentPage'
import Abacusdashboard from './component/admin-Page/abacus-dashboard';

// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
class Routes extends Component {
   render(){
   			return(
				<Router>
					<div class="content-wrapper">     
				       <Route path="/" component={Abacusdashboard}/>
					   <Route path="/classes" component={classRoom}/>
					   <Route path="/studentPage" component={StudentPage}/>   
					  
					</div> 
				 </Router>
				 );
			}
}
export default Routes;