# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
Code for Change for Teach For India 2017
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* [Summary of set up](https://bitbucket.org/riteshkalra/c4c_tfi_2017/overview#markdown-header-summary-of-set-up)
* [Configuration](https://bitbucket.org/riteshkalra/c4c_tfi_2017/overview#markdown-header-configuration)
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Technology Stack ###
* ReactJS
* Backbone.js
* Firebase
* Other as needed

### Design ###
* [Data Model](https://bitbucket.org/riteshkalra/c4c_tfi_2017/overview#markdown-header-data-model)

### Deliverables ###
* [Class diagram of data model](https://bitbucket.org/riteshkalra/c4c_tfi_2017/src/HEAD/Deliverables/?at=master)
* Source code in BitBucket (this repo)
* Demo Package


- - -

## Summary of set up

### Setup user accounts
Create necessary accounts to be used in the project and ask Ritesh to add your accounts to the team setup.

* Personal email
* Trello
* BitBucket
* Other accounts

### Setup user workspace

### Setup database

## Configuration

### BitBucket

    $ git clone https://<username>@bitbucket.org/riteshkalra/c4c_tfi_2017.git
    $ cd c4c_tfi_2017/
    $ git config user.name "user name" (If you need to change to full name)
    $ git add <files>
    $ git commit -m "message"
    $ git push

Work on a branch:

    Create a branch
    $ git branch my_branch
    $ git checkout my_branch
    $ git merge master
    $ git add <files>
    $ git commit -m "message"
    
    Merge to master
    $ git checkout master
    $ git pull
    $ git merge my_branch
    
    Push
    $ git push

### React App

1. Install npm

2. Install react packages

    $ cd my-app
    $ npm install
    $ npm start

## Data Model

### Class Diagram
![Class Diagram](https://bitbucket.org/riteshkalra/c4c_tfi_2017/raw/HEAD/Documents/Data%20Model/class_diagram.png?at=master)
