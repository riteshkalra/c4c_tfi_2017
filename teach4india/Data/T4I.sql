-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: t4i
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `academicyear_holidays`
--

DROP TABLE IF EXISTS `academicyear_holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academicyear_holidays` (
  `AcademicYear` int(11) NOT NULL,
  `Holiday` int(11) NOT NULL,
  PRIMARY KEY (`AcademicYear`,`Holiday`),
  KEY `FK_AcademicYear_Holiday_Holidays_idx` (`Holiday`),
  CONSTRAINT `FK_AcademicYear_Holiday_AcademicYear` FOREIGN KEY (`AcademicYear`) REFERENCES `academicyears` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AcademicYear_Holiday_Holidays` FOREIGN KEY (`Holiday`) REFERENCES `holidays` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academicyear_holidays`
--

LOCK TABLES `academicyear_holidays` WRITE;
/*!40000 ALTER TABLE `academicyear_holidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `academicyear_holidays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `academicyear_schools`
--

DROP TABLE IF EXISTS `academicyear_schools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academicyear_schools` (
  `AcademicYear` int(11) NOT NULL,
  `School` int(11) NOT NULL,
  PRIMARY KEY (`AcademicYear`,`School`),
  KEY `FK_AcademicYear_School_School_idx` (`School`),
  CONSTRAINT `FK_AcademicYear_School_AcademicYear` FOREIGN KEY (`AcademicYear`) REFERENCES `academicyears` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AcademicYear_School_School` FOREIGN KEY (`School`) REFERENCES `schools` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academicyear_schools`
--

LOCK TABLES `academicyear_schools` WRITE;
/*!40000 ALTER TABLE `academicyear_schools` DISABLE KEYS */;
/*!40000 ALTER TABLE `academicyear_schools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `academicyears`
--

DROP TABLE IF EXISTS `academicyears`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `academicyears` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `academicyears`
--

LOCK TABLES `academicyears` WRITE;
/*!40000 ALTER TABLE `academicyears` DISABLE KEYS */;
/*!40000 ALTER TABLE `academicyears` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_student_subjects`
--

DROP TABLE IF EXISTS `class_student_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_student_subjects` (
  `AcademicYear` int(11) NOT NULL,
  `School` int(11) NOT NULL,
  `Class` int(11) NOT NULL,
  `Student` int(11) NOT NULL,
  `Subject` int(11) NOT NULL,
  PRIMARY KEY (`AcademicYear`,`School`,`Class`,`Student`,`Subject`),
  KEY `FK_Student_Subject_AcademicYear_idx` (`AcademicYear`),
  KEY `FK_Student_Subject_Class_idx` (`Class`),
  KEY `FK_Student_Subject_School_idx` (`School`),
  KEY `FK_Student_Subject_Student_idx` (`Student`),
  KEY `FK_Student_Subject_Subject_idx` (`Subject`),
  CONSTRAINT `FK_Student_Subject_AcademicYear` FOREIGN KEY (`AcademicYear`) REFERENCES `academicyears` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Student_Subject_Class` FOREIGN KEY (`Class`) REFERENCES `classes` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Student_Subject_School` FOREIGN KEY (`School`) REFERENCES `schools` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Student_Subject_Student` FOREIGN KEY (`Student`) REFERENCES `students` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Student_Subject_Subject` FOREIGN KEY (`Subject`) REFERENCES `subjects` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_student_subjects`
--

LOCK TABLES `class_student_subjects` WRITE;
/*!40000 ALTER TABLE `class_student_subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_student_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_students`
--

DROP TABLE IF EXISTS `class_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_students` (
  `Class` int(11) NOT NULL,
  `Student` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date DEFAULT NULL,
  PRIMARY KEY (`Class`,`Student`),
  KEY `FK_STUDENT_CLASS_idx` (`Class`),
  KEY `FK_STUDENT_STUDENT_idx` (`Student`),
  CONSTRAINT `FK_STUDENT_CLASS` FOREIGN KEY (`Class`) REFERENCES `classes` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_STUDENT_STUDENT` FOREIGN KEY (`Student`) REFERENCES `students` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_students`
--

LOCK TABLES `class_students` WRITE;
/*!40000 ALTER TABLE `class_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_subject_teachers`
--

DROP TABLE IF EXISTS `class_subject_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_subject_teachers` (
  `Id` int(11) NOT NULL,
  `Class` int(11) NOT NULL,
  `Teacher` int(11) NOT NULL,
  `Subject` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_TEACHER_CLASS_idx` (`Class`),
  KEY `FK_TEACHER_TEACHER_idx` (`Teacher`),
  KEY `FK_TEACHER_SUBJECT_idx` (`Subject`),
  CONSTRAINT `FK_TEACHER_CLASS` FOREIGN KEY (`Class`) REFERENCES `classes` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TEACHER_SUBJECT` FOREIGN KEY (`Subject`) REFERENCES `subjects` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TEACHER_TEACHER` FOREIGN KEY (`Teacher`) REFERENCES `teachers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_subject_teachers`
--

LOCK TABLES `class_subject_teachers` WRITE;
/*!40000 ALTER TABLE `class_subject_teachers` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_subject_teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_subjects`
--

DROP TABLE IF EXISTS `class_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_subjects` (
  `Class` int(11) NOT NULL,
  `Subject` int(11) NOT NULL,
  `Mandatory` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`Class`,`Subject`),
  KEY `FK_SUBJECT_CLASS_idx` (`Class`),
  KEY `FK_SUBJECT_SUBJECT_idx` (`Subject`),
  CONSTRAINT `FK_SUBJECT_CLASS` FOREIGN KEY (`Class`) REFERENCES `classes` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_SUBJECT_SUBJECT` FOREIGN KEY (`Subject`) REFERENCES `subjects` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_subjects`
--

LOCK TABLES `class_subjects` WRITE;
/*!40000 ALTER TABLE `class_subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `class_teachers`
--

DROP TABLE IF EXISTS `class_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_teachers` (
  `Class` int(11) NOT NULL,
  `Teacher` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date DEFAULT NULL,
  PRIMARY KEY (`Class`,`Teacher`,`StartDate`),
  KEY `FK_Class_Teacher_Class_idx` (`Class`),
  KEY `FK_Class_Teacher_Teacher_idx` (`Teacher`),
  CONSTRAINT `FK_Class_Teacher_Class` FOREIGN KEY (`Class`) REFERENCES `classes` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Class_Teacher_Teacher` FOREIGN KEY (`Teacher`) REFERENCES `teachers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `class_teachers`
--

LOCK TABLES `class_teachers` WRITE;
/*!40000 ALTER TABLE `class_teachers` DISABLE KEYS */;
/*!40000 ALTER TABLE `class_teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Grade` varchar(45) NOT NULL,
  `Section` varchar(45) NOT NULL,
  `Code` varchar(45) NOT NULL,
  `TeachingMedium` varchar(45) NOT NULL,
  `AcademicYear` int(11) NOT NULL,
  `School` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Class_AcademicYear_idx` (`AcademicYear`),
  KEY `FK_Class_School` (`School`),
  CONSTRAINT `FK_Class_AcademicYear` FOREIGN KEY (`AcademicYear`) REFERENCES `academicyears` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Class_School` FOREIGN KEY (`School`) REFERENCES `schools` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `Id` int(11) NOT NULL,
  `EntityId` int(11) NOT NULL,
  `First Name` varchar(45) NOT NULL,
  `Last Name` varchar(45) NOT NULL,
  `Relationship` varchar(45) NOT NULL,
  `Phone` varchar(45) DEFAULT NULL,
  `Mobile` varchar(45) NOT NULL,
  `Fax` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_questions`
--

DROP TABLE IF EXISTS `exam_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_questions` (
  `Exam` int(11) NOT NULL,
  `Question` int(11) NOT NULL,
  PRIMARY KEY (`Exam`,`Question`),
  KEY `FK_Exam_Questions_Question_idx` (`Question`),
  CONSTRAINT `FK_Exam_Questions_Exam` FOREIGN KEY (`Exam`) REFERENCES `exams` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Exam_Questions_Question` FOREIGN KEY (`Question`) REFERENCES `questions` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_questions`
--

LOCK TABLES `exam_questions` WRITE;
/*!40000 ALTER TABLE `exam_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_results`
--

DROP TABLE IF EXISTS `exam_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exam_results` (
  `Id` int(11) NOT NULL,
  `Exam` int(11) NOT NULL,
  `Student` int(11) NOT NULL,
  `Question` int(11) NOT NULL,
  `Answer` varchar(145) NOT NULL,
  `Mastery` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_RESULT_EXAM_idx` (`Exam`),
  KEY `FK_RESULT_QUESTION_idx` (`Question`),
  KEY `FK_RESULT_STUDENT_idx` (`Student`),
  KEY `FK_RESULT_MASTERY_idx` (`Mastery`),
  CONSTRAINT `FK_Exam_Result_Exam` FOREIGN KEY (`Exam`) REFERENCES `exams` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Exam_Result_Mastery` FOREIGN KEY (`Mastery`) REFERENCES `question_mastery` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Exam_Result_Question` FOREIGN KEY (`Question`) REFERENCES `questions` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Exam_Result_Student` FOREIGN KEY (`Student`) REFERENCES `students` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_results`
--

LOCK TABLES `exam_results` WRITE;
/*!40000 ALTER TABLE `exam_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `exam_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exams`
--

DROP TABLE IF EXISTS `exams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exams` (
  `Id` int(11) NOT NULL,
  `Class` int(11) NOT NULL,
  `Subject` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Date` date NOT NULL,
  `PassMarks` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_EXAM_SUBJECT_idx` (`Subject`),
  KEY `FK_EXAM_CLASS_idx` (`Class`),
  CONSTRAINT `FK_EXAM_CLASS` FOREIGN KEY (`Class`) REFERENCES `classes` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_EXAM_SUBJECT` FOREIGN KEY (`Subject`) REFERENCES `subjects` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exams`
--

LOCK TABLES `exams` WRITE;
/*!40000 ALTER TABLE `exams` DISABLE KEYS */;
/*!40000 ALTER TABLE `exams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holidays`
--

DROP TABLE IF EXISTS `holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holidays` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Date` date NOT NULL,
  `Description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holidays`
--

LOCK TABLES `holidays` WRITE;
/*!40000 ALTER TABLE `holidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `holidays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `contents` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_mastery`
--

DROP TABLE IF EXISTS `question_mastery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_mastery` (
  `Id` int(11) NOT NULL,
  `Question` int(11) DEFAULT NULL,
  `Mastery` varchar(45) DEFAULT NULL,
  `Mark` float DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_QUESTION_MASTERY_idx` (`Question`),
  CONSTRAINT `FK_QUESTION_MASTERY` FOREIGN KEY (`Question`) REFERENCES `questions` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_mastery`
--

LOCK TABLES `question_mastery` WRITE;
/*!40000 ALTER TABLE `question_mastery` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_mastery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `Id` int(11) NOT NULL,
  `Subject` int(11) DEFAULT NULL,
  `Description` varchar(145) NOT NULL,
  `Concept` varchar(145) NOT NULL,
  `Type` varchar(45) NOT NULL,
  `DifficultyLevel` varchar(45) DEFAULT NULL,
  `Options` varchar(45) DEFAULT NULL,
  `Answer` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_QUESTION_SUBJECT_idx` (`Subject`),
  CONSTRAINT `FK_QUESTION_SUBJECT` FOREIGN KEY (`Subject`) REFERENCES `subjects` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_classes`
--

DROP TABLE IF EXISTS `school_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_classes` (
  `AcademicYear` int(11) NOT NULL,
  `School` int(11) NOT NULL,
  `Class` int(11) NOT NULL,
  PRIMARY KEY (`AcademicYear`,`School`,`Class`),
  KEY `FK_AcademicYear_idx` (`AcademicYear`),
  KEY `FK_School_idx` (`School`),
  KEY `FK_Class_idx` (`Class`),
  CONSTRAINT `FK_CM_AcademicYear` FOREIGN KEY (`AcademicYear`) REFERENCES `academicyears` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CM_Class` FOREIGN KEY (`Class`) REFERENCES `classes` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CM_School` FOREIGN KEY (`School`) REFERENCES `schools` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_classes`
--

LOCK TABLES `school_classes` WRITE;
/*!40000 ALTER TABLE `school_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_holidays`
--

DROP TABLE IF EXISTS `school_holidays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_holidays` (
  `School` int(11) NOT NULL,
  `Holiday` int(11) NOT NULL,
  PRIMARY KEY (`School`,`Holiday`),
  KEY `FK_School_Holiday_Holiday_idx` (`Holiday`),
  CONSTRAINT `FK_School_Holiday_Holidays` FOREIGN KEY (`Holiday`) REFERENCES `holidays` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_School_Holiday_School` FOREIGN KEY (`School`) REFERENCES `schools` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_holidays`
--

LOCK TABLES `school_holidays` WRITE;
/*!40000 ALTER TABLE `school_holidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_holidays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_teachers`
--

DROP TABLE IF EXISTS `school_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_teachers` (
  `Id` int(11) NOT NULL,
  `School` int(11) NOT NULL,
  `Teacher` int(11) NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_School_Teacher_Teacher_idx` (`Teacher`),
  KEY `FK_School_Teacher_School` (`School`),
  CONSTRAINT `FK_School_Teacher_School` FOREIGN KEY (`School`) REFERENCES `schools` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_School_Teacher_Teacher` FOREIGN KEY (`Teacher`) REFERENCES `teachers` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_teachers`
--

LOCK TABLES `school_teachers` WRITE;
/*!40000 ALTER TABLE `school_teachers` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schools`
--

DROP TABLE IF EXISTS `schools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schools` (
  `Id` int(11) NOT NULL,
  `Name` varchar(145) NOT NULL,
  `BuildingName` varchar(45) NOT NULL,
  `StreetName` varchar(45) NOT NULL,
  `City` varchar(45) NOT NULL,
  `District` varchar(45) NOT NULL,
  `State` varchar(45) NOT NULL,
  `Country` varchar(45) NOT NULL,
  `PostalCode` varchar(45) NOT NULL,
  `Phone1` varchar(45) NOT NULL,
  `Phone2` varchar(45) DEFAULT NULL,
  `Fax` varchar(45) DEFAULT NULL,
  `Email` varchar(45) NOT NULL,
  `ImageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Image_idx` (`ImageId`),
  CONSTRAINT `FK_Image` FOREIGN KEY (`ImageId`) REFERENCES `images` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schools`
--

LOCK TABLES `schools` WRITE;
/*!40000 ALTER TABLE `schools` DISABLE KEYS */;
/*!40000 ALTER TABLE `schools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `RollNumber` varchar(45) DEFAULT NULL,
  `Gender` varchar(1) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `BuildingName` varchar(45) DEFAULT NULL,
  `StreetName` varchar(45) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `District` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `PostalCode` varchar(45) DEFAULT NULL,
  `Phone1` varchar(45) DEFAULT NULL,
  `Phone2` varchar(45) DEFAULT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Image` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_CLASS_IMAGE_idx` (`Image`),
  CONSTRAINT `FK_CLASS_IMAGE` FOREIGN KEY (`Image`) REFERENCES `images` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Category` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `EmpID` varchar(45) NOT NULL,
  `Gender` varchar(1) NOT NULL,
  `DOB` date NOT NULL,
  `BuildingName` varchar(45) NOT NULL,
  `StreetName` varchar(45) NOT NULL,
  `City` varchar(45) NOT NULL,
  `District` varchar(45) NOT NULL,
  `State` varchar(45) NOT NULL,
  `Country` varchar(45) NOT NULL,
  `PostalCode` varchar(45) NOT NULL,
  `Phone1` varchar(45) NOT NULL,
  `Phone2` varchar(45) NOT NULL,
  `Email` varchar(45) NOT NULL,
  `Image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-02 18:41:51
