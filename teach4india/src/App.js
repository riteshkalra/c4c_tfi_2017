// This component handles the App template used on every page.
import React from 'react';
import {connect} from 'react-redux';
import Routes from './routes'
import { currentAcademicYear } from './reducers/academicYearReducer';


class App extends React.Component {
  render() {
    return (
      <div>
        <Routes/>  
        {this.props.children}
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    loading: state.ajaxCallsInProgress > 0,
    currentAcademicYear: state.currentAcademicYear
  };
}
export default connect(mapStateToProps)(App);
