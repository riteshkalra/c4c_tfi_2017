import {MockDataSource} from "../DataSource/MockDataSource";
import {FireStoreDataSource} from "../DataSource/FireStoreDataSource";

export class Admin {
    static FSDS = new FireStoreDataSource();

    
    static getDataSource() {
        return Admin.FSDS;
    }

 
    static async getAcademicYears() {        
        return await Admin.getDataSource().getAcademicYears();
    }

    static async getCurrentAcademicYear() {  
        return await Admin.getDataSource().getAcademicYear('tmBex1S8nqlSRj1p5Mn9');
    }

    static getCurrentSchool() {  
        return Admin.getDataSource().getSchollObject('anFIrOb0iPqbii8h3C2Y');
    }

    static async getAcademicYear(academicYearId) {
        return await Admin.getDataSource().getAcademicYear(academicYearId);
    }

    static async addAcademicYear(academicYear) {
        return await Admin.getDataSource().addAcademicYear(academicYear);
    }
    
    static async getStudents() {
        return await Admin.getDataSource().getStudents();
    }
    
    static async addStudent(student) {
        return await Admin.getDataSource().addStudent(student);
    }
    
    static async getTeachers() {
        return await Admin.getDataSource().getTeachers();
    }
    
    static async getClasses() {
        return await Admin.getDataSource().getClasses();
    }
    
    static async getClass(classId) {
        return await Admin.getDataSource().getClass(classId);
    }

    static async addClass(classRoom) {
        return await Admin.getDataSource().addClass(classRoom);
    }
    
    static async getSchools() {
        return await Admin.getDataSource().getSchools();
    }
    
    static async getSchool(schoolId) {
        return await Admin.getDataSource().getSchool(schoolId);    
    }




    static async getAllQuestions() {
        return await Admin.getDataSource().getAllQuestions();
    }
    
    static async getQuestion(questionId) {
        return await Admin.getDataSource().getQuestion(questionId);    
    }




    static async saveSchool(school) {
        return await Admin.getDataSource().saveSchool(school);
    }

    static async saveQuestion(question) {
        return Admin.getDataSource().addQuestion(question, question.subject);
    }

    static saveMastery(mastery, question) {
        return Admin.DS.addMasteryLevel(mastery, question);
    }

    static getSubjects() {
        return Admin.getDataSource().getSubjects();
    }

    static getSubjectNameById(subjectId){
        let subject = Admin.getDataSource().getSubject(subjectId);
        if(subject){
            return subject.name;
        }else{
            return null;
        }
        
    }
}
