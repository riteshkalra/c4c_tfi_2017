export class Entity{

    constructor(dataSource, id=null){
        this.dataSource = dataSource;
        this.id = id;
        this.name = null;
    }

    get Id(){
        return this.id;
    }

    get Name(){
        return this.name;
    }

    get DataSource(){
        return this.dataSource;
    }

    setId(newId){
        if (this.id == null){
            this.id = newId;
        }
    }

    toJSON(){
        throw "Not implemented";
    }

    static fromJSON(dataSource, id, json){
        throw "Not implemented";
    }
}