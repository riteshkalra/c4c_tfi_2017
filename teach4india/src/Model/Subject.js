import {Entity} from "./Entity.js";
import {Question} from "./Question.js";

export class Subject extends Entity{
    constructor(dataSource, name, category, isMandatory, id=null){
        super(dataSource, id);
        this.name = name;
        this.category = category;
        this.isMandatory = isMandatory;
        this.questions = null;
    }

    get Name(){
        return this.name;
    }

    get Category(){
        return this.category;
    }

    get IsMandatory(){
        return this.isMandatory;
    }

    get Questions(){
        if (this.questions == null){
            this.dataSource.getQuestionsForSubject(this).
            then(resolvedQuestions => { this.questions = resolvedQuestions; });
        }
        return this.questions;
    }

    addQuestion(description, concept, type, difficultyLevel, options, answer){
        let question = new Question(this.dataSource, description, concept, type, difficultyLevel, options, answer, this);
        this.dataSource.addQuestion(question);
        this.questions = null;
    }

    removeQuestion(question){
        this.dataSource.removeQuestion(question);
        this.questions = null;
    }

    update(){
        this.dataSource.updateSubject(this);
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        async function getQuestions(questions) {
          return await dataSource.getQuestionObjects(questions); 
        }

        async function executeParallelAsyncTasks () {
            let promises = []
            json['questions'] && promises.push(getQuestions(json["questions"]));            
            return await Promise.all(promises);         
        }
        let subject = new Subject(dataSource,
            json["name"],
            json["category"],
            json["isMandatory"],
            id)
        executeParallelAsyncTasks().then(result => {          
            let questions = result[0] || null;
            subject.questions = questions;                   
            return subject;
        });
        return subject;
      
    }

    toJSON(){
        let qIds = [];
        for(let index in this.questions) {
            qIds.push(this.questions[index].id)
        }
        return {
            "name": this.name ||null,
            "category": this.category || null,
            "isMandatory": this.isMandatory || null,
            "questions":  (this.questions && qIds) || null
        }
    }

 }
