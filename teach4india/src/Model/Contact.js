import {Entity} from "./Entity.js";

export class Contact extends Entity{
    constructor(dataSource, firstName,middleName, lastName, relationship, phone, mobile, fax, email, entityName, entityId, id=null){
        super(dataSource, id);
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.relationship = relationship;
        this.phone = phone;
        this.mobile = mobile;
        this.fax = fax;
        this.email = email;
        this.entityName = entityName;
        this.entityId = entityId;
    }

    get FirstName(){
        return this.firstName;
    }

    get MiddleName(){
        return this.middleName;
    }

    get LastName(){
        return this.lastName;
    }

    get Name(){
        return this.firstName + this.lastName;
    }

    get Relationship(){
        return this.relationship;
    }

    get Phone(){
        return this.phone;
    }

    get Mobile(){
        return this.mobile;
    }

    get Fax(){
        return this.fax;
    }

    get Email(){
        return this.email;
    }

    get Entity(){
        return this.entityName;
    }

    get EntityId(){
        return this.entityId;
    }

    update(){
        this.dataSource.updateContact(this);
    }

    async save() {
      return this.dataSource.saveContact(this)
    }
    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        return new Contact(dataSource,
                           json["firstName"] || null,
                           json["middleName"] || null,
                           json["lastName"] || null,
                           json["relationship"] || null,
                           json["phone"] || null,
                           json["mobile"] || null,
                           json["fax"] || null,
                           json["email"] || null,
                           json["entityName"] || null,
                           json["entityId"] || null,
                           id);
    }

    toJSON(){
        return {
            "firstName": this.firstName || null,
            "middleName": this.middleName || null,
            "lastName": this.lastName || null,
            "relationship": this.relationship || null,
            "phone": this.phone || null,
            "mobile": this.mobile || null,
            "fax": this.fax || null,
            "email": this.email || null,
            "entityName": this.entityName || null,
            "entityId": this.entityId || null,
        };
    }
}
