import {Entity} from "./Entity.js"
import {School} from "./School.js"
import {Address} from "./Address.js"

export class AcademicYear extends Entity {
    
    constructor(dataSource, name, board, startDate, endDate, id = null) {
        super(dataSource, id);
        this.name = name;
        this.board = board;
        this.startDate = startDate;
        this.endDate = endDate;
        this.schools = null;
    }

    get Name() {
        return this.name;
    }

    get Id() {
        return this.id;
    }

    get Board() {
        return this.board;
    }

    get StartDate() {
        return this.startDate;
    }

    get EndDate() {
        return this.endDate;
    }

    get Schools() {
        if (this.schools == null) {
            this.dataSource.getSchoolsForAcademicYear(this).then(resolvedSchools => {
                this.schools = resolvedSchools;
            })
        }
        return this.schools;
    }

    addSchool(school) {
        console.log("addSchool called");
        
        return this.schools.push(school);
    }
 
    async save() {
        return await this.dataSource.addAcademicYear(this);
    }

    async update() {
        return await this.dataSource.editAcademicYear(this);
    }

    static toDate(val) {
        if (typeof val === "string" || val instanceof String) {
            return new Date(val);
        }
        return val;
    }

    // sync
    static fromJSON(dataSource, id, json) {
        if(!json) return null;
        
        async function getSchools(schoolIds) { 
            return await dataSource.getSchoolObjects(schoolIds)     
        }

        async function executeParallelAsyncTasks () {
            let promises = []
            json['schools'] && promises.push(getSchools(json["schools"]));
            return await Promise.all(promises);         
        }
        return executeParallelAsyncTasks().then(result => {
            let schools = result[0] || null;
            let ay = new AcademicYear(dataSource,
                json["name"] || null,
                json["board"] || null,
                (json["startDate"] && this.toDate(json["startDate"])) || null,
                (json["endDate"] && this.toDate(json["endDate"])) || null,
                id);
            ay.schools = schools;
            return ay;
        });
    }

    // sync
    toJSON() {
        let schoolIds = []
        for(let index in this.schools) {
            let school = this.schools[index];
            schoolIds.push(school.id)
        }
        return {
            "name": this.name || null,
            "board": this.board || null,
            "startDate": this.startDate || null,
            "endDate": this.endDate || null,
            "schools": (this.schools && schoolIds) || null,
        };
    }
}
