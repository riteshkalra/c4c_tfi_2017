export class Util {

static toDate(val) {
        if (typeof val === "string" || val instanceof String) {
            return new Date(val);
        }
        return val;
    }
}