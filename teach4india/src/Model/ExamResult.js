import {Entity} from "./Entity";

export class ExamResult extends Entity{
    constructor(dataSource, exam, student, id=null){
        super(dataSource, id);
        this.exam = exam;
        this.student = student;
        this.answers = [];
     }

    get Exam() {
        return this.exam;
    }

    get Student(){
        return this.student;
    }

    get Answers(){
        // Dictionary of question to answer
        return this.answers;
    }

    get AnswerIds(){
        return this.answerIds;
    }


    get Mark(){
        // Calculate mark based on mastery
        return null;
    }

    get Grade(){
        // Calculate grade based on mastery
    }

    addAnswer(answer){        
        this.answers.push(answer)
        return this.answers;
    }

     async save() {
        for(let index in this.answers){
            let ans = await this.answers[index].save();
        }

       return await this.dataSource.addExamResult(this);  


    }

    updateAnswer(question, answer, mastery){
        this.dataSource.updateExamAnswer(this.Exam, this.Student, question, answer, mastery);
        this.answers = null;
    }

    static fromJSON(dataSource, id, json){
        return new ExamResult(dataSource,
                              dataSource.getExamById(json["exam"]),
                              dataSource.getStudentById(json["student"]),
                              id);
    }

    toJSON(){

        let answerIds = [];
        for(let index in this.answers){
            answerIds.push(this.answers[index].id);
        }
            return {
            "exam": this.Exam.id,
            "student": this.Student.id,
            "answers": answerIds
            }
   }
        
    
}