export class Image{
    constructor(name, contents){
        this.name = name;
        this.contents = contents;
    }

    get Name(){
        return this.name;
    }

    get Contents(){
        return this.contents;
    }

    static fromJSON(json){
        if(!json) return null;
        return new Image(json["name"] | null,
                         json["contents"] || null)
    }

    toJSON(){
        return {
            "name": this.name || null,
            "contents": this.contents || null
        };
    }
}
