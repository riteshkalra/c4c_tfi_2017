import {Entity} from "./Entity.js";
import {Contact} from "./Contact.js";
import {Address} from "./Address";
import {Image} from "./Image";

export class Student extends Entity{
    constructor(dataSource, firstName, middleName,lastName, rollNo, gender, dob, doj, address, image, phone1, phone2, email, id=null){
        super(dataSource, id);
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.rollNo = rollNo;
        this.doj =doj;
        this.gender = gender;
        this.dob = dob;
        this.image = image;
        this.address = address;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.email = email;
        this.contacts = null;
        this.school = null;
        this.cls = null;
        this.subjects = null;
        this.exams = null;
    }

    get FirstName(){
        return this.firstName;
    }

    get MiddleName(){
        return this.middleName;
    }

    get LastName(){
        return this.lastName;
    }

    get Name(){
        return this.firstName + " " + this.lastName;
    }

    get RollNumber(){
        return this.rollNo;
    }    

    get Gender() {
        return this.gender;
    }

    get DOB(){
        return this.dob;
    }

     get DOJ(){
        return this.doj;
    }

    get Image(){
        return this.image;
    }

    get Address(){
        return this.address;
    }

    get Phone1(){
        return this.phone1;
    }

    get Phone2(){
        return this.phone2;
    }

    get Email(){
        return this.email;
    }

    get Contacts(){
        if (this.contacts == null){
            this.contacts = this.dataSource.getStudentContacts(this);
        }
        return this.contacts;
    }

    get School(){
        // Current school for this student
        if (this.school == null){
            this.school = this.dataSource.getSchoolForStudent(this);
        }
        return this.school;
    }

    async save(classObj) {
        for(let index in this.contacts){
             let ans = await this.contacts[index].save();
         }           
        let address = await this.address.save();                        
        let student = await this.dataSource.saveStudent(this);                
        return await student.addStudentToClass(classObj)      
     }
     addStudentToClass(classObj) {        
        return this.dataSource.addStudentToClass(classObj, this);
    }
    
    get AcademicYear(){
        return this.School.AcademicYear;
    }

    get Class(){
        // Current class for this student
        if (this.cls == null){
            this.cls = this.dataSource.getClassForStudent(this.AcademicYear, this.School, this);
        }
        return this.cls;
    }

    get Subjects(){
        // Subjects from the current class
        if (this.subjects == null){
            this.subjects = this.dataSource.getSubjectsForStudent(this.Class, this);
        }
        return this.subjects;
    }

    getExamResultsForSubject(subject){
        if (!(subject.Name in this.exams)){
            this.exams[subject] = this.dataSource.getExamResultsForStudent(this.Class, subject, this);
        }
        return this.exams[subject];
    }

    addContact(contact){      
        if(this.contacts === null) {    
            this.contacts =[];           
        }
        this.contacts.push(contact);
        return this.contacts;         
    }

    updateContact(contact){
        this.dataSource.updateContact(this, contact);
    }

    getSchools(){
        return this.getSchoolsForStudent(this);
    }

    getClassesFromSchool(school){
        return this.getClassesForStudent(school, this);
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        async function getAddress(addressId) {
          return await dataSource.getAddressObject(addressId) 
        }
        async function getContacts(contactIds) {
          return await dataSource.getContactObjects(contactIds) 
        }
       
        async function getImage(imageId) {
            return await dataSource.getImageObject(imageId)
        }
        async function getSubjects(subjectIds) {
            return await dataSource.getSubjectObjects(subjectIds)
        }
        async function getExams(examIds) {
            return await dataSource.getExamObjects(examIds)
        }

        async function executeParallelAsyncTasks () {
            let promises = []
            json['address'] && promises.push(getAddress(json["address"]));
            json['contacts'] && promises.push(getContacts(json["contacts"]));
            json['image'] && promises.push(getImage(json["image"]));
            json['subjects'] && promises.push(getSubjects(json["subjects"]));
            json['exams'] && promises.push(getExams(json["exams"]));
            return  await Promise.all(promises)         
        }

        //firstName, middleName,lastName, rollNo, gender, dob, doj,  address, image, phone1, phone2, email, 
        let student = new Student(dataSource,
            json["firstName"] || null,
            json["middleName"] || null,
            json["lastName"] || null,            
            json["rollNo"] || null,
            json["gender"] || null,            
            (json["dob"] && new Date(json["dob"])) || null,
            (json["doj"] && new Date(json["doj"])) || null,           
            null,
            null,
            json["phone1"] || null,
            json["phone2"] || null,
            json["email"] || null,
            id);
        return executeParallelAsyncTasks().then(result => { 
            let address = result[0] || null;
            let contacts = result[1] || null;
            let image = result[2] || null;
            let subjects = result[3] || null;
            let exams = result[4] || null;
            student.address =address;
            student.image =image           
            student.contacts = contacts;           
            student.subjects = subjects;
            student.exams = exams;
            return student;
        });
    }

    toJSON(){
        let contactIds = [];
        for(let index in this.contacts){
            contactIds.push(this.contacts[index].id);
        }
        let subjectIds = [];
        for(let index in this.subjects){
            subjectIds.push(this.subjects[index].id);
        }
        let examIds = [];
        for(let index in this.exams){
            examIds.push(this.exams[index].id);
        }
        return {
            "firstName": this.firstName || null,
            "middleName":this.middleName || null,
            "lastName": this.lastName || null,
            "rollNo": this.rollNo || null,
            "gender": this.gender || null,      
            "dob": (this.dob && this.dob.toString()) || null,
            "doj": (this.doj && this.doj.toString()) || null,
            "image": (this.image && this.image.id) || null,
            "address": (this.address && this.address.id) || null,
            "phone1": this.phone1 || null,
            "phone2": this.phone2 || null,
            "email": this.email || null,
            "contacts": (this.contacts && contactIds) || null,          
            "subjects" : (this.subjects && subjectIds) || null,
            "exams": (this.exams && examIds) || null,
        };
    }
}

export default Student;
