import {Entity} from "./Entity.js";
import {Address} from "./Address.js";
import {Image} from "./Image.js";
import {Teacher} from "./Teacher.js";
import {Student} from "./Student.js";
import {Class} from "./Class.js";
import {Contact} from "./Contact.js";
import {AcademicYear} from "./AcademicYear.js";

export class School extends Entity{

    constructor(dataSource, name, registrationDate, address, image, phone1, phone2, fax, email, id=null) {
        super(dataSource, id);
        this.name = name;
        this.registrationDate = registrationDate;
        this.address = address;
        this.image = image;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.fax = fax;
        this.email = email;
        this.contacts = null;        
        this.classes = null;
        this.teachers = null;
        this.students = null;     
    }

    get Name(){
        return this.name;
    }
    
    get RegistrationDate(){
        return this.registrationDate;
    }

    get Address(){
        return this.address;
    }

    get Image(){
        return this.image;
    }

    get Phone1(){
        return this.phone1;
    }

    get Phone2(){
        return this.phone2;
    }
    
    get Fax(){
        return this.fax;
    }

    get Email(){
        return this.email;
    }

    get Contacts(){
        if (this.contacts == null){
            this.contacts = this.dataSource.getSchoolContacts(this);
        }
        return this.contacts;
    }

    async save(academicYear) {
       for(let index in this.contacts){
            let ans = await this.contacts[index].save();
        }     
       let address = await this.address.save();     
       let school = await this.dataSource.saveSchool(this);     
       return await school.addSchoolToAcademicYear(academicYear)
     
    }

    get AcademicYear(){
         if (this.academicYear == null){
             this.academicYear = this.dataSource.getCurrentAcademicYearForSchool(this);
        }
        return this.academicYear;
    }

    get Classes(){
        if (this.classes == null){
            this.dataSource.getClassesForSchool(this)
            .then(resolvedClasses => { this.classes = resolvedClasses; });
        }
        return this.classes;
    }

    get Teachers(){
        console.log("teachers called", this.teachers);
        if (this.teachers == null){
            let classes = this.Classes
            console.log("getiing classseds", classes);
            for(let index in classes) {
                let cls = classes[index];
                this.teachers = cls.Teachers
            }
        }
        return this.teachers;
    }

    get Students(){
        console.log("student called", this.students);
        if (this.students == null){
            let classes = this.Classes
            console.log("getiing classseds", classes);
            for(let index in classes) {
                let cls = classes[index];
                this.students = cls.Students
            }
        }
        return this.students;
    }

    async addClassToSchool(cls){
        if(this.classes == null){
             this.classes =[]
        }
        this.classes.push(cls);
        return this.classes;
       
    }

    
    get Holidays(){
        if(this.holidays == null){
            this.holidays = this.dataSource.getHolidays(this);
        }
        return this.holidays;
    }

   

    addClass(name, grade, section, code, teachingMedium, academicYear=null){
        if(academicYear == null){
            academicYear = this.AcademicYear;
        }
        let cls = new Class(this.dataSource, name, grade, section, code, teachingMedium, academicYear, this);
        this.dataSource.addClass(cls);
        this.classes = null;
        return cls;
    }

    addContact(contact){  
        if(this.contacts === null) {    
            this.contacts =[];           
        }
        this.contacts.push(contact);
        return this.contacts;       
    }

    addSchoolToAcademicYear(academicYear) {
        return this.dataSource.addSchoolToAcademicYear(academicYear, this);       
    }


    addHoliday(date){
        this.dataSource.addHoliday(this, date);
        this.holidays = null;
    }

      static toDate(val) {
        if (typeof val === "string" || val instanceof String) {
            return new Date(val);
        }
        return val;
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        async function getAddress(address) {
            return await dataSource.getAddressObject(address) 
        }
        async function getContacts(contacts) {
            return await dataSource.getContactObjects(contacts) 
        }
        async function getClasses(classes) {
            return await dataSource.getClassObjects(classes) 
        }
        async function getImage(imageId) {
            return await dataSource.getImageObject(imageId) 
        }
        async function executeParallelAsyncTasks () {
            let promises = []
            json['address'] && promises.push(getAddress(json["address"]));
            json['contacts'] && promises.push(getContacts(json["contacts"]));
            json['classes'] && promises.push(getClasses(json["classes"]));
            json['image'] && promises.push(getImage(json["image"]));
          
            return await Promise.all(promises);         
        }
        return executeParallelAsyncTasks().then(result => {
            let address = result[0] || null;
            let contacts = result[1] || null;
            let classes = result[2] || null; 
            let image = result[3] || null;
            let school = new School(dataSource,
                          json["name"],
                          this.toDate(json["registrationDate"] || null),
                          address,
                          image,
                          json["phone1"],
                          json["phone2"],
                          json["fax"],
                          json["email"],                             
                          id);
            school.contacts = contacts;
            school.classes = classes;
            console.log("school=>", school);
            return school;
        });
    }
    
    toJSON() {
        let contactIds = [];
        let classIds = [];       
        for(let index in this.contacts){
            contactIds.push(this.contacts[index].id);
        }
        for(let cls in this.classes){
            classIds.push(this.classes[cls].id);
        }       
        
        return {
            "name": this.name || null,
            "registrationDate": this.registrationDate || null,
            "address": (this.address && this.address.id) || null,
            "image": (this.image && this.image.id) || null,
            "phone1": this.phone1 || null,
            "phone2": this.phone2 || null,
            "fax": this.fax || null,
            "email": this.email || null,
            "contacts": (this.contacts && contactIds) || null,
            "classes": (this.classes && classIds) || null           
        };
    }
}

export default School;
