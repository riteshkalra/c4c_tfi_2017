import {Entity} from "./Entity.js";
import {Student} from "./Student.js";
import {Address} from "./Address.js";
import {Subject} from "./Subject.js";
import {Teacher} from "./Teacher.js";
import {Exam} from "./Exam.js";


export class Class extends Entity{
    constructor(dataSource, name, grade, section, code, teachingMedium, id=null){
        super(dataSource, id);
        this.name = name;
        this.grade = grade;
        this.section = section;
        this.code = code;
        this.teachingMedium = teachingMedium;
        this.subjects = null;
        this.adminTeacher = null;
        this.teachers = null;
        this.students = null;
        this.studentsByOptionalSubject = null;
        this.examsBySubject = null;

    }

    get Name(){
        return this.name;
    }

    get Grade(){
        return this.grade;
    }

    get Section(){
        return this.section;
    }

    get Code(){
        return this.code;
    }

    get TeachingMedium(){
        return this.teachingMedium;
    }

    get School() {
        return this.school;
    }

    get AcademicYear(){
        return this.academicYear;
    }

    get Subjects(){
        if (this.subjects === null){
            this.dataSource.getSubjectsForClass(this)
            .then(resolvedSubjects => { this.subjects = resolvedSubjects; });
        }
        return this.subjects;
    }

    get Teachers(){
        if (this.teachers === null){
            //have to implement this
           // this.teachers = this.dataSource.getTeachersBySubjectForClass(this);
        }
        return this.teachers;
    }

    get Students(){
        if (this.students == null){
             //have to implement this
          //  this.students = this.dataSource.getStudentsFromClass(this);
        }
        return this.students;
    }

    get AdminTeacher(){
        if (this.adminTeacher == null){
            this.adminTeacher = this.dataSource.getAdminTeacher(this);
        }
        return this.adminTeacher;
    }

    get StudentsByOptionalSubjects(){
        if (this.studentsByOptionalSubject == null){
            this.studentsByOptionalSubject = new Map();
            for(let subject of this.Subjects) {
                if (subject.IsMandatory) continue;
                this.studentsByOptionalSubject.set(subject, this.dataSource.getStudentsBySubject(this, subject));
            }
        }
        return this.studentsByOptionalSubject;
    }

    get ExamsBySubject(){
        if(this.examsBySubject == null){
            this.examsBySubject = new Map();
            for(let subject of this.Subjects){
                this.examsBySubject.set(subject, this.dataSource.getExamsForSubject(this, subject));
            }
        }
        return this.examsBySubject;
    }

    addAdminTeacher(teacher){   
        if(this.teachers === null)
        {
          this.teachers =[];  
        }
       return this.teachers.push(teacher);      
    }

    addStudentToClass(student){   
        if(this.students === null)
        {
          this.students =[];  
        }
       return this.students.push(student);      
    }

    addSubject(name, category, isMandatory){
        let subject = new Subject(this.dataSource, name, category, isMandatory);
        this.dataSource.addSubject(subject);
        this.addExistingSubject(subject);
    }

    addExistingSubject(subject){
        this.dataSource.addSubjectToClass(this, subject);
        this.subjects = null;
    }

    removeSubject(subject){
        this.dataSource.removeSubjectFromClass(this, subject);
        this.subjects = null;
    }

    addTeacherForSubject(teacher, subject){
        this.dataSource.addTeacherToSubject(this, teacher, subject);
        this.teachers = null;
    }
    
    removeTeacherForSubject(teacher, subject){
        this.dataSource.removeTeacherForSubject(this, teacher, subject);
        this.teachers = null;
    }

    addStudent(firstName, lastName, rollNo, gender, dob, image, building, street, city, districy, state, country, postal, phone1, phone2, email) {
        // Add a new student to this school in a class
        let address = new Address(building, street, city, districy, state, country, postal);
        let student = new Student(this.dataSource, firstName, lastName, rollNo, gender, dob, image, address, phone1, phone2, email);
        this.dataSource.addStudent(student);
        return this.addExistingStudent(student);
    }

    addExistingStudent(student){
        this.dataSource.addStudentToClass(this, student);
        this.students = null;
        return student;
    }

    removeStudent(student){
        // Student leaves the school, so update the end date
        this.dataSource.removeStudentFromClass(this, student);
        this.students = null;
    }

    addStudentToSubject(student, subject){
        if (!subject.IsMandatory){
            this.dataSource.addStudentToSubject(this, subject, student);
            this.studentsByOptionalSubject = null;
        }
    }

    removeStudentFromSubject(student, subject){
        if (!subject.IsMandatory){
            this.dataSource.removeStudentFromSubject(this, subject, student);
            this.studentsByOptionalSubject = null;
        }
    }

    getStudentsBySubject(subject){
        if (!subject.IsMandatory){
            return this.StudentsByOptionalSubjects[subject];
        }
        else{
            return this.Students;
        }
    }

    addExam(name, date, passMarks, passGrades, subject){
        let exam = new Exam(name, date, passMarks, passGrades, this, subject);
        this.dataSource.addExam(this, exam);
    }

    removeExam(exam){
        this.dataSource.removeExam(exam);
    }

    getExamsForSubject(subject){
        return this.dataSource.getExamsForSubject(this, subject);
    }

    getExamResults(student, exam){
        return this.dataSource.getExamResults(student, exam);
    }

    update() {
        this.dataSource.updateClass(this);
    }
    
    async save(school) {
        let cls = await this.dataSource.saveClass(this);
        debugger;
        cls =await cls.addClassToSchool(school);
        return cls
    }
    
    addClassToSchool(school) {        
        return this.dataSource.addClassToSchool(school, this);
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        async function getSubjects(subjectIds) {
           return await dataSource.getSubjectObjects(subjectIds) 
        }
        async function getAdminTeacher(adminTeacherId) {
           return await dataSource.getTeacherObject(adminTeacherId) 
        }
        async function getTeachers(teacherIds) {
           return await dataSource.getTeacherObjects(teacherIds) 
        }
        async function getStudents(studentIds) {
           return await dataSource.getStudentObjects(studentIds) 
        }
        async function executeParallelAsyncTasks(){
            let promises = []
            json["subjects"] && promises.push(getSubjects(json["subjects"]))
            json["adminTeacher"] && promises.push(getAdminTeacher(json["adminTeacher"]))
            json["teachers"] && promises.push(getTeachers(json["teachers"]))
            json["students"] && promises.push(getStudents(json["students"])) 
            console.log("promises subject ->"+promises)
            return await Promise.all(promises);
        }

        return executeParallelAsyncTasks().then((result) => {          
            let subjects = result[0] || null;
            let adminTeacher = result[1] || null;
            let teachers = result[2] || null;
            let students = result[3] || null; 
            let classObj = new Class(dataSource,
                json["name"],
                json["grade"],
                json["section"],
                json["code"],
                json["teachingMedium"],
                id
            );
            classObj.subjects = subjects;
            classObj.adminTeacher = adminTeacher;
            classObj.teachers = teachers;
            classObj.students = students;          
            return classObj;
        });
    }

    toJSON(){
        let subjectIds = [];
        for(let index in this.subjects){
            subjectIds.push(this.subjects[index].id);
        }
        let teacherIds = [];
        for(let index in this.teachers){
            teacherIds.push(this.teachers[index].id);
        }
        let studentIds = [];
        for(let index in this.students){
            studentIds.push(this.students[index].id);
        }
        return {
            "name": this.name || null,
            "grade": this.grade || null,
            "section": this.section || null,
            "code": this.code || null,
            "teachingMedium": this.teachingMedium || null,
            "subjects":(this.subjects &&  subjectIds) || null,
            "adminTeacher": (this.adminTeacher && this.adminTeacher.id )|| null,
            "teachers": (this.teachers && teacherIds) || null,
            "students": (this.students && studentIds) || null
                   
        }
    }
}
