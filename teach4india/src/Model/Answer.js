import {Entity} from "./Entity";

export class Answer extends Entity{
    constructor(dataSource, exam,question, answer, mastery,id=null){
        super(dataSource, id);
        this.exam = exam;
        this.question = question;
        this.answer = answer;
        this.mastery = mastery;
    }

    get Exam(){
    	return this.exam;
    }
     get Question() {
        return this.question;
    }

     get Answer() {
        return this.answer;
    }

    get Mastery() {
        return this.mastery;
    }

    async save() {
      return this.dataSource.addExamAnswer(this)
    }

    static fromJSON(dataSource, id, json) {
        if(!json) return null;
        
        async function getExam(examId) { 
            return await dataSource.getExamObject(examId)     
        }
        async function getQuestion(questionId) { 
            return await dataSource.getQuestionObject(questionId)     
        }
        async function executeParallelAsyncTasks () {
            let promises = []
            json['exam'] && promises.push(getExam(json["exam"]));
            json['question'] && promises.push(getQuestion(json["question"]));
            return await Promise.all(promises);         
        }
        return executeParallelAsyncTasks().then(result => {
            let exam = result[0] || null;
            let question = result[1] || null;
            let answer = new Answer(dataSource,
                              exam,
                              json["answer"] || null,
                              question,
                              json["mastery"] || null,
                              id);
            return answer;
        });
    }

    toJSON(){ 
        return {
        	 "exam": (this.exam && this.exam.id) || null,
            "question": (this.question && this.question.id) || null,
            "answer": this.answer || null,
            "mastery": this.mastery || null,
            
        };
    }

}