import {Entity} from "./Entity.js";
import {Contact} from "./Contact.js";
import {Address} from "./Address.js";
import {Image} from "./Image.js";

export class Teacher extends Entity {
    constructor(dataSource, firstName, lastName, empId, gender, dob, address, image, phone1, phone2, email, registrationDate, id=null){
        super(dataSource, id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.empId = empId;
        this.gender = gender;
        this.dob = dob;
        this.image = image;
        this.address = address;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.email = email;
        this.registrationDate = registrationDate;
        this.contacts = null;
        this.acadYear = null;
        this.classes = null;
        this.subjects = null;
    }


    get FirstName(){
        return this.firstName;
    }

    get LastName(){
        return this.lastName;
    }

    get Name(){
        return this.FirstName + " " + this.LastName;
    }

    get EmployeeID(){
        return this.empId;
    }

    get Gender() {
        return this.gender;
    }

    get DOB(){
        return this.dob;
    }

    get Image(){
        return this.image;
    }

    get Phone1(){
        return this.phone1;
    }

    get Phone2(){
        return this.phone2;
    }

    get Email(){
        return this.email;
    }

    get Address(){
        return this.address;
    }

    get RegistrationDate(){
        return this.registrationDate;
    }

    get Contact(){
        if (this.contacts == null){
            this.contacts = this.dataSource.getTeacherContacts(this);
        }
        return this.contacts;
    }S

    get School(){
        if (this.school == null){
            this.school = this.dataSource.getSchoolForTeacher(this);
        }
        return this.school;
    }

    get AcademicYear(){
        return this.School.AcademicYear;
    }

    get Classes(){
        // Classes where this teacher is the admin
        if (this.cls == null){
            this.cls = this.dataSource.getClassesForTeacher(this.AcademicYear, this.School, this);
        }
        return this.cls;
    }

    get SubjectsByClasses(){
        // All subjects handled by this teacher in various classes
        if (this.subjects == null){
            this.subjects = this.dataSource.getSubjectsForTeacher(this.AcademicYear, this.School, this.Classes, this);
        }
        return this.subjects;
    }

    addContact(contact){      
        if(this.contacts === null) {    
            this.contacts =[];           
        }
        this.contacts.push(contact);
        return this.contacts;         
    }

    updateContact(contact){
        this.dataSource.updateContact(this, contact);
    }


    async save(classObj) {
        for(let index in this.contacts){
             let ans = await this.contacts[index].save();
         }  
         debugger;   
        let address = await this.address.save();   
        debugger;          
        let teacher = await this.dataSource.saveTeacher(this);    
         debugger;
        return await teacher.addTeacherToClass(classObj)      
     }

    addTeacherToClass(classObj) {        
       return this.dataSource.addTeacherToClass(classObj, this);
    }

    static toDate(val) {
        if (typeof val === "string" || val instanceof String) {
            return new Date(val);
        }
        return val;
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        async function getContacts(contactIds) { 
            return await dataSource.getContactObjects(contactIds)     
        }
        async function getAddress(addressId) { 
            return await dataSource.getAddressObject(addressId)     
        }
        async function getImage(imageId) { 
            return await dataSource.getImageObject(imageId)     
        }
        async function executeParallelAsyncTasks () {
            let promises = []
            json['contacts'] && promises.push(getContacts(json["contacts"]));
            json['address'] && promises.push(getAddress(json["address"]));
            json['image'] && promises.push(getImage(json["image"]));
            return await Promise.all(promises);         
        }
        let teacher = new Teacher(dataSource,
            json["firstName"] || null,
            json["lastName"] || null,
            json["empId"] || null,
            json["gender"] || null,
            this.toDate(json["dob"] || null),
            null,
            null,
            json["phone1"] || null,
            json["phone2"] || null,
            json["email"] || null,
            this.toDate(json["registrationDate"] || null),
            id);
        return executeParallelAsyncTasks().then(result => {
            let contacts = result[0] || null;
            let address = result[1] || null;
            let image = result[2] || null;   
            teacher.address =address;
            teacher.image =image        
            teacher.contacts = contacts;        
            return teacher;
        });   
        return teacher;  
    }

    toJSON(){
        let contactIds = [];
        debugger;
        for(let index in this.contacts){
            contactIds.push(this.contacts[index].id);
        }
        return {
            "firstName": this.firstName || null,
            "lastName": this.lastName || null,
            "empId": this.empId || null,
            "gender": this.gender || null,
            "dob":   this.doj || null,
            "address": (this.address && this.address.id) || null,
            "image": (this.image && this.image.id) || null,
            "phone1": this.phone1 || null,
            "phone2": this.phone2 || null,
            "email": this.email || null,
            "registrationDate": this.registrationDate || null,
            "contacts": (this.contactIds && contactIds) || null          
          
        };
    }
}
