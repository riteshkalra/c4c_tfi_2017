import {Entity} from "./Entity.js"
import {School} from "./School.js"
import {Address} from "./Address.js"

export class Term extends Entity{
    constructor(dataSource, name, startDate, endDate, cls, id=null){
        super(dataSource, id);
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.cls = cls;
    }

    get Name(){
        return this.name;
    }

    get StartDate(){
        return this.startDate;
    }

    get EndDate(){
        return this.endDate;
    }

    get Class(){
        return this.cls;
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        return new Term(dataSource,
            json["name"],
            new Date(json["startDate"]),
            new Date(json["endDate"]),
            id);
    }

    toJSON(){
        return {
            "name": this.name,
            "startDate": this.startDate.toString(),
            "endDate": this.endDate.toString()
        };
    }
}
