import {Entity} from "./Entity";
import {ExamResult} from "./ExamResult";
import {Util} from "./Util"

export class Exam extends Entity {
    constructor(dataSource, name, date, passMarks, passGrades, id=null){
        super(dataSource, id);
        this.name = name;
        this.date = date;
        this.passMarks = passMarks;
        this.passGrades = passGrades;        
        this.questions = null;
        this.results = null;
    }


    get Id() {
        return this.id;
    }
    get Name(){
        return this.name;
    }

    get Date(){
        return this.date;
    }

    get PassMarks(){
        return this.passMarks;
    }

    get PassGrades(){
        return this.passGrades;
    }

    get Class(){
        return this.cls;
    }

    get Subject(){
        return this.subject;
    }

    get AcademicYear(){
        return this.Class.AcademicYear;
    }

    get Questions(){
         if (this.questions == null){
            this.questions = this._questions()
        }
        return this.questions;
    }

    async _questions() {
       let questions = await this.dataSource.getExamQuestions(this);
       console.log(questions);
       return questions;
    }


    get Results(){
        if(this.results == null) {
            this.results = new Map();
            for (let student of this.Class.Students) {
                let result = new ExamResult(this.dataSource, this, student);
                this.results.set(student, result);
            }
        }
        return this.results;
    }


    addQuestion(question){
        this.dataSource.addQuestionToExam(this, question);
        this.questions = null;
    }

    removeQuestion(question){
        this.dataSource.removeQuestionFromExam(this, question);
        this.questions = null;
    }

    update(){
        this.dataSource.updateExam(this);
    }

    static toDate(val) {
        if (typeof val === "string" || val instanceof String) {
            return new Date(val);
        }
        return val;
    }

     static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        async function getClass(classId) {
            return await dataSource.getClassObject(classId)     
        }
        async function getSubject(subjectId) { 
            return await dataSource.getSubjectObject(subjectId)     
        }
        async function getQuestions(questionIds) { 
            return await dataSource.getQuestionObjects(questionIds)     
        }
        async function getResults(resultIds) { 
            return await dataSource.getResultObjects(resultIds)     
        }
        async function executeParallelAsyncTasks () {
            let promises = []
            json['questions'] && promises.push(getQuestions(json["questions"]));
            json['results'] && promises.push(getResults(json["results"]));
            return await Promise.all(promises);         
        }
        return executeParallelAsyncTasks().then(result => {
            let questions = result[0] || null;
            let results = result[1] || null;
            let exam = new Exam(dataSource,
                        json["name"] || null,
                        (json["date"] && Util.toDate(json["date"])) || null,
                        json["passMarks"] || null,
                        json["passGrades"] || null,                                            
                        id)
            exam.questions = questions;
            exam.results = results;
            return exam;
        });
    }

    toJSON(){
        let questionIds = []
        for(let index in this.questions) {
            let question = this.questions[index];
            questionIds.push(question.id)
        }
        let resultIds = []
        for(let index in this.results) {
            let result = this.results[index];
            resultIds.push(result.id)
        }
        return {
            "name": this.name || null,
            "date": this.date || null,
            "passMarks": this.passMarks || null,
            "passGrades": this.passGrades || null,    
            "questions": (this.questions && this.questionIds) || null,
            "results": (this.results && this.resultIds) || null, 
        };
    }
}
