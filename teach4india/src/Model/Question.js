import {Entity} from "./Entity.js";
import {MasteryLevel} from "./Mastery.js";

export class Question extends Entity{
    constructor(dataSource, description, concept, type, difficultyLevel, options, answer, subject, id=null){
        super(dataSource, id);
        this.description = description;
        this.concept = concept;
        this.type = type;
        this.difficultyLevel = difficultyLevel;
        this.options = options;
        this.answer = answer;
        this.subject = subject;
        this.masteryLevels = null;
    }

    get Id(){
        return this.id;
    }

    get Description(){
        return this.description;
    }

    get Concept() {
        return this.concept;
    }

    get Type(){
        return this.type;
    }

    get DifficultyLevel(){
        return this.difficultyLevel;
    }

    get Options(){
        return this.options;
    }

    get Answer(){
        return this.answer;
    }

    get Subject(){
        return this.subject;
    }

    get MasteryLevels(){
        if (this.masteryLevels == null){
           this.masteryLevels = this.dataSource.getMastery(this);
        }
        return this.masteryLevels;
    }

    async save() {
       for(let index in this.masteryLevels){
            let ans = await this.masteryLevels[index].save();
        }
       return this.dataSource.saveQuestion(this);
    }
   
    addMasteryLevel(mastery, mark, grade){
        let masteryLevel = new MasteryLevel(this.dataSource, this, mastery, mark, grade);
        this.dataSource.addMasteryLevel(masteryLevel,this);
        this.masteryLevels = null;
    }

    removeMasteryLevel(masteryLevel){
        this.dataSource.removeMasteryLevel(masteryLevel);
        this.masteryLevels = null;
    }

    update(){
        this.dataSource.updateQuestion(this);
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        async function getMasteryLevels(masteryLevelIds) {
          return await dataSource.getMasteryLevelObjects(masteryLevelIds); 
        }
        async function getSubject(subjectId) {
          return await dataSource.getSubjectObject(subjectId); 
        }
        async function getAnswer(answerId) {
          return await dataSource.getAnswerObject(answerId); 
        }
        async function executeParallelAsyncTasks () {
          return  await Promise.all([ getSubject(json["subject"]), getAnswer(json["answer"]), getMasteryLevels(json["masteryLevels"]) ]);
        }
        return executeParallelAsyncTasks().then(result => {
            let answer = result[0];
            let subject = result[1];
            let masteryLevels = result[2];
            let question =  new Question(dataSource,
                            json["description"],
                            json["concept"],
                            json["type"],
                            json["difficultyLevel"],
                            json["options"],
                            answer,
                            subject,
                            id);
            question.masteryLevels = masteryLevels;
            return subject;
        });
    }

    toJSON(){
        let masteryIds = []
        for(let index in this.masteryLevels) {
            let masteryLevel = this.masteryLevels[index];
            masteryIds.push(masteryLevel.id)
        }
        return {
            "description": this.description || null,
            "concept": this.concept || null,
            "type": this.type || null,
            "difficultyLevel": this.difficultyLevel || null,
            "options": this.options,
            "answer": (this.answer != null ? this.answer.id : null),
            "subject":(this.subject != null ? this.subject.id : null),
            "masteryLevels": masteryIds
        }
    }
}

export default Question;
