import {Entity} from "./Entity.js";

export class MasteryLevel extends Entity{
    constructor(dataSource, question, mastery, mark, grade, id=null){
        super(dataSource, id);
        this.question = question;
        this.mastery = mastery;
        this.mark = mark;
        this.grade = grade;
    }

    get Question(){
        return this.question;
    }

    get Mastery(){
        return this.mastery;
    }

    get Mark(){
        return this.mark;
    }

    get Grade(){
        return this.grade;
    }

    async save() {
      return this.dataSource.saveMasteryLevels(this)
    }

    update(){
        this.dataSource.updateMasteryLevel(this);
    }

    static fromJSON(dataSource, id, json){
        if(!json) return null;
        
        return new MasteryLevel(dataSource,
                           dataSource.getQuestion(json["question"]),
                           json["mastery"],
                           json["mark"],
                           json["grade"],
                           id);
    }

    toJSON(){
        return {
            "question": this.Question.Id,
            "mastery": this.mastery,
            "mark": this.Mark,
            "grade": this.Grade
        };
    }
}
