import {Entity} from "./Entity";
export class Address extends Entity{
    constructor(dataSource,building, street, city, district, state, country, postal,id=null){
        super(dataSource, id);
        this.building = building;
        this.street = street;
        this.city = city;
        this.district = district;
        this.state = state;
        this.country = country;
        this.postal = postal;
    }

    get BuildingName(){
        return this.building;
    }

    get StreetName(){
        return this.street;
    }

    get City(){
        return this.city;
    }

    get District(){
        return this.district;
    }

    get State(){
        return this.state;
    }

    get Country() {
        return this.country;
    }

    get Postal(){
        return this.postal;
    }

    async save() {
      return this.dataSource.saveAddress(this)
    }

    static fromJSON(dataSource, id,json){
        if(!json) return null;
        return new Address(dataSource,
                           json["buildingName"] || null,
                           json["streetName"] || null,
                           json["city"] || null,
                           json["district"] || null,
                           json["state"] || null,
                           json["country"] || null,
                           json["postal"] || null,
                           id);
    }

    toJSON(){
        return {
            "buildingName": this.building || null,
            "streetName": this.street || null,
            "city": this.city || null,
            "district": this.district || null,
            "state": this.state || null,
            "country": this.country || null,
            "postal": this.postal || null,
        }
    }
}

