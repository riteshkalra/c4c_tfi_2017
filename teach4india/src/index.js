import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter } from 'react-router-dom'
import configureStore from './store/configureStore';
import App from './App';
import {loadAcademicYears,loadCurrentAcademicYear} from './actions/AcademicYear/academicYearAction';
import {loadExams} from './actions/examActions';
import {loadQuestions} from './actions/questionActions';
const store = configureStore();
//store.dispatch(loadAcademicYears());
store.dispatch(loadCurrentAcademicYear());
store.dispatch(loadExams());
store.dispatch(loadQuestions());


render(
  <Provider store={store}>
  	<BrowserRouter>
    	<App/>
  	</BrowserRouter>  	
  </Provider>,
  document.getElementById('dashboard')
);
