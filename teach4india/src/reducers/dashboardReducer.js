import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function dashboardReducer(state = initialState.academicYear, action) {
    switch (action.type) {
        case types.SET_ACADEMIC_YEAR_SUCCESS:
            return action.academicYear;

        default:
            return state;
    }
}