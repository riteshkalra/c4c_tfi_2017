import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function subjectReducer(state = initialState.subjects, action) {

 switch (action.type) {
    case types.LOAD_SUBJECTS_SUCCESS:
      return action.subjects;  
    case types.SAVE_SUBJECT_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.subject)
      ];
    default:
      return state;
  }
}
