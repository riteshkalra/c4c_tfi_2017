import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function schoolReducer(state = initialState.schools, action) {
  switch (action.type) {
    case types.LOAD_STUDENTS_SUCCESS:
      return action.students; 
     case types.LOAD_STUDENT_SUCCESS:
      return action.student;  
    case types.CREATE_STUDENT_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.student)
      ];


    default:
      return state;
  }
}
