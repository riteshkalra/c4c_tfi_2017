import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function masteryReducer(state = initialState.masteries, action) {

 switch (action.type) {
    case types.LOAD_MASTERIES_SUCCESS:
      return action.masteries;  
    case types.SAVE_MASTERY_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.mastery)
      ];
    default:
      return state;
  }
}
