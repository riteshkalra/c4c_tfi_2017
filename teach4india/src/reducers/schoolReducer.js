import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function schoolReducer(state = initialState.schools, action) {

 switch (action.type) {
    case types.LOAD_SCHOOLS_SUCCESS:
     console.log("schoolReducer", state, action)
      return action.schools;  
    case types.SAVE_SCHOOL_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.school)
      ];
    default:
      return state;
  }
}
