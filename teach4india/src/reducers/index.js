import {combineReducers} from 'redux';
import ajaxCallsInProgress from './ajaxStatusReducer';

import students from './studentReducer';
import academicYears from './academicYearReducer';
import {currentAcademicYear} from './academicYearReducer';
import schools from './schoolReducer';
import classes from './classReducer';
import teachers from './teacherReducer';
import questions from './questionReducer';
import subjects from './subjectReducer';
import academicYear from './dashboardReducer';
import exams from './examReducer';
import masteries from './masteryReducer';
import completedExams from './examEvaluationReducer';
import {examQuestions,masteryLevels,examResults,selectedExam} from './examEvaluationReducer';



const rootReducer = combineReducers({
    students,
    academicYears,
    currentAcademicYear,
    schools,
    classes,
    teachers,
    questions,
    exams,
    subjects,
    ajaxCallsInProgress,
    academicYear,
    masteries,
    completedExams,
    examQuestions,
    masteryLevels,
    selectedExam
});

export default rootReducer;
