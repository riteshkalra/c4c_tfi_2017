export default {
    academicYears: [],
    academicYear: null,
    currentAcademicYear:null,
    students: [],
    schools: [],
    classes: [],
    exams: [],
    teachers: [],
    questions: [],
    subjects: [],
    masteries: [],
    masteryLevels:[],
    completedExams:[],
    selectedExam:null,
    examQuestions:[],
    examResults:[],
    ajaxCallsInProgress: 0

};
