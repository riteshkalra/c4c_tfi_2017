import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function teacherReducer(state = initialState.teachers, action) {
  //console.log("teacher reducer called", state, action);
  switch (action.type) {
    case types.LOAD_TEACHERS_SUCCESS:
      return action.teachers;
    case types.SAVE_TEACHER_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.teacher)
      ];
    default:
      return state;
  }
}
