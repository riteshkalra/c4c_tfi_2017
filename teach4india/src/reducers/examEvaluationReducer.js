import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function examEvaluationReducer(state =  initialState.completedExams, action) { 
  switch (action.type) {  	
    case types.LOAD_COMPLETED_EXAM_SUCCESS:   
    	return  action.completedExams;     
    case types.SAVE_EXAMRESULT_SUCCESS:
      return [
                ...state,
                Object.assign({}, action.completedExam)
            ];
    default:
      return state;
  }
}

export  function selectedExam(state =initialState.selectedExam , action) { 
  
  switch (action.type) {    
       case types.VIEW_SELECTED_EXAM_ID_SUCCESS:
      return action.selectedExam;

    default:
      return state;
  }
}

export  function examQuestions(state =  initialState.examQuestions, action) { 
  
  switch (action.type) {    
       case types.LOAD_EXAM_QUESTIONS_SUCCESS:
          console.log("LOAD_EXAM_QUESTIONS_SUCCESS", action.examQuestions);
          return action.examQuestions;
    default:
      return state;
  }
}

export  function masteryLevels(state =  initialState.masteryLevels, action) { 
 switch (action.type) {    
       case types.LOAD_MASTERY_FOR_QUESTION_SUCCESS:
      return action.masteryLevels;

    default:
      return state;
  }
}

// export  function examResults(state =  initialState.examResults, action) { 
//  switch (action.type) {    
//        case types.SAVE_EXAMRESULT_SUCCESS:
//       return action.examResults;

//     default:
//       return state;
//   }
// }