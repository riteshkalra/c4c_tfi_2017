import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function classReducer(state = initialState.classes, action) {
  switch (action.type) {
    case types.LOAD_CLASSES_SUCCESS:
      return action.classes;  

    case types.SAVE_CLASS_ROOM_SUCCESS:
        return [
            ...state,
            Object.assign({}, action.classRoom)
       ];

    default:
      return state;
  }
}
