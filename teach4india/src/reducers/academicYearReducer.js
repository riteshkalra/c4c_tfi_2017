import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function academicYearReducer(state = initialState.academicYears, action) {
    switch (action.type) {
        case types.LOAD_ACADEMIC_YEARS_SUCCESS:
          console.log( "LOAD_ACADEMIC_YEARS_SUCCESS "+ action.academicYears)
            return action.academicYears;

        case types.SAVE_ACADEMIC_YEARS_SUCCESS:
            return [
                ...state,
                Object.assign({}, action.academicYear)
            ];

        case types.EDIT_ACADEMIC_YEARS_SUCCESS:
            let oldState = [...state];
            let rowIndex = oldState.findIndex(element => {
                return element.id === action.academicYear.id;
            });
            oldState[rowIndex] = action.academicYear;
            return oldState;

        default:
            return state;
    }
}

export function currentAcademicYear(state =initialState.currentAcademicYear , action) { 
  
  switch (action.type) {    
       case types.LOAD_ACADEMIC_YEAR_SUCCESS:
      return action.currentAcademicYear;

    default:
      return state;
  }
}
