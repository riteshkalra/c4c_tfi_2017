import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function examReducer(state = initialState.exams, action) { 
  switch (action.type) {
    case types.LOAD_EXAMS_SUCCESS:
      return action.exams;  
    case types.SAVE_EXAM_SUCCESS:
      return [
        ...state,
        Object.assign({}, action.exam)
      ];
    default:
      return state;
  }
}