import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import * as subjectActions from '../../actions/subjectActions';

class SubjectListRow extends Component {
  render(){
    const {subject} = this.props;
    return (
      <tr>     
        <td>{subject.name}</td>
        <td>{subject.category}</td>
        <td>{subject.isMandatory}</td>
        <td>
          <div>
            <i className="fa fa-edit pull-middle">Edit</i>  
            <span> </span>
            <i className="fa fa-eye pull-middle">View</i>
          </div> 
        </td>
      </tr>
    );
  };
}

export default SubjectListRow;
