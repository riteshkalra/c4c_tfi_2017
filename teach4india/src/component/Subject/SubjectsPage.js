import React, { Component } from 'react';
import SubjectList from './SubjectList'
import * as subjectAction from '../../actions/subjectActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import queryString from 'query-string'
import { Link } from 'react-router-dom'


class SubjectsPage extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      subjects: props.subjects
    }
  }

  componentDidMount() {
    this.props.actions.loadSubjects();
  }

  render(){
    const {subjects} = this.props;
    return (
               
      <section class="content ">
        <h2>Subjects</h2>
        <div class="row">
          <div class="col-md-12">
            <div class="box-body">
              <SubjectList subjects={subjects}/>
            </div>
          </div>
        </div>
        <div class="row ">
        <div class="col-md-2 pull-right">
          <div class="box-body">
           <Link to ="/subjectCreation"><button type="button" class="btn btn-block btn-primary" >Add</button></Link>
          </div>
        </div>
      </div>            
      </section>        
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    subjects: state.subjects
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(subjectAction, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SubjectsPage);
