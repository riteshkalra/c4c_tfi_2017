import React, { Component } from 'react';
import SubjectListRow from './SubjectListRow';

class SubjectList extends Component {
  render(){
    const {subjects} = this.props;
    return (
      <table id="example3" class="table table-bordered table-striped"> 
        <thead class ="bg-blue">
          <tr>
            <th>Name</th>
            <th>Category</th> 
            <th>isMandatory</th> 
            <th>Action</th>         
          </tr>
        </thead>
        <tbody>
          {subjects.map(subject =>
            <SubjectListRow key={subject.id} subject={subject}/>
          )}
        </tbody>
      </table>
    );
  }
}
export default SubjectList;
