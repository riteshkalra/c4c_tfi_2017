import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import PropTypes from 'prop-types';

const QuestionDetail = ({question}) => {
  
  console.log("question pass to detail page",question);
  
  return (

    <div>
      { question.type == 'MCQ' ?
        //for MCQ questions
        <div> 
          {question.description} <br/><br/>
          A: {question.options[0]} <br/>
          B: {question.options[1]} <br/>
          C: {question.options[2]} <br/>
          D: {question.options[3]} <br/><br/>
          <b>Answer: </b> {question.answer} <br/><br/>
        </div>
        : //for non-MCQ questions
        <div> {question.description} </div>
      }
      <b>Mastery Levels: </b><br/>
    </div>
      
  );
};


QuestionDetail.propTypes = {
  question: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default QuestionDetail;
