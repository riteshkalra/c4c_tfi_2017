import React, { Component } from 'react';
import QuestionList from './QuestionList'
import * as questionAction from '../../actions/questionActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import queryString from 'query-string'
import { Link } from 'react-router-dom'


class QuestionsPage extends Component {

  constructor(props, context) {
    super(props, context);
  }

  componentWillUnmount() {
    console.log("componentWillUnmount: Component is about to be removed from the DOM!");
  }

  componentWillMount() {
    const search = this.props.location.pathname; 
    const params = new URLSearchParams(search);
    const academicYearId = params.get('academicYearId'); 
    this.props.actions.loadQuestions(); 
  }

  render(){
    const {questions} = this.props;
    return (
               
      <section class="content ">
        <h2>Questions</h2>
        <div class="row">
          <div class="col-md-12">
            <div class="box-body">
              <QuestionList questions={questions}/>
            </div>
          </div>
        </div>
        <div class="row ">
        <div class="col-md-2 pull-right">
          <div class="box-body">
           <Link to ="/questionCreation"><button type="button" class="btn btn-block btn-primary" >Add</button></Link>
          </div>
        </div>
      </div>            
      </section>        
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    questions: state.questions
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(questionAction, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(QuestionsPage);
