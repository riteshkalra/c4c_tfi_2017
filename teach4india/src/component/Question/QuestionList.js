import React, { Component } from 'react';
import QuestionListRow from './QuestionListRow';

class QuestionList extends Component {
  render(){
    const {questions} = this.props;
    return (
      <table id="example3" class="table table-bordered table-striped"> 
        <thead class ="bg-blue">
          <tr>
            <th>Description</th>
            <th>Subject</th>
            <th>Concept</th> 
            <th>Type</th> 
            <th>Action</th>         
          </tr>
        </thead>
        <tbody>
          {questions.map(question =>
            <QuestionListRow key={question.id} question={question}/>
          )}
        </tbody>
      </table>
    );
  }
}
export default QuestionList;
