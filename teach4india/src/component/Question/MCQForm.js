import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import PropTypes from 'prop-types';

const MCQForm = ({question, onChange, errors, answers}) => {

  if (question.options != null) {
      question.optionA = question.options[0];
      question.optionB = question.options[1];
      question.optionC = question.options[2];
      question.optionD = question.options[3];
  }

  return (
    <div class="box box-default">
      <form>

        <div class="box-header with-border">  
          <h3 className="box-title"><b><i> MCQ Inputs </i></b></h3>
        </div>

        <div className="box-body">
          <div className="row">
            <div className="col-md-4"> 
              <TextInput
                name="optionA"
                label="Option A"
                value={question.optionA}
                onChange={onChange}/>
              <TextInput
                name="optionB"
                label="Option B"
                value={question.optionB}
                onChange={onChange}/>
              <TextInput
                name="optionC"
                label="Option C"
                value={question.optionC}
                onChange={onChange}/>
              <TextInput
                name="optionD"
                label="Option D"
                value={question.optionD}
                onChange={onChange}/>
            </div>

            <div className="col-md-4"> 
              <SelectInput 
                name="answer"
                label="Answer"
                value={question.answer}
                onChange={onChange}
                options={answers}/>
              <TextInput
                name="mark"
                label="Mark"
                //value={question.masteryLevel}
                onChange={onChange}/>
            </div>

          
          </div>
        </div>
      </form>
      
    </div>
  );

};


MCQForm.propTypes = {
  question: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  errors: PropTypes.object
};

export default MCQForm;
