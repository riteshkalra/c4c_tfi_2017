import React from 'react';
import TextInput from '../common/TextInput';
import * as Utils  from '../common/common-function'
import SelectInput from '../common/SelectInput';
import PropTypes from 'prop-types';
import MCQForm from './MCQForm';
import MasteryCreation from '../Mastery/MasteryCreation';

const QuestionForm = ({question, subjects, types, onSave, onChange, saving, errors}) => {

  let subjectOption =Utils.modelFormattedForDropdown(subjects)
  return (
      
    <form onSubmit ={onSave}>
      <h2>Create New Question</h2><br/>
      <div className="box box-default">
        <div className="box-body">
          <div className="row">

            <div className="col-md-6"> 
              <SelectInput 
                name="type"
                label="Type"
                value={question.type}
                onChange={onChange}
                options={types}
                error={errors.type}/>
            </div>
            <div className="col-md-6"> 
              <TextInput
                name="difficultyLevel"
                label="Difficulty Level"
                value={question.difficultyLevel}
                onChange={onChange}
                error={errors.difficultyLevel}/>
            </div>
            <div className="col-md-6"> 
              <SelectInput 
                name="subject"
                 label="Subject"
                 value={question.subject}
                 onChange={onChange}
                 options={subjectOption}
                 error={errors.subject}/>
            </div>
            <div className="col-md-6"> 
              <TextInput
                name="concept"
                label="Concept"
                value={question.concept}
                onChange={onChange}
                error={errors.concept}/>
            </div>
            
            
            <div class="col-md-12">
              <TextInput
                name="description"
                label="Description"
                value={question.description}
                onChange={onChange}
                error={errors.description}/>
              <br/><br/>
              <SecondaryForm question={question} onChange={onChange}/>
            </div>
            
          </div>
        </div>
      </div>

      <input
        type="submit"
        disabled={saving}
        value={saving ? 'Saving...' : 'Add Question'}
        class="btn btn-info pull-right"
        onClick={onSave}/> 

    </form>
  );
};


function SecondaryForm(props) {
  
  const type = props.question.type; //0-MCQ,1-SA,2-LA
  const answers = [
        {value: "0", text: "A"},
        {value: "1", text: "B"},
        {value: "2", text: "C"},
        {value: "3", text: "D"}]

  if (type === "0") {
    return <MCQForm question={props.question} onChange={props.onChange} answers={answers}/>;
  }else if (type === "1" || type === "2"){
    return <MasteryCreation question={props.question}/>;
  }else{
    return null;
  }
  
}


QuestionForm.propTypes = {
 // question: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object,
  subjects: PropTypes.object.isRequired,
  types: PropTypes.object.isRequired
};

export default QuestionForm;


