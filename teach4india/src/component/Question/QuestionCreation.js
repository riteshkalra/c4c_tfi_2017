import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'
import {Question} from '../../Model/Question'
import {Admin} from '../../Model/Admin'
import toastr from 'toastr';
import PropTypes from 'prop-types';
import QuestionForm from './QuestionForm'
import * as questionActions from '../../actions/questionActions';

class QuestionCreation extends Component { 
  constructor(props, context) {
    super(props, context);
    // mapping list of subject class to dictionary
    const subjects = [];
    let subs = props.subjects;
    for (let sub of subs){
      subjects[sub.id] = sub.name;
    }

    console.log("prop subjects", props.subjects);

    this.state = {
      question: Object.assign({}, props.question),
      subjects: subjects,
      types: [
        {value: "0", text: "MCQ"},
        {value: "1", text: "SA"},
        {value: "2", text: "LA"}],
      errors: {},
      saving: false
    };

    this.onSaveQuestion = this.onSaveQuestion.bind(this);
    this.onChangeComponent = this.onChangeComponent.bind(this);
  }

  onSaveQuestion(event) {
    this.setState({saving: true}) 
    event.preventDefault();

    let question = this.state.question;
    // map type index to name
    if (question.type == 0){
      question.type = 'MCQ';
      if (question.options == null) {
        question.options = [question.optionA, question.optionB, question.optionC, question.optionD];
      }
    } else if (question.type == 1){
      question.type = 'SA';
    } else if (question.type == 2){
      question.type = 'LA';
    }

    // map answer index to name
    if (question.answer == 0){
      question.answer = 'A';
    } else if (question.answer == 1){
      question.answer = 'B';
    } else if (question.answer == 2){
      question.answer = 'C';
    } else if (question.answer == 3){
      question.answer = 'D';
    }

    let questionObj = new Question(
                          Admin.getDataSource(),
                          question.description,
                          question.concept,
                          question.type,
                          question.difficultyLevel,
                          question.options,
                          question.answer,
                          question.subject,
                          question.masteryLevels);
    
    

    this.props.actions.saveQuestion(questionObj).then(() => this.redirect())
      .catch(error => {
        toastr.error(error);
        this.setState({saving: false});
      }); 
  }

  onChangeComponent(event) {
    const field = event.target.name;
    let question = Object.assign({}, this.state.question);
    if(event.target.type==="date")
     {
        question[field] = event.target.valueAsDate;
     }
    else{
      question[field] = event.target.value;
    }
 
   return this.setState({question: question});

  }

  redirect() {
    this.setState({saving: false});
    toastr.success('Question saved');
    this.props.history.push('/questions');
  }

 render(){
    return (               
      <section class="content">             
        <QuestionForm
          onChange={this.onChangeComponent}                      
          onSave={this.onSaveQuestion}
          question={this.state.question}
          errors={this.state.errors}
          saving={this.state.saving}
          subjects={this.state.subjects}
          types={this.state.types}
          />
      </section>           
    );
 	}
 }


function getQuestionById(questions, id) {
  const question = questions.filter(question => question.id == id);
  if (question) return question[0]; //since filter returns an array, have to grab the first.
  return null;
}
  
function mapStateToProps(state, ownProps) {
  let questionId = null;
  if(ownProps.match) questionId = ownProps.match.params.id   
  let question = {};
  let subjects = state.subjects;

  console.log("state subjects", state.subjects);

  if (questionId && state.questions.length > 0) {
    question = getQuestionById(state.questions, questionId);
  }

  return {   
    question: question,
    subjects: subjects
  };
  
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(questionActions, dispatch)
  };
}

QuestionCreation.contextTypes = {
  router: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(QuestionCreation);


