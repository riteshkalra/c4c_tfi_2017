import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as questionActions from '../../actions/questionActions';
import QuestionDetail from './QuestionDetail'
import {Admin} from '../../Model/Admin'

class QuestionListRow extends Component {
  render(){
    const {question} = this.props;
    //let subjectName = Admin.getSubjectNameById(question.subject);
    let shortDes = question.description
    if (shortDes.length > 40){
      shortDes = shortDes.substring(0,40) + '...';
    }

    return (
      <tr>     
        <td>{shortDes}</td>
        <td>{question.subject}</td>
        <td>{question.concept}</td>
        <td>{question.type}</td>
        <td>
          <div class="btn-group">
            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-edit pull-middle">Edit</i></button>
            <button type="button" class="btn btn-default btn-sm" data-toggle="modal"
              data-target="#question-detail"><i className="fa fa-eye pull-middle">View</i></button>
          </div>

          <div className="modal fade" id="question-detail">
            <div className="modal-dialog">
              <div className="modal-content">

                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                  <h4 className="modal-title">Question </h4>
                </div>
{/*
                <div className="modal-body">
                  <QuestionDetail question={question}/>
                </div>
*/}
    
                <div className="modal-body">
                  TEST
                </div>


              </div>
            </div>
          </div>

        </td>
      </tr>
    );
  };
}

export default QuestionListRow;
