import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import PropTypes from 'prop-types';
import ImageUploader from 'react-images-upload';
import PhoneInput from '../common/PhoneInput';
import DatePickerInput from '../common/DatePickerInput';
import * as Utils  from '../common/common-function';


class SchoolForm extends React.Component {
 
  render(){
    let contactOptions = Utils.getSchoolRelationShipOption();
    return (
     <form onSubmit ={this.props.onSave}>
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Official Details</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>           
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">  
                 <TextInput name="academicYear" label="Academic Year" value={this.props.currentAcademicYear.name} disabled onChange={this.props.onChange} error=""/>               
          
              </div>
              <div class="col-md-4"> 
                  <TextInput name="registrationNumber" label="Registration Number" value={this.props.school.id} disabled onChange={this.props.onChange} error=""/> 
              </div>
              <div class="col-md-4">            
                <DatePickerInput name="registrationDate" label="Registraton date"  value={this.props.school.registrationDate} onChange={this.props.onChange} error={this.props.errors.registrationDate}/> 
            </div>        
            </div>
          </div>
        </div>
            {/* Student details form start*/}
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">School Details</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">              
                <TextInput name="name" label="Name"  value={this.props.school.name} onChange={this.props.onChange} error={this.props.errors.name}/>  
            </div>
            <div class="col-md-4">            
                <TextInput name="image" label="Image Upload"  value={this.props.school.image} onChange={this.props.onChange} error={this.props.errors.image}/> 
            </div>
            <div class="col-md-4">
                <TextInput name="email" label="Email"  value={this.props.school.email} onChange={this.props.onChange} error={this.props.errors.email}/>
            </div>
          </div>                
           <div class="row"> 
            <div class="col-md-4"> 
              <PhoneInput name="phone1" label="Phone 1"  value={this.props.school.phone1} onChange={this.props.onChange} error={this.props.errors.phone1}/> 
            </div>
             <div class="col-md-4"> 
              <PhoneInput name="phone2" label="Phone 2"  value={this.props.school.phone2} onChange={this.props.onChange} error={this.props.errors.phone2}/> 
            </div>
             <div class="col-md-4"> 
              <TextInput name="fax" label="Fax"  value={this.props.school.fax} onChange={this.props.onChange} error={this.props.errors.fax}/> 
            </div>
            </div>
            
        </div>
      </div>
      {/* Contact details form start*/}
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Contact Details</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">              
                <TextInput name="contactFirstName" label="First Name"  value={this.props.school.contacts[0].firstName} onChange={this.props.onChange} error={this.props.errors.contactFirstName}/>         
                <SelectInput name="relationship" label="RelationShip" value={this.props.school.contacts[0].relationship} defaultOption="Select RelationShip" options={contactOptions} onChange={this.props.onChange} error={this.props.errors.relationship}/>
                <TextInput name="contactFax" label="Fax"  value={this.props.school.contacts[0].fax} onChange={this.props.onChange} error={this.props.errors.contactFax}/>                 
            </div>
            <div class="col-md-4">
              <TextInput name="contactMiddleName" label="Middle Name"  value={this.props.school.contacts[0].middleName} onChange={this.props.onChange} error={this.props.errors.contactMiddleName}/>              
              <PhoneInput name="contactHomePhone" label="Home Phone"  value={this.props.school.contacts[0].phone} onChange={this.props.onChange} error= {this.props.errors.contactHomePhone}/>  
              <TextInput name="contactEmail" label="Email"  value={this.props.school.contacts[0].email} onChange={this.props.onChange} error={this.props.errors.contactEmail}/>         
            </div>
             <div class="col-md-4">
              <TextInput name="contactLastName" label="Last Name"  value={this.props.school.contacts[0].lastName} onChange={this.props.onChange} error={this.props.errors.contactLastname}/>              
              <PhoneInput name="contactMobile" label="Mobile Phone"  value={this.props.school.contacts[0].mobile} onChange={this.props.onChange} error={this.props.errors.contactMobile}/>          
            </div>
          </div>
        </div>
      </div>
    {/* Address details form start*/}
       <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Address Details</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">              
                  <TextInput name="city" label="City"  value={this.props.school.address.city} onChange={this.props.onChange} error={this.props.errors.city}/>   
                  <TextInput name="postal" label="PinCode"  value={this.props.school.address.postal} onChange={this.props.onChange} error={this.props.errors.postal}/>                 
              </div>
              <div class="col-md-4">
                <TextInput name="building" label="Building/House No"  value={this.props.school.address.building} onChange={this.props.onChange} error={this.props.errors.building}/>              
                <TextInput name="state" label="State"  value={this.props.school.address.state} onChange={this.props.onChange} error={this.props.errors.state}/>    
                     
              </div>
              <div class="col-md-4">
                <TextInput name="street" label="Street Name"  value={this.props.school.address.street} onChange={this.props.onChange} error={this.props.errors.street}/>              
                <TextInput name="country" label="Country"  value={this.props.school.address.country} onChange={this.props.onChange} error={this.props.errors.country}/>
              </div>
            </div>
          </div>
        </div>

      {/* Button for Registration */}
        
          <div class="row">
            <div class="col-md-12">              
                <input type="submit" disabled={this.props.saving} value={this.props.saving ? 'Saving...' : 'Save'} class="btn btn-info pull-right"/>                     
            </div>      
          </div>     
     </form>
     
  );

};
}


SchoolForm.propTypes = {
  school: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default SchoolForm;


