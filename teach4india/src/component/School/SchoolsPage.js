import React, { Component } from 'react';
import SchoolList from './SchoolList'
import * as schoolAction from '../../actions/schoolActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import queryString from 'query-string'
import { Link } from 'react-router-dom'
import {Admin} from '../../Model/Admin'


class SchoolsPage extends Component {

 constructor(props, context) {
    super(props, context);
  }

 render() {
    let {schools,currentAcademicYear} = this.props;     
    return (               
      <section class="content ">
        <h2>Schools</h2>
          <div class="row">
            <div class="col-md-13">
              <div class="box-body">
                <SchoolList schools={schools} currentAcademicYear ={currentAcademicYear} />
              </div>
            </div>
          </div>
        <div class="row ">
          <div class="col-md-2 pull-right">
            <div class="box-body">
             <Link to ="/schoolRegistration"><button type="button" class="btn btn-block btn-primary" >Add</button></Link>
            </div>
          </div>
        </div>            
      </section>        
    );
  }
}

function addSchool(){
  console.log('add school')
  window.location.replace("/newSchool");
}

function mapStateToProps(state, ownProps) {
  console.log("state", state.currentAcademicYear);
  let currentAcademicYear = state.currentAcademicYear || {}

  return {
    currentAcademicYear: currentAcademicYear,
    schools: currentAcademicYear.Schools
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(schoolAction, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SchoolsPage);
