import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { Link } from 'react-router-dom'
import * as Utils  from '../common/common-function';
 
function dateFormatter(cell, row) {
 let dateObj= new Date(`${cell}`);
  return dateObj.toLocaleDateString("en-US")
}

function actionFormatter(cell,row) {
  return  <div>
            <i className="fa fa-edit pull-middle"><Link to={'/schoolRegistration/'+`${cell}`}>Edit</Link></i>  
            <span> </span>
            <i className="fa fa-bank pull-middle"><Link to={'/schoolDetail/'+`${cell}`}>View</Link></i>
            <span> </span>
           </div>  
}
function _setTableOption(){   
    return(     
        <i class="fa fa-refresh fa-spin"></i>
    )
}

class SchoolList extends Component {
  render(){
    const {schools, academicYear} = this.props; 
    const options = { 
      noDataText: _setTableOption()
    }    
    let flattenSchools =[];    
    for(let index in schools)
    {
      flattenSchools.push(Utils.flatten(schools[index])); 
    }
   
   return (
      <div class="box box-default">
      <div class="box-header with-border">
        <BootstrapTable data={flattenSchools} striped hover search pagination bordered options={options} >
          <TableHeaderColumn width='30px' isKey dataField='id' className='bg-blue' dataSort hidden>AcademicYear ID</TableHeaderColumn>
          <TableHeaderColumn width='180px' dataField='id' className='bg-blue' dataFormat={actionFormatter }>Action</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='name'className='bg-blue' dataSort>Name</TableHeaderColumn>
          <TableHeaderColumn width='120px' dataField='phone1'className='bg-blue' dataSort>Phone</TableHeaderColumn>
          <TableHeaderColumn width='250px' dataField='email'className='bg-blue' dataSort>Email</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='address.city'className='bg-blue' dataSort>City</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='address.country'className='bg-blue' dataSort>Country</TableHeaderColumn>
         
          
        </BootstrapTable>
      </div>
    </div>
   

    );
  }
}
export default SchoolList;
