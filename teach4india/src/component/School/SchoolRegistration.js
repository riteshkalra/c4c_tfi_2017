import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'
import toastr from 'toastr';
import PropTypes from 'prop-types';
import SchoolForm from './SchoolForm'
import * as schoolActions from '../../actions/schoolActions';
import {School} from '../../Model/School'
import {Admin} from '../../Model/Admin'
import {Address} from '../../Model/Address'
import {Contact} from '../../Model/Contact'
import * as Utils  from '../common/common-function';
import * as Validation  from '../common/form-validation';


class SchoolRegistration extends Component { 
  constructor(props, context) {
    super(props, context);

    console.log('AYs', props.academicYears)

    this.state = {
      school: Object.assign({}, props.school),      
      errors: {},
      saving: false
    };

    this.onSaveSchool = this.onSaveSchool.bind(this); 
    this.onChangeComponent = this.onChangeComponent.bind(this);      
  }

  onSaveSchool(event) {  
    event.preventDefault();
    let tempSchoolError = Validation.schoolRegistrationFormIsValid(this.state.school)
    let tempAddressError = Validation.addressIsValid(this.state.school)
    let tempContactError = Validation.contactIsValid(this.state.school)
    let allErrors = Object.assign({}, tempSchoolError, tempAddressError, tempContactError);    

     // if (!Utils.isEmptyObject(allErrors)) {
     //   this.setState({errors: allErrors});     
     //   return;
     // }
     
    this.setState({saving: true})   
    let schoolObj = new School(
                        Admin.getDataSource(),
                        this.state.school.name, 
                        this.state.school.registrationDate, 
                        new Address(Admin.getDataSource(),
                        this.state.school.building,
                        this.state.school.street, 
                        this.state.school.city,
                        '', 
                        this.state.school.state,
                        this.state.school.country, 
                        this.state.school.postal),
                        null, 
                        this.state.school.phone1, 
                        this.state.school.phone2, 
                        this.state.school.fax, 
                        this.state.school.email                        
                        );
    schoolObj.addContact(new Contact( Admin.getDataSource(),
                                      this.state.school.contactFirstName,
                                      this.state.school.contactMiddleName, 
                                      this.state.school.contactLastName, 
                                      this.state.school.relationship, 
                                      this.state.school.contactPhone,
                                      this.state.school.contactMobile,
                                      this.state.school.contactFax,
                                      this.state.school.contactEmail, '', '', ));
    
    
    this.props.actions.saveSchool(schoolObj,this.props.currentAcademicYear).then(() => this.redirect())
      .catch(error => {
        
        toastr.error(error);
        this.setState({saving: false});
      });  
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('School saved');
    this.props.history.push('/schools');
  }

  onChangeComponent(event) {
    const field = event.target.name;
    let school = Object.assign({}, this.state.school);
    if(event.target.type==="date"){
        school[field] = event.target.valueAsDate;
     }
    else{
      school[field] = event.target.value;
    } 
   return this.setState({school: school});
  }

 render(){   
    return (               
	          <section class="content">
  	          <div class="row ">
                <div class="col-md-13">
                  <div class="box-body">
                  
                    <SchoolForm
                      onChange={this.onChangeComponent}                                         
                      onSave={this.onSaveSchool}
                      school={this.state.school}
                      errors={this.state.errors}
                      saving={this.state.saving}
                      academicYears={this.props.academicYears} 
                      currentAcademicYear={this.props.currentAcademicYear}
                      />
                  </div>
                </div>
              </div>
            </section>           
	         
    	);
 	}
 }

SchoolRegistration.contextTypes = {
  router: PropTypes.object
};
  
function mapStateToProps(state, ownProps) {
   let schoolId = ownProps.match.params.id   

   let address ={}
   let school = null;

   let currentAcademicYear = state.currentAcademicYear || {}
   let schools = currentAcademicYear && currentAcademicYear.Schools
   if (schoolId && schools) {
      school = Utils.getSchoolById(schools, schoolId);  
      if (!school.contacts) {
        school.contacts =[{}]
      }   
   } else {
    school = {}
    school.address ={}
    school.academicYear={}
    school.contacts =[{}]
   }
   

   let academicYearOptions =Utils.modelFormattedForDropdown(state.academicYears)
   return {   
       school: school,       
       academicYears:state.academicYears,
       currentAcademicYear:currentAcademicYear,    
     };  
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(schoolActions, dispatch)
   
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SchoolRegistration);


