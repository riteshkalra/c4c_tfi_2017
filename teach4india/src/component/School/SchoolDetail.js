import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import schoolImage from '../../../node_modules/admin-lte/dist/img/photo1.png';
import * as schoolActions from '../../actions/schoolActions';
import {School} from '../../Model/School'
import {Admin} from '../../Model/Admin'
import {Address} from '../../Model/Address'
import {Contact} from '../../Model/Contact'
import * as Utils  from '../common/common-function';
import studentImage from '../../avatar2.png';
import ClassList from '../ClassRoom/ClassList';
import StudentList from '../Student-Page/StudentList';
import TeacherList from '../Teacher/TeacherList';

class SchoolDetail extends Component {  

  constructor(props, context) {
    super(props, context);
  }
  
  render() {
     return (
      <section class="content">
        <div class="row box-body">       
             
          <div class="col-md-3">
            <div class="box box-primary">
              <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src={schoolImage}></img>
                <h3 class="profile-username text-center">{this.props.school && this.props.school.name}</h3>
                <p class="text-muted text-center">{this.props.school.address && this.props.school.address.city}</p>
                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Class</b> <a class="pull-right">{this.props.school.classes && this.props.school.classes.length}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Students</b> <a class="pull-right">2</a>
                  </li>
                  <li class="list-group-item">
                    <b>Teachers</b> <a class="pull-right">2</a>
                  </li>
                </ul>
              </div>
            </div>                
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">About {this.props.school && this.props.school.name}</h3>
              </div>
              <div class="box-body">
                <strong><i class="fa fa-phone margin-r-5"></i> Phone</strong>
                <p class="text-muted">
                   {this.props.school && this.props.school.phone1}
                </p>
                <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                <p class="text-muted">
                   {this.props.school && this.props.school.email}
                </p>               
                <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>
                <p class="text-muted">{this.props.school.address && this.props.school.address.city}, {this.props.school.address && this.props.school.address.country}</p>
                <strong><i class="fa fa-user margin-r-5"></i> Contacts</strong>
                <p class="text-muted">{this.props.school.contacts && this.props.school.contacts[0].firstName}, {this.props.school.contacts && this.props.school.contacts[0].lastName}</p>
              </div>
            </div>
          </div>          
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class ="active"><a href="#classrooms" data-toggle="tab">Classes</a></li>
              <li><a href="#teachers" data-toggle="tab">Teachers</a></li>
              <li><a href="#students" data-toggle="tab">Students</a></li>
            </ul>
            <div class="tab-content">
            <div class="active tab-pane" id="classrooms">  
                <div class ="post">          
                  <ClassList classes ={this.props.school.Classes}/>
                </div>
              </div>
              <div class="tab-pane"  id="teachers">
              <TeacherList teachers ={this.props.school.Teachers} />
              <div class ="post">
              </div> 
              </div>
              <div class="tab-pane" id="students" >
              <StudentList students ={this.props.school.Students} />
              <div class ="post">
        
              </div>
              </div>
            </div>
          </div> 
        </div> 
      </div> 
    </section> 

    );
  }
}

function getSchoolById(schools, id) {
  const school = schools.filter(school => school.id == id);
  if (school) return school[0]; //since filter returns an array, have to grab the first.
  return null;
}

function mapStateToProps(state, ownProps) {
   let schoolId = ownProps.match.params.id 
   let school = null;
   let currentAcademicYear = state.currentAcademicYear || {}
   let schools = currentAcademicYear && currentAcademicYear.Schools
   if (schoolId && schools) {
      school = Utils.getSchoolById(schools, schoolId);            
   } else {
    school = []
   }
   

   let academicYearOptions =Utils.modelFormattedForDropdown(state.academicYears)
   return {   
       school: school,       
       academicYears:state.academicYears,
       currentAcademicYear:currentAcademicYear      
     };  
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(schoolActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SchoolDetail);
