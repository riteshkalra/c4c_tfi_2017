import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import * as questionActions from '../../actions/questionActions';

class QuestionListRow extends Component {
  render(){
    const question = this.props.question;
    const select = this.props.select;
    return (
      <tr>     
        <td>{question.description}</td>
        <td>{question.concept}</td>
        <td>{question.type}</td>
        <td>{question.subject}</td>
        <td>{select ? <button type="button" onClick={() => {this.props.onAddQuestion(question)}} class="btn btn-block btn-success btn-xs">Add</button> : <button type="button" onClick={() => {this.props.onRemoveQuestion(question)}} class="btn btn-block btn-danger btn-xs">X</button> }</td> 
      </tr>
    );
  };
}

export default QuestionListRow;
