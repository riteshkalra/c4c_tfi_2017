import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'
import toastr from 'toastr';
import PropTypes from 'prop-types';
import ExamForm from './ExamForm'
import * as examActions from '../../actions/examActions';

import {Question} from '../../Model/Question'

class CreateExam extends Component { 
  constructor(props, context) {
    super(props, context);
    this.state = {
      exam: Object.assign({}, props.exam),
      questions: props.questions,
      showAddNewQuestion: false,
      errors: {},
      saving: false
    };

    this.onSaveExam = this.onSaveExam.bind(this);
    this.onAddQuestion = this.onAddQuestion.bind(this);
    this.onRemoveQuestion = this.onRemoveQuestion.bind(this);
    this.onAddNewQuestion = this.onAddNewQuestion.bind(this);
    this.onChangeComponent = this.onChangeComponent.bind(this);
  }

  onSaveExam(event) {
    this.setState({saving: true})    
    this.props.actions.saveExam(this.state.exam)
    this.redirect();
    event.preventDefault();
  }

  onAddNewQuestion(status) {
    console.log("onAddNewQuestion", this.state.onAddNewQuestion);
    this.setState({showAddNewQuestion: status})
  }

  onAddQuestion(question) {
    // add the question to the exam.questions
    let q1s = this.state.exam.questions;
    let present = false;
    for(let index in q1s) {
      let ques = q1s[index];
      if(ques.id == question.id) {
        present = true;
        break;
      }
    }
    if(!present) {
      q1s.push(question);
    }
    let exam = this.state.exam;
    exam.questions = q1s;

    // remove the question from questions
    let q2s = this.state.questions;
    let rqs = []
    for(let index in q2s) {
      let ques = q2s[index];
      if(ques.id != question.id) {
        rqs.push(ques);
      }
    }
    
    this.setState( {exam: exam, questions: rqs});
  }

  onRemoveQuestion(question) {
    // add the question to the questions
    let q1s = this.state.questions;
    let present = false;
    for(let index in q1s) {
      let ques = q1s[index];
      if(ques.id == question.id) {
        present = true;
        break;
      }
    }
    if(!present) {
      q1s.push(question);
    }

    // remove the question from exam.questions
    let q2s = this.state.exam.questions;
    let rqs = []
    for(let index in q2s) {
      let ques = q2s[index];
      if(ques.id != question.id) {
        rqs.push(ques);
      }
    }
    let exam = this.state.exam;
    exam.questions = rqs;
    
    this.setState( {exam: exam, questions: q1s}); 
  }

  onChangeComponent(event) {
    const field = event.target.name;
    console.log("event=", field, exam);
    let exam = Object.assign({}, this.state.exam);
    console.log("event exam=", field, exam);
    if(event.target.type==="date")
    {
      exam[field] = event.target.valueAsDate;
    }
    else{
      exam[field] = event.target.value;
    }
 
   return this.setState({exam: exam});

  }

  onDateChange(event) {
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('Exam saved');
    this.props.history.push('/ExamDetails');
  }

  render(){
    return (               
	     <section className="content">
        <ExamForm
          onChange={this.onChangeComponent}                      
          onSave={this.onSaveExam}
          onAddQuestion={this.onAddQuestion}
          onRemoveQuestion={this.onRemoveQuestion}
          showAddNewQuestion={this.state.showAddNewQuestion}
          onAddNewQuestion={this.onAddNewQuestion}
          newQuestion={this.props.ques}
          exam={this.state.exam}
          questions={this.state.questions}
          errors={this.state.errors}
          saving={this.state.saving}
          />
      </section>
    	);
 	}
 }

CreateExam.contextTypes = {
  router: PropTypes.object
};
  
function mapStateToProps(state, ownProps) {
   let questions = state.questions;
   let exam = state.exams[0];
  // exam.questions =questions

   let subjects = state.subjects;
   return {   
       exam: exam,
       questions: questions,
       subjects
     };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(examActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateExam);


