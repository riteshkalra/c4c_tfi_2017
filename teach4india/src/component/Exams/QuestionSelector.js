import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import QuestionCreation from '../Question/QuestionCreation'
import QuestionList from './QuestionList'

class QuestionSelector extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let onAddNewQuestion = this.props.onAddNewQuestion;
    let onRemoveQuestion = this.props.onRemoveQuestion;
    let onAddQuestion = this.props.onAddQuestion;
    let exam = this.props.exam;
    let showAddNewQuestion = this.props.showAddNewQuestion;
    let newQuestion = this.props.newQuestion;
    let questions = this.props.questions;

    console.log("Question Selector", showAddNewQuestion);
    return (
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">
          { showAddNewQuestion ? "Add Question" : `All ${exam.subject} Questions` }
          </h3>
          <div className="box-tools pull-right">
            <button type="button" className="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i className="fa fa-minus"></i></button>
            <button type="button" className="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
              <i className="fa fa-times"></i></button>
          </div>
        </div>
        <div className="box-body">
          { showAddNewQuestion ?
            <QuestionCreation question={newQuestion} />
          : <QuestionList questions={questions} select={true} onAddQuestion={onAddQuestion} onRemoveQuestion={onRemoveQuestion}/> }
        </div>
        <div className="box-footer">
          { showAddNewQuestion ?
            <button type="button" className="btn btn-default pull-left" onClick={() => { onAddNewQuestion(false)} }>Back</button>
          : <button type="button" className="btn btn-default pull-left" onClick={() => { onAddNewQuestion(true)} }>Add Question</button> }
        </div>
      </div>
    );
  };
}

export default QuestionSelector;
