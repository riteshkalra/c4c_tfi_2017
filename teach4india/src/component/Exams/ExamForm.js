import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import DatePickerInput from '../common/DatePickerInput';
import PropTypes from 'prop-types';
import QuestionSelector from './QuestionSelector'
import QuestionList from './QuestionList'

const ExamForm = ({exam, questions, onAddQuestion, onRemoveQuestion, showAddNewQuestion, onAddNewQuestion, newQuestion, onSave, onChange, saving, errors}) => {
  
  return (
    <div>
      <form>
        <h2>Create New Exam</h2><br/>
        <div className="box box-default">
          
          <div className="box-header with-border">
            <h3 className="box-title">Exam Form</h3>
            <div className="box-tools pull-right">
              <button type="button" className="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i className="fa fa-minus"></i></button>
              <button type="button" className="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                <i className="fa fa-times"></i></button>
            </div>
          </div>

          <div className="box-body">
            <div className="row">
              <div className="col-md-4">              
                <TextInput
                  name="name"
                  label="Name"
                  value={exam.name}
                  onChange={onChange}
                  error={errors.name}/>
                <DatePickerInput
                  name="date"
                  label="Date"
                  value={exam.date}
                  onChange={onChange}
                  error={errors.date}/> 
              </div>
              <div className="col-md-4">
                <TextInput
                  name="passMarks"
                  label="Pass Marks"
                  value={exam.passMarks}
                  onChange={onChange}
                  error={errors.passMarks}/>
                <TextInput
                  name="passGrades"
                  label="Pass Grades"
                  value={exam.passGrades}
                  onChange={onChange}
                  error={errors.passGrades}/>
              </div>
              <div className="col-md-4">
                <TextInput
                  name="cls"
                  label="Class"
                  value={exam.cls}
                  onChange={onChange}
                  error={errors.cls}/>
                <TextInput
                  name="subject"
                  label="Subject"
                  value={exam.subject}
                  onChange={onChange}
                  error={errors.subject}/>              
              </div>
              <div className="col-md-12">
                <label>Exam Questions</label>
                {exam.questions && exam.questions.length === 0 ? <div>No Questions added. Use the list below to add questions to the exam.</div> : 
                  <QuestionList questions={exam.questions} select={false} onAddQuestion={onAddQuestion} onRemoveQuestion={onRemoveQuestion}/>
                }
              </div>   
            </div>
          </div>
        </div>
        
        <div className="row">
          <div className="col-md-12">              
             <input
              type="submit"
              disabled={saving}
              value={saving ? 'Saving...' : 'Add'}
              className="btn btn-info pull-right"
              onClick={onSave}/>
          </div>      
        </div>  
      </form>
      <br />
      <div>
        <QuestionSelector 
          exam={exam}
          questions={questions}
          onAddQuestion={onAddQuestion}
          onRemoveQuestion={onRemoveQuestion}
          showAddNewQuestion={showAddNewQuestion}
          onAddNewQuestion={onAddNewQuestion}
          newQuestion={newQuestion}
          onSave={onSave}
          onChange={onChange}
          saving={saving}
          errors={errors}
        />
        </div>
    </div>
  );
};

ExamForm.propTypes = {
  exam: PropTypes.object,
  onSave: PropTypes.func,
  onChange: PropTypes.func,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default ExamForm;


