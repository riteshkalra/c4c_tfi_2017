import React, { Component } from 'react';
import QuestionListRow from './QuestionListRow';

class QuestionList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      questions: Object.assign({}, props.questions),
    };
  }

  render(){
    const questions = this.props.questions;
    const select = this.props.select;
    return (
      <div>
        <table id="example3" className="table table-bordered table-striped"> 
          <thead className ="bg-blue">
            <tr>
              <th>Description</th>
              <th>Concept</th> 
              <th>Type</th> 
              <th>Subject</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {questions.map(question =>
              <QuestionListRow key={question.id} question={question} select={select} onAddQuestion={this.props.onAddQuestion} onRemoveQuestion={this.props.onRemoveQuestion}/>
            )}
          </tbody>
        </table> 
      </div>
    );
  }
}
export default QuestionList;