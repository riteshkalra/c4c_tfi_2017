import React, { Component } from 'react';
import JvectorMap from 'jvectormap-next'


const IndiaMap =({}) => {  

    return (

              <div style={{width: 500, height: 500}}>
                <JvectorMap map={'in_mill'}
                           backgroundColor="#3b96ce"
                           ref="map"
                           containerStyle={{
                               width: '100%',
                               height: '100%'
                           }}
                           containerClassName="map"
                />
</div>
              );
 };
export default IndiaMap;
              
             