import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {Map} from 'react-d3-map'
import {MarkerGroup} from 'react-d3-map'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


const MapComponent =({}) => {  

var data = {
    "type": "Feature",
    "properties": {
      "text": "this is a Point!!!"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [122, 23.5]
    }
} 
    return (

            <div class="box-body no-padding">
               <Map
                width="700"
                height= "700"
                scale= "6000"
                scaleExtent= " [1 << 12, 1 << 13]"
                center=  "[122, 23.5]"
               >
              <MarkerGroup
                key= {"polygon-test"}
                data= {data}               
               
              />
           </Map>
            </div>
              );
 };
export default MapComponent;
              
             