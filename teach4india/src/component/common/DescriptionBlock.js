import React, { Component } from 'react';
import PropTypes from 'prop-types'

const DescriptionBlock =({classStyle,style,imageClass,headerValue,headerText}) => {   
		return (
			<div class={classStyle}>
	          <span class={style}><i class={imageClass}></i></span>
	          <h5 class="description-header">{headerValue}</h5>
	          <span class="description-text">{headerText}</span>
	        </div>
    );
};
export default DescriptionBlock;