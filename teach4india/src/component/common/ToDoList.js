
import React, { Component } from 'react';



class ToDoList extends Component {
  render() {
    let questionNumber = 0;
    return (


      <div class="box box-primary">
        <div class="box-header">
          <i class="ion ion-clipboard"></i>
          <h3 class="box-title">Holiday Calendar</h3>
          <div class="box-tools pull-right">
            <ul class="pagination pagination-sm inline">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>              
              <li><a href="#">&raquo;</a></li>
            </ul>
          </div>
        </div>
        <div class="box-body">
          <ul class="todo-list">
            <li>
              <span class="handle">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
              </span>
              <span class="text">Labour Day</span>
              <small class="label label-danger"><i class="fa fa-clock-o"></i> 01-05-2018</small>
              <div class="tools">
                <i class="fa fa-edit"></i>
                <i class="fa fa-trash-o"></i>
              </div>
            </li>
            <li>
              <span class="handle">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
              </span>
              <span class="text">Independence Day</span>
              <small class="label label-danger"><i class="fa fa-clock-o"></i> 15-08-2018</small>
              <div class="tools">
                <i class="fa fa-edit"></i>
                <i class="fa fa-trash-o"></i>
              </div>
            </li>
            <li>
              <span class="handle">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
              </span>
              <span class="text">Gandhi Jayanti</span>
              <small class="label label-danger"><i class="fa fa-clock-o"></i> 02-10-2018</small>
              <div class="tools">
                <i class="fa fa-edit"></i>
                <i class="fa fa-trash-o"></i>
              </div>
            </li>


          </ul>
        </div>
      </div>

    );
  }
};

export default ToDoList;
