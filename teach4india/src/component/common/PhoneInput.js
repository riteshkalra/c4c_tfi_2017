import React from 'react';
import PropTypes from 'prop-types';


const PhoneInput = ({name, label, onChange,  value, error}) => {
  //       

  let wrapperClass = 'input-group';
  if (error && error.length > 0) {
    wrapperClass += " " + 'has-error';
  }
  
 
  
 return (
   <div className="form-group">
    <label htmlFor={name}>{label}</label>
     <div className={wrapperClass}>
      <div class="input-group-addon">
        <i class="fa fa-phone"></i>
      </div>
      <div className="field">
        <input
          type="text"
          name={name}
          className="form-control"          
          value={value}
          onChange={onChange}
          data-inputmask='"mask": "(999) 999-9999"' data-mask/>       
      </div>
       {error && <div className="alert alert-danger">{error}</div>}
    </div>
   </div>
  );
};

 


PhoneInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.date,
  error: PropTypes.string
};

export default PhoneInput;
