export function schoolRegistrationFormIsValid(school) {
    let errors = {};
    if (!school) {
        errors.generic = 'Please fill the form first.';
        return errors;
    }

    if (!school.academicYear || school.academicYear.length < 1) {
        errors.academicYear = 'Academic Year must not be blank.';
    }

    if (!school.name || school.name.length < 1) {
        errors.name = 'School Name must not be blank.';
    }

    if (!school.email || school.email.length < 1) {
        errors.email = 'School Email Id must not be blank.';
    }


    if (school.registrationDate == null) {
        errors.registrationDate = 'School Registration date Id must not be blank.';
    }


    return errors;
}

export function teacheRegistrationDataIsValid(entity) {
    let errors = {};
    if (!entity) {
        errors.generic = 'Please fill the form first.';
        return errors;
    }

    if (!entity.academicYear || entity.academicYear.length < 1) {
        errors.academicYear = 'Academic Year must not be blank.';
    }
    if (entity.registrationDate == null) {
        errors.registrationDate = 'Registration date must not be blank.';
    }
    if (!entity.school || entity.school.length < 1) {
        errors.school = 'School must not be blank.';
    }
    if (!entity.class || entity.class.length < 1) {
        errors.class = 'Class must not be blank.';
    }
    return errors;
}

export function classRoomRegistrationDataIsValid(entity) {
    let errors = {};
    if (!entity) {
        errors.generic = 'Please fill the form first.';
        return errors;
    }

    if (!entity.school || entity.school.length < 1) {
        errors.school = 'School must not be blank.';
    }

    if (!entity.code || entity.code.length < 1) {
        errors.code = 'Code must not be blank.';
    }

    if (!entity.grade || entity.grade.length < 1) {
        errors.grade = 'Code must not be blank.';
    }

    if (!entity.name || entity.name.length < 1) {
        errors.name = 'Name must not be blank.';
    }

    if (!entity.section || entity.section.length < 1) {
        errors.section = 'Section must not be blank.';
    }

    if (!entity.teachingMedium || entity.teachingMedium.length < 1) {
        errors.teachingMedium = 'Teaching Medium must not be blank.';
    }

    if (!entity.adminTeacher || entity.adminTeacher.length < 1) {
        errors.adminTeacher = 'Admin Teacher must not be blank.';
    }

    return errors;
}

export function addressIsValid(entity) {
    let errors = {};
    if (!entity) {
        errors.generic = 'Please fill the address form first.';
        return errors;
    }

    if (!entity.street || entity.street.length < 1) {
        errors.street = 'Street address must not be blank.';
    }

    if (!entity.city || entity.city.length < 1) {
        errors.city = 'City Name must not be blank.';
    }

    if (!entity.building || entity.building.length < 1) {
        errors.building = 'Building/House must not be blank.';
    }

    if (!entity.country || entity.country.length < 1) {
        errors.country = 'Country must not be blank.';
    }
    if (!entity.postal || entity.postal.length < 1) {
        errors.postal = 'Postal code must not be blank.';
    }

    if (!entity.state || entity.state.length < 1) {
        errors.state = 'State must not be blank.';
    }
    return errors;
}

export function personalDetailIsValid(entity) {
    let errors = {};
    if (!entity) {
        errors.generic = 'Please fill the perosnal info form first.';
        return errors;
    }

    if (!entity.firstName || entity.firstName.length < 1) {
        errors.firstName = 'First Name must not be blank.';
    }

    if (!entity.lastName || entity.lastName.length < 1) {
        errors.lastName = 'Last Name must not be blank.';
    }

    if (!entity.phone1 || entity.phone1.length < 1) {
        errors.phone1 = 'Mobile must not be blank.';
    }
    if (!entity.email || entity.email.length < 1) {
        errors.email = ' Email not be blank.';
    }

    if (!entity.gender || entity.gender.length < 1) {
        errors.gender = ' Gender must not be blank.';
    }
    if (!entity.dob) {
        errors.dob = ' Date of Birth must not be blank.';
    }
    //put validation for gender and DOB
    return errors;
}

export function contactIsValid(entity) {
    let errors = {};
    if (!entity) {
        errors.generic = 'Please fill the address form first.';
        return errors;
    }

    if (!entity.contactFirstName || entity.contactFirstName.length < 1) {
        errors.contactFirstName = 'Contact First Name must not be blank.';
    }

    if (!entity.relationship || entity.relationship.length < 1) {
        errors.relationship = 'Contact designation must not be blank.';
    }


    if (!entity.contactLastName || entity.contactLastName.length < 1) {
        errors.contactLastName = 'Contact Last Name must not be blank.';
    }

    if (!entity.contactMobile || entity.contactMobile.length < 1) {
        errors.contactMobile = 'Contact Mobile must not be blank.';
    }


    if (!entity.contactEmail || entity.contactEmail.length < 1) {
        errors.contactEmail = 'Contact Email not be blank.';
    }
    return errors;
}