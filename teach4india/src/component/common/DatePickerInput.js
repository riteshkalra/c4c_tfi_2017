import React from 'react';
import PropTypes from 'prop-types';


const DatePickerInput = ({name, label, onChange,  value, error}) => {
  //       

  let wrapperClass = 'input-group';
  if (error && error.length > 0) {
    wrapperClass += " " + 'has-error';
  }
  
  let dateValue = value;
  if (dateValue instanceof Date) {
    // convert to ISO date 2018-01-31
    dateValue = dateValue.toISOString().substring(0,10);
  }
  
 return (
   <div className="form-group">
    <label htmlFor={name}>{label}</label>
     <div className={wrapperClass}>
      <div className="input-group-addon">
        <i className="fa fa-calendar"></i>
      </div>
      <div className="field">
        <input
          type="date"
          name={name}
          className="form-control"          
          value={dateValue}
          onChange={onChange}/>
        {error && <div className="alert alert-danger">{error}</div>}
      </div>
    </div>
   </div>
  );
};

 


DatePickerInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.date,
  error: PropTypes.string
};

export default DatePickerInput;
