import React, { Component } from 'react';
import PropTypes from 'prop-types';



const InfoBox =({name,length,style ,imageClass}) => {   
   
        return (
          
             <div class="info-box">  
              <span class={style}><i class={imageClass}></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">{name}</span>
                  <span class="info-box-number">{length}</span>
                </div>             
            </div>
           

        );
      }


InfoBox.propTypes = {
  name: PropTypes.string.isRequired,
  length: PropTypes.string.isRequired,
  style:PropTypes.string.isRequired,
  imageClass:PropTypes.string.isRequired,
 
};

export default InfoBox;


