export function emptyDropDown()
{
      return [];
}

export function modelFormattedForDropdown(modelObjs) {  
    if (isEmptyObject(modelObjs))  
    {
        return emptyDropDown();
    }
    return modelObjs.map(modelObj => {
      return {
        value: modelObj.id,
        text: modelObj.name
      };
    });
    }

export function getTeacherRoleOptions()
{
    let roleOptions =[]
    roleOptions.push( {value:"Subject", text:"Subject Teacher"})
    roleOptions.push( {value:"Admin", text:"Admin"}) 
    return roleOptions;  
}

export function getGenderOptions()
{
  let genderOptions =[]
  genderOptions.push( {value:"Male", text:"Male"})
  genderOptions.push( {value:"Female", text:"Female"}) 
  return genderOptions;

}

export function getSchoolRelationShipOption()
{
  let contactOptions =[]
  contactOptions.push( {value:"Principal", text:"Principal"})
  contactOptions.push( {value:"Administrator", text:"Administrator"}) 
  return contactOptions;

}

export function getContactRelationShipOption()
{
  let contactOptions =[]
  contactOptions.push( {value:"Father", text:"Father"})
  contactOptions.push( {value:"Mother", text:"Mother"}) 
  contactOptions.push( {value:"Guardian", text:"Guardian"}) 
  return contactOptions;

}

export function getTeachingMediumOptions()
{
    let roleOptions =[]
    roleOptions.push( {value:"English", text:"English"})
    roleOptions.push( {value:"Hindi", text:"Hindi"}) 
    roleOptions.push( {value:"Marathi", text:"Marathi"}) 
    return roleOptions;  
}

export function getClassSectionOptions()
{
    let roleOptions =[]
    roleOptions.push( {value:"A", text:"A"})
    roleOptions.push( {value:"B", text:"B"}) 
    roleOptions.push( {value:"C", text:"C"}) 
    return roleOptions;  
}


export function isEmptyObject(obj) {
  for(var prop in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
      return false;
    }
  }
  return true;
}

export function getTeacherById(teachers, id) {
  const teacher = teachers.filter(teacher => teacher.id == id);
  if (teacher) return teacher[0]; //since filter returns an array, have to grab the first.
  return null;
}


export function getStudentById(students, id) {
  const student = students.filter(student => student.id == id);
  if (student) return student[0]; //since filter returns an array, have to grab the first.
  return null;
}

export function getSchoolById(schools, id) {
  const school = schools.filter(school => school.id == id);
  if (school) return school[0]; //since filter returns an array, have to grab the first.
  return null;
}

export function getAcademicYearById(academicYears, id) {
  const academicYear = academicYears.filter(academicYear => academicYear.id == id);
  if (academicYear) return academicYear[0]; //since filter returns an array, have to grab the first.
  return null;
}

export function getClassById(classes, id) {  
  const classObj = classes.filter(classObj => classObj.id == id);
  if (classObj) return classObj[0]; //since filter returns an array, have to grab the first.
  return null;
}

export  function flatten (data) {
    const result = {};

    function recurse(cur, prop) {
        if (Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
            let l = cur.length;
            for (let i = 0 ; i < l; i++)
            recurse(cur[i], prop + "." + i);
            if (l === 0) result[prop] = [];
        } else {
            let isEmpty = true;
            for (let p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop + "." + p : p);
            }
            if (isEmpty && prop) result[prop] = {};
        }
    }
    recurse(data, "");
    return result;
};