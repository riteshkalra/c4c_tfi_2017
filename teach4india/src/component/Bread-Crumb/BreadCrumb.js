import React, { Component } from 'react';
import queryString from 'query-string'
import {Admin} from '../../Model/Admin'

class BreadCrumb extends Component {  

  componentWillMount() {
      let academicYearName = "Academic Year";
      let schoolName = "School";
      let className = "Class";
      let studentName = "Students";
      
      this.setState({academicYearName: academicYearName, 
                    schoolName: schoolName,
                    className: className,
                    studentName: studentName
                    });

      let queryParams = queryString.parse(this.props.location.search, { ignoreQueryPrefix: true });
      
      if (queryParams.academicYearId != null) {
            Admin.getAcademicYear(queryParams.academicYearId).then(academicYear => {
                if (academicYear != null) {
                    console.log(academicYear);
                    this.setState({academicYearName: academicYear.Name});
                }
            });
        }
        
      if (queryParams.schoolId != null) {
            Admin.getSchool(queryParams.schoolId).then(school => {
                if (school != null) {
                    this.setState({schoolName: school.Name});
                }
            });
        }
        
      if (queryParams.classId != null) {
            Admin.getClass(queryParams.classId).then(cls => {
                if (cls != null) {
                    this.setState({className: cls.Name});
                }
            });
        }

  }
    
  render() {
  
    
    return (        
          
            <section className="content-header">
              <ol className="breadcrumb">
                  <li><a href="#"><i className="fa fa-dashboard"></i>{this.state.academicYearName}</a></li>
                  <li><a href="#">{this.state.schoolName}</a></li>
                  <li className="active">{this.state.className}</li>
                  <li className="active">{this.state.studentName}</li>
              </ol>
            </section>
     
      
     
    );
  }
}

export default BreadCrumb;



