import React, { Component } from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';


class MasterySelectionListRow extends Component {  
  render(){          
   
    let masteryOptions = {};
    let masteryLevels = this.props.question.masteryLevels;
    if(masteryLevels !=null)
      {
        for (var i = 0; i < masteryLevels.length; i++) {
            var mastery = masteryLevels[i];    
            masteryOptions[mastery.mastery] = mastery.mark;
      }
    }
    return (          
            <div class="row">
                               
              <div class="col-md-4">  
              <SelectInput 
                name={this.props.question.id}
                 label="Mastery"
                 value={masteryOptions[1]}
                 onChange={this.props.onChange}
                 options={masteryOptions}
                 error=""/>
                                       
              </div>
            </div>
          )
        }      
}

export default MasterySelectionListRow;