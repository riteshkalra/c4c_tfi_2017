import React, { Component } from 'react';
import StudentExamListRow from './StudentExamListRow';
import QuestionHeaderListRow from './QuestionHeaderListRow'

class StudentEvaluationList extends Component {
  render(){
    
    return (
       <table id="example1" class="table table-bordered table-striped">
        <thead class ="bg-blue">   
          <tr>
            <th>Roll No</th>
            <th>Name</th>
                 
              {this.props.examQuestions.map(question =>
                <QuestionHeaderListRow key={question.id} question={question}/>
              )}
          </tr>
        </thead>
         
        <tbody>
       
        </tbody>
      </table>
    );
  }
};

export default StudentEvaluationList;
