import React, { Component } from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import * as Utils  from '../common/common-function';


class QuestionListRow extends Component {  
  render(){          
   
   let masteryOptions =Utils.modelFormattedForDropdown(this.props.question.masteryLevels)    

    return (          
            <div class="row">
              <div class="col-md-8">   
                  <TextInput name="Question1" label={"Question :" + this.props.questionNumber} value={this.props.question.description}  disabled ="disabled" onChange={this.props.onChange} error=""/>                                 
              </div>                       
              <div class="col-md-4">  
              <SelectInput name={this.props.question.id}
               label="Grade" 
               value=''
               defaultOption="Select Grade"
               options={masteryOptions} onChange={this.props.onChange} error=""/>               
          
             
                                       
              </div>
            </div>
          )
        }      
}

export default QuestionListRow;