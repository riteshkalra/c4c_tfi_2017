import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ExamList from './ExamList';
import * as examEvaluationAction from '../../actions/examEvaluationAction';
import {FireStoreDataSource} from "../../DataSource/FireStoreDataSource";


class ExamDetails extends Component {

   constructor(props, context) {
        super(props, context);       
        this.onViewExam = this.onViewExam.bind(this);
    }

    onViewExam(exam){
      this.props.actions.viewSelectedExam(exam);      
      this.props.history.push('/ExamEvaluation/?id='+exam.id);
    }
     

    componentWillMount() {
      let dataSource = new FireStoreDataSource();
      this.props.actions.loadCompletedExams(dataSource);

    }
    render() {       
        return (
            <section class="content">
                <section class="content-header"/>
               <div class="col-md-12"> 
                  <div class="box">
                    <div class="box-body">                  
                        <ExamList exams={this.props.completedExams} onViewExam ={this.onViewExam}/>
                    </div>
                  </div>
                </div>
            </section>

        );
    }
}

function mapStateToProps(state) {
     return {
        completedExams: state.completedExams
    };


}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(examEvaluationAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamDetails);


