import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import ExamList from './ExamList';
import StudentSelector from './StudentSelector'
import ExamEvaluationForm from './ExamEvaluationForm'
import StudentEvaluationList from './StudentEvaluationList'
import * as examEvaluationAction from '../../actions/examEvaluationAction';
import {ExamResult} from "../../Model/ExamResult";
import {Answer} from "../../Model/Answer";
import {Exam} from "../../Model/Exam";
import {Admin} from "../../Model/Admin";
import queryString from 'query-string'

class ExamEvaluation extends Component {
    constructor(props, context) {
        super(props, context);
         this.state = {
            students:  Object.assign({}, props.students),
            selectedExam:  Object.assign({}, props.selectedExam),
            student: Object.assign({}, props.student),
            examQuestions:Object.assign({}, props.examQuestions),
         
        };
        this.onEvaluate = this.onEvaluate.bind(this);
        this.onSaveStudentResult = this.onSaveStudentResult.bind(this);
        this.onChangeComponent = this.onChangeComponent.bind(this);
      
    }


    onChangeComponent(event) {

        const field = event.target.name;      
        let answers = Object.assign([], this.state.answers);
        let answer = new Answer(Admin.FSDS,this.props.selectedExam.id,field,"" ,event.target.value)
        answers.push(answer)       
        return this.setState({answers: answers});

    }
     onSaveStudentResult(event) {       
       let examResult = new ExamResult(
            Admin.FSDS,
            this.props.selectedExam,
            this.state.student,
            null
        );
        for(let id in this.state.answers)
        {
          examResult.addAnswer(this.state.answers[id])
        }
        this.props.actions.saveStudentExamResult(examResult)
      

       // examResult.addAnswer(questionAnswser[0].question,questionAnswser[0].answer,questionAnswser[0].mastery)
      }

     
    onEvaluate(student) 
    {
      
      this.setState({student: student})    
    }


    componentWillMount() { 
               

        let queryParams = queryString.parse(this.props.location.search, { ignoreQueryPrefix: true }); 
        if(this.props.selectedExam == null)
        {
          //load the exa, as per queryParams
        }     
        this.props.actions.loadExamQuestions(this.props.selectedExam);
    }

    render() {  
      let examName ="";
      let className ="";
      if(this.props.selectedExam != null)
      {
        examName =this.props.selectedExam.name
      //  className =this.props.selectedExam.cls.name
      }
        return (
            <section class="content">           
            <section class="content-header"/>           
            <div class="row">
              <div class="col-md-13"> 
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title"> Pending Evaluation for {examName} for class  {className}  </h3>
                    </div>
                    <div class="box-body">               
                          <StudentSelector students ={this.props.students} onEvaluate={this.onEvaluate}/>
                    </div>
                  </div>
              </div>            
            </div>           
            <div class="row"> 
             <div class="col-md-13">                        
              <ExamEvaluationForm exam ={this.props.completedExam}
                                  examQuestions ={this.props.examQuestions}
                                  student ={this.state.student}
                                  onChange ={this.onChangeComponent}
                                  onSave ={this.onSaveStudentResult}
                                  actions={this.props.actions}/>  
              </div>                 
            </div>
             <div class="row">
              <div class="col-md-13"> 
                  <div class="box">
                  <div class="box-header">
                    <h3 class="box-title"> Completed Evaluation for {className} for class  {className}  </h3>
                  </div>

                    <div class="box-body">               
                        <StudentEvaluationList exam ={this.props.selectedExam} examQuestions ={this.props.examQuestions}/>
                    </div>
                  </div>
              </div>
            </div>
            </section>

        );
    }
}



function mapStateToProps(state, ownProps) {
    return {
        selectedExam:state.selectedExam,
        students:state.students,
        student:state.student,
        answers:state.answers,
        examQuestions:state.examQuestions
        
    };


}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(examEvaluationAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamEvaluation);


