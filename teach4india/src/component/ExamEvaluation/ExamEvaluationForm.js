
import React, { Component } from 'react';
import TextInput from '../common/TextInput';
import QuestionListRow from './QuestionListRow';


class ExamEvaluationForm extends Component {
  
  render(){
    let questionNumber =0;
    return (
      
       <form>
          <div class="box box-default">
          <div class="box-header with-border">   
          <h3 class="box-title">Student Evalution</h3>        
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
             
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">   
                  <TextInput name="firstName" label="First Name" value={this.props.student.firstName} disabled ="disabled" onChange={this.props.onChange} error=""/>                                 
              </div>
              <div class="col-md-4">  
                  <TextInput name="lastName" label="Last Name" value={this.props.student.lastName}  disabled ="disabled" onChange={this.props.onChange} error=""/>                                 
              </div>
              <div class="col-md-4">  
                  <TextInput name="RollNum" label="Roll Num" value={this.props.student.rollNo}  disabled ="disabled" onChange={this.props.onChange} error=""/>                                 
              </div>
            </div>

             {this.props.examQuestions.map(question =>
                                <QuestionListRow key={question.id} question={question} actions={this.props.actions}  onChange={this.props.onChange} questionNumber={questionNumber} />
              )}
             
        </div>
          <div class="row">
            <div class="col-md-12">   
             <div class="btn-group pull-right">                     
                <button type="button" class="btn btn-block btn-info btn-lg" onClick={this.props.onSave}><i class="fa fa-save"></i></button>
             
             </div>           
             
              
            </div>   
          </div>
      </div>
     </form>
     
    );
  }
};

export default ExamEvaluationForm;
