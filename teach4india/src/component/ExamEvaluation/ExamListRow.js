import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import * as questionActions from '../../actions/questionActions';

class ExamListRow extends Component {
  render(){
    const exam = this.props.exam;

     

    return (
      <tr>     
        <td>{exam.name}</td>
        <td>{exam.date.toDateString()}</td>
        <td></td>
        <td></td>
        <td><button type="button" onClick={() => {this.props.onViewExam(this.props.exam)}} class="btn btn-primary  btn-sm">Evaluate</button></td>
        
      </tr>
        
    );
  };
}

export default ExamListRow;