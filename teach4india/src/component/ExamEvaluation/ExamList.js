import React, { Component } from 'react';
import ExamListRow from './ExamListRow';
class ExamList extends Component {
  constructor(props, context) {
    super(props, context);
  }
 

  render(){
    const exams = this.props.exams;
 
    return (
      <div>
        <table id="example3" className="table table-bordered table-striped"> 
          <thead className ="bg-blue">
            <tr>
              <th>Name</th>
              <th>Date</th> 
              <th>Class</th>  
              <th>Subject</th>              
              <th>Evaluate</th>
            </tr>
          </thead>
          <tbody>
            {exams.map(exam =>
              <ExamListRow key={exam.id} exam={exam} onViewExam ={this.props.onViewExam}/>
            )}
          </tbody>
        </table> 
      </div>
    );
  }
}
export default ExamList;