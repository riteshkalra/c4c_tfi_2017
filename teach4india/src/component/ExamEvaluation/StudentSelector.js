import React, { Component } from 'react';
import StudentExamList from './StudentExamList';

class StudentSelector extends Component {
  constructor(props, context) {
    super(props, context);
    
  }

  render() {
  
       return (
       		<div>     
         		<StudentExamList students={this.props.students} onEvaluate={this.props.onEvaluate} />
     		</div>
   
  
    );
  };
}

export default StudentSelector;
