import React, { Component } from 'react';

class StudentExamListRow extends Component {
  render(){
    return (
      <tr>     
        <td>{this.props.student.rollNo}</td>
        <td>{this.props.student.firstName}</td>
        <td> {this.props.student.lastName}</td>
        <td><button type="button" onClick={() => {this.props.onEvaluate(this.props.student)}}  class="btn btn-primary  btn-sm">Evaluate</button></td>

                
      </tr>
    );
  }
};



export default StudentExamListRow;
