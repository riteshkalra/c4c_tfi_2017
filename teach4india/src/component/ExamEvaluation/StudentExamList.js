
import React, { Component } from 'react';
import StudentExamListRow from './StudentExamListRow';

class StudentExamList extends Component {
  render(){
    return (
       <table id="example1" class="table table-bordered table-striped">
        <thead class ="bg-blue">   
          <tr>
            <th>Roll No</th>
            <th>First Name</th> 
            <th>Last Name</th>                    
            <th>Action</th>
          </tr>
        </thead>
         
        <tbody>
        {this.props.students.map(student =>
          <StudentExamListRow key={student.id} student={student} onEvaluate={this.props.onEvaluate}/>
        )}
        </tbody>
      </table>
    );
  }
};

export default StudentExamList;
