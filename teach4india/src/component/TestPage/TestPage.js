import React, { Component } from 'react';
import {FireStoreDataSource} from '../../DataSource/FireStoreDataSource';
import {Admin} from '../../Model/Admin'
import {Class} from "../../Model/Class"
import {loadAcademicYears,loadCurrentAcademicYear} from '../../actions/AcademicYear/academicYearAction';


import {saveSchool}  from '../../actions/schoolActions';

class TestPage extends Component {

  constructor(props, context) {
    super(props, context);
    this.onClick = this.onClick.bind(this);
  }

  onClick(event) {
    console.log("Test on click called"); 
    let obj = new FireStoreDataSource().getClassObject('classRandomId1');
    //let obj = new FireStoreDataSource().getSchoolObject('D3YAj30Y7T6IB1zG3Dol');
    // obj.then(snap => {
    //   for(let index in snap) {
    //     snap[index].then(cnt => console.log(cnt));
    //   }
    // });
    
    console.log("return", obj)
   
  }

  render(){
    return (      
      <section className="content">
        <h1>Test Page</h1>
        <div className="row">
          <button onClick={this.onClick}>Click</button>
        </div>
      </section>
    );
  }
}

export default TestPage;
