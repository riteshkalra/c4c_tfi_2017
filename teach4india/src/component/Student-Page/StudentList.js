import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { Link } from 'react-router-dom'
import * as Utils  from '../common/common-function';
 
function dateFormatter(cell, row) {
  debugger;
 let dateObj= new Date(`${cell}`);
  return dateObj.toLocaleDateString("en-US")
}
function _setTableOption(){   
  return(     
      <i class="fa fa-refresh fa-spin"></i>
  )
}


function actionFormatter(cell,row) {
  return  <div>
            <i className="fa fa-user pull-middle"><Link to={'/studentProfile/'+`${cell}`}>View Profile</Link></i>
           </div>  
}
class StudentList extends Component {
  render(){    
    const {students, academicYear} = this.props; 
    const options = { 
      noDataText: _setTableOption()
    }
    let flattenStudents =[]    
    for(let index in students)
    {
      flattenStudents.push(Utils.flatten(students[index])); 
      
    }
    console.log("flattenStudents"+ flattenStudents)
   return (
      <div class="box box-default">
      <div class="box-header with-border">
        <BootstrapTable data={flattenStudents} striped hover search pagination bordered options={options} >
          <TableHeaderColumn width='30px' isKey dataField='id' className='bg-blue' dataSort hidden>Student ID</TableHeaderColumn>
          <TableHeaderColumn width='100px' dataField='id' className='bg-blue' dataFormat={actionFormatter }>Action</TableHeaderColumn>
          <TableHeaderColumn width='100px' dataField='rollNo'className='bg-blue' dataSort>Roll No.</TableHeaderColumn>
          <TableHeaderColumn width='200px' dataField='firstName'className='bg-blue' dataSort>First Name</TableHeaderColumn>
          <TableHeaderColumn width='200px' dataField='lastName'className='bg-blue' dataSort>Last Name</TableHeaderColumn>
          <TableHeaderColumn width='100px' dataField='gender'className='bg-blue' dataSort>Gender</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='dob'className='bg-blue' dataSort dataSort dataFormat={dateFormatter }>Birth Date</TableHeaderColumn>
          <TableHeaderColumn width='150x' dataField='doj'className='bg-blue' dataSort dataSort dataFormat={dateFormatter }>Joining Date</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='phone1'className='bg-blue' dataSort>Phone 1</TableHeaderColumn>
          <TableHeaderColumn width='150px'dataField='phone2'className='bg-blue' dataSort>Phone 2</TableHeaderColumn>
          <TableHeaderColumn width='200px' dataField='email'className='bg-blue' dataSort>Email</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='address.city'className='bg-blue' dataSort>City</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='address.country'className='bg-blue' dataSort>Country</TableHeaderColumn>
        </BootstrapTable>
      </div>
    </div>
   

    );
  }
}
export default StudentList;
