import React, { Component } from 'react';
import StudentRegistrationForm from './StudentRegistrationForm'
import * as studentActions from '../../actions/studentActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import toastr from 'toastr';
import { Link } from 'react-router-dom'
import {Admin} from '../../Model/Admin'
import {Address} from '../../Model/Address'
import {Contact} from '../../Model/Contact'
import {Student} from '../../Model/Student'
import PropTypes from 'prop-types';
import * as Utils  from '../common/common-function';
import * as Validation  from '../common/form-validation';
import { _saveStudent } from './../../actions/studentActions';

class StudentRegistrationPage extends Component { 
  
  constructor(props, context) {
    super(props, context);
      this.state = {
      student: Object.assign({}, props.student),
      classes: props.classes,
      errors: {},
      saving: false
    };
    this.onSaveStudent = this.onSaveStudent.bind(this);
    this.onChangeComponent = this.onChangeComponent.bind(this);
  }

  onSaveStudent(event) {
    event.preventDefault();
   // let tempSchoolError = Validation.schoolRegistrationFormIsValid(this.state.school)
    // let tempAddressError = Validation.addressIsValid(this.state.student.address)
    // let tempContactError = Validation.contactIsValid(this.state.student.contacts)
    // let allErrors = Object.assign({}, tempAddressError, tempContactError);    
    // if (!Utils.isEmptyObject(allErrors)) {
    //   this.setState({errors: allErrors});     
    //   return;
    // }
    this.setState({saving: true}) 
    let studentObj = new Student(
                        Admin.getDataSource(),
                        this.state.student.firstName,
                        this.state.student.middleName,
                        this.state.student.lastName, 
                        '',
                        this.state.student.gender, 
                        this.state.student.dob, 
                        this.state.student.registrationDate, 
                        new Address(Admin.getDataSource(),
                        this.state.student.building,
                                  this.state.student.street, 
                                  this.state.student.city,
                                  '', 
                                  this.state.student.state,
                                  this.state.student.country, 
                                  this.state.student.postal),
                        null, 
                        this.state.student.phone1, 
                        this.state.student.phone2, 
                        this.state.student.email                     
                        );
    console.log("saving student", studentObj);
    studentObj.addContact(new Contact( Admin.getDataSource(),
                                      this.state.student.contacts.contactFirstName,
                                      this.state.student.contacts.contactMiddleName, 
                                      this.state.student.contacts.contactLastName, 
                                      this.state.student.contacts.relationship, 
                                      this.state.student.contacts.contactPhone,
                                      this.state.student.contacts.contactMobile,
                                      this.state.student.contacts.contactFax,
                                      this.state.student.contacts.contactEmail, '', '', ));
    let classes =Utils.getClassById(this.state.classes,this.state.student.class)
    this.props.actions.saveStudent(studentObj,classes).then(() => this.redirect())
      .catch(error => {
        
        toastr.error(error);
        this.setState({saving: false});
      });  
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('Student saved');
    this.props.history.push('/StudentDetailGrid');
  }


  onChangeComponent(event) {
    const field = event.target.name;
    let student = Object.assign({}, this.state.student);
    if(event.target.type==="date")
    {
      student[field] = event.target.valueAsDate;
    }
    if(field==="school")
    {
      student[field] = event.target.value
        let selectedSchool = Utils.getSchoolById(this.props.currentAcademicYear.schools,event.target.value );
        console.log("selectedSchool: "+selectedSchool)
        if(!Utils.isEmptyObject(selectedSchool))
        {
          this.setState({classes: selectedSchool.Classes})
        }
        
    }
    
    else{
      student[field] = event.target.value;
    }
 
   return this.setState({student: student});

  }

 render(){
    return (               
            <section class="content">
              <div class="row ">
                <div class="col-md-12">
                  <div class="box-body">                 
                    
                      <StudentRegistrationForm
                      onChange={this.onChangeComponent}                                         
                      onSave={this.onSaveStudent}
                      student={this.state.student}
                      errors={this.state.errors}
                      saving={this.state.saving}
                      academicYears={this.props.academicYears}
                      currentAcademicYear ={this.props.currentAcademicYear}
                      schools={this.props.schools}
                      classes ={this.state.classes}
                      />
                  </div>
                </div>
              </div>
            </section>           
           
      );
  }
 }

 StudentRegistrationPage.contextTypes = {
  router: PropTypes.object
};
 
function mapStateToProps(state, ownProps) {
  let studentId = ownProps.match.params.id   
  let address ={}
  let student ={}
  student.address ={}
  student.contacts =[{}]
  student.academicYear={}
  let currentAcademicYear ={}
  let subjects={}
  let classes ={}
  if (studentId && state.students.length > 0) {
    student = Utils.getStudentById(state.students, studentId);      
  }
  if(state.currentAcademicYear!= null)
  {
   currentAcademicYear =state.currentAcademicYear
  }

  
  return {   
      student: student,
      academicYears: Utils.modelFormattedForDropdown(state.academicYears),    
      currentAcademicYear:currentAcademicYear,
      classes:classes 
      
    }; 
}

function mapStateToProps(state, ownProps) {
   let studentId = ownProps.match.params.id   
   console.log("studentId-------"+studentId)

   let student ={}
   student.address ={}
   student.contacts =[{}]
   student.academicYear={}
   let classes ={}
   let currentAcademicYear ={}   
   if (studentId && state.students.length > 0) {
      student = Utils.getStudentById(state.students, studentId);      
   }
   if(state.currentAcademicYear!= null)
   {
    currentAcademicYear =state.currentAcademicYear
   }

   return {   
       student: student,
       academicYears: Utils.modelFormattedForDropdown(state.academicYears),    
       currentAcademicYear:currentAcademicYear,
       classes:classes  
     }; 
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(studentActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(StudentRegistrationPage);