import React, { Component } from 'react';
import StudentInfoBox from './StudentInfoBox';
import StudentNavigationMenu from './StudentNavigationMenu';
import StudentOverallResult from './StudentOverallResult';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as studentActions from '../../actions/studentActions';
import { bindActionCreators } from 'redux';

class StudentProfile extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      student: Object.assign({}, props.student),
      errors: {},
    };
  }

  render() {
    return (

      <section class="content">
        <div class="row">
          <div class="col-md-4">
            <div class="box-body">
              <StudentInfoBox student={this.state.student} />
            </div>
          </div>
          <div class="col-md-8">
            <div class="box-body">
              <StudentOverallResult />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box-body">
              <StudentNavigationMenu />
            </div>
          </div>
        </div>



      </section>


    );
  }
}

StudentProfile.propTypes = {
  student: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

StudentProfile.contextTypes = {
  router: PropTypes.object
};

function getStudentById(students, id) {
  const student = students.filter(student => student.id === id);
  if (student) return student[0]; //since filter returns an array, have to grab the first.
  return null;
}

function mapStateToProps(state, ownProps) {
  const studentId = ownProps.match.params.id; // from the path `/student/:id`
  let student = { id: '', firstName: '', lastName: '', gender: '' };

  if (studentId && state.students.length > 0) {
    student = getStudentById(state.students, studentId);
  }

  return {
    student: student
  };
}



function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(studentActions, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(StudentProfile);
