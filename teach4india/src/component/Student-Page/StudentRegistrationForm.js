import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import DatePickerInput from '../common/DatePickerInput';
import PropTypes from 'prop-types';
import PhoneInput from '../common/PhoneInput';
import * as Utils  from '../common/common-function';


class StudentRegistrationForm extends React.Component {

  render(){
    let contactOptions = Utils.getContactRelationShipOption();
    let genderOptions = Utils.getGenderOptions();
    let schoolOptions = Utils.emptyDropDown()
    let teacherRoleOptions = Utils.getTeacherRoleOptions()
    if(this.props.currentAcademicYear != null)
    {
      schoolOptions = Utils.modelFormattedForDropdown(this.props.currentAcademicYear.schools);
    }     
    let classOptions = Utils.modelFormattedForDropdown(this.props.classes);
    return (
      <form onSubmit ={this.props.onSave}>
        <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Official Details</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">              
                <TextInput name="academicYear" label="Academic Year" value={this.props.currentAcademicYear.name} disabled onChange={this.props.onChange} error=""/>                  
                <SelectInput name="school" label="Schools" value={this.props.student.schoolId} defaultOption="Select Schools " options={schoolOptions} onChange={this.props.onChange} error={this.props.errors.school}/>                
                              
            </div>
            <div class="col-md-4">
              <TextInput name="id" label="Registration Number" value={this.props.student.id} disabled onChange={this.props.onChange} error=""/>              
              <SelectInput name="class" label="Class" value={this.props.student.classId} defaultOption="Select Class " options={classOptions} onChange={this.props.onChange} error={this.props.errors.class}/>    
                   
            </div>
             <div class="col-md-4">
              <DatePickerInput name="registrationDate" label="Date of Joining" value={this.props.student.registrationDate} onChange={this.props.onChange} error={this.props.errors.registrationDate}/>             
                          
            </div>
          </div>
        </div>
      </div>

      {/* student details form start*/}
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Student Details</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
           
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">              
                <TextInput name="firstName" label="First Name" value={this.props.student.firstName}  onChange={this.props.onChange} error={this.props.errors.firstName}/>              
                <DatePickerInput name="dob" label="Date of Birth" value={this.props.student.dob}  onChange={this.props.onChange} error={this.props.errors.dob}/>   
                <PhoneInput name="phone2" label="Home Phone" value={this.props.student.phone2}  onChange={this.props.onChange} error={this.props.errors.phone2}/>                 
            </div>
            <div class="col-md-4">
              <TextInput name="middleName" label="Middle Name" value={this.props.student.middleName}  onChange={this.props.onChange} error={this.props.errors.middleName}/>              
               <SelectInput name="gender" label="Gender" value={this.props.student.gender} defaultOption="Select Gender" options={genderOptions} onChange={this.props.onChange} error={this.props.errors.gender}/>              
              <TextInput name="email" label="Email" value={this.props.student.email}  onChange={this.props.onChange} error={this.props.errors.email}/>         
            </div>
             <div class="col-md-4">
              <TextInput name="lastName" label="Last Name" value={this.props.student.lastName}  onChange={this.props.onChange} error={this.props.errors.lastName}/>              
              <PhoneInput name="phone1" label="Mobile Phone" value={this.props.student.phone1} onChange={this.props.onChange}  error={this.props.errors.phone1}/>          
            </div>
          </div>
        </div>
      </div>
 {/* Contact details form start*/}
 <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Contact Details</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-4">              
                <TextInput name="contactFirstName" label="First Name"  value={this.props.student.contacts[0].firstName} onChange={this.props.onChange} error={this.props.errors.contactFirstName}/>         
                <SelectInput name="relationship" label="RelationShip" value={this.props.student.contacts[0].relationship} defaultOption="Select RelationShip" options={contactOptions} onChange={this.props.onChange} error={this.props.errors.relationship}/>               
                <TextInput name="contactFax" label="Fax"  value={this.props.student.contacts[0].fax} onChange={this.props.onChange} error={this.props.errors.contactFax}/>                 
            </div>
            <div class="col-md-4">
              <TextInput name="contactMiddleName" label="Middle Name"  value={this.props.student.contacts[0].middleName} onChange={this.props.onChange} error={this.props.errors.contactMiddleName}/>              
              <PhoneInput name="contactHomePhone" label="Home Phone"  value={this.props.student.contacts[0].phone} onChange={this.props.onChange} error= {this.props.errors.contactHomePhone}/>  
              <TextInput name="contactEmail" label="Email"  value={this.props.student.contacts[0].email} onChange={this.props.onChange} error={this.props.errors.contactEmail}/>         
            </div>
             <div class="col-md-4">
              <TextInput name="contactLastName" label="Last Name"  value={this.props.student.contacts[0].lastName} onChange={this.props.onChange} error={this.props.errors.contactLastName}/>              
              <PhoneInput name="contactMobile" label="Mobile Phone"  value={this.props.student.contacts[0].mobile} onChange={this.props.onChange} error={this.props.errors.contactMobile}/>          
            </div>
          </div>
        </div>
      </div>
     {/* Address details form start*/}
     <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">Address Details</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-4">              
                  <TextInput name="city" label="City"  value={this.props.student.address.city} onChange={this.props.onChange} error={this.props.errors.city}/>   
                  <TextInput name="postal" label="PinCode"  value={this.props.student.address.postal} onChange={this.props.onChange} error={this.props.errors.postal}/>                 
              </div>
              <div class="col-md-4">
                <TextInput name="building" label="Building/House No"  value={this.props.student.address.building} onChange={this.props.onChange} error={this.props.errors.building}/>              
                <TextInput name="state" label="State"  value={this.props.student.address.state} onChange={this.props.onChange} error={this.props.errors.state}/>    
                     
              </div>
              <div class="col-md-4">
                <TextInput name="street" label="Street Name"  value={this.props.student.address.street} onChange={this.props.onChange} error={this.props.errors.street}/>              
                <TextInput name="country" label="Country"  value={this.props.student.address.country} onChange={this.props.onChange} error={this.props.errors.country}/>    
                     </div>
            </div>
          </div>
        </div>

      {/* Button for Registration*/}
        
      <div class="row">
            <div class="col-md-12">              
                <input type="submit" disabled={this.props.saving} value={this.props.saving ? 'Saving...' : 'Save'} class="btn btn-info pull-right"/>                     
            </div>      
          </div>     
    </form>
  );
};
}

StudentRegistrationForm.propTypes = {
  student: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default StudentRegistrationForm;


