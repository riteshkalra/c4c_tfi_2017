import React, { Component } from 'react';
import {Doughnut} from 'react-chartjs-2';

const readingData = {
  
  datasets: [{
    data: [70, 30],
    backgroundColor: [
    '#00a65a',
    '#f39c12',
    
    ],
    hoverBackgroundColor: [
    '#00a65a',
    '#f39c12',
   
    ]
  }]
};

const writingData = {
  
  datasets: [{
    data: [80, 20],
    backgroundColor: [
    '#00a65a',
    '#f39c12',
    
    ],
    hoverBackgroundColor: [
    '#00a65a',
    '#f39c12',
   
    ]
  }]
};

const listeningData = {
  
  datasets: [{
    data: [75, 25],
    backgroundColor: [
    '#00a65a',
    '#f39c12',
    
    ],
    hoverBackgroundColor: [
    '#00a65a',
    '#f39c12',
   
    ]
  }]
};

const speakingData = {
  
  datasets: [{
    data: [50, 50],
    backgroundColor: [
    '#00a65a',
    '#f39c12',
    
    ],
    hoverBackgroundColor: [
    '#00a65a',
    '#f39c12',
   
    ]
  }]
};
     

class StudentNavigationMenu extends Component { 

  constructor(props, context) {
    super(props, context);
  }

    

    


  render(){
    return (
    
        <div class="nav-tabs-custom ">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#English" data-toggle="tab">English</a></li>
            <li><a href="#math" data-toggle="tab">Math</a></li>
            <li><a href="#physics" data-toggle="tab">Science</a></li>
          
          </ul> 

          <div class="tab-content">
            <div class="active tab-pane" id="English">
              <div class='row'>
                <div class="col-xs-6 col-md-3 text-center">
                  <Doughnut  data={readingData}/>
                  <p>Reading</p>
                </div>
                <div class="col-xs-6 col-md-3 text-center">
                  <Doughnut data={writingData}/>
                  <p>Writing</p>
                </div>
                <div class="col-xs-6 col-md-3 text-center">
                  <Doughnut data={listeningData}/>
                  <p>Listening</p>
                </div>
                <div class="col-xs-6 col-md-3 text-center">
                  <Doughnut data={speakingData}/>
                  <p>Speaking</p>
                </div>
              </div>
            </div>

          </div> 


        </div>
    
    );
  }
}

export default StudentNavigationMenu;
