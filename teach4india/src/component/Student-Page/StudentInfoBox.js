import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import studentImage from '../../avatar2.png';

class StudentInfoBox extends Component {

  render(){
    const {student} = this.props;
    
    if (student.id != null && student.id.length > 0) {
        return (
            <div class="box box-widget widget-user-2">           
               <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img className="img-circle"  src= {studentImage} alt="User profile picture"/>*/}
              </div>
            
              <h3 class="widget-user-username"><b>{student.firstName} {student.lastName}</b></h3>
              <h5 class="widget-user-desc">T20180001</h5>
            </div>


              <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                  <li><a>Class <Link to={'/classes'} class="pull-right"><u>G3SA</u></Link></a></li>
                  <li><a>Date of Birth <span class="pull-right">{student.dob.toDateString()}</span></a></li>
                  <li><a>Gender <span class="pull-right">{student.gender}</span></a></li>
                  <li><a>Father Name <span class="pull-right">{student.firstName}'s father</span></a></li>
                  <li><a>Mother Name <span class="pull-right">{student.firstName}'s mother</span></a></li>
                  <li><a>Phone Number <span class="pull-right">{student.phone1}</span></a></li>
                  <li><a>Email<span class="pull-right">{student.email}</span></a></li>
                  <li><a>Address <span class="pull-right">{student.address}</span></a></li>
                  
                </ul>
              </div>
            </div>        

        );
    } else {
        return (
      
            <div class="box box-widget widget-user-2">           
               <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle"  src= {studentImage} alt="User profile picture"/>
              </div>
            
              <h3 class="widget-user-username">Stella Joseph</h3>
              
            </div>
              <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                  <li><a href="#">Class <span class="pull-right">5A</span></a></li>
                  <li><a href="#">Date of Birth <span class="pull-right">20 Dec 2010</span></a></li>
                  <li><a href="#">Gender <span class="pull-right">Female</span></a></li>
                  <li><a href="#">Father Name <span class="pull-right">Henry Joseph</span></a></li>
                  <li><a href="#">Mother Name <span class="pull-right">Rita Joseph</span></a></li>
                  <li><a href="#">Phone Number <span class="pull-right">91-9332456</span></a></li>
                  <li><a href="#">Email-id <span class="pull-right">stella.joseph@gmail.com</span></a></li>
                  <li><a href="#">Address <span class="pull-right">20 -01 VinayakNagar Bangalore</span></a></li>
                  
                </ul>
              </div>
            </div>      
         
        
        
      
        );
    }
  }
}

export default StudentInfoBox;