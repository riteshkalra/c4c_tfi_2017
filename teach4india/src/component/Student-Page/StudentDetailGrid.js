import React, { Component } from 'react';
import StudentList from './StudentList'
import * as studentActions from '../../actions/studentActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'
import queryString from 'query-string'
import {Admin} from '../../Model/Admin'
import {BrowserRouter } from 'react-router-dom'

class StudentDetailGrid extends Component {

  constructor(props, context) {
    super(props, context);
    this.redirectToAddStudentPage = this.redirectToAddStudentPage.bind(this);
  }

  redirectToAddStudentPage() {
    BrowserRouter.route('/StudentRegistration');
  }
   render(){
    
    let {students, currentAcademicYear} = this.props;     
    return ( 
      <section className="content">
        <h2> Students</h2>
          <div className="row">
            <div className="col-md-12">
              <div className="box-body">
                <StudentList students={students} academicYear={currentAcademicYear}/>
              </div>
            </div>
          </div>
          <div className="row ">
            <div className="col-md-2 pull-right">
              <div className="box-body">
               <Link to ="/StudentRegistration"><button type="button" className="btn btn-block btn-primary" >Add</button></Link>
              </div>
            </div>
          </div>          
      </section>        
    );
  }
}


function mapStateToProps(state, ownProps) {
  console.log("state", state.currentAcademicYear);
  let currentAcademicYear = state.currentAcademicYear || {}
  let schools =  currentAcademicYear.Schools || []
  let studentMasterList =[]
  for(let index in schools)
  {
    let students = schools[index].Students
    for(let ind in students)
    { 
      studentMasterList.push(students[ind]);  
    }      
  }
  return {
    currentAcademicYear: currentAcademicYear,
    students: studentMasterList
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(studentActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(StudentDetailGrid);
