import React, { Component } from 'react';
import avatar5 from '../../../node_modules/admin-lte/dist/img/avatar5.png';


class HeaderBarMain extends Component {

  pushMenu()
  {
     var body = document.body;
                if(body.clientWidth > 768){
                    if(body.className.indexOf('sidebar-collapse') === -1){
                        body.className += ' sidebar-collapse';
                    }else {
                        body.className = body.className.replace(' sidebar-collapse', '');
                    }
                }else{
                    if (body.className.indexOf('sidebar-open') === -1) {
                        body.className += ' sidebar-open';
                    }else{
                        body.className = body.className.replace(' sidebar-open','');
                    }
                }
  }
  render() {
    return (
      <header className="main-header">
          {/* Logo */}
          <div className="logo">
              {/* mini logo for sidebar mini 50x50 pixels */}
              <span className="logo-mini"><b>A</b>BC</span>
              {/* logo for regular state and mobile devices */}
              <span className="logo-lg"><b>Abacus</b></span>
          </div>
          {/* Header Navbar: style can be found in header.less */}
          <nav className="navbar navbar-static-top" >
              {/* Sidebar toggle button*/}
              <a href="#" className="sidebar-toggle" data-toggle="pushMenu" role="button" onClick={this.pushMenu}>
                  <span className="sr-only">Toggle navigation</span>
              </a>
              <div className="navbar-custom-menu">
                  <ul className="nav navbar-nav">
                   
                      <li className="dropdown user user-menu">
                          <a href="/" className="dropdown-toggle" data-toggle="dropdown">
                              <img src={avatar5}  className="user-image" alt="User " />
                              <span className="hidden-xs">Karthick Arya</span>
                          </a>
                         
                      </li>
                      { /* ontrol Sidebar Toggle Button */}
                      <li>
                          <a href="#" data-toggle="control-sidebar"><i className="fa fa-gears"></i></a>
                      </li>
                  </ul>
              </div>
          </nav>
      </header>
    );
  }
}

export default HeaderBarMain;
