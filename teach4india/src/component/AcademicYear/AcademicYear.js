import React, {Component} from 'react';
import AcademicYearList from './AcademicYearList'
import * as academicYearAction from '../../actions/AcademicYear/academicYearAction';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import $ from 'jquery';
window.$ =$

class AcademicYear extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            academicYears: props.academicYear

        };

    }

    render() {
        const {academicYears, actions} = this.props;

        return (
            <section class="content">
                <h2> Academic Years</h2>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-body">
                            <AcademicYearList academicYears={academicYears} actions={actions}/>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-2 pull-right">
                        <div class="box-body">
                            <Link to="/academicYearRegistration">
                                <button type="button" class="btn btn-block btn-primary">Add</button>
                            </Link>
                        </div>
                    </div>
                </div>

            </section>

        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        academicYears: state.academicYears
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(academicYearAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AcademicYear);


