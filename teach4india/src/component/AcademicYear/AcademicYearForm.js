import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import DatePickerInput from '../common/DatePickerInput';
import PhoneInput from '../common/PhoneInput';
import PropTypes from 'prop-types';



const AcademicYearForm = ({academicYear,  onSave, onChange, saving, errors}) => {
  return (
      
    

    <form>
    
      
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Register Academic Year</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">              
                <TextInput name="name" label="Name" value={academicYear.name} onChange={onChange} error={errors.name}/> 
                <DatePickerInput name="startDate" label="Start Date" value={academicYear.startDate} onChange={onChange} error={errors.startDate}/> 
                                
            </div>
            <div class="col-md-6">
              <TextInput name="board" label="Board" value={academicYear.board} onChange={onChange} error={errors.board} dataInputMask ='"mask": "(999) 999-9999"'/>             
              <DatePickerInput name="endDate" label="End Date" value={academicYear.endDate} onChange={onChange} error={errors.endDate}/>       
            </div>            
          </div>
        </div>
      </div>

      {/* Button for Registration*/}
        
          <div class="row">
            <div class="col-md-12">              
               <input type="submit" disabled={saving} value={saving ? 'Saving...' : 'Add'} class="btn btn-info pull-right" onClick={onSave}/>                    
            </div>      
          </div>     
    </form>    
  );
};

AcademicYearForm.propTypes = {
  academicYear: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default AcademicYearForm;


