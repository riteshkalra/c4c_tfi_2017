import React, {Component} from 'react';
import AcademicYearForm from './AcademicYearForm'
import * as academicYearAction from '../../actions/AcademicYear/academicYearAction';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import toastr from 'toastr';
import PropTypes from 'prop-types';
import {AcademicYear} from "../../Model/AcademicYear";
import {FireStoreDataSource} from "../../DataSource/FireStoreDataSource";

class AcademicYearRegistration extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            academicYear: Object.assign({}, props.academicYear),
            errors: {},
            saving: false
        };
        this.onSaveAcademicYear = this.onSaveAcademicYear.bind(this);
        this.onChangeComponent = this.onChangeComponent.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
    }

     onSaveAcademicYear(event) {
        this.setState({saving: true});
        let academicYear = new AcademicYear(
            this.state.academicYear.dataSource,
            this.state.academicYear.name,
            this.state.academicYear.board,
            this.state.academicYear.startDate,
            this.state.academicYear.endDate,
            this.state.academicYear.id
        );

        this.props.actions.saveAcademicYear(academicYear);
        this.redirect();
        event.preventDefault();
    }

    onChangeComponent(event) {
        const field = event.target.name;
        let academicYear = Object.assign({}, this.state.academicYear);
        if (event.target.type === "date") {
            academicYear[field] = event.target.valueAsDate;
        }
        else {
            academicYear[field] = event.target.value;
        }

        return this.setState({academicYear: academicYear});

    }

    onDateChange(event) {
    }

    redirect() {
        this.setState({saving: false});
        toastr.success('AcademicYear saved');
        this.props.history.push('/academicYear');
    }

    render() {


        return (
            <section class="content">
                <div class="row ">
                    <div class="col-md-12 pull-left">
                        <div class="box-body">

                            <AcademicYearForm
                                onChange={this.onChangeComponent}
                                onSave={this.onSaveAcademicYear}
                                academicYear={this.state.academicYear}
                                errors={this.state.errors}
                                saving={this.state.saving}
                                onDateChange={this.onDateChange}
                            />
                        </div>
                    </div>
                </div>
            </section>

        );
    }
}


function getAcademicYearById(academicYears, id) {
    const academicYear = academicYears.filter(academicYear => academicYear.id === id);
    if (academicYear) return academicYear[0]; //since filter returns an array, have to grab the first.
    return null;
}

AcademicYearRegistration.contextTypes = {
    router: PropTypes.object
};

function mapStateToProps(state, ownProps) {
    let academicYearId = ownProps.match.params.id;
    let dataSource = new FireStoreDataSource();
    if (state.academicYear) {
        dataSource = state.academicYear.dataSource;
    }
    let academicYear = {dataSource: dataSource, id: '', name: '', board: '', startDate: '', endDate: ''};
    if (academicYearId && state.academicYears.length > 0) {
        academicYear = getAcademicYearById(state.academicYears, academicYearId);
    }
    return {
        academicYear: academicYear
    };

}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(academicYearAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AcademicYearRegistration);


