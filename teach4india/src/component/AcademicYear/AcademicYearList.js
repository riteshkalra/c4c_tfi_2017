import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { Link } from 'react-router-dom'

function dateFormatter(cell, row) {
  let dateObj= new Date(`${cell}`);
  return dateObj.toLocaleDateString("en-US")
}

function actionFormatter(cell) {
  return <div>
                <i className="fa fa-edit pull-middle"><Link to={'/academicYearRegistration/'+`${cell}`}><span>Edit</span></Link></i>
                <span>     </span>
                <i className="fa fa-eye pull-middle"><Link to={'/schools/?academicYearId=' + `${cell}`}><span>Schools</span></Link></i>
          </div> 
}

function _setTableOption(){   
  return(     
      <i class="fa fa-refresh fa-spin"></i>
  )
}

class AcademicYearList extends Component {
  render(){
  const options = {
      noDataText: _setTableOption(this.props.academicYears)
  }
   return (
    <div class="box box-default">
      <div class="box-header with-border">
        <BootstrapTable data={this.props.academicYears} striped hover  bordered options={options}>
          <TableHeaderColumn isKey dataField='id' className='bg-blue' dataSort hidden>AcademicYear ID</TableHeaderColumn>
          <TableHeaderColumn dataField='name'className='bg-blue' dataSort>Name</TableHeaderColumn>
          <TableHeaderColumn dataField='startDate' className='bg-blue' dataSort dataFormat={dateFormatter }>Start Date</TableHeaderColumn>
          <TableHeaderColumn dataField='endDate' className='bg-blue' dataSort dataFormat={dateFormatter }>End Date</TableHeaderColumn>

          
        </BootstrapTable>
      </div>
    </div>
    );
  }
}
export default AcademicYearList;
