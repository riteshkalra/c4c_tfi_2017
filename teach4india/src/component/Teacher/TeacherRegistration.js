import React, { Component } from 'react';
import TeacherForm from './TeacherForm'
import * as teacherActions from '../../actions/teacherActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import toastr from 'toastr';
import { Link } from 'react-router-dom'
import {Admin} from '../../Model/Admin'
import {Address} from '../../Model/Address'
import {Contact} from '../../Model/Contact'
import {Teacher} from '../../Model/Teacher'
import PropTypes from 'prop-types';
import * as Utils  from '../common/common-function';
import * as Validation  from '../common/form-validation';

class TeacherRegistration extends Component { 
  
  constructor(props, context) {
    super(props, context);
      this.state = {
      teacher: Object.assign({}, props.teacher),
      classes: props.classes,
      errors: {},
      saving: false,
      adminTeacherFlag: true
    };
    this.onSaveTeacher = this.onSaveTeacher.bind(this);
    this.onChangeComponent = this.onChangeComponent.bind(this);
  
  }

  onSaveTeacher(event) {
    event.preventDefault();
    
    let tempTeacherError = Validation.teacheRegistrationDataIsValid(this.state.teacher)
    let personalDetails = Validation.personalDetailIsValid(this.state.teacher)
    let tempAddressError = Validation.addressIsValid(this.state.teacher)
    let tempContactError = Validation.contactIsValid(this.state.teacher)
    let allErrors = Object.assign({}, tempTeacherError,tempAddressError,tempContactError,personalDetails);    
    // if (!Utils.isEmptyObject(allErrors)) {
    //   this.setState({errors: allErrors});     
    //   return;
    // }
    this.setState({saving: true}) 
    let teacherObj =new Teacher(
                        Admin.getDataSource(),
                        this.state.teacher.firstName,
                        this.state.teacher.lastName, 
                        '',
                        this.state.teacher.gender, 
                        this.state.teacher.dob, 
                        new Address(Admin.getDataSource(),
                          this.state.teacher.building,
                                    this.state.teacher.street, 
                                    this.state.teacher.city,
                                    '', 
                                    this.state.teacher.state,
                                    this.state.teacher.country, 
                                    this.state.teacher.postal),
                        null, 
                        this.state.teacher.phone1, 
                        this.state.teacher.phone2, 
                        this.state.teacher.fax, 
                        this.state.teacher.email,     
                        this.state.teacher.registrationDate                      
                        );
    teacherObj.addContact(new Contact( Admin.getDataSource(),
                                      this.state.teacher.contactFirstName,
                                      this.state.teacher.contactMiddleName, 
                                      this.state.teacher.contactLastName, 
                                      this.state.teacher.relationship, 
                                      this.state.teacher.contactPhone,
                                      this.state.teacher.contactMobile,
                                      this.state.teacher.contactFax,
                                      this.state.teacher.contactEmail, '', '', ));
                                        debugger;
    let classes =Utils.getClassById(this.state.classes,this.state.teacher.class)
    
    if(this.state.teacher.role ==='Admin')
    {
      this.props.actions.saveTeacher(teacherObj,classes).then(() => this.redirect())
       .catch(error => {
        toastr.error(error);
        this.setState({saving: false});
      }); 
    } 
  }
   redirect() {
    this.setState({saving: false});
    toastr.success('Teacher saved');
    this.props.history.push('/teachers');
  }

  
  onChangeComponent(event) {
    const field = event.target.name;
    let teacher = Object.assign({}, this.state.teacher);
    if(event.target.type==="date")
    {
        teacher[field] = event.target.valueAsDate;
    }
    if(field==="school")
    {
        teacher[field] = event.target.value
        let selectedSchool = Utils.getSchoolById(this.props.currentAcademicYear.schools,event.target.value );
        console.log("selectedSchool: "+selectedSchool)
        if(!Utils.isEmptyObject(selectedSchool))
        {
          this.setState({classes: selectedSchool.Classes})
        }
        
    }
    if(field==="class")
    {
        teacher[field] = event.target.value
        let selectedClass = Utils.getClassById(this.state.classes,event.target.value );
        
        if(!Utils.isEmptyObject(selectedClass))
        {
          this.setState({subjects: selectedClass.Subjects})
        }
       
    }
    else if(field ==="role")
    {
      teacher[field] = event.target.value;
      this.setState({adminTeacherFlag:teacher[field]})

    }

    else{
        teacher[field] = event.target.value;
    } 
   return this.setState({teacher: teacher});
  }

 

 

 render(){
    return (               
	          <section class="content">
  	          <div class="row ">
                <div class="col-md-12">
                  <div class="box-body">                 
                    
                      <TeacherForm
                      onChange={this.onChangeComponent}                                         
                      onSave={this.onSaveTeacher}
                      teacher={this.state.teacher}
                      errors={this.state.errors}
                      saving={this.state.saving}
                      academicYears={this.props.academicYears}
                      currentAcademicYear ={this.props.currentAcademicYear}                     
                      classes ={this.state.classes}
                      subjects={this.state.subjects}
                      adminTeacherFlag ={this.state.adminTeacherFlag}

                      />
                  </div>
                </div>
              </div>
            </section>           
	         
    	);
 	}
 }

 TeacherRegistration.contextTypes = {
  router: PropTypes.object
};
  

function mapStateToProps(state, ownProps) {
   let teacherId = ownProps.match.params.id   

   let address ={}
   let teacher ={}
   teacher.address ={}
   teacher.contacts =[{}]
   teacher.academicYear={}
   let currentAcademicYear ={}
   let subjects={}
   let classes ={}
   if (teacherId && state.teachers.length > 0) {
      teacher = Utils.getTeacherById(state.teachers, teacherId);      
   }
   if(state.currentAcademicYear!= null)
   {
    currentAcademicYear =state.currentAcademicYear
   }

   
   return {   
       teacher: teacher,
       academicYears: Utils.modelFormattedForDropdown(state.academicYears),    
       currentAcademicYear:currentAcademicYear,
       classes:classes 
       
     }; 
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(teacherActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TeacherRegistration);