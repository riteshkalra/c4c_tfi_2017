import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import { Link } from 'react-router-dom'
import * as Utils  from '../common/common-function';
 
function dateFormatter(cell, row) {
 let dateObj= new Date(`${cell}`);
  return dateObj.toLocaleDateString("en-US")
}

function actionFormatter(cell,row) {
  return  <div>
            <i className="fa fa-edit pull-middle"><Link to={'/schoolRegistration/'+`${cell}`}>Edit</Link></i>  
            <span> </span>
            <i className="fa fa-eye pull-middle"><Link to={'/schoolDetail/'+`${cell}`}>View</Link></i>
            <span> </span>
            <i className="fa fa-eye pull-middle"><Link to={'/classes/?schoolId='+`${cell}`}>Classes</Link></i>
          </div>  
}
function _setTableOption(){   
    return(     
        <i class="fa fa-refresh fa-spin"></i>
    )
}

function isExpandableRow(row) {
 return true;
}

function expandComponent(row) {
  debugger;
  console.log("row ->" + row.teachers)
  let flattenTeachers =[];    
    for(let index in  row.teachers)
    {
      flattenTeachers.push(Utils.flatten(row.teachers[index])); 
     
    }
   
  return (
    <BootstrapTable data={flattenTeachers} striped hover bordered search pagination >
    <TableHeaderColumn width='30px' isKey dataField='id' className='bg-blue' dataSort hidden>Teacher ID</TableHeaderColumn>
    <TableHeaderColumn width='200px' dataField='firstName'className='bg-blue' dataSort>First Name</TableHeaderColumn>
    <TableHeaderColumn width='200px' dataField='lastName'className='bg-blue' dataSort>Last Name</TableHeaderColumn>
    <TableHeaderColumn width='100px' dataField='gender'className='bg-blue' dataSort>Gender</TableHeaderColumn>
    <TableHeaderColumn width='150px' dataField='dob'className='bg-blue' dataSort dataSort dataFormat={dateFormatter }>Birth Date</TableHeaderColumn>
    <TableHeaderColumn width='150x' dataField='doj'className='bg-blue' dataSort dataSort dataFormat={dateFormatter }>Joining Date</TableHeaderColumn>
    <TableHeaderColumn width='150px' dataField='phone1'className='bg-blue' dataSort>Phone 1</TableHeaderColumn>
    <TableHeaderColumn width='150px'dataField='phone2'className='bg-blue' dataSort>Phone 2</TableHeaderColumn>
    <TableHeaderColumn width='200px' dataField='email'className='bg-blue' dataSort>Email</TableHeaderColumn>
    <TableHeaderColumn width='150px' dataField='address.city'className='bg-blue' dataSort>City</TableHeaderColumn>
    <TableHeaderColumn width='150px' dataField='address.country'className='bg-blue' dataSort>Country</TableHeaderColumn>
  </BootstrapTable>
  );
}

class ExpandableTeacherList extends Component {
  render(){
    const {schools, academicYear} = this.props; 
    const options = { 
      noDataText: _setTableOption(),
      expandBy: 'column' ,
      expandRowBgColor: 'bg-gray'
    }    
    let flattenSchools =[];    
    for(let index in schools)
    {
      schools[index].Teachers;
     flattenSchools.push(Utils.flatten(schools[index])); 
     
    }
   
   return (
      <div class="box box-default">
      <div class="box-header with-border">
        <BootstrapTable data={schools} striped hover bordered options={options} expandColumnOptions={ {
          expandColumnVisible: true,
          expandColumnComponent: this.expandColumnComponent,
          columnWidth: 10
        } } expandComponent={expandComponent} expandableRow={isExpandableRow }>
          <TableHeaderColumn width='30px' isKey dataField='id' className='bg-blue' dataSort hidden>AcademicYear ID</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='name'className='bg-blue' dataSort>Name</TableHeaderColumn>
          <TableHeaderColumn width='150px' dataField='address.city'className='bg-blue' dataSort>City</TableHeaderColumn>

         
          
        </BootstrapTable>
      </div>
    </div>
   

    );
  }
}
export default ExpandableTeacherList;
