import React, { Component } from 'react';
import TeacherList from './TeacherList'
import TeacherForm from './TeacherForm'
import ExpandableTeacherList from './ExpandableTeacherList'
import * as teacherActions from '../../actions/teacherActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {BrowserRouter } from 'react-router-dom'
import { Link } from 'react-router-dom'

class Teacher extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      teachers: props.teachers      
    };
  }

  render(){
    const {teachers} = this.props;

    return (   
      <section className="content">
        <h2> Teachers</h2>
          <div className="row">
            <div className="col-md-12">
              <div className="box-body">
                <TeacherList teachers={teachers}/>
              </div>
            </div>
          </div> 
          <div className="row ">
            <div className="col-md-2 pull-right">
              <div className="box-body">
               <Link to ="/teacherRegistration"><button type="button" className="btn btn-block btn-primary" >Add</button></Link>
              </div>
            </div>
          </div>           
      </section>        
    );
  }
}

function mapStateToProps(state, ownProps) {
  console.log("state", state.currentAcademicYear);
  let currentAcademicYear = state.currentAcademicYear || {}
  let schools =  currentAcademicYear.Schools || []
  let teachers =[]
  for(let index in schools)
  {
    let teachersObjs = schools[index].Teachers
    for(let ind in teachersObjs)
    { 
        teachers.push(teachersObjs[ind]);  
    }      
  }
  return {
    currentAcademicYear: currentAcademicYear,
    teachers: teachers
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(teacherActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Teacher);