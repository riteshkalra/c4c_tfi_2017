import React, { Component } from 'react';
import * as academicYearAction from '../../actions/AcademicYear/academicYearAction';
import * as dashboardAction from '../../actions/dashboardActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SelectInput from '../common/SelectInput';
import * as Utils from '../common/common-function';
import { FireStoreDataSource } from "../../DataSource/FireStoreDataSource";
import InfoBox from '../common/InfoBox'
import AcademicYearList from '../AcademicYear/AcademicYearList'
import ToDoList from '../common/ToDoList'
import SchoolBar from '../common/SchoolBar'
import StudentBar from '../common/StudentBar'



class AdminDashBoard extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            academicYearsOptions: [],
            academicYear: null
        };
        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentWillReceiveProps = this.componentWillReceiveProps.bind(this);
        this.onSelectAcademicYear = this.onSelectAcademicYear.bind(this);
        this.getCurrentAcademicYear = this.getCurrentAcademicYear.bind(this);
    }

    componentDidMount() {
        this.props.actions.loadAcademicYears();
    }

    componentWillReceiveProps(nextProps) {

        let academicYears = nextProps.academicYears;
        this.setState({ academicYearsOptions: Utils.modelFormattedForDropdown(academicYears) });
        if (academicYears && !this.state.academicYear) {
            this.setState({ academicYear: this.getCurrentAcademicYear(academicYears) });
        }
    }

    getCurrentAcademicYear(academicYears) {
        let currentDate = new Date();
        let currentAcademicYear = academicYears.find(function (element) {
            return element.startDate <= currentDate <= element.endDate;
        });
        return currentAcademicYear
    }

    onSelectAcademicYear(event) {
        let rowIndex = this.props.academicYears.findIndex(element => {
            return element.id === event.target.value;
        });
        this.props.dashboards.setAcademicYear(this.props.academicYears[rowIndex]);
    }

    render() {
        return (
            <section class="content">

                <div class="row">

                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <InfoBox name="Schools" length="10" imageClass="fa fa-university"
                            style="info-box-icon bg-red" />
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <InfoBox name="Students" length="315" imageClass="fa fa-users"
                            style="info-box-icon bg-blue" />
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <InfoBox name="Teachers" length="30" imageClass="fa fa-user"
                            style="info-box-icon bg-yellow" />
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-8">
                        <div class="box box-success">
                            <div class="box-header with-border">
                            <h3 class="box-title">Academic Years</h3>


                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <AcademicYearList academicYears={this.props.academicYears} />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box-body">
                            <ToDoList />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">


                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="chart">
                                    <StudentBar />
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">


                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="chart">
                                    <SchoolBar />
                                </div>
                            </div>

                        </div>
                    </div>
                </div>





                {/*
            <div class="pad box-pane-right bg-green col-md-2"  >
              <div class="row">
                  <div class="col-md-9 col-sm-8">            
                    <MapComponent/>
                    <div class="col-md-3 col-sm-4">
                      <DescriptionBlock classStyle="description-block margin-bottom" headerText="Schools" headerValue={this.state.numberOfSchools} imageClass="fa fa-university" />
                      <DescriptionBlock classStyle="description-block margin-bottom" headerText="Teachers" headerValue={this.state.numberOfSchools}/>
                      <DescriptionBlock classStyle="description-block" headerText="Students" headerValue={this.state.numberOfSchools}/>
                    </div>
                  </div>
                  
              </div>
            </div>
            */}

            </section >

        );
    }
}


function getNumberOfTeachers(academicYear) {
    let numberOfTeachers = 1;


    // for(var i = 0, size = academicYear.schools.length; i < size ; i++)
    // {
    //    numberOfTeacher =(numberOfTeacher+academicYear.schools[i].teachers.length);
    // }
    return numberOfTeachers

}

function mapStateToProps(state, ownProps) {
    let academicYears = state.academicYears;

    return {
        dataSource: new FireStoreDataSource(),
        academicYears: academicYears,
    };

}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(academicYearAction, dispatch),
        dashboards: bindActionCreators(dashboardAction, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminDashBoard);




