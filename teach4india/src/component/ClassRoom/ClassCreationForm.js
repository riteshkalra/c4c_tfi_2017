import React from 'react';
import TextInput from '../common/TextInput';
import PropTypes from 'prop-types';
import SelectInput from "../common/SelectInput";
import * as Utils from "../common/common-function";

class ClassCreationForm extends React.Component{
    render(){
        
        let classSectionOptions=Utils.getClassSectionOptions()
        let teachingMediumOptions =Utils.getTeachingMediumOptions()
        let schoolOptions = Utils.emptyDropDown()
        if(this.props.currentAcademicYear != null)
         {
             schoolOptions = Utils.modelFormattedForDropdown(this.props.currentAcademicYear.schools);
        }  

        return (
            <form onSubmit={this.props.onSave}>
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Class Details</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <TextInput name="academicYear" label="Academic Year" value={this.props.currentAcademicYear.name} disabled onChange={this.props.onChange} error=""/>
                                <SelectInput name="school" label="School" value={this.props.classRoom.school} defaultOption="Select School" options={schoolOptions} onChange={this.props.onChange} error={this.props.errors.school}/>
                            </div>

                            <div class="col-md-4">
                                <TextInput name="code" label="Code" value={this.props.classRoom.code}  onChange={this.props.onChange} error={this.props.errors.code}/>
                                <TextInput name="grade" label="Grade" value={this.props.classRoom.grade}  onChange={this.props.onChange} error={this.props.errors.grade}/>
                            </div>

                            <div class="col-md-4">
                                <TextInput name="name" label="Name" value={this.props.classRoom.name}  onChange={this.props.onChange} error={this.props.errors.name}/>
                                <SelectInput name="section" label="Section" value={this.props.classRoom.section} defaultOption="Select Section" options={classSectionOptions}  onChange={this.props.onChange} error={this.props.errors.section}/>
                            </div>

                            <div class="col-md-4">
                            <SelectInput name="teachingMedium" label="Teaching Medium" value={this.props.classRoom.teachingMedium} defaultOption="Select Teaching Medium" options={teachingMediumOptions}  onChange={this.props.onChange} error={this.props.errors.teachingMedium}/>
                            </div>                           
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" disabled={this.props.saving} value={this.props.saving ? 'Saving...' : 'Save'} class="btn btn-info pull-right"/>
                    </div>
                </div>
            </form>

        );
    }
}

ClassCreationForm.propTypes = {
    classRoom: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    saving: PropTypes.bool,
    errors: PropTypes.object
};

export default ClassCreationForm;