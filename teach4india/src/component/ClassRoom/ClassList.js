import React, { Component } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';


function _setTableOption(classes){ 
  if(classes){
    return(     
        <i class="fa fa-refresh fa-spin"></i>     
    ) 
  } 
}

class ClassList extends Component {
  render(){
    const options = {
      noDataText: _setTableOption(this.props.classes)
  }
    const {classes, academicYearId} = this.props;
    return (
      <div class="box box-default">
          <div class="box-header with-border">
          <BootstrapTable data={classes} striped hover search pagination bordered scroll options={options}>
              <TableHeaderColumn width='30px' isKey dataField='id' className='bg-blue' dataSort hidden>Class ID</TableHeaderColumn>
              <TableHeaderColumn width='100px' dataField='name'className='bg-blue' dataSort>Name</TableHeaderColumn>
              <TableHeaderColumn width='50px' dataField='grade'className='bg-blue' dataSort>Grade</TableHeaderColumn>
              <TableHeaderColumn width='50px' dataField='section'className='bg-blue' dataSort>Section</TableHeaderColumn>
              <TableHeaderColumn width='50px' dataField='teachingMedium'className='bg-blue' dataSort>Teaching Medium</TableHeaderColumn>
              
          </BootstrapTable>
          </div>
      </div>
    );
  }
}

export default ClassList;
