import React, { Component } from 'react';
import classImage from '../../../node_modules/admin-lte/dist/img/photo2.png';
class ClassRoomInfoBox extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render(){
        const {classroom} = this.props;
        return (

          <section class="content">
              <div class="row">
                 <div class="col-md-3">
                  <div class="box box-primary">
                     <div class="box-body box-profile">
                       <img class="profile-user-img img-responsive img-circle" src= {classImage} alt="User profile picture"/>
                       <h3 class="profile-username text-center">{classroom.name}</h3>
                       <p class="text-muted text-center">{classroom.accademicYear}</p>
                       <p class="text-muted text-center">{classroom.grade}</p>
                       <p class="text-muted text-center">{classroom.section}</p>
                       <p class="text-muted text-center">{classroom.teachingMedium}</p>
                       <p class="text-muted text-center">{classroom.adminTeacher}</p>
                       <a href="#" class="btn btn-primary btn-block"><b>Details</b></a>
                   </div>
                 </div>
              </div>
           </div>
           </section>


        );
      }
}

export default ClassRoomInfoBox;
