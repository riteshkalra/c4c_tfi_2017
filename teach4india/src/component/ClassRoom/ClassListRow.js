import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ClassListRow extends Component {
    render(){
        const {classobj, school, academicYearId} = this.props;
        let queryStr = '';
        if (school != null) {
            queryStr = "&schoolId=" + school.id;
            if (academicYearId != null) {
                queryStr = queryStr + "&academicYearId=" + academicYearId;
            }
        }
        return (
            <tr>
                <td>{classobj.name}</td>
                <td>{classobj.grade}</td>
                <td>{classobj.section}</td>
                <td>{classobj.code}</td>
                <td>{classobj.teachingMedium}</td>
                <td>
                    <div>
                        <i className="fa fa-edit pull-middle"><Link to ={'/classRoomCreation/'+classobj.id}><span>Edit</span></Link></i>
                        <span>     </span>
                        <i className="fa fa-eye pull-middle"><Link to ={'/StudentDetailGrid/?classId='+classobj.id+queryStr}><span>Students</span></Link></i>
                    </div>
                </td>

            </tr>
        );
    };
}


export default ClassListRow;
