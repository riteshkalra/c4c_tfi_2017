import React, { Component } from 'react';
import ClassCreationForm from './ClassCreationForm';
import * as classActions from '../../actions/classActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import toastr from 'toastr';
import PropTypes from 'prop-types';
import {Class} from '../../Model/Class'
import {Admin} from '../../Model/Admin'
import * as Utils  from '../common/common-function';
import * as Validation from '../common/form-validation'

class ClassRoomCreation extends Component{
    constructor(props, context){
        super(props, context);

        this.state = {
            classRoom: Object.assign({}, props.classRoom),
            adminTeachers:props.adminTeachers,
            subjects: props.subjects,
            errors: {},
            saving: false
        };
        this.onSaveClassRoom = this.onSaveClassRoom.bind(this);
        this.onChangeComponent = this.onChangeComponent.bind(this);
    }

    onSaveClassRoom(event) {
        event.preventDefault();
        this.setState({saving: true});
        //let errors = Validation.classRoomRegistrationDataIsValid(this.state.classRoom);
        //let selectedSchool = Utils.getSchoolById(this.props.currentAcademicYear.schools, this.state.classRoom.school);
        //let selectedAdminTeacher = Utils.getTeacherById(this.state.adminTeachers, this.state.adminTeacher);
        let classObj = new Class(
                    Admin.getDataSource(), 
                    this.state.classRoom.name, 
                    this.state.classRoom.grade, 
                    this.state.classRoom.section, 
                    this.state.classRoom.code, 
                    this.state.classRoom.teachingMedium                  
                    );
                        debugger;
        let school =Utils.getSchoolById(this.props.schools,this.state.classRoom.school)
       
        this.props.actions.saveClass(classObj,school).then(() => this.redirect())
        .catch(error => {        
        toastr.error(error);
        this.setState({saving: false});
      });      
       
    }

    onChangeComponent(event) {
        const field = event.target.name;
        let classRoom = Object.assign({}, this.state.classRoom);
        if(field==="school"){
            classRoom[field] = event.target.value;
            let selectedSchool = Utils.getSchoolById(this.props.currentAcademicYear.schools, event.target.value);
            if(!Utils.isEmptyObject(selectedSchool)){
                this.setState({adminTeachers: selectedSchool.Teachers});
            }
        }else{
            classRoom[field] = event.target.value;
        }
        return this.setState({classRoom: classRoom});
     }
    redirect(){
        this.setState({saving: false});
        toastr.success('Classroom created');
        this.props.history.push('/classes');
    }

    render(){
        return(
            <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box-body">
                            <ClassCreationForm
                                onChange={this.onChangeComponent}
                                onSave={this.onSaveClassRoom}
                                classRoom={this.state.classRoom}
                                currentAcademicYear ={this.props.currentAcademicYear}
                                saving={this.state.saving}
                                schools ={this.state.schools}
                                errors={this.state.errors}
                                />
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

ClassRoomCreation.contextType ={
    router: PropTypes.object
};

function mapStateToProps(state, ownProps) {
    let classRoomId =   ownProps.match.params.id;
    let classRoom = {};
    let schools ={};
    let currentAcademicYear ={};  
     if (classRoomId && state.classes.length > 0) {
        classRoom = Utils.getClassById(state.classes, classRoomId);
    }
    if(state.currentAcademicYear!= null)
    {
        currentAcademicYear =state.currentAcademicYear;
        schools=currentAcademicYear.Schools
              
    }   
    
    return {
        classRoom: classRoom,
        schools:schools,
        currentAcademicYear:currentAcademicYear
       
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(classActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassRoomCreation);
