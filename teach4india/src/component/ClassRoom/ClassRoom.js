import React, { Component } from 'react';
import * as classAction from '../../actions/classActions';
import ClassList from './ClassList'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import * as Utils from '../common/common-function';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

class ClassRoom extends Component {

 constructor(props, context) {
    super(props, context);
    this.state = {
        classes:  props.classes,
        currentAcademicYear: props.currentAcademicYear
     };
  }

    
  render(){
    let {classes,currentAcademicYear} = this.props;     
     return (
      <section class="content ">
        <h2>Classes</h2>
          <div class="row">
            <div class="col-md-13">
              <div class="box-body">
                <ClassList classes={classes} currentAcademicYear ={currentAcademicYear} />
              </div>
            </div>
          </div>
        <div class="row ">
          <div class="col-md-2 pull-right">
            <div class="box-body">
             <Link to ="/classRoomCreation"><button type="button" class="btn btn-block btn-primary" >Add</button></Link>
            </div>
          </div>
        </div>            
      </section>    
    );
  }
}


function mapStateToProps(state, ownProps) {
  console.log("state", state.currentAcademicYear);
  let currentAcademicYear = state.currentAcademicYear || {}
  let schools =  currentAcademicYear.Schools || []
  let classes =[]
  for(let index in schools)
  {
    let classObjs = schools[index].Classes
    for(let ind in classObjs)
    { 
      classes.push(classObjs[ind]);  
    }      
  }
  return {
    currentAcademicYear: currentAcademicYear,
    classes: classes
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(classAction, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ClassRoom);
