import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'
import toastr from 'toastr';
import PropTypes from 'prop-types';
import MasteryRow from './MasteryRow'
import * as masteryActions from '../../actions/masteryActions';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';

class MasteryCreation extends Component { 
  constructor(props, context) {
    super(props, context);

    this.state = {
      question: props.question,
      masteryLevels: [Object.assign({}, props.mastery)],
      errors: {},
      saving: false
    };

    this.onSaveMastery = this.onSaveMastery.bind(this);
    this.onMasteryChange = this.onMasteryChange.bind(this);
    this.handleAddMasteryLevel = this.handleAddMasteryLevel.bind(this);
  }

  onSaveMastery(event) {
    this.setState({saving: true})    
    this.props.actions.saveMastery(this.state.mastery, this.state.question)
    this.redirect();
    event.preventDefault();
  }

  onMasteryChange = (idx) => (evt) =>{

    // TBC
    const newMasteryLevels = this.state.masteryLevels.map((masteryLevel, sidx) => {
      let mastery = masteryLevel[evt.target.name];
      if (idx !== sidx) return masteryLevel;
      return { ...masteryLevel, mastery: evt.target.value };
    });

    this.setState({ masteryLevels: newMasteryLevels });

  }

  onMarkChange = (idx) => (evt) =>{

    // TBC
    const newMasteryLevels = this.state.masteryLevels.map((masteryLevel, sidx) => {
      let mark = masteryLevel[evt.target.name];
      if (idx !== sidx) return masteryLevel;
      return { ...masteryLevel, mark: evt.target.value };
    });

    this.setState({ masteryLevels: newMasteryLevels });

  }

  handleShareholderNameChange = (idx) => (evt) => {
    const newShareholders = this.state.shareholders.map((shareholder, sidx) => {
      if (idx !== sidx) return shareholder;
      return { ...shareholder, name: evt.target.value };
    });

    this.setState({ shareholders: newShareholders });
  }

  handleAddMasteryLevel(event){
    this.setState({
      masteryLevels: this.state.masteryLevels.concat([Object.assign({}, this.props.mastery)])
    });
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('Mastery saved');
    this.props.history.push('/questions');
  }

  render(){
  
    return (         
      <div class="box box-default">

        <div class="box-header with-border"> 
          <h3 className="box-title"><b><i> Mastery Level </i></b></h3>
        </div> 

        <div className="box-body">
          {this.state.masteryLevels.map((masteryLevel, idx) => (
            <MasteryRow
             onMasteryChange={this.onMasteryChange} 
             onMarkChange={this.onMarkChange}                      
             onSave={this.onSaveMastery}
             masteryLevel={masteryLevel}
             errors={this.state.errors}
             saving={this.state.saving}
             idx={idx}
             />
          ))}
        </div>

        <div class="box-footer">
          <a className="btn btn-info pull-right" onClick={this.handleAddMasteryLevel}><i class="fa fa-plus"></i></a>  
        </div>

      </div>
    );
 	}
}


function getMasteryById(masteries, id) {
  const mastery = masteries.filter(mastery => mastery.id == id);
  if (mastery) return mastery[0]; //since filter returns an array, have to grab the first.
  return null;
}

function mapStateToProps(state, ownProps) {
  let masteryId = null;
  if(ownProps.match) masteryId = ownProps.match.params.id 
  let mastery = {id:'', question:'', mastery: '', mark: '', grade: '' };
  if (masteryId && state.masteries.length > 0) {
    mastery = getMasteryById(state.masteries, masteryId);
  }
  return {   
    mastery: mastery
  };
  
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(masteryActions, dispatch)
  };
}

MasteryCreation.contextTypes = {
  router: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(MasteryCreation);


