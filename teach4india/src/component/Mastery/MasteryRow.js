import React from 'react';
import TextInput from '../common/TextInput';
import SelectInput from '../common/SelectInput';
import PropTypes from 'prop-types';

const MasteryRow = ({masteryLevel, onSave, onMasteryChange, onMarkChange, saving, errors, idx}) => {
  return (      
    <form>
 
      <div className="row">
        <div className="col-md-8"> 
          <TextInput
            name="description"
            label="Mastery Level Description"
            value={masteryLevel.mastery}
            onChange={onMasteryChange(idx)}
            error={errors.mastery}/>
        </div>
        <div className="col-md-4">
          <TextInput
            name="mark"
            label="Mark"
            value={masteryLevel.mark}
            onChange={onMarkChange(idx)}
            error={errors.mark}/> 
        </div>
      </div>
        
    </form>
  );
};


MasteryRow.propTypes = {
  masteryLevel: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  saving: PropTypes.bool,
  errors: PropTypes.object
};

export default MasteryRow;
