import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import avatar5 from '../../../node_modules/admin-lte/dist/img/avatar5.png';

class NavigationMenu extends Component {
 

    render(){
        return (
            <aside className="main-sidebar">
                {/* sidebar: style can be found in sidebar.less */}
                <section className="sidebar" >
                    {/* Sidebar user panel */}
                    <div className="user-panel">
                        <div className="pull-left image">
                            <img src= {avatar5}  className="img-circle" alt="User " />
                        </div>
                        <div className="pull-left info">
                            <p>Karthick Arya</p>
                        </div>
                    </div>
                    
                    {/* sidebar menu: : style can be found in sidebar.less */}
                    <ul className="sidebar-menu" data-widget="tree">
                        <li><Link to ="/dashboard"><i className="fa fa-laptop"></i> <span>DashBoard</span></Link> </li>
                        
                        
                        {/*<li><Link to ="/schoolDetail"><i className="fa fa-institution"></i> <span>School Detail</span></Link> </li>   
                       
                  
                        <li><Link to ="/testpage"><i className="fa fa-book"></i> <span>TestPage</span></Link> </li>
                        */}

                        <li className="treeview">
                                    <Link to ="/classes">
                                        <i className="fa fa-table"></i> <span>Academic Year</span>
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </Link>
                                    <ul className="treeview-menu">
                                        <li><Link to ="/academicYear"><i className="fa fa-user"></i>Manage</Link></li>
                                        <li><Link to ="/academicYearRegistration"><i className="fa fa-edit"></i>Registration</Link></li>
                                    </ul>
                         </li>
                          <li className="treeview">
                                    <Link to ="/schools">
                                        <i className="fa fa-institution"></i> <span>School</span>
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </Link>
                                    <ul className="treeview-menu">
                                        <li><Link to ="/schools"><i className="fa fa-user"></i>Manage</Link></li>
                                        <li><Link to ="/schoolDetail"><i className="fa fa-edit"></i>Profile</Link></li>
                                        <li><Link to ="/schoolRegistration"><i className="fa fa-edit"></i>Registration</Link></li>
                                    </ul>
                         </li>
                        
                        <li className="treeview">
                                    <Link to ="/testpage">
                                        <i className="fa fa-book"></i> <span>Teacher</span>
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </Link>
                                    <ul className="treeview-menu">
                                        <li><Link to ="/teachers"><i className="fa fa-user"></i>Manage</Link></li>
                                        <li><Link to ="/teacherRegistration"><i className="fa fa-edit"></i>Registration</Link></li>
                                    </ul>
                         </li>
                        <li className="treeview">
                                    <Link to ="/classes">
                                        <i className="fa fa-book"></i> <span>Class</span>
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </Link>
                                    <ul className="treeview-menu">
                                        <li><Link to ="/classes"><i className="fa fa-user"></i>Manage</Link></li>
                                        <li><Link to ="/ClassRoomCreation"><i className="fa fa-edit"></i>Registration</Link></li>
                                    </ul>
                         </li>
                         <li className="treeview">
                                    <Link to ="/studentPage">
                                        <i className="fa fa-users"></i> <span>Student</span>
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </Link>
                                    <ul className="treeview-menu">

                                         <li><Link to ="/StudentDetailGrid"><i className="fa fa-user"></i>Manage</Link></li>
                                        <li><Link to ="/StudentProfile"><i className="fa fa-user"></i>Profile</Link></li>
                                        <li><Link to ="/StudentRegistration"><i className="fa fa-edit"></i>Registration</Link></li>
                                    </ul>
                         </li>
                         <li className="treeview">
                                    <Link to ="/questionsPage">
                                        <i className="fa fa-question"></i> <span>Question</span>
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </Link>
                                    <ul className="treeview-menu">
                                        <li><Link to ="/questions"><i className="fa fa-question"></i>QBank</Link></li>
                                        <li><Link to ="/subjects"><i className="fa fa-cube"></i>Subjects</Link></li>
                                        <li><Link to ="/questionCreation"><i className="fa fa-edit"></i>AddQuestion</Link></li>
                                    </ul>
                         </li>
                         <li className="treeview">
                                    <Link to ="/createExam">
                                        <i className="fa fa-pencil-square-o"></i> <span>Exams</span>
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </Link>
                                    <ul className="treeview-menu">
                                        <li><Link to ="/createExam"><i className="fa fa-file-text-o"></i>create</Link></li>
                                        <li><Link to ="/ExamDetails"><i className="fa fa-file-text-o"></i>Evaluate</Link></li>
                                    </ul>
                         </li>
                          <li>
                                   
                          </li>
                    </ul>                                       
                                      
                </section>                   
            </aside>
        );
      }

    }


export default NavigationMenu
