import React, { Component } from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import classRoom from './component/ClassRoom/ClassRoom';
import StudentDetailGrid from './component/Student-Page/StudentDetailGrid';
import SchoolsPage from './component/School/SchoolsPage';
import StudentRegistrationPage from './component/Student-Page/StudentRegistrationPage';
import Abacusdashboard from './component/admin-Page/abacus-dashboard';
import BreadCrumb from './component/Bread-Crumb/BreadCrumb';
import AcademicYear from './component/AcademicYear/AcademicYear';
import AcademicYearRegistration from './component/AcademicYear/AcademicYearRegistration';
import StudentProfile from './component/Student-Page/StudentProfile';
import SchoolRegistration from './component/School/SchoolRegistration';
import TestPage from './component/TestPage/TestPage';
import Teacher from './component/Teacher/Teacher';
import TeacherRegistration from './component/Teacher/TeacherRegistration';
import ClassRoomCreation from './component/ClassRoom/ClassRoomCreation';
import SchoolDetail from './component/School/SchoolDetail';
import AdminDashboard from './component/DashBoard/AdminDashboard';
import CreateExam from './component/Exams/CreateExam'
import QuestionsPage from './component/Question/QuestionsPage';
import SubjectsPage from './component/Subject/SubjectsPage';
import QuestionCreation from './component/Question/QuestionCreation';
import StudentRegistrationForm from './component/Student-Page/StudentRegistrationForm';
import ExamDetails from './component/ExamEvaluation/ExamDetails';
import ExamEvaluation from './component/ExamEvaluation/ExamEvaluation';

class Routes extends Component {
    render(){
		return(
				
			<Router>
				<div className ="content-wrapper">   
					<div className ="content">							
							<Route exact path="/" component={AdminDashboard}/>
							<Route  path="/" component={Abacusdashboard}/>
									
							
							<Route exact path="/academicYear" component={AcademicYear}/>
							<Route exact path ="/academicYearRegistration" component={AcademicYearRegistration}/>
							<Route exact path ="/academicYearRegistration/:id" component={AcademicYearRegistration}/>

							<Route exact path="/classes" component={classRoom}/>
							<Route exact path ="/ClassRoomCreation" component={ClassRoomCreation}/>
							<Route exact path ="/ClassRoomCreation/:id" component={ClassRoomCreation}/>

							<Route exact path="/StudentDetailGrid" component={StudentDetailGrid}/> 
							
							<Route exact path="/StudentProfile" component={StudentProfile}/> 
							<Route exact path="/StudentProfile/:id" component={StudentProfile}/> 
							<Route exact path="/StudentRegistration" component={StudentRegistrationPage}/> 
							<Route exact path="/StudentRegistration/:id" component={StudentRegistrationPage}/> 

							<Route exact path="/schools" component={SchoolsPage}/> 
							<Route exact path="/schoolRegistration" component={SchoolRegistration}/> 
							<Route exact path="/schoolRegistration/:id" component={SchoolRegistration}/> 
							<Route exact path="/schoolDetail" component={SchoolDetail}/> 
							<Route exact path="/schoolDetail/:id" component={SchoolDetail}/> 
							
							<Route exact path="/teachers" component={Teacher}/>
							<Route path ="/teacherRegistration" component={TeacherRegistration}/>

							<Route exact path="/createExam" component={CreateExam}/>
							<Route exact path="/createExam/:id" component={CreateExam}/>
							<Route exact path="/ExamDetails" component={ExamDetails}/>
							<Route exact path="/ExamDetails/:id" component={ExamDetails}/>
							<Route exact path="/ExamEvaluation" component={ExamEvaluation}/>
							
							
							<Route exact path="/questions" component={QuestionsPage}/>
							<Route exact path ="/questionCreation" component={QuestionCreation}/>
							<Route exact path ="/questionCreation/:id" component={QuestionCreation}/>

							<Route exact path="/subjects" component={SubjectsPage}/>

							<Route exact path="/testpage" component={TestPage}/>
						
				  </div>
				</div> 
				
			</Router>

		);
	}
}

export default Routes;