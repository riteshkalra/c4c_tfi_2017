import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Admin} from '../Model/Admin'
import {Question} from '../Model/Question'
import {Mastery} from '../Model/Mastery'

export function loadQuestions() {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return Admin.getAllQuestions().then(questions => {
            dispatch(_loadQuestions(questions))
         }).catch(error => {
      throw(error);
    });
  };
}

export function _loadQuestions(questions) {
  return {
    type: types.LOAD_QUESTIONS_SUCCESS,
    questions
  }
}

export function saveQuestion(question) {
  console.log('saveQuestion', question)
  return function (dispatch) {
    dispatch(beginAjaxCall());
    return question.save().then(question => {
      console.log('calling _saveQuestion')
      dispatch(_saveQuestion(question))
    }).catch(error => {
      console.log('caught error', error)
      dispatch(ajaxCallError(error));
      throw(error);
    });
      
  }

}

export function _saveQuestion(question) {
  console.log('_saveQuestion', question)
  return {
    type: types.SAVE_QUESTION_SUCCESS,
    question
  }

} 
