import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Exam} from '../Model/Exam'
import {Question} from '../Model/Question'

export function loadExams() {
	let exam = new Exam(null, 'Semester 1', new Date(), '50', 'D', '1A', 'English', "123");
	exam.questions = [];
	let exams = [exam];
  return {
    type: types.LOAD_EXAMS_SUCCESS,
    exams
  }
}

export function saveExam(exam) {
  return {
      type: types.SAVE_EXAM_SUCCESS,
      exam
    }
}