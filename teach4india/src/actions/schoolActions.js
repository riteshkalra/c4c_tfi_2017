import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Admin} from '../Model/Admin'

export function _loadSchools(schools) {
   return {
    type: types.LOAD_SCHOOLS_SUCCESS,
    schools
  }
}

export function loadSchoolForAcademicYear(academicYear)
{
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    console.log("loading schools");
    return dispatch(_loadSchools(academicYear.Schools));

    // .then(schools => {      
    //     console.log("dispatching schools", schools);
    //     dispatch(_loadSchools(schools));
    // }).catch(error => {
    //   dispatch(ajaxCallError(error));
    //   throw(error);
    // });
  };
}


export function saveSchool(school,academicYear) {
  
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return school.save(academicYear).then(school => {       
      dispatch(_saveSchool(school));
        }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}


export function _saveSchool(school) {
    return {
        type: types.SAVE_SCHOOL_SUCCESS,
    school
  }

} 
