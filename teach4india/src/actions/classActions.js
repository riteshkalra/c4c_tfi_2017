import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';

import {Admin} from '../Model/Admin'


export function loadClasses() {
    return function (dispatch) {
        return Admin.getClasses().then(classes => {
            dispatch(_loadClasses(classes))
        })
    }
}

export function _loadClasses(classes) {
   return {
    type: types.LOAD_CLASSES_SUCCESS,
    classes
  }
}

export function saveClass(classObj,school) {
  
    return function (dispatch, getState) {
      dispatch(beginAjaxCall());
      return classObj.save(school).then(classObj => {       
        dispatch(_saveClassObj(classObj));
          }).catch(error => {
        dispatch(ajaxCallError(error));
        throw(error);
      });
    };
  }

export function _saveClassObj(classRoom){

    return {
        type: types.SAVE_CLASS_ROOM_SUCCESS,
        classRoom
    }
}

