import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Exam} from '../Model/Exam'
import {Question} from '../Model/Question'
import {Admin} from '../Model/Admin'
import {Mastery} from '../Model/Mastery'


export function loadCompletedExams(dataSource) {
    return function (dispatch) {
        return dataSource.getExamObjects(['2VizzCf4d0yAwpNDxMIK','examRandomId1']).then(completedExams => {
            dispatch(_loadCompletedExams(completedExams))
        })
    }
}

export function _loadCompletedExams(completedExams) {  
    return {
      type: types.LOAD_COMPLETED_EXAM_SUCCESS,
      completedExams
    } 
}

export function viewSelectedExam(selectedExam) {
      return dispatch => {
        dispatch(_viewSelectedExam(selectedExam))
    }
}

export function _viewSelectedExam(selectedExam) {
       return {
        type: types.VIEW_SELECTED_EXAM_ID_SUCCESS,
        selectedExam 
         }
}  
export function loadExamQuestions(exam) {
   return function (dispatch) {
        return exam._questions().then(examQuestions => {
            console.log("examquestions", examQuestions);
            dispatch(_loadExamQuestions(examQuestions))
           
        })
    }
}

export function _loadExamQuestions(examQuestions) {  
    return {
      type: types.LOAD_EXAM_QUESTIONS_SUCCESS,
      examQuestions
    } 
}

export function _loadMasteryForQuestion(masteryLevels) { 
    return {
      type: types.LOAD_MASTERY_FOR_QUESTION_SUCCESS,
      masteryLevels
    } 
}

export function saveStudentExamResult(examResult) {
   return function (dispatch) {

        return examResult.save().then(examResult => {
            console.log("examResult", examResult);
            dispatch(_saveStudentExamResult(examResult))
           
        })
    }
}


export function _saveStudentExamResult(examResult) { 
    return {
      type: types.SAVE_EXAMRESULT_SUCCESS,
      examResult
    } 
}

export function loadMasteryForQuestion(question) {
   return function (dispatch) {
          

        return question._masteryLevels().then(masteryLevels => {
            dispatch(_loadMasteryForQuestion(masteryLevels))
        })
    }
}




