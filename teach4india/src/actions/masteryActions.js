import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Admin} from '../Model/Admin'
import {MasteryLevel} from '../Model/Mastery'

export function loadMasteries() {
  
  let masteries = [new MasteryLevel(null, null, 'master description', 100, 'F', null)]

  console.log("load masteries", masteries);
   return {
    type: types.LOAD_MASTERIES_SUCCESS,
    masteries
  }
}

export function saveMastery(mastery, question) {
    console.log("save mastery", mastery);
    Admin.saveMastery(mastery, question);
    
    return {
        type: types.SAVE_MASTERY_SUCCESS,
    mastery
  }

} 
