import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Admin} from '../Model/Admin'

export function loadSubjects() {
  return function(dispatch){
    dispatch(beginAjaxCall());
    return Admin.getSubjects().then(subjects =>{
        dispatch(_loadSubject(subjects))
    })
  }
}

export function _loadSubject(subjects){
    return {
        type: types.LOAD_SUBJECTS_SUCCESS,
        subjects
    }
}

export function addSubject(subject) {    
    return {
        type: types.SAVE_SUBJECT_SUCCESS,
    subject
  }

} 
