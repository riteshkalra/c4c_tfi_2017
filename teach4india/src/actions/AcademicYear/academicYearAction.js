import * as types from '../actionTypes';
import {Admin} from '../../Model/Admin'
import {beginAjaxCall, ajaxCallError} from '../ajaxStatusActions';


export function loadAcademicYears() {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return Admin.getAcademicYears().then(academicYears => {
            dispatch(_loadAcademicYears(academicYears))
         }).catch(error => {
      throw(error);
    });
  };
}


export function loadCurrentAcademicYear()
{   
  return function (dispatch) {   
    return Admin.getCurrentAcademicYear().then(currentAcademicYear => {      
        dispatch(_loadCurrentAcademicYear(currentAcademicYear))  
    });
    };
  }

export function _loadCurrentAcademicYear(currentAcademicYear) {
    return {
        type: types.LOAD_ACADEMIC_YEAR_SUCCESS,
        currentAcademicYear
    }
}

export function _loadAcademicYears(academicYears) {
    return {
        type: types.LOAD_ACADEMIC_YEARS_SUCCESS,
        academicYears
    }
}

export function saveAcademicYear(academicYear) {
    return function (dispatch) {
        if (academicYear.id) {
            return academicYear.update().then(acadYear => {
                dispatch(_editAcademicYear(acadYear))
            })
        } else {
            return academicYear.save().then(acadYear => {
                dispatch(_saveAcademicYear(acadYear))
            })
        }
    }
}

export function _editAcademicYear(academicYear) {
    return {
        type: types.EDIT_ACADEMIC_YEARS_SUCCESS,
        academicYear
    }
}

export function _saveAcademicYear(academicYear) {
    return {
        type: types.SAVE_ACADEMIC_YEARS_SUCCESS,
        academicYear
    }
}

export function viewAcademicYear(academicYear) {
    let schools = academicYear.Schools
    return {
        type: types.LOAD_SCHOOLS_SUCCESS,
        schools
    }

} 


