import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Admin} from '../Model/Admin'

export function loadStudentsForAcademicYear(academicYear) {
  
  console.log("calling load student");
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return academicYear.Schools().then(schools => {      
        console.log("schools->", schools);
        for(let index in schools) {
          let sch = schools[index];
          sch.Students().then(students => {
            dispatch(_loadStudents(students));
          })
        }
    }).catch(error => {
      dispatch(ajaxCallError(error));
      throw(error);
    });
  };
}

export function _loadStudents(students) {
   return {
    type: types.LOAD_STUDENTS_SUCCESS,
    students
  }
}

export function loadStudent() {
  let academicYear = Admin.getAcademicYears()[0]
  let school = academicYear.Schools[0]
  let classObj = school.Classes[0]
  let student = classObj.Students[0]

   return {
    type: types.LOAD_STUDENT_SUCCESS,
    student
  }
}
export function saveStudent(student,classObj) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return student.save(classObj).then(student => {     
     dispatch(_saveStudent(student));
        }).catch(error => {
        dispatch(ajaxCallError(error));
        throw(error);
      });
    };
  }

export function _saveStudent(student) {

    return {
        type: types.CREATE_STUDENT_SUCCESS,
        student
      }
}


