import * as types from './actionTypes';
import {beginAjaxCall, ajaxCallError} from './ajaxStatusActions';
import {Teacher} from '../Model/Teacher'
import {Admin} from '../Model/Admin'


export function loadTeachers() {
    return function (dispatch) {
        return Admin.getTeachers().then(teachers => {
            dispatch(_loadTeachers(teachers))
        })
    }
}

export function _loadTeachers(teachers) {
  teachers = [new Teacher(null, "Teacher First Name", "Teacher Last Name", "Teacher empId", "male", "DOB", "address", "image", "phone1", "phone2", "email", "teacher_id")]

   return {
    type: types.LOAD_TEACHERS_SUCCESS,
    teachers
  }
}

export function _loadTeacher(teachers){
    return {
        type: types.LOAD_TEACHERS_SUCCESS,
        teachers
    }
}

export function saveTeacher(teacher,classObj) {
  return function (dispatch, getState) {
    dispatch(beginAjaxCall());
    return teacher.save(classObj).then(teacher => {     
     dispatch(_saveTeacher(teacher));
        }).catch(error => {
        dispatch(ajaxCallError(error));
        throw(error);
      });
    };
  }

export function _saveTeacher(teacher) {
    return {
        type: types.SAVE_TEACHER_SUCCESS,
        teacher
    }
}
