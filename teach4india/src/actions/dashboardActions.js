import * as types from './actionTypes';

export function setAcademicYear(academicYear) {
    return dispatch => {
        dispatch(_setAcademicYear(academicYear))
    }
}

export function _setAcademicYear(academicYear) {
    return {
        type: types.SET_ACADEMIC_YEAR_SUCCESS,
        academicYear
    }
}