class SQLDataSource{
    constructor(){

    }

    // Academic Year related methods

    getAcademicYears(){
        // Get all academic years
    }

    getCurrentAcademicYear(){
        // Get the currently running academic year
    }

    getPrevAcademicYear(acadYear){
        // Return an instance of AcademicYear object
    }

    getSchools(acadYear){

    }

    addAcademicYear(acadYear) {
        // add to t4i.academicyears
    }


    // School related methos


    getClassesForAcademicYear(acadYear, school){
        // Get all classes for the given school in the given academic year
    }

    getTeachersForAcademicYear(acadYear, school){
        // Get all teachers in this school for the given academic year
    }

    addSchool(school){
        // add to t4i.schools
    }

    addClassToSchool(acadYear, school, cls){
        // add to t4i.school_classes
    }

    addTeacher(teacher){
        // add to t4i.teachers
    }

    addTeacherToSchool(school, teacher){
        // add to t4i.school_teachers
    }

    removeTeacherFromSchool(acadYear, school, teacher){
        // update t4i.school_teachers
        // update t4i.class_teachers
        // update t4i.class_subject_teachers
    }


    // Class related methods

    addClass(cls){
        // add to t4i.classes
    }

    updateClass(cls){
        // update t4i.classes
    }

    getSubjectsForClass(cls){
        // Get subjects for the given class
    }

    getTeachersForClass(cls){
        // Get all teachers for the given class
    }

    getStudentsFromClass(cls){
        // Get all students for the given class
    }

    getAdminTeacher(cls){
        // Get admin teacher for class
    }

    getStudentsBySubject(cls, students, subject){
        studentIds = [] // Get this fro DB
        studentsBySubject = [];
        for(i=0;i<students.length; i++){
            student = students[i];
            if(studentIds.indexOf(student.Id) != -1){
                studentsBySubject.push(student);
            }
        }
        return studentsBySubject;
    }

    addAdminTeacher(acadYear, school, cls, teacher){
        // add to t4i.class_teachers
    }

    addSubject(subject){
        // add to t4i.subjects
    }

    updateSubject(subject){
        // Update t4i.subjects
    }

    removeSubject(subject){
        // Remove from t4i.subjects
    }

    addSubjectToClass(cls, subject, isMandatory){
        // add to t4i.class_subjects
    }

    removeSubjectFromClass(cls, subject){
        // Remove from t4i.class_subjects
        // If no other class is using it then remove from t4i.subjects
    }

    addTeacherToSubject(cls, teacher, subject){
        // add to t4i.class_subject_teachers
    }

    removeTeacherForSubject(cls, teacher, subject){
        // remove from t4i.class_subject_teachers
    }

    addStudentToClass(cls, student){
        // add to t4i.class_student_subjects
    }

    removeStudentFromClass(cls, student){
        // update t4i.class_students
    }

    addStudentToSubject(cls, student, subject){
        // add to t4i.class_student_subjects
    }

    removeStudentFromSubject(cls, student, subject){
        // remove from t4i.class_student_subjects
    }


    // Student related methods

    addStudent(student){
        // add to t4i.students
    }

    addContactsToStudent(student, contacts){
        // add to t4i.student_contacts
    }
    getSchoolForStudent(acadYear, student){
        // Get the current school for the given student
    }

    getClassForStudent(acadYear, school, student){
        // Get the current class for the student
    }

    getSubjectsForStudent(acadYear, school, cls, student){
        //  Get Subjects from the class for the given student
    }

    getExamsForStudent(acadYear, cls, student){
        // Return all exams for the given student in the given academic year
    }

    getSubjectExamsForStudent(acadYear, cls, student, subject){
        // Return all exams for the given student in the given academic year for the given subject
    }


    // Contact related methods

    getContacts(entity, contact){
        // Read from t4i.contacts
    }

    addContact(entity, contact){
        // Add to t4i.contacts
    }

    updateContact(entity, contact){
        // Update t4i.contacts
    }



    // Exam related methods

    addQuestion(question){
        // Add to t4i.questions
    }

    updateQuestion(question){
        // Update t4i.questions
    }

    removeQuestion(question){
        // Remove from t4i.questions
    }

    getQuestions(subject){
        // Return a list of Question objects
    }

    addMastery(mastery){
        // Add to t4i.question_mastery
    }

    removeMastery(mastery){
        // Remove from t4i.question_mastery
    }

    updateMastery(mastery){
        // Update t4i.questions_mastery
    }

    getMastery(question){
        // Return a list of Mastery objects from t4i.questions_mastery
    }


    addExam(cls, exam){
        // Add to t4i.exams
    }

    removeExam(exam){
        // Remove from t4i.exams
    }

    updateExam(exam){
        // Update t4i.exams
    }

    getExamsForSubject(cls, subject){
        //
    }

    addQuestionToExam(exam, question){
        // Add to t4i.exam_questions
    }

    removeQuestionFromExam(exam, question){
        // Remove from t4i.exam_questions
    }

    getExamQuestions(exam){
        // Read from t4i.exam_questions
    }

    getExamResults(student, exam){
        // Return an instance of ExamResult object
    }

    addExamAnswer(exam, student, question, answer, mastery){
        // Add to t4i.exam_results
    }

    updateExamAnswer(exam, student, question, answer, mastery){
        // Update t4i.exam_results
    }
}