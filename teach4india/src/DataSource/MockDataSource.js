import {School} from "../Model/School.js";
import {AcademicYear} from "../Model/AcademicYear.js";
import {Address} from "../Model/Address.js";
import {Class} from "../Model/Class.js";
import {Contact} from "../Model/Contact.js";
import {Exam} from "../Model/Exam.js";
import {ExamResult} from "../Model/ExamResult.js";
import {MasteryLevel} from "../Model/Mastery.js";
import {Question} from "../Model/Question.js";
import {Student} from "../Model/Student.js";
import {Subject} from "../Model/Subject.js";
import {Teacher} from "../Model/Teacher.js";
import {Term} from "../Model/Term.js";

export class MockDataSource{

    constructor(){
        this.initializeAcademicYears();
        this.initializeSchools();
        this.initializeClasses();
        this.initializeStudents();
        this.initializeStudentsBySchool();
        this.initializeStudentsByClass();
        this.initializeTeachers();
        this.initializeTeachersBySchool();
        this.initializeTeachersByClass();
        this.initializeSubjects();
        this.initializeSubjectsByClass();
        this.initializeTeachersBySubject();
        this.initializeSubjectsByStudent();
        this.initializeQuestions();
        this.initializeMasteryLevels();
        this.initializeExams();
        this.initializeExamQuestions();
        this.initializeExamResults();
    }

    toDate(val){
        return val != null ? new Date(val) : null;
    }

    getUniqueId() {
        return Math.random().toString(36).substr(2, 16);
    }

    initializeAcademicYears(){
        this.academicYears = new Map();
        let academicYearsObj = require("./Mock/academicyears.json");
        for(let academicYearId in academicYearsObj){
            let mapping = academicYearsObj[academicYearId];
            this.academicYears.set(academicYearId, AcademicYear.fromJSON(this, academicYearId, mapping));
        }
    }

    initializeContacts(entityName, entityId){
        let contacts = [];
        let jsonObj = require("./Mock/contacts.json");
        for(let contactId in jsonObj){
            let mapping = jsonObj[contactId];
            if (mapping["entityName"] == entityName && mapping["entityId"] == entityId){
                contacts.push(Contact.fromJSON(this, contactId, mapping));
            }
        }
        return contacts;
    }

    initializeSchools(){
        this.schools = new Map();
        this.schoolContacts = new Map();
        this.holidaysBySchool = new Map();
        let jsonObj = require("./Mock/schools.json");
        for(let schoolId in jsonObj){
            let mapping = jsonObj[schoolId];
            let school = School.fromJSON(this, schoolId, mapping);
            this.schools.set(schoolId, school);
            this.schoolContacts.set(school.Id, this.initializeContacts("School", schoolId));
            let holidays = [];
            this.holidaysBySchool.set(school, holidays);
            for(let date of mapping["holidays"] || []){
                holidays.push(this.toDate(date));
            }
        }

        this.schoolsByAcademicYear = new Map();
        jsonObj = require("./Mock/academicyear_schools.json");
        for(let academicYearId in jsonObj){
            for(let i=0; i < jsonObj[academicYearId].length; i++) {
                let schoolId = jsonObj[academicYearId][i];
                let academicYear = this.academicYears.get(academicYearId);
                let school = this.schools.get(schoolId);
                if (!this.schoolsByAcademicYear.has(academicYearId)) {
                    this.schoolsByAcademicYear.set(academicYear.Id, [])
                }
                this.schoolsByAcademicYear.get(academicYear.Id).push(school.Id);
            }
        }
    }

    initializeClasses(){
        this.classes = new Map();
        this.termsByClass = new Map();
        let jsonObj = require("./Mock/classes.json");
        for(let classId in jsonObj){
            let mapping = jsonObj[classId];
            let cls = Class.fromJSON(this, classId, mapping);
            this.classes.set(classId, cls);
            let terms = [];
            this.termsByClass.set(cls, terms);
            for(let term in cls["terms"]){
                terms.push(Term.fromJSON(term));
            }
        }
    }

    initializeStudents() {
        let jsonObj = require("./Mock/students.json");
        this.students = new Map();
        this.studentContacts = new Map();
        for (let studentId in jsonObj) {
            let mapping = jsonObj[studentId];
            let student = Student.fromJSON(this, studentId, mapping);
            this.students.set(studentId, student);
            this.studentContacts.set(student.Id, this.initializeContacts("Student", studentId));
        }
    }

    initializeStudentsBySchool(){
        let jsonObj = require("./Mock/school_students.json");
        this.studentsBySchool = new Map();
        for(let schoolId in jsonObj){
            let mappings = jsonObj[schoolId];
            if(this.schools.has(schoolId)) {
                let school = this.schools.get(schoolId);
                if(!this.studentsBySchool.has(schoolId)){
                    this.studentsBySchool.set(schoolId, []);
                }
                for(let mapping of mappings) {
                    let studentId = mapping["student"];
                    if (this.students.has(studentId)) {
                        this.studentsBySchool.get(schoolId).push({
                            "student": this.students.get(studentId),
                            "startDate": this.toDate(mapping["startDate"]),
                            "endDate": this.toDate(mapping["endDate"])
                        });
                    }
                }
            }
        }
    }

    initializeStudentsByClass() {
        let jsonObj = require("./Mock/class_students.json");
        this.studentsByClass = new Map();
        for (let classId in jsonObj) {
            let mappings = jsonObj[classId];
            if (!this.classes.has(classId)) continue;
            let cls = this.classes.get(classId);
            if (!this.studentsByClass.has(cls.Id)) {
                this.studentsByClass.set(cls.Id, []);
            }
            for (let studentId in mappings) {
                let mapping = mappings[studentId];
                if (this.students.has(studentId)) {
                    this.studentsByClass.get(cls.Id).push({
                        "student": this.students.get(studentId),
                        "startDate": this.toDate(mapping["startDate"]),
                        "endDate": this.toDate(mapping["endDate"])
                    });
                }
            }
        }
    }

    initializeTeachers() {
        let jsonObj = require("./Mock/teachers.json");
        this.teachers = new Map();
        this.teacherContacts = new Map();
        for (let teacherId in jsonObj) {
            let mapping = jsonObj[teacherId];
            let teacher = Teacher.fromJSON(this, teacherId, mapping);
            this.teachers.set(teacherId, teacher);
            this.teacherContacts.set(teacherId, this.initializeContacts("Teacher", teacherId));
        }
    }

    initializeTeachersBySchool(){
        let jsonObj = require("./Mock/school_teachers.json");
        this.teachersBySchool = new Map();
        for(let schoolId in jsonObj){
            let mappings = jsonObj[schoolId];
            if(this.schools.has(schoolId)) {
                let school = this.schools.get(schoolId);
                if(!this.teachersBySchool.has(schoolId)){
                    this.teachersBySchool.set(schoolId, []);
                }
                for(let mapping of mappings) {
                    let teacherId = mapping["teacher"];
                    if (this.teachers.has(teacherId)) {
                        let teacher = this.teachers.get(teacherId);
                        this.teachersBySchool.get(schoolId).push({
                            "teacher": this.teachers.get(teacherId),
                            "startDate": this.toDate(mapping["startDate"]),
                            "endDate": this.toDate(mapping["endDate"])
                        });
                    }
                }
            }
        }
    }

    initializeTeachersByClass() {
        let jsonObj = require("./Mock/class_teachers.json");
        this.teachersByClass = new Map();
        for (let classId in jsonObj) {
            let mappings = jsonObj[classId];
            if (!this.classes.has(classId)) continue;
            let cls = this.classes.get(classId);
            if (!this.teachersByClass.has(classId)) {
                this.teachersByClass.set(classId, []);
            }
            for (let mapping of mappings) {
                let teacherId = mapping["teacher"];
                if (this.teachers.has(teacherId)) {
                    this.teachersByClass.get(classId).push({
                        "teacher": this.teachers.get(teacherId),
                        "startDate": this.toDate(mapping["startDate"]),
                        "endDate": this.toDate(mapping["endDate"])
                    });
                }
            }
        }
    }

    initializeSubjects() {
        let jsonObj = require("./Mock/subjects.json");
        this.subjects = new Map();
        for (let subjectId in jsonObj) {
            let mapping = jsonObj[subjectId];
            this.subjects.set(subjectId, Subject.fromJSON(this, subjectId, mapping));
        }
    }

    initializeSubjectsByClass() {
        let jsonObj = require("./Mock/class_subjects.json");
        this.subjectsByClass = new Map();
        for (let classId in jsonObj) {
            let subjects = jsonObj[classId];
            if (!this.classes.has(classId)) continue;
            let cls = this.classes.get(classId);
            if (!this.subjectsByClass.has(classId)) {
                this.subjectsByClass.set(classId, []);
            }
            for (let subjectId of subjects) {
                if (!this.subjects.has(subjectId)) continue;
                let subject = this.subjects.get(subjectId);
                this.subjectsByClass.get(classId).push(subject.Id);
            }
        }
    }

    initializeTeachersBySubject() {
        let jsonObj = require("./Mock/class_subject_teachers.json");
        this.teachersByClassBySubject = new Map();
        for (let classId in jsonObj) {
            let subjects = jsonObj[classId];
            if (!this.classes.has(classId)) continue;
            let cls = this.classes.get(classId);
            if (!this.teachersByClassBySubject.has(classId)) {
                this.teachersByClassBySubject.set(classId, new Map());
            }
            for (let subjectId in subjects) {
                let mappings = subjects[subjectId];
                if (!this.subjects.has(subjectId)) continue;
                let subject = this.subjects.get(subjectId);
                if (!this.teachersByClassBySubject.get(classId).has(subjectId)) {
                    this.teachersByClassBySubject.get(classId).set(subjectId, []);
                }
                for (let mapping of mappings) {
                    let teacherId = mapping["teacher"];
                    if (this.teachers.has(teacherId)) {
                        this.teachersByClassBySubject.get(classId).get(subjectId).push({
                            "teacher": this.teachers.get(teacherId),
                            "startDate": this.toDate(mapping["startDate"]),
                            "endDate": this.toDate(mapping["endDate"])
                        });
                    }
                }
            }
        }
    }

    initializeSubjectsByStudent() {
        let jsonObj = require("./Mock/class_student_subjects.json");
        this.subjectsByClassByStudent = new Map();
        for (let classId in jsonObj) {
            if (!this.classes.has(classId)) continue;
            let cls = this.classes.get(classId);
            if (!this.subjectsByClassByStudent.has(classId)) {
                this.subjectsByClassByStudent.set(classId, new Map());
            }
            let subjects = jsonObj[classId];
            for (let subjectId in subjects) {
                let studentIds = subjects[subjectId];
                if (!this.subjects.has(subjectId)) continue;
                let subject = this.subjects.get(subjectId);
                if (!this.subjectsByClassByStudent.get(classId).has(subjectId)) {
                    this.subjectsByClassByStudent.get(classId).set(subjectId, []);
                }
                for (let studentId of studentIds) {
                    if (this.students.has(studentId)) {
                        this.subjectsByClassByStudent.get(classId).get(subjectId).push(this.students.get(studentId));
                    }
                }
            }
        }
    }

    initializeQuestions() {
        let jsonObj = require("./Mock/questions.json");
        this.questions = new Map();
        this.questionsBySubject = new Map();
        for (let subjectId in jsonObj) {
            let mappings = jsonObj[subjectId];
            if (this.subjects.has(subjectId)) {
                let subject = this.subjects.get(subjectId);
                if (!this.questionsBySubject.has(subjectId)) {
                    this.questionsBySubject.set(subjectId, []);
                }
                for (let questionId in mappings) {
                    let mapping = mappings[questionId];
                    let question = Question.fromJSON(this, questionId, mapping);
                    this.questions.set(questionId, question);
                    this.questionsBySubject.get(subjectId).push(question);
                }
            }
        }
    }

    initializeMasteryLevels(){
        let jsonObj = require("./Mock/question_masteries.json");
        this.masteries = new Map();
        this.masteriesByQuestion = new Map();
        for (let questionId in jsonObj) {
            let mappings = jsonObj[questionId];
            if (this.questions.has(questionId)) {
                let question = this.questions.get(questionId);
                if (!this.masteriesByQuestion.has(questionId)) {
                    this.masteriesByQuestion.set(questionId, []);
                }
                for (let masteryId in mappings) {
                    let mapping = mappings[masteryId];
                    let mastery = MasteryLevel.fromJSON(this, masteryId, mapping);
                    this.masteries.set(masteryId, mastery);
                    this.masteriesByQuestion.get(questionId).push(mastery);
                }
            }
        }
    }

    initializeExams() {
        let jsonObj = require("./Mock/exams.json");
        this.exams = new Map();
        this.examsByClassBySubject = new Map();
        for (let examId in jsonObj) {
            let mapping = jsonObj[examId];
            let clsId = mapping["class"];
            let subjectId = mapping["subject"];
            if (this.classes.has(clsId) && this.subjects.has(subjectId)) {
                let cls = this.classes.get(clsId);
                let subject = this.subjects.get(subjectId);
                if (!this.examsByClassBySubject.has(clsId)) {
                    this.examsByClassBySubject.set(clsId, new Map());
                }
                if (!this.examsByClassBySubject.get(clsId).has(subjectId)) {
                    this.examsByClassBySubject.get(clsId).set(subjectId, []);
                }
                let exam = Exam.fromJSON(this, examId, mapping);
                this.exams.set(examId, exam);
                this.examsByClassBySubject.get(clsId).get(subjectId).push(exam.Id);
            }
        }
    }

    initializeExamQuestions(){
        let jsonObj = require("./Mock/exam_questions.json");
        this.examQuestions = new Map();
        for (let examId in jsonObj) {
            let questionIds = jsonObj[examId];
            if (this.exams.has(examId)) {
                let exam = this.exams.get(examId);
                if (!this.examQuestions.has(examId)) {
                    this.examQuestions.set(examId, []);
                }
                for (let questionId of questionIds) {
                    if (this.questions.has(questionId)) {
                        this.examQuestions.get(examId).push(questionId);
                    }
                }
            }
        }
    }

    initializeExamResults() {
        let jsonObj = require("./Mock/exam_answers.json");
        this.examsByStudent = new Map();
        for (let examId in jsonObj) {
            let mappings = jsonObj[examId];
            if (!this.exams.has(examId)) continue;
            let exam = this.exams.get(examId);
            for (let studentId in mappings) {
                if (!this.students.has(studentId)) continue;
                let answers = mappings[studentId];
                let student = this.students[studentId];
                if (!this.examsByStudent.has(examId)) {
                    this.examsByStudent.set(examId, new Map());
                }
                if (!this.examsByStudent.get(examId).has(studentId)) {
                    this.examsByStudent.get(examId).set(studentId, new Map());
                }
                for (let questionId in answers) {
                    if (!this.questions.has(questionId)) continue;
                    let mapping = answers[questionId];
                    let masteryId = mapping["mastery"];
                    let question = this.questions.get(questionId);
                    if (this.masteries.has(masteryId)) {
                        this.examsByStudent.get(examId).get(studentId).set(question, {
                            "answer": mapping["answer"],
                            "mastery": this.masteries.get(masteryId)
                        });
                    }
                }
            }
        }
    }


    getAcademicYears(){
        return Array.from(this.academicYears.values());
    }

    getAcademicYear(academicYearId){
        return this.academicYears.get(academicYearId);
    }

 

    getSchools(){
        return this.schools.values();
    }

    getSchool(schoolId){
        return this.schools.get(schoolId);
    }

    getClasses(){
        return this.classes.values();
    }

    getClass(classId){
        return this.classes.get(classId);
    }

    getTeachers(){
        return this.teachers.values();
    }

    getTeacher(teacherId){
        return this.teachers.get(teacherId);
    }

    getStudents(){
        return this.students.values();
    }

    getStudent(studentId){
        return this.students.get(studentId);
    }

    getSubjects(){
        return Array.from(this.subjects.values());
    }

    getSubject(subjectId){
        return this.subjects.get(subjectId);
    }

    getQuestion(questionId){
        return this.questions.get(questionId);
    }

    getExams(){
        return this.exams.values();
    }

    getExam(examId){
        return this.exams.get(examId);
    }


    // AcademicYear related

    getCurrentAcademicYear(){
        // Get the currently running academic year
        let academicYears = Array.from(this.academicYears.values());
        // May be order them by start dates
        return academicYears[academicYears.length - 1];
    }

    getPreviousAcademicYear(academicYear) {
        // Return an instance of AcademicYear object
        let academicYears = this.getAcademicYears();
        for (let index = 0; index < academicYears.length; index++) {
            if (academicYears[index].Id == academicYear.Id) {
                return academicYears[index - 1];
            }
        }
    }

    getSchoolsForAcademicYear(academicYear){
        let schools = [];
        if(this.schoolsByAcademicYear.has(academicYear.Id)){
            let schoolIds = this.schoolsByAcademicYear.get(academicYear.Id);
            for(let i=0; i<schoolIds.length; i++){
                let schoolId = schoolIds[i];
                schools.push(this.schools.get(schoolId));
            }
        }
        return schools;
    }

    addAcademicYear(academicYear) {
        let newId = this.getUniqueId();
        academicYear.id=newId;
        this.academicYears.set(newId, academicYear);
    }

    saveSchool(school){
        if(school.id){
            this.schools.set(school.id, school);
        }else{
            this.addSchool(school);
        }
    }
    
    addSchool(school) {
        let newId = this.getUniqueId();
        school.id=newId;
        this.schools.set(newId, school);
    }

    addSchoolToAcademicYear(academicYear, school){
        if(this.academicYears.has(academicYear.Id)){
            if(this.schools.has(school.Id)){
                if(!this.schoolsByAcademicYear.has(academicYear.Id)){
                    this.schoolsByAcademicYear.set(academicYear.Id, []);
                }
                this.schoolsByAcademicYear.get(academicYear.Id).push(school.Id);
            }
        }
    }


    // School related methods

    getContactsForSchool(school){
        return this.schoolContacts.get(school.Id);
    }

    addSchoolContact(school, contact){
        contact.setId(this.getUniqueId());
        if(!this.schoolContacts.has(school.Id)){
            this.schoolContacts.set(school.Id, []);
        }
        this.schoolContacts.get(school.Id).push(contact);
    }

    getHolidays(school){
        return this.holidaysBySchool.get(school);
    }

    addHoliday(school, date){
        if(!this.holidaysBySchool.has(school)){
            this.holidaysBySchool.set(school, []);
        }
        if(!this.holidaysBySchool.get(school).includes(date)){
            this.holidaysBySchool.get(school).push(date);
        }
    }

    getAcademicYearsForSchool(school){
        let academicYears = [];
        for(let [academicYearId, schoolIds] of this.schoolsByAcademicYear){
            if (schoolIds.includes(school.Id))
            {
                academicYears.push(this.getAcademicYear(academicYearId));
            }
        }
        return academicYears;
    }

    getCurrentAcademicYearForSchool(school){
        // Get the currently running academic year
        let academicYears = this.getAcademicYearsForSchool(school);
        // May be order them by start dates
        return academicYears[academicYears.length - 1];
    }

    getClassesForAcademicYear(academicYear, school){
        // Get all classes for the given school in the given academic year
        let allClasses = [];
        if(this.schoolsByAcademicYear.has(academicYear.Id)){
            for (let academicYearSchoolId of this.schoolsByAcademicYear.get(academicYear.Id)){
                if(school.Id == academicYearSchoolId) {
                    for (let [classId, cls] of this.classes) {
                        if (cls.AcademicYear == academicYear && cls.School == school) {
                            allClasses.push(cls);
                        }
                    }
                }
            }
        }
        return allClasses;
    }

    getTeachersForAcademicYear(academicYear, school) {
        // Get all teachers in this school for the given academic year
        let allTeachers = [];
        for (let [schoolId, mappings] of this.teachersBySchool) {
            if(school.Id != schoolId) continue;
            for(let mapping of mappings){
                let teacher = mapping["teacher"];
                let startDate = this.toDate(mapping["startDate"]);
                let endDate = this.toDate(mapping["endDate"]);
                if (endDate == null) {
                    if (startDate <= academicYear.EndDate) {
                        allTeachers.push(teacher);
                    }
                } else {
                    if (endDate > academicYear.StartDate) {
                        allTeachers.push(teacher);
                    }
                }

            }
        }
        return allTeachers;
    }

    getStudentsForAcademicYear(academicYear, school) {
        let allStudents = [];
        for (let [schoolId, mappings] of this.studentsBySchool) {
            if(school.Id != schoolId) continue;
            for(let mapping of mappings){
                let student = mapping["student"];
                let startDate = this.toDate(mapping["startDate"]);
                let endDate = this.toDate(mapping["endDate"]);
                if (endDate == null) {
                    if (startDate <= academicYear.EndDate) {
                        allStudents.push(student);
                    }
                } else {
                    if (endDate > academicYear.StartDate) {
                        allStudents.push(student);
                    }
                }

            }
        }
        return allStudents;
    }

    addTeacher(teacher){
        let newId = this.getUniqueId();
        teacher.setId(newId);
        this.teachers.set(newId, teacher.toJSON());
    }

    addTeacherToSchool(school, teacher){
        if (!this.teachersBySchool.has(school.Id)){
            this.teachersBySchool.set(school.Id, []);
        }
        this.teachersBySchool.get(school.Id).push({
            "teacher": teacher,
            "startDate": new Date(),
            "endDate": null});
    }

    removeTeacherFromSchool(school, teacher, reason){
        if(this.teachersBySchool.has(school.Id)){
            for(let mapping of this.teachersBySchool[school.Id]) {
                if (mapping["teacher"] == teacher) {
                    mapping["endDate"] = new Date();
                }
            }
            let academicYear = this.getCurrentAcademicYearForSchool(school);
            let classes = this.getClassesForAcademicYear(academicYear, school);
            for(let cls in classes){
                this.removeTeacherFromClass(cls, teacher);
                this.removeTeacherFromSubjects(cls, teacher);
            }
        }
    }

    removeTeacherFromClass(cls, teacher) {
        for (let mapping of this.teachersByClass.get(cls)) {
            if (mapping["teacher"] == teacher) {
                mapping["endDate"] = new Date();
            }
        }
    }

    removeTeacherFromSubjects(cls, teacher) {
        for(let [subjectId, mappings] of this.teachersByClassBySubject.get(cls.Id)) {
            for (let mapping of mappings) {
                if (mapping["teacher"] == teacher) {
                    mapping["endDate"] = new Date();
                }
            }
        }
    }

    addClass(cls){
        let newId = this.getUniqueId();
        cls.id = newId;
        this.classes.set(newId, cls);
    }
    
    getClassesForSchool(school){
        let allClasses = [];
        for(let [clsId, cls] of this.classes) {
            if (cls.AcademicYear == school.AcademicYear && cls.School == school){
                allClasses.push(cls);
            }
        }
        return allClasses;
    }

    addAdminTeacher(cls, teacher){
        if(this.classes.has(cls.Id)) {
            if (!this.teachersByClass.has(cls.Id)) {
                this.teachersByClass.set(cls.Id, []);
            }
            this.teachersByClass.get(cls.Id).push({
                "teacher": teacher,
                "startDate": new Date(),
                "endDate": null
            });
        }
    }

    getSchoolContacts(school){
        return this.schoolContacts.get(school.Id) || [];
    }

    addStudent(student){
        let studentId = this.getUniqueId();
        student.setId(studentId);
        this.students.set(studentId, student);
    }

    addStudentToSchool(school, student){
        if(this.schools.has(school.Id)) {
            if (!this.studentsBySchool.has(school.Id)) {
                this.studentsBySchool.set(school.Id, []);
            }
            this.studentsBySchool.get(school.Id).push({
                "student": student,
                "startDate": new Date(),
                "endDate": null
            });
        }
    }

    removeStudentFromSchool(school, student, reason){
        if(this.schools.has(school.Id)) {
            for(let [schoolId, mappings] of this.studentsBySchool) {
                for (let mapping of mappings) {
                    if (mapping["student"] == student.Id) {
                        mapping["endDate"] = new Date();
                    }
                }
            }
            for (let cls in this.getClassesForSchool(school)) {
                for (let mappings of this.studentsByClass.get(cls.Id)) {
                    for(let mapping of mappings) {
                        if (mapping["student"] == student) {
                            mapping["endDate"] = new Date();
                        }
                    }
                }
                for (let [subjectId, studentIds] of this.subjectsByClassByStudent.get(cls.Id)) {
                    let studentIndex = studentIds.indexOf(student.Id);
                    if (studentIndex >= 0) {
                        this.subjectsByClassByStudent.get(cls.Id).set(subjectId, studentIds.splice(studentIndex, 0));
                    }
                }
            }
        }
    }


    // Class related methods

    updateClass(cls){
        // Nothing to update
    }

    getTerms(cls){
        return this.termsByClass.get(cls);
    }
    
    addTerm(cls, term){
        if(!this.termsByClass.has(cls)){
            this.termsByClass.set(cls, []);
        }
        this.termsByClass.get(cls).push(term);
    }

    getSubjectsForClass(cls){
        // Get subjects for the given class
        if(this.subjectsByClass.has(cls.Id)){
            let subjects = [];
            for(let subjectId of this.subjectsByClass.get(cls.Id)){
                subjects.push(this.subjects.get(subjectId));
            }
            return subjects;
        }
    }

    getTeachersBySubjectForClass(cls){
        // Get all teachers for the given class
        let subjectTeachers = new Map();
        if(this.teachersByClassBySubject.has(cls.Id)){
            for(let [subjectId, mappings] of this.teachersByClassBySubject.get(cls.Id)){
                let subject = this.subjects.get(subjectId);
                for(let mapping of mappings) {
                    if (mapping["endDate"] == null) {
                        subjectTeachers.set(subject, mapping["teacher"]);
                    }
                }
            }
        }
        return subjectTeachers;
    }

    getStudentsFromClass(cls){
        // Get all students for the given class
        let allStudents = [];
        if (this.studentsByClass.has(cls.Id)){
            for(let mapping of this.studentsByClass.get(cls.Id)){
                let endDate = mapping["endDate"];
                if(endDate == null || endDate <= cls.AcademicYear.EndDate){
                    allStudents.push(mapping["student"]);
                }
            }
        }
        return allStudents;
    }

    getAdminTeacher(cls){
        if (this.teachersByClass.has(cls.Id)){
            for(let mapping of this.teachersByClass.get(cls.Id)){
                if(mapping["endDate"] == null){
                    return mapping["teacher"];
                }
            }
        }
    }

    getStudentsBySubject(cls, subject) {
        if(this.subjectsByClassByStudent.has(cls.Id)){
            if(this.subjectsByClassByStudent.get(cls.Id).has(subject.Id)){
                return this.subjectsByClassByStudent.get(cls.Id).get(subject.Id);
            }
        }
        return [];
    }

    addSubject(subject){
        let newId = this.getUniqueId();
        subject.setId(newId);
        this.subjects.set(newId, subject);
    }

    updateSubject(subject){
        // Nothing to update here
    }

    removeSubject(subject){
        // lets get to this later as we need to check questions and exams
    }

    addSubjectToClass(cls, subject){
        if(!this.subjectsByClass.has(cls.Id)){
            this.subjectsByClass.set(cls.Id, []);
        }
        if(this.subjectsByClass.get(cls.Id).includes(subject.Id)){
            throw "Subject is already part of the class";
        }
        this.subjectsByClass.get(cls.Id).push(subject.Id);
    }

    removeSubjectFromClass(cls, subject){
        // Lets get to this later
    }

    addTeacherToSubject(cls, teacher, subject) {
        if (!this.teachersByClassBySubject.has(cls.Id)) {
            this.teachersByClassBySubject.set(cls.Id, new Map());
        }
        if (!this.teachersByClassBySubject.get(cls.Id).has(subject.Id)) {
            this.teachersByClassBySubject.get(cls.Id).set(subject.Id, [])
        }
        for (let mapping of this.teachersByClassBySubject.get(cls.Id).get(subject.Id)) {
            if (mapping["endDate"] == null) {
                mapping["endDate"] = new Date();
            }
        }
        this.teachersByClassBySubject.get(cls.Id).get(subject.Id).push({
            "teacher": teacher,
            "startDate": new Date(),
            "endDate": null
        });
    }

    removeTeacherForSubject(cls, teacher, subject){
        if (!this.teachersByClassBySubject.has(cls.Id)) {
            throw "Unknown class";
        }
        if (!this.teachersByClassBySubject.get(cls.Id).has(subject.Id)) {
            throw "Subject is not part of the class";
        }
        for (let mapping of this.teachersByClassBySubject.get(cls.Id).get(subject.Id)) {
            if (mapping["endDate"] == null) {
                mapping["endDate"] = new Date();
            }
        }
    }

    addStudentToClass(cls, student){
        if(!this.studentsByClass.has(cls.Id)){
            this.studentsByClass.set(cls.Id, []);
        }
        this.studentsByClass.get(cls.Id).push({
            "student": student,
            "startDate": new Date(),
            "endDate": null
        });
    }

    removeStudentFromClass(cls, student) {
        if(!this.studentsByClass.has(cls.Id)){
            throw "Unknown class";
        }
        for(let mapping of this.studentsByClass.get(cls.Id)){
            if(mapping["student"] == student){
                mapping["endDate"] = new Date();
            }
        }
    }

    addStudentToSubject(cls, student, subject){
        if(!this.subjectsByClassByStudent.has(cls.Id)){
            this.subjectsByClassByStudent.set(cls.Id, new Map());
        }
        if(!this.subjectsByClassByStudent.get(cls.Id).has(subject.Id)){
            this.subjectsByClassByStudent.get(cls.Id).set(subject.Id, []);
        }
        this.subjectsByClassByStudent.get(cls.Id).get(subject.Id).push({
            "student": student,
            "startDate": new Date(),
            "endDate": null
        });
    }

    removeStudentFromSubject(cls, student, subject){
        if(!this.subjectsByClassByStudent.has(cls.Id)){
            throw "Unknown class";
        }
        if(!this.subjectsByClassByStudent.get(cls.Id).has(subject.Id)){
            throw "Subject is not part of the class";
        }
        for(let mapping of this.subjectsByClassByStudent.get(cls.Id).get(subject.Id)){
            if(mapping["student"] == student){
                mapping["endDate"] = new Date();
            }
        }
    }


    // Student related methods

    getStudentContacts(student){
        return this.studentContacts.get(student.Id);
    }

    addStudentContact(student, contact){
        contact.setId(this.getUniqueId());
        if(!this.studentContacts.has(student.Id)){
            this.studentContacts.set(student.Id, []);
        }
        this.studentContacts.get(student.Id).push(contact);
    }

    getSchoolForStudent(academicYear, student){
        for(let [schoolId, mappings] of this.studentsBySchool){
            for(let mapping of mappings) {
                if (mapping["student"] == student) {
                    let endDate = this.toDate(mapping["endDate"]);
                    if (endDate == null || (endDate >= academicYear.StartDate && endDate <= academicYear.EndDate)) {
                        return this.schools.get(schoolId);
                    }
                }
            }
        }
    }

    getSchoolsForStudent(student){
        let allSchools = [];
        for(let [schoolId, mappings] of this.studentsBySchool){
            for(let mapping of mappings) {
                if (mapping["student"] == student) {
                    allSchools.push(this.schools.get(schoolId));
                }
            }
        }
        return allSchools;
    }

    getClassForStudent(academicYear, student){
        for(let [clsId, mappings] of this.studentsByClass){
            for(let mapping of mappings) {
                if (mapping["student"] == student) {
                    let endDate = this.toDate(mapping["endDate"]);
                    if (endDate == null || (endDate >= academicYear.StartDate && endDate <= academicYear.EndDate)) {
                        return this.classes.get(clsId);
                    }
                }
            }
        }
    }

    getClassesForStudent(school, student){
        let allClasses = [];
        for(let [clsId, mappings] of this.studentsByClass){
            for(let mapping of mappings) {
                if (mapping["student"] == student) {
                    allClasses.push(this.classes.get(clsId));
                }
            }
        }
        return allClasses;
    }

    getSubjectsForStudent(cls, student){
        //  Get Subjects from the class for the given student
        let allSubjects = [];
        if(this.subjectsByClassByStudent.has(cls.Id)){
            for(let [subjectId, mappings] in this.subjectsByClassByStudent.get(cls.Id)){
                for(let mapping of mappings){
                    if(mapping["student"] == student && mapping["endDate"] == null){
                        allSubjects.push(this.subjects.get(subjectId));
                    }
                }
            }
        }
        return allSubjects;
    }

    getExamResultsForStudent(cls, subject, student){
        // Return all exams for the given student in the given academic year for the given subject
        let examResults = {};
        for(let exam in this.getExamsForSubject(cls, subject)){
            examResults[exam] = this.getExamResults(student, exam);
        }
        return examResults;
    }


    // Teacher related methods

    getTeacherContacts(teacher){
        return this.teacherContacts.get(teacher);
    }

    addTeacherContact(teacher, contact){
        contact.setId(this.getUniqueId());
        if(!this.teacherContacts.has(teacher.Id)){
            this.teacherContacts.set(teacher.Id, []);
        }
        this.teacherContacts.get(teacher.Id).push(contact);
    }

    getSchoolForTeacher(academicYear, teacher){
        for(let [schoolId, mappings] of this.teachersBySchool){
            for(let mapping of mappings) {
                if (mapping["teacher"] == teacher) {
                    let endDate = this.toDate(mapping["endDate"]);
                    if (endDate == null || (endDate >= academicYear.StartDate && endDate <= academicYear.EndDate)) {
                        return this.schools.get(schoolId);
                    }
                }
            }
        }
    }

    getSchoolsForTeacher(teacher){
        let allSchools = [];
        for(let [schoolId, mappings] of this.teachersBySchool){
            for(let mapping of mappings) {
                if (mapping["teacher"] == teacher) {
                    allSchools.push(this.schools.get(schoolId));
                }
            }
        }
        return allSchools;
    }

    getClassesForTeacher(academicYear, school, teacher){
        let allClasses = [];
        for(let [clsId, mappings] of this.teachersByClass){
            for(let mapping of mappings){
                let endDate = mapping["endDate"];
                if (endDate == null || (endDate >= academicYear.StartDate && endDate <= academicYear.EndDate)) {
                    allClasses.push(this.classes.get(clsId));
                }
            }
        }
        return allClasses;
    }

    getSubjectsForTeacher(teacher){
        let allSubjects = {};
        for(let [clsId, subjects] of this.teachersByClassBySubject) {
            let cls = this.classes.get(clsId);
            for (let [subjectId, mappings] of subjects) {
                let subject = this.subjects.get(subjectId);
                for (let mapping of mappings) {
                    if (mapping["teacher"] == teacher && mapping["endDate"] == null) {
                        if(!allSubjects.has(cls)){
                            allSubjects.set(cls, []);
                        }
                        allSubjects[cls].push(subject);
                    }
                }
            }
        }
        return allSubjects;
    }


    // Subject Related

    getQuestions(subject){
        // Return a list of Question objects
        if(this.questionsBySubject.has(subject.Id)){
            return this.questionsBySubject.get(subject.Id);
        }
    }

    addQuestion(question, subject){
        let questionId = this.getUniqueId();
        question.id=questionId;
        if(!this.questionsBySubject.has(subject)){
            this.questionsBySubject.set(subject, []);
        }
        this.questionsBySubject.get(subject).push(questionId);
    }

    removeQuestion(question, subject){
        if(this.questions.has(question.Id)){
            this.questions.delete(question.Id);
        }
        if(this.questionsBySubject.has(subject.Id)){
            let subjectQuestionIds = this.questionsBySubject.get(subject.Id);
            let questionIndex = subjectQuestionIds.indexOf(question.Id);
            if(questionIndex >= 0) {
                subjectQuestionIds.splice(questionIndex, 1);
            }
        }
    }

    updateQuestion(question, subject){
    }

    addExam(exam){
        let examId = this.getUniqueId();
        exam.setId(examId);
        this.exams[examId] = exam;
        if(!this.examsByClassBySubject.has(exam.Class.Id)){
            this.examsByClassBySubject.set(exam.Class.Id, new Map());
        }
        if(!this.examsByClassBySubject.get(exam.Class.Id).has(exam.Subject.Id)){
            this.examsByClassBySubject.get(exam.Class.Id).set(exam.Subject.Id, []);
        }
        this.examsByClassBySubject.get(exam.Class.Id).get(exam.Subject.Id).push(exam.Id);
    }

    removeExam(exam){
        // lets get to this later
    }

    updateExam(exam){
    }

    getExamsForSubject(cls, subject){
        let exams = [];
        if(this.examsByClassBySubject.has(cls.Id)){
            if(this.examsByClassBySubject.get(cls.Id).has(subject.Id)){
                for(let examId of this.examsByClassBySubject.get(cls.Id).get(subject.Id))
                {
                    exams.push(this.exams.get(examId));
                }
            }
        }
        return exams;
    }


    // Question Related

    addMasteryLevel(masteryLevel, question){
        if(!this.masteriesByQuestion.has(question.Id)){
            this.masteriesByQuestion.set(question.Id, []);
        }
        let masteryId = this.getUniqueId();
        masteryLevel.id=masteryId;
        this.masteriesByQuestion.get(question.Id).push(masteryLevel.Id);
    }

    removeMasteryLevel(masteryLevel, question) {
        if(this.masteries.has(masteryLevel.Id)){
            this.masteries.delete(masteryLevel.Id);
        }
        if (this.masteriesByQuestion.has(question.Id)) {
            let allMasteryIds = this.masteriesByQuestion.get(question.Id);
            let masteryIndex = allMasteryIds.indexOf(masteryLevel.Id);
            if(masteryIndex >= 0){
                allMasteryIds.splice(masteryIndex, 1);
            }
        }
    }

    getMasteryLevel(masteryId){
        return this.masteries.get(masteryId);
    }

    getMastery(question){
        return this.masteriesByQuestion.get(question.Id);
    }


    // Mastery related

    updateMasteryLevel(mastery){
    }


    // Exam related

    addQuestionToExam(exam, question) {
        if (!this.examQuestions.has(exam.Id)) {
            this.examQuestions.set(exam.Id, []);
        }
        this.examQuestions.get(exam.Id).push(question.Id);
    }

    removeQuestionFromExam(exam, question){
        if(this.examQuestions.has(exam.Id)){
            let questionIndex = this.examQuestions.get(exam.Id.Id).indexOf(question);
            if(questionIndex >= 0){
                this.examQuestions.get(exam.Id).splice(questionIndex, 1);
            }
        }
    }

    getExamQuestions(exam){
        let questions = [];
        for(let questionId of this.examQuestions.get(exam.Id)){
            questions.push(this.questions.get(questionId));
        }
        return questions;
    }

    getExamResults(exam, student){
        if(this.examsByStudent.has(exam.Id)){
            if(this.examsByStudent.get(exam.Id).has(student.Id)){
                return this.examsByStudent.get(exam.Id).get(student.Id);
            }
        }
    }

    addExamAnswer(exam, student, question, answer, mastery){
        if(!this.examsByStudent.has(exam.Id)){
            this.examsByStudent.set(exam.Id, new Map());
        }
        if(!this.examsByStudent.get(exam.Id).has(student.Id)){
            this.examsByStudent.get(exam.Id).set(student.Id, new Map());
        }
        this.examsByStudent.get(exam.Id).get(student.Id).set(question, {
            "answer": answer,
            "mastery": mastery
        });
    }

    updateExamAnswer(exam, student, question, answer, mastery){
        if(this.examsByStudent.has(exam.Id)){
            if(this.examsByStudent.get(exam.Id).has(student.Id)){
                this.examsByStudent.get(exam.Id).get(student.Id).set(question, {
                    "answer": answer,
                    "mastery": mastery.Id
                });
            }
        }
    }
}
