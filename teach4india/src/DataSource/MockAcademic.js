import {School} from "../Model/School.js";
import {AcademicYear} from "../Model/AcademicYear.js";

class MockDataAcademic{

    constructor(){
        this.initializeAcademicYears();
    }

    initializeAcademicYears(){
        this.academicYears = new Map();
        let academicYearsObj = require("./Mock/academicyears.json");
        for(let academicYearId in academicYearsObj){
            let mapping = academicYearsObj[academicYearId];
            this.academicYears.set(academicYearId, AcademicYear.fromJSON(this, academicYearId, mapping));
        }
    }

    getAcademicYears(){       
        return Array.from(this.academicYears.values());
    }

    getAcademicYear(academicYearId){
        return this.academicYears.get(academicYearId);
    }

   
}
export default MockDataAcademic;