import {addDocument} from './firestore';
import firebase from 'firebase';

// Initialize Cloud Firestore through Firebase
var db = firebase.firestore();
var content = require("./mockData.json");

/**
 * Delete a collection, in batches of batchSize. Note that this does
 * not recursively delete subcollections of documents in the collection
 */
function deleteCollection(db, collectionRef, batchSize) {
    var query = collectionRef.orderBy('__name__').limit(batchSize);

    return new Promise(function(resolve, reject) {
        deleteQueryBatch(db, query, batchSize, resolve, reject);
    });
}

function deleteQueryBatch(db, query, batchSize, resolve, reject) {
  query.get()
  .then((snapshot) => {
    
    // When there are no documents left, we are done
    if (snapshot.size == 0) {
      return 0;
    }

    // Delete documents in a batch
    var batch = db.batch();
    snapshot.docs.forEach(function(doc) {
      batch.delete(doc.ref);
    });

    return batch.commit().then(function() {
      return snapshot.size;
    });
  }).then(function(numDeleted) {
    if (numDeleted <= batchSize) {
      resolve();
      return;
    }

    // Recurse on the next process tick, to avoid
    // exploding the stack.
    process.nextTick(function() {
      deleteQueryBatch(db, query, batchSize, resolve, reject);
    });
  })
  .catch(reject);
}

export function setup() {
  if(!content) {
    console.log("content is empty");
    return;
  }
  let promises = [];
  Object.keys(content).forEach(contentKey => {
    let pro = deleteCollection(db, db.collection(contentKey), 10)
    promises.push(pro);
    pro
    .then(ref => {
      const nestedContent = content[contentKey];
      if (typeof nestedContent === "object") {
        Object.keys(nestedContent).forEach(docTitle => {
          let setPromise = db
          .collection(contentKey)
          .doc(docTitle)
          .set(nestedContent[docTitle]);
          promises.push(setPromise);
        });
      }
    })
    .catch(err => {
      console.log("error in deleting", contentKey);
    });
  });
  return Promise.all(promises);
}

export function teardown() {
  if(!content) {
    console.log("content is empty");
    return;
  }
  let promises = [];
  Object.keys(content).forEach(contentKey => {
    let pro = deleteCollection(db, db.collection(contentKey), 10)
    promises.push(pro);
  });
  return Promise.all(promises);
}