import {
    addDocument,
    deleteDocument,
    documentExists,
    getAll,
    getCollection,
    getDocument,
    setDocument
} from './firestore.js';
import {School} from "../Model/School";
import {AcademicYear} from "../Model/AcademicYear"
import {Question} from "../Model/Question"
import {Exam} from "../Model/Exam"
import {ExamResult} from "../Model/ExamResult"
import {MasteryLevel} from "../Model/Mastery"
import {Class} from "../Model/Class"
import {Student} from "../Model/Student"
import {Teacher} from "../Model/Teacher"
import {Answer} from "../Model/Answer"
import {Address} from "../Model/Address"
import {Contact} from "../Model/Contact"
import {Subject} from "../Model/Subject"
import {Image} from "../Model/Image"


//var sync = require("sync");

const ACADEMIC_YEARS = "/academicYears";
const SCHOOLS = "/schools";
const CLASSES = "/classes";
const STUDENTS = "/students";
const TEACHERS = "/teachers";
const MASTERYLEVELS = "/masteries";
const QUESTIONS = "/questions";
const EXAMRESULTS = "/examResults";
const EXAMS = "/exams";
const SUBJECTS = "/subjects";
const ANSWERS ="/answers";
const ADDRESS ="/address";
const CONTACTS ="/contacts";
const IMAGES ="/images";

function _toJsonObject(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function _toClassObject(objClass, json) {
    let jObj = json;
    if (typeof(jObj) != "object") {
        jObj = JSON.parse(jObj);
    }
    return Object.assign(new objClass, jObj);
}

export class FireStoreDataSource {

    _studentExists(student) {
        getCollection(STUDENTS)
            .where("rollNo", "==", student.rollNo)
            .get()
            .then(function (querySnapshot) {
                if (querySnapshot && querySnapshot.empty) {
                    return true;
                }
                return false;
            })
            .catch(function (error) {
                return false;
            });
    }

    _schoolExists(school) {
        return getCollection(SCHOOLS)
            .where("name", "==", school.name)
            .get()
            .then(function (querySnapshot) {
                if (querySnapshot && !querySnapshot.empty) {
                    return true;
                }
                return false;
            })
            .catch(function (error) {
                return false;
            });
    }

    _academicYearExists(name, startDate, endDate) {
        getCollection(ACADEMIC_YEARS)
            .where("name", "==", name)
            .where("startDate", "==", startDate)
            .where("endDate", "==", endDate)
            .get()
            .then(function (querySnapshot) {
                if (querySnapshot && querySnapshot.empty) {
                    return true;
                }
                return false;
            })
            .catch(function (error) {
                return false;
            });
    }

    _examExists(exam) {
        return getCollection(EXAMS)
            .where("name", "==", exam.name)
            .get()
            .then(function (querySnapshot) {
                if (querySnapshot && !querySnapshot.empty) {
                    return true;
                }
                return false;
            })
            .catch(function (error) {
                return false;
            });
    }

     _examResultExists(examResult) {
               

        return getCollection(EXAMRESULTS)
            .where("id", "==", examResult.id)
            .get()
            .then(function (querySnapshot) {
                if (querySnapshot && !querySnapshot.empty) {
                    return true;
                }
                return false;
            })
            .catch(function (error) {
                return false;
            });
    }

    // Academic Year Related
    async getSchoolsForAcademicYear(academicYear) {
        let schools = await this.getAcademicYear(academicYear.id).then(ay => ay.schools);
        return schools
    }

    //General Contact method
    addContact(person, contact) {

    }

    updateContact(person, contact) {

    }

    getContacts(person) {

    }

    async getAcademicYear(academicYearId) {       
        let doc = await getDocument(ACADEMIC_YEARS, academicYearId);        
        return AcademicYear.fromJSON(this, doc.id, doc.data());
    }

   async addAcademicYear(academicYear) {
        if (this._academicYearExists(academicYear.name, academicYear.startDate, academicYear.endDate)) {
            throw 'Academic Year exist.'
        }
        let acadYearID = await addDocument(ACADEMIC_YEARS, academicYear.toJSON());
        academicYear.id = acadYearID.id;
        return academicYear;
    }
    async editAcademicYear(academicYear) {
        if (this.getAcademicYearObject(academicYear.id)) {
            let acdYear = await setDocument(ACADEMIC_YEARS, academicYear.id, academicYear.toJSON());
            return AcademicYear.fromJSON(this, acdYear.id, acdYear.data());
        }
    }
    // Academic Year Related Done

    // Admin Related

    // Admin Related Done

    // School Related
    async getClassesForSchool(school) {
        let classes = await this.getSchoolObject(school.id).then(school => school.classes);
        return classes
    }

    

    // School Related Done

    // Class Related

    async getSubjectsForClass(classObj) {       
        let subjects = await this.getClassObject(classObj.id).then(classObj => classObj.subjects);
        return subjects
    }

    // Class Related Done
    async addClassToSchool(school, cls) {
        school.addClassToSchool(cls)
        cls =await setDocument(SCHOOLS, school.id, school.toJSON());
        return cls; 
    }
    // Student Related
    
    async addStudentToClass(classObj, student)
    {      
        classObj.addStudentToClass(student)
        student =await setDocument(CLASSES, classObj.id, classObj.toJSON());
        return student; 
    }
    // Student Related Done

    // Teacher Related
    async getTeacherContacts(teacher) {
        let contacts = await this.getTeacherObject(teacher.id).then(teacher => teacher.contacts);
        return contacts 
    }

    async addTeacherToClass(classObj, teacher)
    {      
        classObj.addAdminTeacher(teacher)
        teacher =await setDocument(CLASSES, classObj.id, classObj.toJSON());
        return teacher; 
    }
    
    // Teacher Related Done

    // Exam Related

    // Exam Related Done

    // ExamResult Related
    
    // ExamResult Related Done

    // MasteryLevel Related
    
    // MasteryLevel Related Done
    

    async getAcademicYears() {
        let querySnapShot = await getAll(ACADEMIC_YEARS);
        let academicYears = [];
        let dataSource = this;
        let docs = []
        querySnapShot.forEach((doc) => { docs.push(doc); });
        for(let index in docs) {
            let doc = docs[index];
            let academicYear = await AcademicYear.fromJSON(dataSource, doc.id, doc.data())
            academicYears.push(academicYear);
        }
        return academicYears;
    }


    async getSchools(schoolIds) {
        let querySnapShot = await getAll(SCHOOLS);
        let schools = [];
        let dataSource = this;
        querySnapShot.forEach(function (doc) {
            schools.push(School.fromJSON(dataSource, doc.id, doc.data()));
        });
        return schools;
    }

    async getClasses() {
        let querySnapShot = await getAll(CLASSES);
        let classes = [];
        let dataSource = this;
        querySnapShot.forEach(function (doc) {
            classes.push(Class.fromJSON(dataSource, doc.id, doc.data()));
        });
        return classes;
    }

    async getStudents() {
        let querySnapShot = await getAll(STUDENTS);
        let students = [];
        let dataSource = this;
        querySnapShot.forEach(function (doc) {
            students.push(Student.fromJSON(dataSource, doc.id, doc.data()));
        });
        return students;
    }

    // async getSubjects() {
    //     let querySnapshot = await getAll(SUBJECTS);
    //     let subjects = [];
    //     let dataSource = this;
    //     querySnapshot.forEach(function (doc) {
    //         subjects.push(Subject.fromJSON(dataSource, doc.id, doc.data()));
    //     });
    //     return subjects;
    // }

    async getTeachers() {
        let querySnapShot = await getAll(TEACHERS);
        let teachers = [];
        let dataSource = this;
        querySnapShot.forEach(function (doc) {
            teachers.push(Student.fromJSON(dataSource, doc.id, doc.data()));
        });
        return teachers;
    }

    getAdminTeacher(acadYear, school, cls) {

    }

    getStudentsBySubject(acadYear, school, cls, subject, students) {
        var studentIds = [] // Get this fro DB
        var studentsBySubject = [];
        for (var i = 0; i < students.length; i++) {
            var student = students[i];
            if (studentIds.indexOf(student.Id) !== -1) {
                studentsBySubject.push(student);
            }
        }
        return studentsBySubject;
    }


    getPrevAcademicYear(acadYear) {
        // Return an instance of AcademicYear object
    }

    async saveTeacher(teacher) {        
        // add to t4i.teachers
        let res = await addDocument(TEACHERS, teacher.toJSON());
        teacher.id = res.id;
        return teacher;
    }

    async saveSchool(school) {
        let res = await addDocument(SCHOOLS, school.toJSON());
        school.id = res.id;       
        return school;
    }
    async saveClass(cls) {        
        // add to t4i.teachers
        let res = await addDocument(CLASSES, cls.toJSON());
        cls.id = res.id;
        return cls;
    }


    async saveStudent(student) {
        
        // add to t4i.teachers
        let res = await addDocument(STUDENTS, student.toJSON());
        student.id = res.id;
        return student;
    }

    async saveMasteryLevel(masteryLevel) {
        let res = await addDocument(MASTERYLEVELS, masteryLevel.toJSON());
        masteryLevel.id = res.id;       
        return masteryLevel;
    }

    async saveQuestion(question) {
        console.log(question.toJSON())
        let res = await addDocument(QUESTIONS, question.toJSON());
        
        question.id = res.id;       
        return question;
    }

    async addSchool(school) {
        // add to t4i.schools

        // check if the school has the academic year
        if (!school.academicYear) {
            console.warn("academicYear is not set in school");
            return null;
        }

        // check if the academic year of the school exists
        let ayExists = documentExists(ACADEMIC_YEARS, school.academicYear.id);
        // check if the school does not exist
        let schoolExists = this._schoolExists(school);
        let promise = await Promise.all([ayExists, schoolExists]);

        let expected = [true, false];
        if (promise && promise.length == expected.length && promise.every((v, i) => v === expected[i])) {
            let sch = await addDocument(SCHOOLS, school.toJSON());
            school.id = sch.id;
            return school;
        }
        return null;
    }

    // async getQuestionsForSubject(subject) {
    //     let querySnapshot = await getCollection(QUESTIONS).where("subject", "==", subject.id).get();
    //     let questions = [];
    //     let dataSource =this;
    //     querySnapshot.forEach(function (doc) {
    //         questions.push(Question.fromJSON(dataSource, doc.id, doc.data()));
    //     });
    //     return questions;
    // }

    

    
    addTeacherToSchool(school, teacher) {
        // add to t4i.school_teachers
    }

    removeTeacherFromSchool(acadYear, school, teacher) {
        // update t4i.school_teachers
        // update t4i.class_teachers
        // update t4i.class_subject_teachers
    }

    async addClass(cls) {
        // add to t4i.classes
        let res = await addDocument(CLASSES, cls.toJSON());
        cls.id = res.id;
        return cls;
    }

    async addStudent(student) {
        // add to t4i.students
        if (this._studentExists(student)) {
            console.log(student, "exists.");
            return;
        }
        let res = await addDocument(STUDENTS, _toJsonObject(student))
        student.id = res.id;
        return student;
    }

    addMastery(mastery) {
        return addDocument(MASTERYLEVELS, _toJsonObject(mastery));
    }

    removeMastery(mastery) {
        return deleteDocument(MASTERYLEVELS, _toJsonObject(mastery));
    }

    updateMastery(mastery) {
        return setDocument(MASTERYLEVELS, mastery.id, _toJsonObject(mastery));
    }

    updateQuestion(question) {
        return setDocument(QUESTIONS, question.id, _toJsonObject(question));
    }

    addExamAnswer(exam, student, question, answer, mastery) {
        // var answers = {question:{answer, mastery}};  //TBC
        // var examResult = new ExamResult (exam, student, answers);
        // return addDocument(EXAMRESULTS, _toJsonObject(examResult));
    }

    getExamResult(student, exam) {
        // var results = getAll(EXAMRESULTS);
        // for (var i = 0; i < results.length; i++) {
        //     var result = results[i];
        //     if (result.exam === exam && result.student === student) {
        //         return result;
        //     }
        // }
        // return null;
    }

    updateExamAnswer(exam, student, question, answer, mastery) {
        // var examResult = this.getExamResult(student, exam);
        // examResult.answers.question = { answer, mastery };
        // return setDocument(EXAMRESULTS, examResult.id, _toJsonObject(examResult));
    }

    addQuestionToExam(exam, question) {
        return this._examExists(exam)
            .then(_exists => {
                if (_exists) {
                    return getCollection(EXAMS)
                        .where("name", "==", exam.name)
                        .get()
                        .then(ex => {
                            if (exam.questions) {
                                exam.questions.push(question.id);
                            } else {
                                exam.questions = [question.id];
                            }
                            console.log("questions after adding: ", exam.questions);
                            return true;
                        })
                        .catch(e => {
                            console.log("error in adding question to exam", e);
                            return false;
                        });

                } else {
                    console.log("exam does not exist");
                    return false;
                }
            })
            .catch(e => {
                console.log("error in checking exam exists", e);
                return false;
            });
    }

    removeQuestionFromExam(exam, question) {
        // var exam = getDocument(EXAMS, exam.id);
        // exam.questions.splice(questions.indexOf(question));
        // return setDocument(EXAMS, exam.id, _toJsonObject(exam));
    }

    updateExam(exam) {
        return setDocument(EXAMS, exam.id, _toJsonObject(exam));
    }

    async getAllExams(dataSource) {
        let querySnapShot = await getAll(EXAMS);
        let exams = [];
        querySnapShot.forEach(function (doc) {
            exams.push(Exam.fromJSON(dataSource, doc.id, doc.data()));
        });
        return exams;
    }

    async getSchool(schoolId) {
        let res =  await getDocument(SCHOOLS, schoolId);
        return School.fromJSON(this, res.id, res.data());
    }

    async getClass(classId) {
        let classObj =  await getDocument(CLASSES, classId);
        return classObj;
    }

    // async getSubject(subjectId)
    // {
    //     let subject = await getDocument(SUBJECTS, subjectId);
    //     return subject;
    // }

    async getAllQuestions() {
        let querySnapshot = await getAll(QUESTIONS);
        let questions = [];
        let dataSource = this;
        let docs = []
        querySnapshot.forEach((doc) => { docs.push(doc); });
        for(let index in docs) {
            let doc = docs[index];
            let question = await Question.fromJSON(dataSource, doc.id, doc.data())
            questions.push(question);
        }
        return questions;
    }

    async getExamQuestions(exam) {
        let querySnapshot = await getAll(QUESTIONS);
        let questions = [];
        let dataSource = this;
        let docs = []
        querySnapshot.forEach((doc) => { docs.push(doc); });
        for(let index in docs) {
            let doc = docs[index];
            let question = await Question.fromJSON(dataSource, doc.id, doc.data())
            questions.push(question);
        }
        return questions;
    }

    async getClassesForAcademicYear(academicYear, school) {
        // Return a list of Class objects
        let querySnapShot = await getCollection(CLASSES).where("academicYear", "==", academicYear.id).where("school", "==", school.id).get();
        let classes = [];
        let docs = []   
        let dataSource = this;
        querySnapShot.forEach((doc) => {  
                 docs.push(doc); 
        });
       for(let index in docs) {
            let doc = docs[index];         
            let classObj = await Class.fromJSON(dataSource, doc.id, doc.data())
            classes.push(classObj);
        }
        return classes;
    }

    async getStudentsFromClass(cls) {
        let mycls = await getDocument(CLASSES, cls.id);
        let studentDocs = mycls.data().students;
        let students = [];
        let dataSource = this;
        for(let index in studentDocs) {
            let studentDoc = await getDocument(STUDENTS, studentDocs[index]);
            let student = await Student.fromJSON(dataSource, studentDoc.id, studentDoc.data());
            students.push(student);
        }
        return students;
    }

    async getMasteryObject(masteryId) { 
        return await getDocument(MASTERYLEVELS, masteryId);
    }

    async getMastery(question) {           
       var querySnapshot = await getCollection(MASTERYLEVELS).where("question", "==", question.id).get();
       let masteries = [];
       let dataSource = this;
        querySnapshot.forEach(function (doc) {
            masteries.push(MasteryLevel.fromJSON(dataSource, doc.id, doc.data()));
        });
       return masteries;
    }

    async getQuestion(questionId) {
        return await getDocument(QUESTIONS, questionId);
    }


    async addExamResult(examResult) {        
        let examResultID = await addDocument(EXAMRESULTS, examResult.toJSON());
        examResult.id = examResultID.id;
        return examResult;
    }

    async addExamAnswer(answer) {
        // add to t4i.students
        let res = await addDocument(ANSWERS, _toJsonObject(answer))
        answer.id = res.id;
        return answer;
    }

    async getAnswers(examResult) {           
       var querySnapshot = await getCollection(ANSWERS).where("exam", "==", examResult.id).get();
       let answers = [];
       let dataSource = this;
        querySnapshot.forEach(function (doc) {
            answers.push(Answer.fromJSON(dataSource, doc.id, doc.data()));
        });
       return answers;
    }

 

    async saveAddress(address){
         let response = await addDocument(ADDRESS, _toJsonObject(address))
         address.id = response.id;
         return address;
    }

    async saveContact(contact){
         let response = await addDocument(CONTACTS, _toJsonObject(contact))
         contact.id = response.id;
         return contact;
    }

    async addSchoolToAcademicYear(academicYear, school)
    {   
        academicYear.addSchool(school);
        let acdYear = await setDocument(ACADEMIC_YEARS, academicYear.id, academicYear.toJSON());
        return school; 
    }

    

   // get model objects based on Id
    async getObjectForId(objectClass, type, objectId) {
        let json = await getDocument(type, objectId);
        let object = await objectClass.fromJSON(this, json.id, json.data())
        console.log("getObjectForId", type, objectId, "=>", object);
        return object; 
    }
    async getAcademicYearObject(academicYearId) {
        return this.getObjectForId(AcademicYear, ACADEMIC_YEARS, academicYearId)
    }
    async getAddressObject(addressId) {           
        return this.getObjectForId(Address, ADDRESS, addressId)
    }
    async getAnswerObject(answerId) {
        return this.getObjectForId(Answer, ANSWERS, answerId)
    }
    async getClassObject(classId) {
        return this.getObjectForId(Class, CLASSES, classId)
    }
    async getContactObject(contactId) {           
        return this.getObjectForId(Contact, CONTACTS, contactId)
    }
    async getExamObject(examId) {
        return this.getObjectForId(Exam, EXAMS, examId)
    }
    async getExamResultObject(examResultId) {
        return this.getObjectForId(ExamResult, EXAMRESULTS, examResultId)
    }
    async getImageObject(imageId) {
        return this.getObjectForId(Image, IMAGES, imageId)
    }
    async getMasteryLevelObject(masteryLevelId) {
        return this.getObjectForId(MasteryLevel, MASTERYLEVELS, masteryLevelId)
    }
    async getQuestionObject(questionId) {
        return this.getObjectForId(Question, QUESTIONS, questionId)
    }
    async getSchoolObject(schoolId) {
        return this.getObjectForId(School, SCHOOLS, schoolId)
    }
    async getStudentObject(studentId) {
        return this.getObjectForId(Student, STUDENTS, studentId)
    }
    async getSubjectObject(subjectId) {      
        return this.getObjectForId(Subject, SUBJECTS, subjectId)
    }
    async getTeacherObject(teacherId) {
        return this.getObjectForId(Teacher, TEACHERS, teacherId)
    }

    // get model objects based on Ids
    async getObjectsForIds(objectClass, type, objectIds) {
        
        let promises = [];
        for(let index in objectIds) {
            let objectId = objectIds[index];
            let promise = getDocument(type, objectId);
            promises.push(promise)
        }
        let snaps = await Promise.all(promises);        
        let objPromises = []
        for(let index in snaps) {
            let snapshot = snaps[index];
            let objectPromise = objectClass.fromJSON(this, snapshot.id, snapshot.data())
            objPromises.push(objectPromise);
        }
        let objects = await Promise.all(objPromises);        
        console.log("getObjectsForIds", type, objectIds, "=>", objects)
        return objects;
    }
    async getAcademicYearObjects(academicYearIds) {
        return this.getObjectsForIds(AcademicYear, ACADEMIC_YEARS, academicYearIds)
    }
    async getAddressObjects(addressIds) {           
        return this.getObjectsForIds(Address, ADDRESS, addressIds)
    }
    async getAnswerObjects(answerIds) {
        return this.getObjectsForIds(Answer, ANSWERS, answerIds)
    }
    async getClassObjects(classIds) {
        return this.getObjectsForIds(Class, CLASSES, classIds)
    }
    async getContactObjects(contactIds) {           
        return this.getObjectsForIds(Contact, CONTACTS, contactIds)
    }
    async getExamObjects(examIds) {
        return this.getObjectsForIds(Exam, EXAMS, examIds)
    }
    async getExamResultObjects(examResultIds) {
        return this.getObjectsForIds(ExamResult, EXAMRESULTS, examResultIds)
    }
    async getImageObjects(imageIds) {
        return this.getObjectsForIds(Image, IMAGES, imageIds)
    }
    async getMasteryLevelObjects(masteryLevelIds) {
        return this.getObjectsForIds(MasteryLevel, MASTERYLEVELS, masteryLevelIds)
    }
    async getQuestionObjects(questionIds) {
        return this.getObjectsForIds(Question, QUESTIONS, questionIds)
    }
    async getSchoolObjects(schoolIds) {
        return this.getObjectsForIds(School, SCHOOLS, schoolIds)
    }
    async getStudentObjects(studentIds) {
        return this.getObjectsForIds(Student, STUDENTS, studentIds)
    }
    async getSubjectObjects(subjectIds) {
        return this.getObjectsForIds(Subject, SUBJECTS, subjectIds)
    }
    async getTeacherObjects(teacherIds) {
        return this.getObjectsForIds(Teacher, TEACHERS, teacherIds)
    }
}
