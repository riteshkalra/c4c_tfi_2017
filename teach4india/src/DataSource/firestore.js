import firebase from 'firebase'
require('firebase/firestore')

// Initialize Firebase
var config = require("./firestore_credential.json");
firebase.initializeApp(config);

// Initialize Cloud Firestore through Firebase
var db;
firebase.firestore().enablePersistence()
  .then(function() {
    // Initialize Cloud Firestore through firebase
    db = firebase.firestore();
  })
  .catch(function(err) {
    if (err.code === 'failed-precondition') {
      // Multiple tabs open, persistence can only be enabled
      // in one tab at a a time.
      // ...
      console.log("Multiple tabs open. Offline mode disabled.");
    } else if (err.code === 'unimplemented') {
      // The current browser does not support all of the
      // features required to enable persistence
      console.log("The current browser does not support. Offline mode disabled.");
    }
  });

while (!db) {
  console.log("waiting for db to be initiailzed");
  db = firebase.firestore();
}

console.log("db initialised: " + db);

export function addDocument(collection, json) {
  /* add the document with auto-id and returns {"id": id, "data": data} */
  if(!collection) return Promise.reject("addDocument: null found", collection);
  return db.collection(collection.toString()).add(json)
    .then(ref => {
      return new Promise((resolve, reject) => {
        ref.get()
          .then(data => {
            resolve({ "id": data.id, "data": data.data() });
          })
          .catch(error => {
            reject("could not get document", error);
          });
      });
    })
    .catch(e => {
      console.log("error while adding the document to", collection);
    })
}

export function setDocument(collection, document, json) {
  if(!(collection && document)) return Promise.reject("setDocument: null found", collection, document);
  return db.collection(collection.toString()).doc(document.toString()).set(json, { merge: true })
    .then(() => { return getDocument(collection, document); })
    .catch(e => { console.log("error while setting the document"); })
}

export function deleteDocument(collection, document) {
  if(!(collection && document)) return Promise.reject("deleteDocument: null found", collection, document);
  return getDocument(collection, document)
    .then(ref => { db.collection(collection.toString()).doc(document.toString()).delete(); return ref; })
    .catch(error => { console.log(collection, document, "does not exist. not deleted") })
}

function _toStr(object) {
  return (object && object.toString()) || null
}

export function getDocument(collection, document) {
  if(!(collection && document)) {
    console.trace();
    return Promise.reject("getDocument: null found " + _toStr(collection) + " " + _toStr(document));
  }
  return db.collection(collection.toString()).doc(document.toString()).get();
}

export function documentExists(collection, document) {
  if(!(collection && document)) return Promise.reject("documentExists: null found", collection, document);
  return getDocument(collection, document)
    .then(doc => {
      if (doc.exists) {
        return true;
      } else {
        return false;
      }
    })
    .catch(err => {
      console.error("error in getting document ref in documentExists:", collection, document, err);
    });
}

export function getAll(collection) {
  if(!collection) return Promise.reject("getAll: null found", collection);
  return getCollection(collection).get();
}

export function getCollection(collection) {
  if(!collection) return Promise.reject("getCollection: null found", collection);
  return db.collection(collection.toString());
}