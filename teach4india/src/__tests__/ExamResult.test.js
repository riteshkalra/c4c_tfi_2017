import {Exam} from "../Model/Exam.js";
import {Student} from "../Model/Student.js";
import {ExamResult} from "../Model/ExamResult.js";

describe('ExamResult Tests', () => {
    beforeEach(() => {
        jest.mock('dataSource', () =>{
            return {
                getExamResults: jest.fn(),
                addExamAnswer: jest.fn(),
                updateExamAnswer: jest.fn(),
            };
        }, {virtual: true});
    });

    it('Test Properties', () => {
        let dataSource = require('dataSource');
        let exam = jest.mock();
        let student = jest.mock();
        let result = new ExamResult(dataSource, exam, student, null);
        expect(result.Exam).toBe(exam);
        expect(result.Student).toBe(student);
    });

//    it('Test Results', () => {
//        let dataSource = require('dataSource');
//        let exam = jest.mock();
//        let student = jest.mock();
//        let result = new ExamResult(dataSource, exam, student, null);
//        let returnValue = jest.mock();
//        dataSource.getExamResults.mockReturnValueOnce(returnValue);
//        let n = dataSource.getExamResults.mock.calls.length;
//        expect(result.Answers).toEqual(returnValue);
//        expect(dataSource.getExamResults.mock.calls.length).toBe(n+1);
//    });

//    it("Test Add Answer", function(){
//        let dataSource = require('dataSource');
//        let result = new ExamResult(dataSource, jest.mock(), jest.mock(), null);
//        let question = jest.mock();
//        let answer = jest.mock();
//        let mastery = jest.mock();
//        result.Answers;
//        result.addAnswer(question, answer, mastery);
//        let n = dataSource.getExamResults.mock.calls.length;

//        expect(dataSource.addExamAnswer.mock.calls.length).toBe(1);
//        expect(dataSource.addExamAnswer.mock.calls[0][0]).toEqual(question);
//        expect(dataSource.addExamAnswer.mock.calls[0][1]).toEqual(answer);
//        expect(dataSource.addExamAnswer.mock.calls[0][2]).toEqual(mastery);
//        result.Answers;
//        expect(dataSource.getExamResults.mock.calls.length).toBe(n + 1);
//    });

    it("Test Remove Answer", function(){
        let dataSource = require('dataSource');
        let result = new ExamResult(dataSource, jest.mock(), jest.mock(), null);
        let question = jest.mock();
        let answer = jest.mock();
        let mastery = jest.mock();
        result.Answers;
        result.updateAnswer(question, answer, mastery);
        let n = dataSource.getExamResults.mock.calls.length;

        expect(dataSource.updateExamAnswer.mock.calls.length).toBe(1);
        expect(dataSource.updateExamAnswer.mock.calls[0][0]).toEqual(question);
        expect(dataSource.updateExamAnswer.mock.calls[0][1]).toEqual(answer);
        expect(dataSource.updateExamAnswer.mock.calls[0][2]).toEqual(mastery);
//        result.Answers;
//        expect(dataSource.getExamResults.mock.calls.length).toBe(n + 1);
    });

});
