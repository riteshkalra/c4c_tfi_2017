import {Subject} from "../Model/Subject.js";
import {Question} from "../Model/Question.js";
import {Exam} from "../Model/Exam";

describe('Subject Tests', () => {
    beforeEach(() => {
        jest.mock('dataSource', () =>{
            return {
                getQuestions: jest.fn(),
                addQuestion: jest.fn(),
                removeQuestion: jest.fn(),
                updateSubject: jest.fn()
            };
        }, {virtual: true});
    });

    it('Test Properties', () => {
        let dataSource = require('dataSource');
        let name = jest.mock();
        let category = jest.mock();
        let isMandatory = jest.mock();
        let subject = new Subject(dataSource, name, category, isMandatory);
        expect(subject.Name).toBe(name);
        expect(subject.Category).toBe(category);
        expect(subject.IsMandatory).toBe(isMandatory);
    });

    it('Test Questions', () => {
        let dataSource = require('dataSource');
        let subject = new Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        let returnValue = jest.mock();
        dataSource.getQuestions.mockReturnValueOnce(returnValue);
        let n = dataSource.getQuestions.mock.calls.length;
        expect(subject.Questions).toEqual(returnValue);
        expect(dataSource.getQuestions.mock.calls.length).toBe(n+1);
    });

    it("Test Add Question", function(){
        let dataSource = require('dataSource');
        let subject = new Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        let question = new Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), subject);
        subject.Questions;
        subject.addQuestion(question.Description,
            question.Concept,
            question.Type,
            question.DifficultyLevel,
            question.Options,
            question.Answer);
        let n = dataSource.getQuestions.mock.calls.length;

        expect(dataSource.addQuestion.mock.calls.length).toBe(1);
        expect(dataSource.addQuestion.mock.calls[0][0]).toEqual(question);
        subject.Questions;
        expect(dataSource.getQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Remove Question", function(){
        let dataSource = require('dataSource');
        let subject = new Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        let question = new Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), subject);
        subject.Questions;
        subject.removeQuestion(question);
        let n = dataSource.getQuestions.mock.calls.length;

        expect(dataSource.removeQuestion.mock.calls.length).toBe(1);
        expect(dataSource.removeQuestion.mock.calls[0][0]).toEqual(question);
        subject.Questions;
        expect(dataSource.getQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Update", function(){
        let dataSource = require('dataSource');
        let subject = new Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        subject.update();

        expect(dataSource.updateSubject.mock.calls.length).toBe(1);
        expect(dataSource.updateSubject.mock.calls[0][0]).toEqual(subject);
    });

});
