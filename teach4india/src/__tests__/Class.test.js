import {Class} from "../Model/Class";
import {School} from "../Model/School";
import {AcademicYear} from "../Model/AcademicYear";

describe('Class Test', () => {
    beforeAll(() => {

    });

    it('Class Getter Test', function() {
        let ay = new AcademicYear(null, '2017', null, null, null);
        const sch =  new School(null, 'School for Student', 'Test Address', null);
        let cls = new Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch)

        expect(cls.Name).toBe('Class1');
        expect(cls.Grade).toBe('grade1');
        expect(cls.Section).toBe(1);
        expect(cls.Code).toBe(12);
        expect(cls.TeachingMedium).toBe('English');
        expect(cls.AcademicYear).toBe(ay);
        expect(cls.School).toBe(sch);
    });

});
