import {Question} from "../Model/Question.js";
import {ExamResult} from "../Model/ExamResult.js";
import {MasteryLevel} from "../Model/Mastery";

describe('Question Tests', () => {

    beforeEach(() => {
        jest.mock('dataSource', () =>{
            return {
                getMastery: jest.fn(),
                addMasteryLevel: jest.fn(),
                removeMasteryLevel: jest.fn(),
                updateQuestion: jest.fn()
            };
        }, {virtual: true});
    });

    it('Test Properties', () => {
        let dataSource = require('dataSource');
        let description = jest.mock();
        let concept = jest.mock();
        let questionType = jest.mock();
        let difficultyLevel = jest.mock();
        let options = jest.mock();
        let answer = jest.mock();
        let subject = jest.mock();
        let question = new Question(dataSource, description, concept, questionType, difficultyLevel, options, answer, subject);
        expect(question.Description).toEqual(description);
        expect(question.Concept).toEqual(concept);
        expect(question.Type).toEqual(questionType);
        expect(question.DifficultyLevel).toEqual(difficultyLevel);
        expect(question.Options).toEqual(options);
        expect(question.Answer).toEqual(answer);
        expect(question.Subject).toEqual(subject);
    });

    it("Test Mastery Levels", function(){
        let dataSource = require('dataSource');
        let question = new Question(dataSource,
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock());
        let returnValue = jest.mock();
        dataSource.getMastery.mockReturnValue(returnValue);
        expect(question.MasteryLevels).toEqual(returnValue);
        expect(dataSource.getMastery.mock.calls.length).toBe(1);
        expect(dataSource.getMastery.mock.calls[0][0]).toEqual(question);
    });

    it("Test Add Mastery Level", function(){
        let dataSource = require('dataSource');
        let question = new Question(dataSource,
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock());
        let masteryLevel = new MasteryLevel(dataSource, question, "Partial answer", 50, "A");
        question.MasteryLevels;
        question.addMasteryLevel(masteryLevel.Mastery,
            masteryLevel.Mark,
            masteryLevel.Grade);
        let n = dataSource.getMastery.mock.calls.length;

        expect(dataSource.addMasteryLevel.mock.calls.length).toBe(1);
        expect(dataSource.addMasteryLevel.mock.calls[0][0]).toEqual(masteryLevel);
        question.MasteryLevels;
        expect(dataSource.getMastery.mock.calls.length).toBe(n + 1);
    });

    it("Test Remove Mastery Level", function(){
        let dataSource = require('dataSource');
        let question = new Question(dataSource,
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock());
        let masteryLevel = new MasteryLevel(dataSource, question, "Partial answer", 50, "A");
        question.MasteryLevels;
        question.removeMasteryLevel(masteryLevel);
        let n = dataSource.getMastery.mock.calls.length;

        expect(dataSource.removeMasteryLevel.mock.calls.length).toBe(1);
        expect(dataSource.removeMasteryLevel.mock.calls[0][0]).toEqual(masteryLevel);
        question.MasteryLevels;
        expect(dataSource.getMastery.mock.calls.length).toBe(n + 1);
    });

    it("Test Update", function(){
        let dataSource = require('dataSource');
        let question = new Question(dataSource,
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock());
        question.update();

        expect(dataSource.updateQuestion.mock.calls.length).toBe(1);
        expect(dataSource.updateQuestion.mock.calls[0][0]).toEqual(question);
    });

});
