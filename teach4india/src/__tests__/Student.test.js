import {Student} from "../Model/Student";
import {Contact} from "../Model/Contact";
import {School} from "../Model/School";
import {Class} from "../Model/Class";
import {AcademicYear} from "../Model/AcademicYear";

describe('Student Test', () => {
    beforeAll(() => {
        jest.mock('dataSource', () =>{
            const contact = require('../Model/Contact.js');
            const school = require('../Model/School.js') ;
            const academicYear = require('../Model/AcademicYear.js');
            const cls = require('../Model/Class.js');
            const Contact = contact.Contact;
            const School = school.School;
            const AcademicYear = academicYear.AcademicYear;
            const Class = cls.Class;

            const sch =  new School(null, 'School for Student', 'Test Address', null);
            const ay = new AcademicYear(null, '2017', null, null, null);
            sch.academicYear = ay;
            return {
                getStudentContacts: (student) => {return new Contact(null, 'Jane', '', 'Smith', null, '991', '9991', null, 'mother@tfi.com');},
                getSchoolForStudent: (student) => {return sch;},
                getClassForStudent: (ay, scl, student) =>{return new Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch)},
            };
        }, {virtual: true});
    });

    it('Student Getter Test', function() {
        let student = new Student(null, 'John', '', 'Smith', 1, 'M', new Date(2010, 10, 2), new Date(2018,1,1), 'India', null,  6458201, 6458021, 'test@tfi.com', 12);

        expect(student.Name).toBe('John Smith');
        expect(student.Address).toBe('India');
        expect(student.Gender).toBe('M');
        expect(student.DOB).toBeInstanceOf(Date);
        expect(student.Phone1).toBe(6458201);
        expect(student.Phone2).toBe(6458021);
        expect(student.RollNumber).toBe(1);
        expect(student.Email).toBe('test@tfi.com')
    });

    it('Student dataSource Getter Test', function() {
        const dataSource = require('dataSource');
        let ay = new AcademicYear(null, '2017', null, null, null);
        let sch =  new School(null, 'School for Student', 'Test Address', null);
        let cls = new Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch);
        sch.academicYear = ay;

        let student = new Student(dataSource, 'John', 'Smith', 1, 'M', new Date(2010, 10, 2), null, 'India', 6458201, 6458021, 'test@tfi.com', 12);
        expect(student.Class).toBeInstanceOf(Class);
        expect(student.School).toBeInstanceOf(School);
        expect(student.Class).toBeInstanceOf(Class);

        expect(student.Contacts).toEqual(new Contact(null, 'Jane', '', 'Smith', null, '991', '9991', null, 'mother@tfi.com'));
        expect(student.School).toEqual(sch);
        expect(student.Class).toEqual(cls);
        expect(student.AcademicYear).toEqual(ay);
    });

});
