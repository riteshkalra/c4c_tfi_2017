import {MasteryLevel} from "../Model/Mastery";

describe('Mastery Level Tests', () => {

    beforeEach(() => {
        jest.mock('dataSource', () =>{
            return {
                updateMasteryLevel: jest.fn()
            };
        }, {virtual: true});
    });

    it('Test Properties', () => {
        let dataSource = require('dataSource');
        let question = jest.mock();
        let mastery = jest.mock();
        let mark = jest.mock();
        let grade = jest.mock();
        let masteryLevel = new MasteryLevel(dataSource, question, mastery, mark, mark);
        expect(masteryLevel.Question).toEqual(question);
        expect(masteryLevel.Mastery).toEqual(mastery);
        expect(masteryLevel.Mark).toEqual(mark);
        expect(masteryLevel.Grade).toEqual(mark);
    });

    it("Test Update", function(){
        let dataSource = require('dataSource');
        let masteryLevel = new MasteryLevel(dataSource,
            jest.mock(),
            jest.mock(),
            jest.mock(),
            jest.mock());
        masteryLevel.update();

        expect(dataSource.updateMasteryLevel.mock.calls.length).toBe(1);
        expect(dataSource.updateMasteryLevel.mock.calls[0][0]).toEqual(masteryLevel);
    });

});