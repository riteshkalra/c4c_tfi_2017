import {MockDataSource} from "../DataSource/MockDataSource.js";
import {ExamResult} from "../Model/ExamResult.js";
import {School} from "../Model/School";
import {Image} from "../Model/Image";
import {Address} from "../Model/Address";
import {Contact} from "../Model/Contact";
import {Teacher} from "../Model/Teacher";
import {Student} from "../Model/Student";

describe('T4I Use Cases', () => {
    beforeAll(() => {
    });

    it('Init Academic Year Test', () => {
        let dataSource = new MockDataSource();
        let academicYears = dataSource.getAcademicYears();
        expect(academicYears.length).toBe(3);

//        expect(academicYears[0].Schools.length).toBe(1);
//        expect(academicYears[1].Schools.length).toBe(1);
//        expect(academicYears[2].Schools.length).toBe(0);
        
    });

//    it('Init School Test', () => {
//        let dataSource = new MockDataSource();
//        let academicYears = dataSource.getAcademicYears();
//        expect(academicYears[0].Schools[0].Classes.length).toBe(2);
//        expect(academicYears[1].Schools[0].Classes.length).toBe(2);

//        let school = dataSource.getSchool("SCH1");
//        expect(school.AcademicYear).toEqual(dataSource.getAcademicYear("ACAD02"));
//        expect(school.Classes).toEqual([
//            dataSource.getClass("C2"),
//            dataSource.getClass("C3"),
//        ]);
//        expect(school.Teachers).toEqual([
//            dataSource.getTeacher("T001"),
//            dataSource.getTeacher("T003"),
//            dataSource.getTeacher("T004"),
//            dataSource.getTeacher("T005"),
//        ]);
//        expect(school.Students).toEqual([
//            dataSource.getStudent("S001"),
//            dataSource.getStudent("S003"),
//            dataSource.getStudent("S004"),
//            dataSource.getStudent("S005"),
//        ]);

//        school.setAcademicYear(dataSource.getAcademicYear("ACAD01"));
//        expect(school.Classes).toEqual([
//            dataSource.getClass("C1"),
//        ]);
//        expect(school.Teachers).toEqual([
//            dataSource.getTeacher("T001"),
//            dataSource.getTeacher("T002"),
//            dataSource.getTeacher("T003"),
//            dataSource.getTeacher("T004"),
//        ]);
//        expect(school.Students).toEqual([
//            dataSource.getStudent("S001"),
//            dataSource.getStudent("S002"),
//            dataSource.getStudent("S003"),
//        ]);
//    });

//    it('Init Classes Test', () => {
//        let dataSource = new MockDataSource();
//        let school = dataSource.getSchool("SCH1");
//        let cls1A = school.Classes[0];
//        let cls2A = school.Classes[1];
//        expect(cls1A.School).toEqual(school);
//        expect(cls1A.AcademicYear).toEqual(school.AcademicYear);
//        expect(cls2A.School).toEqual(school);
//        expect(cls2A.AcademicYear).toEqual(school.AcademicYear);
//        expect(cls1A.AdminTeacher).toEqual(dataSource.getTeacher("T003"));
//        expect(cls2A.AdminTeacher).toEqual(dataSource.getTeacher("T004"));

//        expect(cls1A.Subjects).toEqual([
//            dataSource.getSubject("SUB1"),
//            dataSource.getSubject("SUB2"),
//            dataSource.getSubject("SUB3"),
//        ]);
//        expect(cls2A.Subjects).toEqual([
//            dataSource.getSubject("SUB4"),
//            dataSource.getSubject("SUB5"),
//        ]);

//        expect(cls1A.Students).toEqual([
//            dataSource.getStudent("S004"),
//            dataSource.getStudent("S005"),
//        ]);
//        expect(cls2A.Students).toEqual([
//            dataSource.getStudent("S001"),
//            dataSource.getStudent("S003"),
//        ]);

//        expect(cls1A.StudentsByOptionalSubjects).toEqual(new Map([
//            [dataSource.getSubject("SUB2"), [
//                dataSource.getStudent("S004"),
//            ]],
//            [dataSource.getSubject("SUB3"), [
//                dataSource.getStudent("S005"),
//            ]]
//        ]));
//        expect(cls2A.StudentsByOptionalSubjects).toEqual(new Map());

//        school.setAcademicYear(dataSource.getAcademicYear("ACAD01"));
//        expect(school.Classes[0].ExamsBySubject).toEqual(new Map([
//            [dataSource.getSubject("SUB1"), [
//                    dataSource.getExam("EX001")
//                ]
//            ],
//            [dataSource.getSubject("SUB2"), []],
//            [dataSource.getSubject("SUB3"), []]
//        ]));
//    });

    it("Init Subject Test", function(){
        let dataSource = new MockDataSource();
        let subject = dataSource.getSubject("SUB1");
        expect(subject.Questions).toEqual([dataSource.getQuestion("Q001")]);
    });

//    it("Init Question Test", function(){
//        let dataSource = new MockDataSource();
//        let question = dataSource.getQuestion("Q001");
//        expect(question.MasteryLevels).toEqual([
//            dataSource.getMasteryLevel("M001"),
//            dataSource.getMasteryLevel("M002"),
//            dataSource.getMasteryLevel("M003")
//        ]);
//    });

//    it("Init Exam test", function() {
//        let dataSource = new MockDataSource();
//        let exam = dataSource.getExam("EX001");

//        expect(exam.Questions).toEqual([
//            dataSource.getQuestion("Q001")
//        ]);

//        expect(exam.Results).toEqual(new Map([
//            [
//                dataSource.getStudent("S001"),
//                new ExamResult(dataSource, exam, dataSource.getStudent("S001"))
//            ],
//            [
//                dataSource.getStudent("S002"),
//                new ExamResult(dataSource, exam, dataSource.getStudent("S002"))
//            ],
//            [
//                dataSource.getStudent("S003"),
//                new ExamResult(dataSource, exam, dataSource.getStudent("S003"))
//            ],
//        ]));
//    });

//    it("Init Exam Result Test", function(){
//        let dataSource = new MockDataSource();
//        let exam = dataSource.getExam("EX001");
//        let result = exam.Results.get(dataSource.getStudent("S001"));
//        expect(result.Answers).toEqual(new Map([
//            [
//                dataSource.getQuestion("Q001"),
//                {
//                    "answer": "",
//                    "mastery": dataSource.getMasteryLevel("M001")
//                }
//            ]
//        ]));
//    });

//    it("New Academic Year Test", function(){
//        let dataSource = new MockDataSource();
//        let academicYears = dataSource.getAcademicYears();
//        let curAY = academicYears[1];
//        let newAY = academicYears[2];
//        let school = curAY.Schools[0];
//        let curClasses = dataSource.getClassesForAcademicYear(curAY, school);
//        
//        school.addAcademicYear(newAY);
//        let newClasses = dataSource.getClassesForAcademicYear(newAY, school);
//        expect(newClasses.length).toBe(curClasses.length);
//        for(let i=0; i<curClasses.length;i++) {
//            expect(newClasses[i].Subjects).toEqual(curClasses[i].Subjects);
//            expect(newClasses[i].AdminTeacher).toEqual(curClasses[i].AdminTeacher);
//            expect(newClasses[i].TeachersBySubject).toEqual(curClasses[i].TeachersBySubject);
//        }
//    });

    it("Add New School Test", function(){
        let dataSource = new MockDataSource();
        let image = new Image("img1", "img");
        let address = new Address(dataSource, "Building1",
            "Street1",
            "City1",
            "District1",
            "State1",
            "Country1",
            "Postal1");
        let school = new School(dataSource,
            "New School",
            null,
            address,
            image,
            "New Phone1",
            "New Phone2",
            "New Fax",
            "New EMail");
        let academicYear = dataSource.getAcademicYear("ACAD02");
        let addedSchool = academicYear.addSchool(school.Name,
            school.RegistrationData,
            school.Image,
            address.BuildingName,
            address.StreetName,
            address.City,
            address.District,
            address.State,
            address.Country,
            address.Postal,
            school.Phone1,
            school.Phone2,
            school.Fax,
            school.Email);
//        expect(academicYear.Schools.length).toBe(2);
        school.setId(addedSchool.Id);
//        expect(addedSchool).toEqual(school);
//        expect(addedSchool.AcademicYear).toEqual(academicYear);
//        expect(addedSchool.Contacts.length).toBe(0);
//        expect(addedSchool.Classes.length).toBe(0);
//        expect(addedSchool.Teachers.length).toBe(0);
//        expect(addedSchool.Students.length).toBe(0);
    });

//    it("School Contacts Test", function(){
//        let dataSource = new MockDataSource();
//        let school = dataSource.getSchool("SCH1");
//        let n = school.Contacts.length;
//        let contact = new Contact(dataSource, "First Name1", "Middle Name1", "Last Name1", "Principal", "Phone", "Mobile", "Fax", "Mail", "School", school.Id);
//        let addedContact = school.addContact(contact.FirstName,
//            contact.MiddleName,
//            contact.LastName,
//            contact.Relationship,
//            contact.Phone,
//            contact.Mobile,
//            contact.Fax,
//            contact.Email);
//        contact.setId(addedContact.Id);
//        expect(school.Contacts.length).toBe(n+1);
//        expect(addedContact).toEqual(contact);
//    });

//    it("Add/Remove School Teacher Test", function(){
//        let dataSource = new MockDataSource();
//        let school = dataSource.getSchool("SCH1");
//        let n = school.Teachers.length;
//        let image = new Image("img1", "img");
//        let address = new Address("Building1",
//            "Street1",
//            "City1",
//            "District1",
//            "State1",
//            "Country1",
//            "Postal1");
//        let teacher = new Teacher(dataSource,
//            "T6",
//            "t6",
//            "TEACHER001",
//            "M",
//            new Date(),
//            address,
//            image,
//            "Phone1",
//            "Phone2",
//            "Email");
//        let addedTeacher = school.addTeacher(teacher.FirstName,
//            teacher.LastName,
//            teacher.EmployeeID,
//            teacher.Gender,
//            teacher.DOB,
//            teacher.Image,
//            address.BuildingName,
//            address.StreetName,
//            address.City,
//            address.District,
//            address.State,
//            address.Country,
//            address.Postal,
//            teacher.Phone1,
//            teacher.Phone2,
//            teacher.Email);
//        teacher.setId(addedTeacher.Id);
//        expect(school.Teachers.length).toBe(n+1);
//        expect(addedTeacher).toEqual(teacher);
//    });

//    it("Add/Remove School Student Test", function(){
//        let dataSource = new MockDataSource();
//        let school = dataSource.getSchool("SCH1");
//        let n = school.Teachers.length;
//        let image = new Image("img1", "img");
//        let address = new Address("Building1",
//            "Street1",
//            "City1",
//            "District1",
//            "State1",
//            "Country1",
//            "Postal1");
//        let student = new Student(dataSource,
//            "ST6",
//            "st6",
//            "St6",
//            "ST008",
//            "M",
//            new Date(),
//            new Date(),
//            address,
//            image,
//            "Phone1",
//            "Phone2",
//            "Email");
//        let addedStudent = school.addStudent(student.FirstName,
//            student.MiddleName,
//            student.LastName,
//            student.RollNumber,
//            student.Gender,
//            student.DOB,
//            student.DOJ,
//            student.Image,
//            address.BuildingName,
//            address.StreetName,
//            address.City,
//            address.District,
//            address.State,
//            address.Country,
//            address.Postal,
//            student.Phone1,
//            student.Phone2,
//            student.Email);
//        student.setId(addedStudent.Id);
//        expect(school.Students.length).toBe(n+1);
//        expect(addedStudent).toEqual(student);
//    });

//    it("Test Add New Class", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add Next Academic Year", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add/Remove Student in Class", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add/Remove Subject", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add/Remove Admin Teacher", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add/Remove Teacher for Subject", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add/Remove Students to Optional Subjects", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Students By Optional Subjects", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add/Remove Exams in Class", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add/Remove Exam Results in Class", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Exams By Subjects", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Create Subject", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Create Question Bank", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Create Exam", function(){
//        expect(1).toBe(2);
//    });

//    it("Test Add Exam Results for Students", function(){
//        expect(1).toBe(2);
//    });

});

