import {Teacher} from "../Model/Teacher";
import {School} from "../Model/School";
import {Contact} from "../Model/Contact";
import {AcademicYear} from "../Model/AcademicYear";
import {Class} from "../Model/Class";

describe('Teacher Test', () => {
    beforeAll(() => {
        jest.mock('dataSource', () =>{
            const contact = require('../Model/Contact.js');
            const school = require('../Model/School.js') ;
            const academicYear = require('../Model/AcademicYear.js');
            const cls = require('../Model/Class.js');
            const Contact = contact.Contact;
            const School = school.School;
            const AcademicYear = academicYear.AcademicYear;
            const Class = cls.Class;

            const sch =  new School(null, 'School for Student', null, 'Test Address', null);
            const ay = new AcademicYear(null, '2017', null, null, null, null);
            sch.academicYear = ay;
            return {
                getTeacherContacts: (teacher) => {return new Contact(null, 'Jane', '', 'Smith', null, '991', '9991', null, 'mother@tfi.com');},
                getSchoolForTeacher: (teacher) => {return sch;},
                getClassesForTeacher: (ay, scl, teacher) =>{return new Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch)},
            };
        }, {virtual: true});
    });

    it('Teacher Getter Test', function() {
        let teacher = new Teacher(null, 'Jane', 'Smith', 1234, 'F', new Date(2017,10,2), null);

        expect(teacher.Name).toBe('Jane Smith');
        expect(teacher.EmployeeID).toBe(1234);
        expect(teacher.Gender).toBe('F');
        expect(teacher.DOB).toBeInstanceOf(Date);
    });

    it('Teacher dataSource Getter Test', function() {
        const dataSource = require('dataSource');
        let ay = new AcademicYear(null, '2017', null, null, null);
        let teacher = new Teacher(dataSource, 'Jane', 'Smith', 1234, 'F', new Date(2017,10,2), null);
        let sch =  new School(null, 'School for Student', null, 'Test Address', null);
        let cls = new Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch);
        sch.academicYear = new AcademicYear(null, '2017', null, null, null);
        expect(teacher.Contact).toEqual(new Contact(null, 'Jane', '', 'Smith', null, '991', '9991', null, 'mother@tfi.com'));
        expect(teacher.School).toEqual(sch);
        expect(teacher.AcademicYear).toEqual(new AcademicYear(null, '2017', null, null, null));
        expect(teacher.Classes).toEqual(cls);
    });
});
