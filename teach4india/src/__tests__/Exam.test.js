import {Exam} from "../Model/Exam.js";
import {ExamResult} from "../Model/ExamResult.js";

describe('Exam Tests', () => {

    beforeEach(() => {
        jest.mock('dataSource', () =>{
            return {
                getExamQuestions: jest.fn(),
                addQuestionToExam: jest.fn(),
                removeQuestionFromExam: jest.fn(),
                updateExam: jest.fn()
            };
        }, {virtual: true});
    });

    it('Test Properties', () => {
        let dataSource = require('dataSource');
        let cls = jest.mock("../Model/Class.js");
        let subject = jest.mock("../Model/Subject.js");
        let date = new Date();
        let exam = new Exam(dataSource, 'Exam1', date, 100, null, cls, subject);
        expect(exam.Name).toBe('Exam1');
        expect(exam.Date).toBe(date);
        expect(exam.PassMarks).toBe(100);
        expect(exam.PassGrades).toBeNull();
        expect(exam.Class).toBe(cls);
        expect(exam.Subject).toBe(subject);
    });

//    it("Test Questions", function(){
//        let dataSource = require('dataSource');
//        let exam = new Exam(dataSource,
//            'Exam1',
//            new Date(),
//            100,
//            null,
//            jest.mock("../Model/Class.js"),
//            jest.mock("../Model/Subject.js"));
//        let returnValue = jest.mock();
//        dataSource.getExamQuestions.mockReturnValue(returnValue);
//        expect(exam.Questions).toEqual(returnValue);
//        expect(dataSource.getExamQuestions.mock.calls.length).toBe(1);
//        let arg0 = dataSource.getExamQuestions.mock.calls[0][0];
//        expect(arg0).toEqual(exam);
//    });

    it("Test Exam Results", function(){
        let dataSource = require('dataSource');
        let cls = jest.mock();
        let subject = jest.mock();
        let exam = new Exam(dataSource,
            'Exam1',
            new Date(),
            100,
            null,
            cls,
            subject);
        let student1 = jest.mock();
        let student2 = jest.mock();
        Object.defineProperty(cls, 'Students', {value: [student1, student2]});
        expect(exam.Results).toEqual(new Map([
            [student1, new ExamResult(dataSource, exam, student1)],
            [student2, new ExamResult(dataSource, exam, student2)],
        ]));
    });

    it("Test Add Question", function(){
        let dataSource = require('dataSource');
        let exam = new Exam(dataSource, 'Exam1', new Date(), 100, null, jest.mock(), jest.mock());
        let question = jest.mock();
        let returnValue = jest.mock();
        dataSource.getExamQuestions.mockReturnValue(returnValue);
        exam.Questions;
        exam.addQuestion(question);
        let n = dataSource.getExamQuestions.mock.calls.length;

        expect(dataSource.addQuestionToExam.mock.calls.length).toBe(1);
        let arg0 = dataSource.addQuestionToExam.mock.calls[0][0];
        let arg1 = dataSource.addQuestionToExam.mock.calls[0][1];
        expect(arg0).toEqual(exam);
        expect(arg1).toEqual(question);
        exam.Questions;
        expect(dataSource.getExamQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Remove Question", function(){
        let dataSource = require('dataSource');
        let exam = new Exam(dataSource, 'Exam1', new Date(), 100, null, jest.mock(), jest.mock());
        let question = jest.mock();
        let returnValue = jest.mock();
        dataSource.getExamQuestions.mockReturnValue(returnValue);
        exam.Questions;
        exam.removeQuestion(question);
        let n = dataSource.getExamQuestions.mock.calls.length;

        expect(dataSource.removeQuestionFromExam.mock.calls.length).toBe(1);
        let arg0 = dataSource.removeQuestionFromExam.mock.calls[0][0];
        let arg1 = dataSource.removeQuestionFromExam.mock.calls[0][1];
        expect(arg0).toEqual(exam);
        expect(arg1).toEqual(question);
        exam.Questions;
        expect(dataSource.getExamQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Update", function(){
        let dataSource = require('dataSource');
        let exam = new Exam(dataSource, 'Exam1', new Date(), 100, null, jest.mock(), jest.mock());
        exam.update();

        expect(dataSource.updateExam.mock.calls.length).toBe(1);
        let arg0 = dataSource.updateExam.mock.calls[0][0];
        expect(arg0).toEqual(exam);
    });

});
