import {School} from "../Model/School.js";
import {Address} from "../Model/Address.js";
import {Image} from "../Model/Image.js";
import {Teacher} from "../Model/Teacher.js";
import {Class} from "../Model/Class.js";
import {AcademicYear} from "../Model/AcademicYear.js";
import {Contact} from "../Model/Contact.js";
import {Subject} from "../Model/Subject.js";
import {Student} from "../Model/Student.js";

const getSchool = function(dataSource){
    return new School(dataSource,
        "School1",
        null,
        null,
        null,
        "p1",
        "p2",
        "fax",
        "email",
        "sch1");
};

describe('School Tests', () => {
    beforeEach(() => {
        jest.mock('dataSource', () =>{
            return {
                getSchoolContacts: jest.fn(),
                getCurrentAcademicYearForSchool: jest.fn(),
                getClassesForAcademicYear: jest.fn(),
                getTeachersForAcademicYear: jest.fn(),
                addTeacher: jest.fn(),
                addTeacherToSchool: jest.fn(),
                removeTeacherFromSchool: jest.fn(),
                addClass: jest.fn(),
                getPreviousAcademicYear: jest.fn(),
                getClassesFromPrevAcademicYear: jest.fn(),
                addSchoolToAcademicYear: jest.fn(),
                getAdminTeacher: jest.fn(),
                addAdminTeacher: jest.fn(),
                getSubjectsForClass: jest.fn(),
                addSchoolContact: jest.fn()
            };
            }, {virtual: true});
    });

    it('Properties Test', function() {
        let dataSource = require('dataSource');
        let id = "school1";
        let name = "School1";
        let registrationDate = jest.mock();
        let phone1 = "Phone1";
        let phone2 = "Phone2";
        let fax = "Fax";
        let email = "Email";
        let schoolContacts = ["contact1"];
        dataSource.getSchoolContacts.mockReturnValue(schoolContacts);
        let academicYear = "AcademicYear1";
        dataSource.getCurrentAcademicYearForSchool.mockReturnValue(academicYear);
        let classes = ["Class1"];
        dataSource.getClassesForAcademicYear.mockReturnValue(classes);
        let teachers = ["Teacher1"];
        dataSource.getTeachersForAcademicYear.mockReturnValue(teachers);

        let image = new Image("img1", "img");
        let address = new Address("Building1",
            "Street1",
            "City1",
            "District1",
            "State1",
            "Country1",
            "Postal1");
        let school = new School(dataSource,
            name,
            registrationDate,
            address,
            image,
            phone1,
            phone2,
            fax,
            email,
            academicYear);

        expect(school.Name).toBe(name);
        expect(school.RegistrationDate).toBe(registrationDate);
        expect(school.Phone1).toBe(phone1);
        expect(school.Phone2).toBe(phone2);
        expect(school.Fax).toBe(fax);
        expect(school.Email).toBe(email);
        expect(school.Address.toJSON()).toEqual(address.toJSON());
        expect(school.Image.toJSON()).toEqual(image.toJSON());
//        expect(school.Contacts).toBe(schoolContacts);
//        school.Contacts;
//        expect(dataSource.getSchoolContacts.mock.calls.length).toBe(1);
        expect(school.AcademicYear).toBe(academicYear);
//        school.AcademicYear;
//        expect(dataSource.getCurrentAcademicYearForSchool.mock.calls.length).toBe(1);
//        expect(school.Classes).toBe(classes);
//        school.Classes;
//        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(1);
//        expect(school.Teachers).toBe(teachers);
//        school.Teachers;
//        expect(dataSource.getTeachersForAcademicYear.mock.calls.length).toBe(1);
    });

    it('Add Teacher Test', function() {
        const dataSource = require('dataSource');
        let school = getSchool(dataSource);
        let image = new Image("img1", "img");
        let address = new Address(dataSource, "Building1",
            "Street1",
            "City1",
            "District1",
            "State1",
            "Country1",
            "Postal1");
        let teacher = new Teacher(dataSource,
            "firstname",
            "lastname",
            "empid",
            "M",
            new Date(),
            address,
            image,
            "Phone1",
            "Phone2",
            "Email");
        let addedTeacher = school.addTeacher(teacher.FirstName,
            teacher.LastName,
            teacher.EmployeeID,
            teacher.Gender,
            teacher.DOB,
            teacher.Image,
            teacher.Address.BuildingName,
            teacher.Address.StreetName,
            teacher.Address.City,
            teacher.Address.District,
            teacher.Address.State,
            teacher.Address.Country,
            teacher.Address.Postal,
            teacher.Phone1,
            teacher.Phone2,
            teacher.Email);

        expect(addedTeacher.toJSON()).toEqual(teacher.toJSON());

        expect(dataSource.addTeacher.mock.calls.length).toBe(1);
        expect(dataSource.addTeacherToSchool.mock.calls.length).toBe(1);

        let arg0 = dataSource.addTeacher.mock.calls[0][0];
        expect(arg0.toJSON()).toEqual(teacher.toJSON());

        arg0 = dataSource.addTeacherToSchool.mock.calls[0][0];
        let arg1 = dataSource.addTeacherToSchool.mock.calls[0][1];
        expect(arg0).toEqual(school);
        expect(arg1.toJSON()).toEqual(teacher.toJSON());

//        // We expect teachers to be reset
//        school.Teachers;
//        expect(dataSource.getTeachersForAcademicYear.mock.calls.length).toBe(2);
    });

    it('Remove Teacher Test', function() {
        const dataSource = require('dataSource');
        let school = getSchool(dataSource);
        let teacher = "Teacher";
        school.removeTeacher(teacher);

        expect(dataSource.removeTeacherFromSchool.mock.calls.length).toBe(1);

        let arg0 = dataSource.removeTeacherFromSchool.mock.calls[0][0];
        let arg1 = dataSource.removeTeacherFromSchool.mock.calls[0][1];
        expect(arg0).toEqual(school);
        expect(arg1).toEqual(teacher);

        // We expect teachers to be reset
        let n = dataSource.getTeachersForAcademicYear.mock.calls.length;
        school.Teachers;
        expect(dataSource.getTeachersForAcademicYear.mock.calls.length).toBe(n + 1);
    });

    it("Add Class Test", function(){
        const dataSource = require('dataSource');
        let academicYear = new AcademicYear(dataSource, "academic year", new Date(), new Date());
        let school = getSchool(dataSource);
        let cls = new Class(dataSource, "class", "grade", "section", "code", "medium", academicYear, school);
        dataSource.getCurrentAcademicYearForSchool.mockReturnValue(academicYear);
        let addedCls = school.addClass(cls.Name, cls.Grade, cls.Section, cls.Code, cls.TeachingMedium, cls.AcademicYear);
        expect(addedCls.toJSON()).toEqual(cls.toJSON());
        let n = dataSource.getClassesForAcademicYear.mock.calls.length;
        school.Classes;
        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(n+1);
    });

    it("Previous Class Test", function(){
        const dataSource = require('dataSource');
        let school = getSchool(dataSource);
        let academicYear = new AcademicYear(dataSource, "academic year", new Date(), new Date());
        let cls1A = new Class(dataSource, "1A", "1", "A", "code", "medium", academicYear, school);
        let cls1B = new Class(dataSource, "1B", "1", "B", "code", "medium", academicYear, school);
        let cls2A = new Class(dataSource, "2A", "2", "A", "code", "medium", academicYear, school);
        dataSource.getClassesForAcademicYear.mockReturnValue([cls1A, cls1B, cls2A]);
        expect(school.getPrevClass(cls1A)).toBe(null);
        expect(school.getPrevClass(cls1B)).toBe(null);
        expect(school.getPrevClass(cls2A).toJSON()).toEqual(cls1A.toJSON());
    });

    it("Previous Classes Test", function(){
        const dataSource = require('dataSource');
        let school = getSchool(dataSource);
        let ay1 = new AcademicYear(dataSource, "ay1", null, new Date(), new Date());
        let ay2 = new AcademicYear(dataSource, "ay2", null, new Date(), new Date());
        let returnValue = "test";
        dataSource.getCurrentAcademicYearForSchool.mockReturnValue(ay2);
        dataSource.getPreviousAcademicYear.mockReturnValue(ay1);
        dataSource.getClassesForAcademicYear.mockReturnValue(returnValue);
        let n = dataSource.getClassesForAcademicYear.mock.calls.length;
        expect(school.getClassesFromPrevAcademicYear(ay2)).toBe(returnValue);
        expect(dataSource.getPreviousAcademicYear.mock.calls.length).toBe(1);
        let arg0 = dataSource.getPreviousAcademicYear.mock.calls[0][0];
        expect(arg0.toJSON()).toEqual(ay2.toJSON());

        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(n+1);
        arg0 = dataSource.getClassesForAcademicYear.mock.calls[n][0];
        let arg1 = dataSource.getClassesForAcademicYear.mock.calls[n][1];
        expect(arg0.toJSON()).toEqual(ay1.toJSON());
        expect(arg1.toJSON()).toEqual(school.toJSON());
    });

//    it("Add Contact Test", function(){
//        const dataSource = require('dataSource');
//        let school = getSchool(dataSource);
//        let contact = new Contact(dataSource, "fn", "mn", "ln", "rel", "phone", "mobile", "fax", "mail", "School", school.Id);
//        let addedContact = school.addContact(contact.FirstName,
//            contact.MiddleName,
//            contact.LastName,
//            contact.Relationship,
//            contact.Phone,
//            contact.Mobile,
//            contact.Fax,
//            contact.Email);
//        expect(addedContact.toJSON()).toEqual(contact.toJSON());

//        let n = dataSource.getSchoolContacts.mock.calls.length;
//        school.Contacts;
//        expect(dataSource.getSchoolContacts.mock.calls.length).toBe(n+1);
//    });

    it("Set Academic Year Test", function(){
        const dataSource = require('dataSource');
        let school = getSchool(dataSource);
        let ay1 = new AcademicYear(dataSource, "ay1", null, new Date(), new Date());

        school.setAcademicYear(ay1);
        expect(school.AcademicYear.toJSON()).toEqual(ay1.toJSON());
        let n = dataSource.getClassesForAcademicYear.mock.calls.length;
        school.Classes;
        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(n+1);
    });

});
