import { AcademicYear } from "../Model/AcademicYear.js";
import { School } from "../Model/School.js";
import { Address } from "../Model/Address";
//import { setup } from "../DataSource/firestore_mock.js"

// describe('AcademicYear Tests With Mock', () => {
//   beforeAll(() => {
//     jest.mock('dataSource', () => {
//       return {
//         addSchool: jest.fn(),
//         addSchoolToAcademicYear: jest.fn(),
//         getSchoolsForAcademicYear: jest.fn(),
//       };
//     }, { virtual: true });
//   });

//   it('Properties Test', function() {
//     let dataSource = null;
//     let id = "acd01";
//     let name = "2016-17";
//     let board = "cbse";
//     let startDate = new Date(2016, 4, 1);
//     let endDate = new Date(2017, 3, 31);
//     let academicYear = new AcademicYear(dataSource, name, board, startDate, endDate, id);

//     expect(academicYear.dataSource).toBe(dataSource);
//     expect(academicYear.Name).toBe(name);
//     expect(academicYear.Board).toBe(board);
//     expect(academicYear.StartDate).toBe(startDate);
//     expect(academicYear.EndDate).toBe(endDate);
//     expect(academicYear.Id).toBe(id);
//   });

//   it('Add School Test', function() {
//     const dataSource = require('dataSource');
//     let academicYear = new AcademicYear(dataSource, "test", null, null, null);
//     let address = new Address("Building1",
//       "Street1",
//       "City1",
//       "District1",
//       "State1",
//       "Country1",
//       "Postal1");
//     let school = new School(dataSource,
//       "School1",
//       null,
//       address,
//       null,
//       "Phone1",
//       "Phone2",
//       "Fax",
//       "Email");
//     let n = dataSource.addSchool.mock.calls.length;
//     academicYear.addSchool(school.Name,
//       school.RegistrationData,
//       school.Image,
//       school.Address.BuildingName,
//       school.Address.StreetName,
//       school.Address.City,
//       school.Address.District,
//       school.Address.State,
//       school.Address.Country,
//       school.Address.Postal,
//       school.Phone1,
//       school.Phone2,
//       school.Fax,
//       school.Email);

//     expect(dataSource.addSchool.mock.calls.length).toBe(n + 1);
//     expect(dataSource.addSchoolToAcademicYear.mock.calls.length).toBe(1);
//     expect(dataSource.addSchool.mock.calls[0][0].toJSON()).toEqual(school.toJSON());
//     expect(dataSource.addSchoolToAcademicYear.mock.calls[0][0]).toEqual(academicYear);
//     expect(dataSource.addSchoolToAcademicYear.mock.calls[0][1]).toEqual(school);
//   });

//   it('Get Schools Test', function() {
//     const dataSource = require('dataSource');
//     let academicYear = new AcademicYear(dataSource, "test", null, null, null);
//     let n = dataSource.getSchoolsForAcademicYear.mock.calls.length;
//     academicYear.Schools;

//     expect(dataSource.getSchoolsForAcademicYear.mock.calls.length).toBe(n + 1);
//     expect(dataSource.getSchoolsForAcademicYear.mock.calls[0][0]).toEqual(academicYear);
//   });
// });
//beforeAll(() => {
//  console.log("running setup");
//  return setup().then(() => {
//    console.log("setup completed");
//  });
//});

  

  // afterAll(() => {
  //   console.log("running teardown");
  //   return teardown().then(() => {
  //     console.log("teardown completed");
  //   });
  // });

  test('Add School', async() => {
    let ay = new AcademicYear(null, '2016', null, null, "academicyearRandomId1");
    //let result = await ay.addSchool('New School Name', null, null, null, null, null, null, null, null, null, null, null, null);
    //expect(result.name).toBe("New School Name");
  });

