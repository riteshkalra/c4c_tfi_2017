"use strict";

var _Subject = require("../Model/Subject.js");

var _Question = require("../Model/Question.js");

var _Exam = require("../Model/Exam");

describe('Subject Tests', function () {
    beforeEach(function () {
        jest.mock('dataSource', function () {
            return {
                getQuestions: jest.fn(),
                addQuestion: jest.fn(),
                removeQuestion: jest.fn(),
                updateSubject: jest.fn()
            };
        }, { virtual: true });
    });

    it('Test Properties', function () {
        var dataSource = require('dataSource');
        var name = jest.mock();
        var category = jest.mock();
        var isMandatory = jest.mock();
        var subject = new _Subject.Subject(dataSource, name, category, isMandatory);
        expect(subject.Name).toBe(name);
        expect(subject.Category).toBe(category);
        expect(subject.IsMandatory).toBe(isMandatory);
    });

    it('Test Questions', function () {
        var dataSource = require('dataSource');
        var subject = new _Subject.Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        var returnValue = jest.mock();
        dataSource.getQuestions.mockReturnValueOnce(returnValue);
        var n = dataSource.getQuestions.mock.calls.length;
        expect(subject.Questions).toEqual(returnValue);
        expect(dataSource.getQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Add Question", function () {
        var dataSource = require('dataSource');
        var subject = new _Subject.Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        var question = new _Question.Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), subject);
        subject.Questions;
        subject.addQuestion(question.Description, question.Concept, question.Type, question.DifficultyLevel, question.Options, question.Answer);
        var n = dataSource.getQuestions.mock.calls.length;

        expect(dataSource.addQuestion.mock.calls.length).toBe(1);
        expect(dataSource.addQuestion.mock.calls[0][0]).toEqual(question);
        subject.Questions;
        expect(dataSource.getQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Remove Question", function () {
        var dataSource = require('dataSource');
        var subject = new _Subject.Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        var question = new _Question.Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), subject);
        subject.Questions;
        subject.removeQuestion(question);
        var n = dataSource.getQuestions.mock.calls.length;

        expect(dataSource.removeQuestion.mock.calls.length).toBe(1);
        expect(dataSource.removeQuestion.mock.calls[0][0]).toEqual(question);
        subject.Questions;
        expect(dataSource.getQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Update", function () {
        var dataSource = require('dataSource');
        var subject = new _Subject.Subject(dataSource, jest.mock(), jest.mock(), jest.mock());
        subject.update();

        expect(dataSource.updateSubject.mock.calls.length).toBe(1);
        expect(dataSource.updateSubject.mock.calls[0][0]).toEqual(subject);
    });
});
//# sourceMappingURL=Subject.test.js.map