"use strict";

var _Teacher = require("../Model/Teacher");

var _School = require("../Model/School");

var _Contact = require("../Model/Contact");

var _AcademicYear = require("../Model/AcademicYear");

var _Class = require("../Model/Class");

describe('Teacher Test', function () {
    beforeAll(function () {
        jest.mock('dataSource', function () {
            var contact = require('../Model/Contact.js');
            var school = require('../Model/School.js');
            var academicYear = require('../Model/AcademicYear.js');
            var cls = require('../Model/Class.js');
            var Contact = contact.Contact;
            var School = school.School;
            var AcademicYear = academicYear.AcademicYear;
            var Class = cls.Class;

            var sch = new School(null, 'School for Student', null, 'Test Address', null);
            var ay = new AcademicYear(null, '2017', null, null, null, null);
            sch.academicYear = ay;
            return {
                getTeacherContacts: function getTeacherContacts(teacher) {
                    return new Contact(null, 'Jane', 'Smith', null, '991', '9991', null, 'mother@tfi.com');
                },
                getSchoolForTeacher: function getSchoolForTeacher(teacher) {
                    return sch;
                },
                getClassesForTeacher: function getClassesForTeacher(ay, scl, teacher) {
                    return new Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch);
                }
            };
        }, { virtual: true });
    });

    it('Teacher Getter Test', function () {
        var teacher = new _Teacher.Teacher(null, 'Jane', 'Smith', 1234, 'F', new Date(2017, 10, 2), null);

        expect(teacher.Name).toBe('Jane Smith');
        expect(teacher.EmployeeID).toBe(1234);
        expect(teacher.Gender).toBe('F');
        expect(teacher.DOB).toBeInstanceOf(Date);
    });

    it('Teacher dataSource Getter Test', function () {
        var dataSource = require('dataSource');
        var ay = new _AcademicYear.AcademicYear(null, '2017', null, null, null);
        var teacher = new _Teacher.Teacher(dataSource, 'Jane', 'Smith', 1234, 'F', new Date(2017, 10, 2), null);
        var sch = new _School.School(null, 'School for Student', null, 'Test Address', null);
        var cls = new _Class.Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch);
        sch.academicYear = new _AcademicYear.AcademicYear(null, '2017', null, null, null);
        expect(teacher.Contact).toEqual(new _Contact.Contact('Jane', 'Smith', null, '991', '9991', null, 'mother@tfi.com'));
        expect(teacher.School).toEqual(sch);
        expect(teacher.AcademicYear).toEqual(new _AcademicYear.AcademicYear(null, '2017', null, null, null));
        expect(teacher.Classes).toEqual(cls);
    });
});
//# sourceMappingURL=Teacher.test.js.map