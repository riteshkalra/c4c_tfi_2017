"use strict";

var _Exam = require("../Model/Exam.js");

var _Student = require("../Model/Student.js");

var _ExamResult = require("../Model/ExamResult.js");

describe('ExamResult Tests', function () {
    beforeEach(function () {
        jest.mock('dataSource', function () {
            return {
                getExamResults: jest.fn(),
                addExamAnswer: jest.fn(),
                updateExamAnswer: jest.fn()
            };
        }, { virtual: true });
    });

    it('Test Properties', function () {
        var dataSource = require('dataSource');
        var exam = jest.mock();
        var student = jest.mock();
        var result = new _ExamResult.ExamResult(dataSource, exam, student, null);
        expect(result.Exam).toBe(exam);
        expect(result.Student).toBe(student);
    });

    it('Test Results', function () {
        var dataSource = require('dataSource');
        var exam = jest.mock();
        var student = jest.mock();
        var result = new _ExamResult.ExamResult(dataSource, exam, student, null);
        var returnValue = jest.mock();
        dataSource.getExamResults.mockReturnValueOnce(returnValue);
        var n = dataSource.getExamResults.mock.calls.length;
        expect(result.Answers).toEqual(returnValue);
        expect(dataSource.getExamResults.mock.calls.length).toBe(n + 1);
    });

    it("Test Add Answer", function () {
        var dataSource = require('dataSource');
        var result = new _ExamResult.ExamResult(dataSource, jest.mock(), jest.mock(), null);
        var question = jest.mock();
        var answer = jest.mock();
        var mastery = jest.mock();
        result.Answers;
        result.addAnswer(question, answer, mastery);
        var n = dataSource.getExamResults.mock.calls.length;

        expect(dataSource.addExamAnswer.mock.calls.length).toBe(1);
        expect(dataSource.addExamAnswer.mock.calls[0][0]).toEqual(question);
        expect(dataSource.addExamAnswer.mock.calls[0][1]).toEqual(answer);
        expect(dataSource.addExamAnswer.mock.calls[0][2]).toEqual(mastery);
        result.Answers;
        expect(dataSource.getExamResults.mock.calls.length).toBe(n + 1);
    });

    it("Test Remove Answer", function () {
        var dataSource = require('dataSource');
        var result = new _ExamResult.ExamResult(dataSource, jest.mock(), jest.mock(), null);
        var question = jest.mock();
        var answer = jest.mock();
        var mastery = jest.mock();
        result.Answers;
        result.updateAnswer(question, answer, mastery);
        var n = dataSource.getExamResults.mock.calls.length;

        expect(dataSource.updateExamAnswer.mock.calls.length).toBe(1);
        expect(dataSource.updateExamAnswer.mock.calls[0][0]).toEqual(question);
        expect(dataSource.updateExamAnswer.mock.calls[0][1]).toEqual(answer);
        expect(dataSource.updateExamAnswer.mock.calls[0][2]).toEqual(mastery);
        result.Answers;
        expect(dataSource.getExamResults.mock.calls.length).toBe(n + 1);
    });
});
//# sourceMappingURL=ExamResult.test.js.map