"use strict";

var _MockDataSource = require("../DataSource/MockDataSource.js");

var _ExamResult = require("../Model/ExamResult.js");

var _School = require("../Model/School");

var _Image = require("../Model/Image");

var _Address = require("../Model/Address");

var _Contact = require("../Model/Contact");

var _Teacher = require("../Model/Teacher");

var _Student = require("../Model/Student");

describe('T4I Use Cases', function () {
    beforeAll(function () {});

    it('Init Academic Year Test', function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var academicYears = dataSource.getAcademicYears();
        expect(academicYears.length).toBe(3);

        expect(academicYears[0].Schools.length).toBe(1);
        expect(academicYears[1].Schools.length).toBe(1);
        expect(academicYears[2].Schools.length).toBe(0);
    });

    it('Init School Test', function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var academicYears = dataSource.getAcademicYears();
        expect(academicYears[0].Schools[0].Classes.length).toBe(2);
        expect(academicYears[1].Schools[0].Classes.length).toBe(2);

        var school = dataSource.getSchool("SCH1");
        expect(school.AcademicYear).toEqual(dataSource.getAcademicYear("ACAD02"));
        expect(school.Classes).toEqual([dataSource.getClass("C2"), dataSource.getClass("C3")]);
        expect(school.Teachers).toEqual([dataSource.getTeacher("T001"), dataSource.getTeacher("T003"), dataSource.getTeacher("T004"), dataSource.getTeacher("T005")]);
        expect(school.Students).toEqual([dataSource.getStudent("S001"), dataSource.getStudent("S003"), dataSource.getStudent("S004"), dataSource.getStudent("S005")]);

        school.setAcademicYear(dataSource.getAcademicYear("ACAD01"));
        expect(school.Classes).toEqual([dataSource.getClass("C1")]);
        expect(school.Teachers).toEqual([dataSource.getTeacher("T001"), dataSource.getTeacher("T002"), dataSource.getTeacher("T003"), dataSource.getTeacher("T004")]);
        expect(school.Students).toEqual([dataSource.getStudent("S001"), dataSource.getStudent("S002"), dataSource.getStudent("S003")]);
    });

    it('Init Classes Test', function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var school = dataSource.getSchool("SCH1");
        var cls1A = school.Classes[0];
        var cls2A = school.Classes[1];
        expect(cls1A.School).toEqual(school);
        expect(cls1A.AcademicYear).toEqual(school.AcademicYear);
        expect(cls2A.School).toEqual(school);
        expect(cls2A.AcademicYear).toEqual(school.AcademicYear);
        expect(cls1A.AdminTeacher).toEqual(dataSource.getTeacher("T003"));
        expect(cls2A.AdminTeacher).toEqual(dataSource.getTeacher("T004"));

        expect(cls1A.Subjects).toEqual([dataSource.getSubject("SUB1"), dataSource.getSubject("SUB2"), dataSource.getSubject("SUB3")]);
        expect(cls2A.Subjects).toEqual([dataSource.getSubject("SUB4"), dataSource.getSubject("SUB5")]);

        expect(cls1A.Students).toEqual([dataSource.getStudent("S004"), dataSource.getStudent("S005")]);
        expect(cls2A.Students).toEqual([dataSource.getStudent("S001"), dataSource.getStudent("S003")]);

        expect(cls1A.StudentsByOptionalSubjects).toEqual(new Map([[dataSource.getSubject("SUB2"), [dataSource.getStudent("S004")]], [dataSource.getSubject("SUB3"), [dataSource.getStudent("S005")]]]));
        expect(cls2A.StudentsByOptionalSubjects).toEqual(new Map());

        school.setAcademicYear(dataSource.getAcademicYear("ACAD01"));
        expect(school.Classes[0].ExamsBySubject).toEqual(new Map([[dataSource.getSubject("SUB1"), [dataSource.getExam("EX001")]], [dataSource.getSubject("SUB2"), []], [dataSource.getSubject("SUB3"), []]]));
    });

    it("Init Subject Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var subject = dataSource.getSubject("SUB1");
        expect(subject.Questions).toEqual([dataSource.getQuestion("Q001")]);
    });

    it("Init Question Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var question = dataSource.getQuestion("Q001");
        expect(question.MasteryLevels).toEqual([dataSource.getMasteryLevel("M001"), dataSource.getMasteryLevel("M002"), dataSource.getMasteryLevel("M003")]);
    });

    it("Init Exam test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var exam = dataSource.getExam("EX001");

        expect(exam.Questions).toEqual([dataSource.getQuestion("Q001")]);

        expect(exam.Results).toEqual(new Map([[dataSource.getStudent("S001"), new _ExamResult.ExamResult(dataSource, exam, dataSource.getStudent("S001"))], [dataSource.getStudent("S002"), new _ExamResult.ExamResult(dataSource, exam, dataSource.getStudent("S002"))], [dataSource.getStudent("S003"), new _ExamResult.ExamResult(dataSource, exam, dataSource.getStudent("S003"))]]));
    });

    it("Init Exam Result Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var exam = dataSource.getExam("EX001");
        var result = exam.Results.get(dataSource.getStudent("S001"));
        expect(result.Answers).toEqual(new Map([[dataSource.getQuestion("Q001"), {
            "Answer": "",
            "Mastery": dataSource.getMasteryLevel("M001")
        }]]));
    });

    it("New Academic Year Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var academicYears = dataSource.getAcademicYears();
        var curAY = academicYears[1];
        var newAY = academicYears[2];
        var school = curAY.Schools[0];
        var curClasses = dataSource.getClassesForAcademicYear(curAY, school);

        school.addAcademicYear(newAY);
        var newClasses = dataSource.getClassesForAcademicYear(newAY, school);
        expect(newClasses.length).toBe(curClasses.length);
        for (var i = 0; i < curClasses.length; i++) {
            expect(newClasses[i].Subjects).toEqual(curClasses[i].Subjects);
            expect(newClasses[i].AdminTeacher).toEqual(curClasses[i].AdminTeacher);
            expect(newClasses[i].TeachersBySubject).toEqual(curClasses[i].TeachersBySubject);
        }
    });

    it("Add New School Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var image = new _Image.Image("img1", "img");
        var address = new _Address.Address("Building1", "Street1", "City1", "District1", "State1", "Country1", "Postal1");
        var school = new _School.School(dataSource, "New School", null, address, image, "New Phone1", "New Phone2", "New Fax", "New EMail");
        var academicYear = dataSource.getAcademicYear("ACAD02");
        var addedSchool = academicYear.addSchool(school.Name, school.RegistrationData, school.Image, address.BuildingName, address.StreetName, address.City, address.District, address.State, address.Country, address.Postal, school.Phone1, school.Phone2, school.Fax, school.Email);
        expect(academicYear.Schools.length).toBe(2);
        school.setId(addedSchool.Id);
        expect(addedSchool).toEqual(school);
        expect(addedSchool.AcademicYear).toEqual(academicYear);
        expect(addedSchool.Contacts.length).toBe(0);
        expect(addedSchool.Classes.length).toBe(0);
        expect(addedSchool.Teachers.length).toBe(0);
        expect(addedSchool.Students.length).toBe(0);
    });

    it("School Contacts Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var school = dataSource.getSchool("SCH1");
        var n = school.Contacts.length;
        var contact = new _Contact.Contact(dataSource, "First Name1", "Last Name1", "Principal", "Phone", "Mobile", "Fax", "Mail", "School", school.Id);
        var addedContact = school.addContact(contact.FirstName, contact.LastName, contact.Relationship, contact.Phone, contact.Mobile, contact.Fax, contact.Email);
        contact.setId(addedContact.Id);
        expect(school.Contacts.length).toBe(n + 1);
        expect(addedContact).toEqual(contact);
    });

    it("Add/Remove School Teacher Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var school = dataSource.getSchool("SCH1");
        var n = school.Teachers.length;
        var image = new _Image.Image("img1", "img");
        var address = new _Address.Address("Building1", "Street1", "City1", "District1", "State1", "Country1", "Postal1");
        var teacher = new _Teacher.Teacher(dataSource, "T6", "t6", "TEACHER001", "M", new Date(), address, image, "Phone1", "Phone2", "Email");
        var addedTeacher = school.addTeacher(teacher.FirstName, teacher.LastName, teacher.EmployeeID, teacher.Gender, teacher.DOB, teacher.Image, address.BuildingName, address.StreetName, address.City, address.District, address.State, address.Country, address.Postal, teacher.Phone1, teacher.Phone2, teacher.Email);
        teacher.setId(addedTeacher.Id);
        expect(school.Teachers.length).toBe(n + 1);
        expect(addedTeacher).toEqual(teacher);
    });

    it("Add/Remove School Student Test", function () {
        var dataSource = new _MockDataSource.MockDataSource();
        var school = dataSource.getSchool("SCH1");
        var n = school.Teachers.length;
        var image = new _Image.Image("img1", "img");
        var address = new _Address.Address("Building1", "Street1", "City1", "District1", "State1", "Country1", "Postal1");
        var student = new _Student.Student(dataSource, "ST6", "St6", "ST008", "M", new Date(), address, image, "Phone1", "Phone2", "Email");
        var addedStudent = school.addStudent(student.FirstName, student.LastName, student.RollNumber, student.Gender, student.DOB, student.Image, address.BuildingName, address.StreetName, address.City, address.District, address.State, address.Country, address.Postal, student.Phone1, student.Phone2, student.Email);
        student.setId(addedStudent.Id);
        expect(school.Students.length).toBe(n + 1);
        expect(addedStudent).toEqual(student);
    });

    it("Test Add New Class", function () {
        expect(1).toBe(2);
    });

    it("Test Add Next Academic Year", function () {
        expect(1).toBe(2);
    });

    it("Test Add/Remove Student in Class", function () {
        expect(1).toBe(2);
    });

    it("Test Add/Remove Subject", function () {
        expect(1).toBe(2);
    });

    it("Test Add/Remove Admin Teacher", function () {
        expect(1).toBe(2);
    });

    it("Test Add/Remove Teacher for Subject", function () {
        expect(1).toBe(2);
    });

    it("Test Add/Remove Students to Optional Subjects", function () {
        expect(1).toBe(2);
    });

    it("Test Students By Optional Subjects", function () {
        expect(1).toBe(2);
    });

    it("Test Add/Remove Exams in Class", function () {
        expect(1).toBe(2);
    });

    it("Test Add/Remove Exam Results in Class", function () {
        expect(1).toBe(2);
    });

    it("Test Exams By Subjects", function () {
        expect(1).toBe(2);
    });

    it("Test Create Subject", function () {
        expect(1).toBe(2);
    });

    it("Test Create Question Bank", function () {
        expect(1).toBe(2);
    });

    it("Test Create Exam", function () {
        expect(1).toBe(2);
    });

    it("Test Add Exam Results for Students", function () {
        expect(1).toBe(2);
    });
});
//# sourceMappingURL=UseCases.test.js.map