"use strict";

var _School = require("../Model/School.js");

var _Address = require("../Model/Address.js");

var _Image = require("../Model/Image.js");

var _Teacher = require("../Model/Teacher.js");

var _Class = require("../Model/Class.js");

var _AcademicYear = require("../Model/AcademicYear.js");

var _Contact = require("../Model/Contact.js");

var _Subject = require("../Model/Subject.js");

var _Student = require("../Model/Student.js");

var getSchool = function getSchool(dataSource) {
    return new _School.School(dataSource, "School1", null, null, null, "p1", "p2", "fax", "email", "sch1");
};

describe('School Tests', function () {
    beforeEach(function () {
        jest.mock('dataSource', function () {
            return {
                getSchoolContacts: jest.fn(),
                getCurrentAcademicYearForSchool: jest.fn(),
                getClassesForAcademicYear: jest.fn(),
                getTeachersForAcademicYear: jest.fn(),
                addTeacher: jest.fn(),
                addTeacherToSchool: jest.fn(),
                removeTeacherFromSchool: jest.fn(),
                addClass: jest.fn(),
                getPreviousAcademicYear: jest.fn(),
                getClassesFromPrevAcademicYear: jest.fn(),
                addSchoolToAcademicYear: jest.fn(),
                getAdminTeacher: jest.fn(),
                addAdminTeacher: jest.fn(),
                getSubjectsForClass: jest.fn(),
                addSchoolContact: jest.fn()
            };
        }, { virtual: true });
    });

    it('Properties Test', function () {
        var dataSource = require('dataSource');
        var id = "school1";
        var name = "School1";
        var registrationData = jest.mock();
        var phone1 = "Phone1";
        var phone2 = "Phone2";
        var fax = "Fax";
        var email = "Email";
        var schoolContacts = ["contact1"];
        dataSource.getSchoolContacts.mockReturnValue(schoolContacts);
        var academicYear = "AcademicYear1";
        dataSource.getCurrentAcademicYearForSchool.mockReturnValue(academicYear);
        var classes = ["Class1"];
        dataSource.getClassesForAcademicYear.mockReturnValue(classes);
        var teachers = ["Teacher1"];
        dataSource.getTeachersForAcademicYear.mockReturnValue(teachers);

        var image = new _Image.Image("img1", "img");
        var address = new _Address.Address("Building1", "Street1", "City1", "District1", "State1", "Country1", "Postal1");
        var school = new _School.School(dataSource, name, registrationData, address, image, phone1, phone2, fax, email, id);

        expect(school.Name).toBe(name);
        expect(school.RegistrationData, registrationData);
        expect(school.Phone1).toBe(phone1);
        expect(school.Phone2).toBe(phone2);
        expect(school.Fax).toBe(fax);
        expect(school.Email).toBe(email);
        expect(school.Id).toBe(id);
        expect(school.Address.toJSON()).toEqual(address.toJSON());
        expect(school.Image.toJSON()).toEqual(image.toJSON());
        expect(school.Contacts).toBe(schoolContacts);
        school.Contacts;
        expect(dataSource.getSchoolContacts.mock.calls.length).toBe(1);
        expect(school.AcademicYear).toBe(academicYear);
        school.AcademicYear;
        expect(dataSource.getCurrentAcademicYearForSchool.mock.calls.length).toBe(1);
        expect(school.Classes).toBe(classes);
        school.Classes;
        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(1);
        expect(school.Teachers).toBe(teachers);
        school.Teachers;
        expect(dataSource.getTeachersForAcademicYear.mock.calls.length).toBe(1);
    });

    it('Add Teacher Test', function () {
        var dataSource = require('dataSource');
        var school = getSchool(dataSource);
        var image = new _Image.Image("img1", "img");
        var address = new _Address.Address("Building1", "Street1", "City1", "District1", "State1", "Country1", "Postal1");
        var teacher = new _Teacher.Teacher(dataSource, "firstname", "lastname", "empid", "M", new Date(), address, image, "Phone1", "Phone2", "Email");
        var addedTeacher = school.addTeacher(teacher.FirstName, teacher.LastName, teacher.EmployeeID, teacher.Gender, teacher.DOB, teacher.Image, teacher.Address.BuildingName, teacher.Address.StreetName, teacher.Address.City, teacher.Address.District, teacher.Address.State, teacher.Address.Country, teacher.Address.Postal, teacher.Phone1, teacher.Phone2, teacher.Email);

        expect(addedTeacher.toJSON()).toEqual(teacher.toJSON());

        expect(dataSource.addTeacher.mock.calls.length).toBe(1);
        expect(dataSource.addTeacherToSchool.mock.calls.length).toBe(1);

        var arg0 = dataSource.addTeacher.mock.calls[0][0];
        expect(arg0.toJSON()).toEqual(teacher.toJSON());

        arg0 = dataSource.addTeacherToSchool.mock.calls[0][0];
        var arg1 = dataSource.addTeacherToSchool.mock.calls[0][1];
        expect(arg0).toEqual(school);
        expect(arg1.toJSON()).toEqual(teacher.toJSON());

        // We expect teachers to be reset
        school.Teachers;
        expect(dataSource.getTeachersForAcademicYear.mock.calls.length).toBe(2);
    });

    it('Remove Teacher Test', function () {
        var dataSource = require('dataSource');
        var school = getSchool(dataSource);
        var teacher = "Teacher";
        school.removeTeacher(teacher);

        expect(dataSource.removeTeacherFromSchool.mock.calls.length).toBe(1);

        var arg0 = dataSource.removeTeacherFromSchool.mock.calls[0][0];
        var arg1 = dataSource.removeTeacherFromSchool.mock.calls[0][1];
        expect(arg0).toEqual(school);
        expect(arg1).toEqual(teacher);

        // We expect teachers to be reset
        var n = dataSource.getTeachersForAcademicYear.mock.calls.length;
        school.Teachers;
        expect(dataSource.getTeachersForAcademicYear.mock.calls.length).toBe(n + 1);
    });

    it("Add Class Test", function () {
        var dataSource = require('dataSource');
        var academicYear = new _AcademicYear.AcademicYear(dataSource, "academic year", new Date(), new Date());
        var school = getSchool(dataSource);
        var cls = new _Class.Class(dataSource, "class", "grade", "section", "code", "medium", academicYear, school);
        dataSource.getCurrentAcademicYearForSchool.mockReturnValue(academicYear);
        var addedCls = school.addClass(cls.Name, cls.Grade, cls.Section, cls.Code, cls.TeachingMedium);
        expect(addedCls.toJSON()).toEqual(cls.toJSON());
        var n = dataSource.getClassesForAcademicYear.mock.calls.length;
        school.Classes;
        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(n + 1);
    });

    it("Previous Class Test", function () {
        var dataSource = require('dataSource');
        var school = getSchool(dataSource);
        var academicYear = new _AcademicYear.AcademicYear(dataSource, "academic year", new Date(), new Date());
        var cls1A = new _Class.Class(dataSource, "1A", "1", "A", "code", "medium", academicYear, school);
        var cls1B = new _Class.Class(dataSource, "1B", "1", "B", "code", "medium", academicYear, school);
        var cls2A = new _Class.Class(dataSource, "2A", "2", "A", "code", "medium", academicYear, school);
        dataSource.getClassesForAcademicYear.mockReturnValue([cls1A, cls1B, cls2A]);
        expect(school.getPrevClass(cls1A)).toBe(null);
        expect(school.getPrevClass(cls1B)).toBe(null);
        expect(school.getPrevClass(cls2A).toJSON()).toEqual(cls1A.toJSON());
    });

    it("Previous Classes Test", function () {
        var dataSource = require('dataSource');
        var school = getSchool(dataSource);
        var ay1 = new _AcademicYear.AcademicYear(dataSource, "ay1", null, new Date(), new Date());
        var ay2 = new _AcademicYear.AcademicYear(dataSource, "ay2", null, new Date(), new Date());
        var returnValue = "test";
        dataSource.getCurrentAcademicYearForSchool.mockReturnValue(ay2);
        dataSource.getPreviousAcademicYear.mockReturnValue(ay1);
        dataSource.getClassesForAcademicYear.mockReturnValue(returnValue);
        var n = dataSource.getClassesForAcademicYear.mock.calls.length;
        expect(school.getClassesFromPrevAcademicYear(ay2)).toBe(returnValue);
        expect(dataSource.getPreviousAcademicYear.mock.calls.length).toBe(1);
        var arg0 = dataSource.getPreviousAcademicYear.mock.calls[0][0];
        expect(arg0.toJSON()).toEqual(ay2.toJSON());

        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(n + 1);
        arg0 = dataSource.getClassesForAcademicYear.mock.calls[n][0];
        var arg1 = dataSource.getClassesForAcademicYear.mock.calls[n][1];
        expect(arg0.toJSON()).toEqual(ay1.toJSON());
        expect(arg1.toJSON()).toEqual(school.toJSON());
    });

    it("Add Contact Test", function () {
        var dataSource = require('dataSource');
        var school = getSchool(dataSource);
        var contact = new _Contact.Contact(dataSource, "fn", "ln", "rel", "phone", "mobile", "fax", "mail", "School", school.Id);
        var addedContact = school.addContact(contact.FirstName, contact.LastName, contact.Relationship, contact.Phone, contact.Mobile, contact.Fax, contact.Email);
        expect(addedContact.toJSON()).toEqual(contact.toJSON());

        var n = dataSource.getSchoolContacts.mock.calls.length;
        school.Contacts;
        expect(dataSource.getSchoolContacts.mock.calls.length).toBe(n + 1);
    });

    it("Set Academic Year Test", function () {
        var dataSource = require('dataSource');
        var school = getSchool(dataSource);
        var ay1 = new _AcademicYear.AcademicYear(dataSource, "ay1", null, new Date(), new Date());

        school.setAcademicYear(ay1);
        expect(school.AcademicYear.toJSON()).toEqual(ay1.toJSON());
        var n = dataSource.getClassesForAcademicYear.mock.calls.length;
        school.Classes;
        expect(dataSource.getClassesForAcademicYear.mock.calls.length).toBe(n + 1);
    });
});
//# sourceMappingURL=School.test.js.map