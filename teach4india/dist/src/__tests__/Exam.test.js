"use strict";

var _Exam = require("../Model/Exam.js");

var _ExamResult = require("../Model/ExamResult.js");

describe('Exam Tests', function () {

    beforeEach(function () {
        jest.mock('dataSource', function () {
            return {
                getExamQuestions: jest.fn(),
                addQuestionToExam: jest.fn(),
                removeQuestionFromExam: jest.fn(),
                updateExam: jest.fn()
            };
        }, { virtual: true });
    });

    it('Test Properties', function () {
        var dataSource = require('dataSource');
        var cls = jest.mock("../Model/Class.js");
        var subject = jest.mock("../Model/Subject.js");
        var date = new Date();
        var exam = new _Exam.Exam(dataSource, 'Exam1', date, 100, null, cls, subject);
        expect(exam.Name).toBe('Exam1');
        expect(exam.Date).toBe(date);
        expect(exam.PassMarks).toBe(100);
        expect(exam.PassGrades).toBeNull();
        expect(exam.Class).toBe(cls);
        expect(exam.Subject).toBe(subject);
    });

    it("Test Questions", function () {
        var dataSource = require('dataSource');
        var exam = new _Exam.Exam(dataSource, 'Exam1', new Date(), 100, null, jest.mock("../Model/Class.js"), jest.mock("../Model/Subject.js"));
        var returnValue = jest.mock();
        dataSource.getExamQuestions.mockReturnValue(returnValue);
        expect(exam.Questions).toEqual(returnValue);
        expect(dataSource.getExamQuestions.mock.calls.length).toBe(1);
        var arg0 = dataSource.getExamQuestions.mock.calls[0][0];
        expect(arg0).toEqual(exam);
    });

    it("Test Exam Results", function () {
        var dataSource = require('dataSource');
        var cls = jest.mock();
        var subject = jest.mock();
        var exam = new _Exam.Exam(dataSource, 'Exam1', new Date(), 100, null, cls, subject);
        var student1 = jest.mock();
        var student2 = jest.mock();
        Object.defineProperty(cls, 'Students', { value: [student1, student2] });
        expect(exam.Results).toEqual(new Map([[student1, new _ExamResult.ExamResult(dataSource, exam, student1)], [student2, new _ExamResult.ExamResult(dataSource, exam, student2)]]));
    });

    it("Test Add Question", function () {
        var dataSource = require('dataSource');
        var exam = new _Exam.Exam(dataSource, 'Exam1', new Date(), 100, null, jest.mock(), jest.mock());
        var question = jest.mock();
        var returnValue = jest.mock();
        dataSource.getExamQuestions.mockReturnValue(returnValue);
        exam.Questions;
        exam.addQuestion(question);
        var n = dataSource.getExamQuestions.mock.calls.length;

        expect(dataSource.addQuestionToExam.mock.calls.length).toBe(1);
        var arg0 = dataSource.addQuestionToExam.mock.calls[0][0];
        var arg1 = dataSource.addQuestionToExam.mock.calls[0][1];
        expect(arg0).toEqual(exam);
        expect(arg1).toEqual(question);
        exam.Questions;
        expect(dataSource.getExamQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Remove Question", function () {
        var dataSource = require('dataSource');
        var exam = new _Exam.Exam(dataSource, 'Exam1', new Date(), 100, null, jest.mock(), jest.mock());
        var question = jest.mock();
        var returnValue = jest.mock();
        dataSource.getExamQuestions.mockReturnValue(returnValue);
        exam.Questions;
        exam.removeQuestion(question);
        var n = dataSource.getExamQuestions.mock.calls.length;

        expect(dataSource.removeQuestionFromExam.mock.calls.length).toBe(1);
        var arg0 = dataSource.removeQuestionFromExam.mock.calls[0][0];
        var arg1 = dataSource.removeQuestionFromExam.mock.calls[0][1];
        expect(arg0).toEqual(exam);
        expect(arg1).toEqual(question);
        exam.Questions;
        expect(dataSource.getExamQuestions.mock.calls.length).toBe(n + 1);
    });

    it("Test Update", function () {
        var dataSource = require('dataSource');
        var exam = new _Exam.Exam(dataSource, 'Exam1', new Date(), 100, null, jest.mock(), jest.mock());
        exam.update();

        expect(dataSource.updateExam.mock.calls.length).toBe(1);
        var arg0 = dataSource.updateExam.mock.calls[0][0];
        expect(arg0).toEqual(exam);
    });
});
//# sourceMappingURL=Exam.test.js.map