"use strict";

var _Student = require("../Model/Student");

var _Contact = require("../Model/Contact");

var _School = require("../Model/School");

var _Class = require("../Model/Class");

var _AcademicYear = require("../Model/AcademicYear");

describe('Student Test', function () {
    beforeAll(function () {
        jest.mock('dataSource', function () {
            var contact = require('../Model/Contact.js');
            var school = require('../Model/School.js');
            var academicYear = require('../Model/AcademicYear.js');
            var cls = require('../Model/Class.js');
            var Contact = contact.Contact;
            var School = school.School;
            var AcademicYear = academicYear.AcademicYear;
            var Class = cls.Class;

            var sch = new School(null, 'School for Student', 'Test Address', null);
            var ay = new AcademicYear(null, '2017', null, null, null);
            sch.academicYear = ay;
            return {
                getStudentContacts: function getStudentContacts(student) {
                    return new Contact(null, 'Jane', 'Smith', null, '991', '9991', null, 'mother@tfi.com');
                },
                getSchoolForStudent: function getSchoolForStudent(student) {
                    return sch;
                },
                getClassForStudent: function getClassForStudent(ay, scl, student) {
                    return new Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch);
                }
            };
        }, { virtual: true });
    });

    it('Student Getter Test', function () {
        var student = new _Student.Student(null, 'John', 'Smith', 1, 'M', new Date(2010, 10, 2), 'India', null, 6458201, 6458021, 'test@tfi.com', 12);

        expect(student.Name).toBe('John Smith');
        expect(student.Address).toBe('India');
        expect(student.Gender).toBe('M');
        expect(student.DOB).toBeInstanceOf(Date);
        expect(student.Phone1).toBe(6458201);
        expect(student.Phone2).toBe(6458021);
        expect(student.RollNumber).toBe(1);
        expect(student.Email).toBe('test@tfi.com');
    });

    it('Student dataSource Getter Test', function () {
        var dataSource = require('dataSource');
        var ay = new _AcademicYear.AcademicYear(null, '2017', null, null, null);
        var sch = new _School.School(null, 'School for Student', 'Test Address', null);
        var cls = new _Class.Class(null, 'Class1', 'grade1', 1, 12, 'English', ay, sch);
        sch.academicYear = ay;

        var student = new _Student.Student(dataSource, 'John', 'Smith', 1, 'M', new Date(2010, 10, 2), null, 'India', 6458201, 6458021, 'test@tfi.com', 12);
        expect(student.Class).toBeInstanceOf(_Class.Class);
        expect(student.School).toBeInstanceOf(_School.School);
        expect(student.Class).toBeInstanceOf(_Class.Class);

        expect(student.Contacts).toEqual(new _Contact.Contact(null, 'Jane', 'Smith', null, '991', '9991', null, 'mother@tfi.com'));
        expect(student.School).toEqual(sch);
        expect(student.Class).toEqual(cls);
        expect(student.AcademicYear).toEqual(ay);
    });
});
//# sourceMappingURL=Student.test.js.map