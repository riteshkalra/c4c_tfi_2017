"use strict";

var _Question = require("../Model/Question.js");

var _ExamResult = require("../Model/ExamResult.js");

var _Mastery = require("../Model/Mastery");

describe('Question Tests', function () {

    beforeEach(function () {
        jest.mock('dataSource', function () {
            return {
                getMasteryLevels: jest.fn(),
                addMasteryLevel: jest.fn(),
                removeMasteryLevel: jest.fn(),
                updateQuestion: jest.fn()
            };
        }, { virtual: true });
    });

    it('Test Properties', function () {
        var dataSource = require('dataSource');
        var description = jest.mock();
        var concept = jest.mock();
        var questionType = jest.mock();
        var difficultyLevel = jest.mock();
        var options = jest.mock();
        var answer = jest.mock();
        var subject = jest.mock();
        var question = new _Question.Question(dataSource, description, concept, questionType, difficultyLevel, options, answer, subject);
        expect(question.Description).toEqual(description);
        expect(question.Concept).toEqual(concept);
        expect(question.Type).toEqual(questionType);
        expect(question.DifficultyLevel).toEqual(difficultyLevel);
        expect(question.Options).toEqual(options);
        expect(question.Answer).toEqual(answer);
        expect(question.Subject).toEqual(subject);
    });

    it("Test Mastery Levels", function () {
        var dataSource = require('dataSource');
        var question = new _Question.Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock());
        var returnValue = jest.mock();
        dataSource.getMasteryLevels.mockReturnValue(returnValue);
        expect(question.MasteryLevels).toEqual(returnValue);
        expect(dataSource.getMasteryLevels.mock.calls.length).toBe(1);
        expect(dataSource.getMasteryLevels.mock.calls[0][0]).toEqual(question);
    });

    it("Test Add Mastery Level", function () {
        var dataSource = require('dataSource');
        var question = new _Question.Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock());
        var masteryLevel = new _Mastery.MasteryLevel(dataSource, question, "Partial answer", 50, "A");
        question.MasteryLevels;
        question.addMasteryLevel(masteryLevel.Mastery, masteryLevel.Mark, masteryLevel.Grade);
        var n = dataSource.getMasteryLevels.mock.calls.length;

        expect(dataSource.addMasteryLevel.mock.calls.length).toBe(1);
        expect(dataSource.addMasteryLevel.mock.calls[0][0]).toEqual(masteryLevel);
        question.MasteryLevels;
        expect(dataSource.getMasteryLevels.mock.calls.length).toBe(n + 1);
    });

    it("Test Remove Mastery Level", function () {
        var dataSource = require('dataSource');
        var question = new _Question.Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock());
        var masteryLevel = new _Mastery.MasteryLevel(dataSource, question, "Partial answer", 50, "A");
        question.MasteryLevels;
        question.removeMasteryLevel(masteryLevel);
        var n = dataSource.getMasteryLevels.mock.calls.length;

        expect(dataSource.removeMasteryLevel.mock.calls.length).toBe(1);
        expect(dataSource.removeMasteryLevel.mock.calls[0][0]).toEqual(masteryLevel);
        question.MasteryLevels;
        expect(dataSource.getMasteryLevels.mock.calls.length).toBe(n + 1);
    });

    it("Test Update", function () {
        var dataSource = require('dataSource');
        var question = new _Question.Question(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock(), jest.mock());
        question.update();

        expect(dataSource.updateQuestion.mock.calls.length).toBe(1);
        expect(dataSource.updateQuestion.mock.calls[0][0]).toEqual(question);
    });
});
//# sourceMappingURL=Question.test.js.map