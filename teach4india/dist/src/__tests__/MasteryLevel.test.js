'use strict';

var _Mastery = require('../Model/Mastery');

describe('Mastery Level Tests', function () {

    beforeEach(function () {
        jest.mock('dataSource', function () {
            return {
                updateMasteryLevel: jest.fn()
            };
        }, { virtual: true });
    });

    it('Test Properties', function () {
        var dataSource = require('dataSource');
        var question = jest.mock();
        var mastery = jest.mock();
        var mark = jest.mock();
        var grade = jest.mock();
        var masteryLevel = new _Mastery.MasteryLevel(dataSource, question, mastery, mark, mark);
        expect(masteryLevel.Question).toEqual(question);
        expect(masteryLevel.Mastery).toEqual(mastery);
        expect(masteryLevel.Mark).toEqual(mark);
        expect(masteryLevel.Grade).toEqual(mark);
    });

    it("Test Update", function () {
        var dataSource = require('dataSource');
        var masteryLevel = new _Mastery.MasteryLevel(dataSource, jest.mock(), jest.mock(), jest.mock(), jest.mock());
        masteryLevel.update();

        expect(dataSource.updateMasteryLevel.mock.calls.length).toBe(1);
        expect(dataSource.updateMasteryLevel.mock.calls[0][0]).toEqual(masteryLevel);
    });
});
//# sourceMappingURL=MasteryLevel.test.js.map