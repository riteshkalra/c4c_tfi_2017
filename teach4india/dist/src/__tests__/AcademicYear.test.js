"use strict";

var _AcademicYear = require("../Model/AcademicYear.js");

var _School = require("../Model/School.js");

var _Address = require("../Model/Address");

describe('AcademicYear Tests', function () {
    beforeAll(function () {
        jest.mock('dataSource', function () {
            return {
                addSchool: jest.fn(),
                addSchoolToAcademicYear: jest.fn(),
                getSchoolsForAcademicYear: jest.fn()
            };
        }, { virtual: true });
    });

    it('Properties Test', function () {
        var dataSource = null;
        var id = "acd01";
        var name = "2016-17";
        var board = "cbse";
        var startDate = new Date(2016, 4, 1);
        var endDate = new Date(2017, 3, 31);
        var academicYear = new _AcademicYear.AcademicYear(dataSource, name, board, startDate, endDate, id);

        expect(academicYear.dataSource).toBe(dataSource);
        expect(academicYear.Name).toBe(name);
        expect(academicYear.Board).toBe(board);
        expect(academicYear.StartDate).toBe(startDate);
        expect(academicYear.EndDate).toBe(endDate);
        expect(academicYear.Id).toBe(id);
    });

    it('Add School Test', function () {
        var dataSource = require('dataSource');
        var academicYear = new _AcademicYear.AcademicYear(dataSource, "test", null, null, null);
        var address = new _Address.Address("Building1", "Street1", "City1", "District1", "State1", "Country1", "Postal1");
        var school = new _School.School(dataSource, "School1", null, address, null, "Phone1", "Phone2", "Fax", "Email");
        var n = dataSource.addSchool.mock.calls.length;
        academicYear.addSchool(school.Name, school.RegistrationData, school.Image, school.Address.BuildingName, school.Address.StreetName, school.Address.City, school.Address.District, school.Address.State, school.Address.Country, school.Address.Postal, school.Phone1, school.Phone2, school.Fax, school.Email);

        expect(dataSource.addSchool.mock.calls.length).toBe(n + 1);
        expect(dataSource.addSchoolToAcademicYear.mock.calls.length).toBe(1);
        expect(dataSource.addSchool.mock.calls[0][0].toJSON()).toEqual(school.toJSON());

        expect(dataSource.addSchoolToAcademicYear.mock.calls[0][0]).toEqual(academicYear);
        expect(dataSource.addSchoolToAcademicYear.mock.calls[0][1]).toEqual(school);
    });

    it('Get Schools Test', function () {
        var dataSource = require('dataSource');
        var academicYear = new _AcademicYear.AcademicYear(dataSource, "test", null, null, null);
        var n = dataSource.getSchoolsForAcademicYear.mock.calls.length;
        academicYear.Schools;

        expect(dataSource.getSchoolsForAcademicYear.mock.calls.length).toBe(n + 1);
        expect(dataSource.getSchoolsForAcademicYear.mock.calls[0][0]).toEqual(academicYear);
    });
});
//# sourceMappingURL=AcademicYear.test.js.map