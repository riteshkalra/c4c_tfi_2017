"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MockDataSource = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _School = require("../Model/School.js");

var _AcademicYear = require("../Model/AcademicYear.js");

var _Address = require("../Model/Address.js");

var _Class = require("../Model/Class.js");

var _Contact = require("../Model/Contact.js");

var _Exam = require("../Model/Exam.js");

var _ExamResult = require("../Model/ExamResult.js");

var _Mastery = require("../Model/Mastery.js");

var _Question = require("../Model/Question.js");

var _Student = require("../Model/Student.js");

var _Subject = require("../Model/Subject.js");

var _Teacher = require("../Model/Teacher.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MockDataSource = exports.MockDataSource = function () {
    function MockDataSource() {
        _classCallCheck(this, MockDataSource);

        this.initializeAcademicYears();
        this.initializeSchools();
        this.initializeClasses();
        this.initializeStudents();
        this.initializeStudentsBySchool();
        this.initializeStudentsByClass();
        this.initializeTeachers();
        this.initializeTeachersBySchool();
        this.initializeSubjects();
        this.initializeSubjectsByClass();
        this.initializeTeachersBySubject();
        this.initializeSubjectsByStudent();
        this.initializeQuestions();
        this.initializeMasteryLevels();
        this.initializeExams();
        this.initializeExamQuestions();
        this.initializeExamResults();
    }

    _createClass(MockDataSource, [{
        key: "toDate",
        value: function toDate(val) {
            return val != null ? new Date(val) : null;
        }
    }, {
        key: "getUniqueId",
        value: function getUniqueId() {
            return Math.random().toString(36).substr(2, 16);
        }
    }, {
        key: "initializeAcademicYears",
        value: function initializeAcademicYears() {
            this.academicYears = new Map();
            var academicYearsObj = require("./Mock/academicyears.json");
            for (var academicYearId in academicYearsObj) {
                var mapping = academicYearsObj[academicYearId];
                this.academicYears.set(academicYearId, _AcademicYear.AcademicYear.fromJSON(this, academicYearId, mapping));
            }
        }
    }, {
        key: "initializeContacts",
        value: function initializeContacts(entityName, entityId) {
            var contacts = new Map();
            var jsonObj = require("./Mock/academicyears.json");
            for (var contactId in jsonObj) {
                var mapping = jsonObj[contactId];
                if (mapping["EntityName"] == entityName && mapping["EntityId"] == entityId) {
                    contacts.push(_Contact.Contact.fromJSON(this, contactId, mapping));
                }
            }
            return contacts;
        }
    }, {
        key: "initializeSchools",
        value: function initializeSchools() {
            this.schools = new Map();
            this.schoolContacts = new Map();
            var jsonObj = require("./Mock/schools.json");
            for (var schoolId in jsonObj) {
                var mapping = jsonObj[schoolId];
                var school = _School.School.fromJSON(this, schoolId, mapping);
                this.schools.set(schoolId, school);
                this.schoolContacts.set(school, this.initializeContacts("School", schoolId));
            }

            this.schoolsByAcademicYear = new Map();
            jsonObj = require("./Mock/academicyear_schools.json");
            for (var academicYearId in jsonObj) {
                var _schoolId = jsonObj[academicYearId];
                var academicYear = this.academicYears.get(academicYearId);
                var _school = this.schools.get(_schoolId);
                if (!(academicYearId in this.schoolsByAcademicYear)) {
                    this.schoolsByAcademicYear.set(academicYear.Id, []);
                }
                this.schoolsByAcademicYear.get(academicYear.Id).push(_school);
            }
        }
    }, {
        key: "initializeClasses",
        value: function initializeClasses() {
            this.classes = new Map();
            var jsonObj = require("./Mock/classes.json");
            for (var classId in jsonObj) {
                var mapping = jsonObj[classId];
                var cls = _Class.Class.fromJSON(this, classId, mapping);
                this.classes.set(classId, cls);
            }
        }
    }, {
        key: "initializeStudents",
        value: function initializeStudents() {
            var jsonObj = require("./Mock/students.json");
            this.students = new Map();
            this.studentContacts = new Map();
            for (var studentId in jsonObj) {
                var mapping = jsonObj[studentId];
                var student = _Student.Student.fromJSON(this, studentId, mapping);
                this.students.set(studentId, student);
                this.studentContacts.set(student, this.initializeContacts("Student", studentId));
            }
        }
    }, {
        key: "initializeStudentsBySchool",
        value: function initializeStudentsBySchool() {
            var jsonObj = require("./Mock/school_students.json");
            this.studentsBySchool = new Map();
            for (var schoolId in jsonObj) {
                var mappings = jsonObj[schoolId];
                if (schoolId in this.schools) {
                    var school = this.schools.get(schoolId);
                    if (!(school in this.teachersBySchool)) {
                        this.studentsBySchool.set(school, []);
                    }
                    for (var mapping in mappings) {
                        var studentId = mapping["Student"];
                        if (studentId in this.students) {
                            this.studentsBySchool.get(school).push({
                                "Student": this.students.get(studentId),
                                "StartDate": this.toDate(mapping["StartDate"]),
                                "EndDate": this.toDate(mapping["EndDate"])
                            });
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeStudentsByClass",
        value: function initializeStudentsByClass() {
            var jsonObj = require("./Mock/class_students.json");
            this.studentsByClass = new Map();
            for (var classId in jsonObj) {
                var mappings = jsonObj[classId];
                if (!(classId in this.classes)) continue;
                var cls = this.classes.get(classId);
                if (!(cls in this.studentsByClass)) {
                    this.studentsByClass.set(cls, []);
                }
                for (var mapping in mappings) {
                    var studentId = mapping["Student"];
                    if (studentId in this.students) {
                        this.studentsByClass.get(cls).push({
                            "Student": this.students.get(studentId),
                            "StartDate": this.toDate(mapping["StartDate"]),
                            "EndDate": this.toDate(mapping["EndDate"])
                        });
                    }
                }
            }
        }
    }, {
        key: "initializeTeachers",
        value: function initializeTeachers() {
            var jsonObj = require("./Mock/teachers.json");
            this.teachers = new Map();
            this.teacherContacts = new Map();
            for (var teacherId in jsonObj) {
                var mapping = jsonObj[teacherId];
                var teacher = _Teacher.Teacher.fromJSON(this, teacherId, mapping);
                this.teachers.set(teacherId, teacher);
                this.teacherContacts.set(teacher, this.initializeContacts("Teacher", teacherId));
            }
        }
    }, {
        key: "initializeTeachersBySchool",
        value: function initializeTeachersBySchool() {
            var jsonObj = require("./Mock/school_teachers.json");
            this.teachersBySchool = new Map();
            for (var schoolId in jsonObj) {
                var mappings = jsonObj[schoolId];
                if (schoolId in this.schools) {
                    var school = this.schools.get(schoolId);
                    if (!(school in this.teachersBySchool)) {
                        this.teachersBySchool.set(school, []);
                    }
                    for (var mapping in mappings) {
                        var teacherId = mapping["Teacher"];
                        if (teacherId in this.teachers) {
                            var teacher = this.teachers.get(teacherId);
                            this.teachersBySchool.get(school).push({
                                "Teacher": this.teachers.get(teacherId),
                                "StartDate": this.toDate(mapping["StartDate"]),
                                "EndDate": this.toDate(mapping["EndDate"])
                            });
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeTeachersByClass",
        value: function initializeTeachersByClass() {
            var jsonObj = require("./Mock/class_teachers.json");
            this.teachersByClass = new Map();
            for (var classId in jsonObj) {
                var mappings = jsonObj[classId];
                if (!(classId in this.classes)) continue;
                var cls = this.classes.get(classId);
                if (!(cls in this.teachersByClass)) {
                    this.teachersByClass.set(cls, []);
                }
                for (var mapping in mappings) {
                    var teacherId = mapping["Teacher"];
                    if (teacherId in this.teachers) {
                        this.teachersByClass.get(cls).push({
                            "Teacher": this.teachers.get(teacherId),
                            "StartDate": this.toDate(mapping["StartDate"]),
                            "EndDate": this.toDate(mapping["EndDate"])
                        });
                    }
                }
            }
        }
    }, {
        key: "initializeSubjects",
        value: function initializeSubjects() {
            var jsonObj = require("./Mock/subjects.json");
            this.subjects = new Map();
            for (var subjectId in jsonObj) {
                var mapping = jsonObj[subjectId];
                this.subjects.set(subjectId, _Subject.Subject.fromJSON(this, subjectId, mapping));
            }
        }
    }, {
        key: "initializeSubjectsByClass",
        value: function initializeSubjectsByClass() {
            var jsonObj = require("./Mock/class_subjects.json");
            this.subjectsByClass = new Map();
            for (var classId in jsonObj) {
                var subjects = jsonObj[classId];
                if (!(classId in this.classes)) continue;
                var cls = this.classes.get(classId);
                if (!(cls in this.subjectsByClass)) {
                    this.subjectsByClass.set(cls, []);
                }
                for (var subjectId in subjects) {
                    if (!(subjectId in this.subjects)) continue;
                    var subject = this.subjects.get(subjectId);
                    this.subjectsByClass.get(cls).push(subject);
                }
            }
        }
    }, {
        key: "initializeTeachersBySubject",
        value: function initializeTeachersBySubject() {
            var jsonObj = require("./Mock/class_subjects.json");
            this.teachersByClassBySubject = new Map();
            for (var classId in jsonObj) {
                var subjects = jsonObj[classId];
                if (!(classId in this.classes)) continue;
                var cls = this.classes.get(classId);
                if (!(cls in this.teachersByClassBySubject)) {
                    this.teachersByClassBySubject.set(cls, []);
                }
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = subjects[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var _ref = _step.value;

                        var _ref2 = _slicedToArray(_ref, 2);

                        var subjectId = _ref2[0];
                        var mappings = _ref2[1];

                        if (!(subjectId in this.subjects)) continue;
                        var subject = this.subjects.get(subjectId);
                        if (!(subject in this.teachersByClassBySubject.get(cls))) {
                            this.teachersByClassBySubject.get(cls).set(subject, []);
                        }
                        for (var mapping in mappings) {
                            var teacherId = mapping["Teacher"];
                            if (teacherId in this.teachers) {
                                this.teachersByClassBySubject.get(cls).get(subject).push({
                                    "Teacher": this.teachers.get(teacherId),
                                    "StartDate": this.toDate(mapping["StartDate"]),
                                    "EndDate": this.toDate(mapping["EndDate"])
                                });
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeSubjectsByStudent",
        value: function initializeSubjectsByStudent() {
            var jsonObj = require("./Mock/class_subjects.json");
            this.subjectsByClassByStudent = new Map();
            for (var classId in jsonObj) {
                var subjects = jsonObj[classId];
                if (!(classId in this.classes)) continue;
                var cls = this.classes.get(classId);
                if (!(cls in this.teachersByClassBySubject)) {
                    this.subjectsByClassByStudent.set(cls, []);
                }
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = subjects[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var _ref3 = _step2.value;

                        var _ref4 = _slicedToArray(_ref3, 2);

                        var subjectId = _ref4[0];
                        var studentIds = _ref4[1];

                        if (!(subjectId in this.subjects)) continue;
                        var subject = this.subjects.get(subjectId);
                        if (!(subject in this.teachersByClassBySubject.get(cls))) {
                            this.subjectsByClassByStudent.get(cls).set(subject, []);
                        }
                        for (var studentId in studentIds) {
                            if (studentId in this.students) {
                                this.subjectsByClassByStudent.get(cls).get(subject).push(this.students.get(studentId));
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeQuestions",
        value: function initializeQuestions() {
            var jsonObj = require("./Mock/questions.json");
            this.questions = new Map();
            this.questionsBySubject = new Map();
            for (var subjectId in jsonObj) {
                var mappings = jsonObj[subjectId];
                if (subjectId in this.subjects) {
                    var subject = this.subjects.get(subjectId);
                    if (!(subject in this.questionsBySubject)) {
                        this.questionsBySubject.set(subject, []);
                    }
                    var _iteratorNormalCompletion3 = true;
                    var _didIteratorError3 = false;
                    var _iteratorError3 = undefined;

                    try {
                        for (var _iterator3 = mappings[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            var _ref5 = _step3.value;

                            var _ref6 = _slicedToArray(_ref5, 2);

                            var questionId = _ref6[0];
                            var mapping = _ref6[1];

                            var question = _Question.Question.fromJSON(this, questionId, mapping);
                            this.questions.set(questionId, question);
                            this.questionsBySubject.get(subject).push(question);
                        }
                    } catch (err) {
                        _didIteratorError3 = true;
                        _iteratorError3 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                _iterator3.return();
                            }
                        } finally {
                            if (_didIteratorError3) {
                                throw _iteratorError3;
                            }
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeMasteryLevels",
        value: function initializeMasteryLevels() {
            var jsonObj = require("./Mock/question_masteries.json");
            this.masteries = new Map();
            this.masteriesByQuestion = new Map();
            for (var questionId in jsonObj) {
                var mappings = jsonObj[questionId];
                if (questionId in this.questions) {
                    var question = this.questions.get(questionId);
                    if (!(question in this.masteriesByQuestion)) {
                        this.masteriesByQuestion.set(question, []);
                    }
                    var _iteratorNormalCompletion4 = true;
                    var _didIteratorError4 = false;
                    var _iteratorError4 = undefined;

                    try {
                        for (var _iterator4 = mappings[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                            var _ref7 = _step4.value;

                            var _ref8 = _slicedToArray(_ref7, 2);

                            var masteryId = _ref8[0];
                            var mapping = _ref8[1];

                            var mastery = _Mastery.Mastery.fromJSON(this, masteryId, mapping);
                            this.masteries.set(masteryId, mastery);
                            this.questionsBySubject.get(question).push(mastery);
                        }
                    } catch (err) {
                        _didIteratorError4 = true;
                        _iteratorError4 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion4 && _iterator4.return) {
                                _iterator4.return();
                            }
                        } finally {
                            if (_didIteratorError4) {
                                throw _iteratorError4;
                            }
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeExams",
        value: function initializeExams() {
            var jsonObj = require("./Mock/exams.json");
            this.exams = new Map();
            this.examsByClassBySubject = new Map();
            for (var examId in jsonObj) {
                var mapping = jsonObj[examId];
                var clsId = mapping["Class"];
                var subjectId = mapping["Subject"];
                if (clsId in this.classes && subjectId in this.subjects) {
                    var cls = this.classes.get(clsId);
                    var subject = this.subjects(subjectId);
                    if (!(cls in this.examsByClassBySubject)) {
                        this.examsByClassBySubject.set(cls, new Map());
                    }
                    if (!(subject in this.examsByClassBySubject.get(cls))) {
                        this.examsByClassBySubject.get(cls).set(subject, []);
                    }
                    var exam = _Exam.Exam.fromJSON(this, examId, mapping);
                    this.exams.set(examId, exam);
                    this.examsByClassBySubject.get(cls).get(subject).push(exam);
                }
            }
        }
    }, {
        key: "initializeExamQuestions",
        value: function initializeExamQuestions() {
            var jsonObj = require("./Mock/exam_questions.json");
            this.examQuestions = new Map();
            for (var examId in jsonObj) {
                var questionIds = jsonObj[examId];
                if (examId in this.exams) {
                    var exam = this.exams.get(examId);
                    if (!(exam in this.examQuestions)) {
                        this.examQuestions.set(exam, []);
                    }
                    for (var questionId in questionIds) {
                        if (questionId in this.questions) {
                            var question = this.questions[questionId];
                            this.examQuestions.get(exam).push(question);
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeExamResults",
        value: function initializeExamResults() {
            var jsonObj = require("./Mock/exam_answers.json");
            this.examsByStudent = new Map();
            for (var examId in jsonObj) {
                var mappings = jsonObj[examId];
                if (!(examId in this.exams)) continue;
                var exam = this.exams.get(examId);
                var _iteratorNormalCompletion5 = true;
                var _didIteratorError5 = false;
                var _iteratorError5 = undefined;

                try {
                    for (var _iterator5 = mappings[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                        var _ref9 = _step5.value;

                        var _ref10 = _slicedToArray(_ref9, 2);

                        var studentId = _ref10[0];
                        var answers = _ref10[1];

                        if (!(studentId in this.students)) continue;
                        var student = this.students[studentId];
                        if (!(student in this.examsByStudent)) {
                            this.examsByStudent.set(student, new Map());
                        }
                        if (!(exam in this.examsByStudent.get(student))) {
                            this.examsByStudent.get(student).set(exam, new Map());
                        }
                        var _iteratorNormalCompletion6 = true;
                        var _didIteratorError6 = false;
                        var _iteratorError6 = undefined;

                        try {
                            for (var _iterator6 = answers[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                                var _ref11 = _step6.value;

                                var _ref12 = _slicedToArray(_ref11, 2);

                                var questionId = _ref12[0];
                                var answer = _ref12[1];

                                if (!(questionId in this.questions)) continue;
                                var question = this.questions[questionId];
                                if (answer["Mastery"] in this.masteries) {
                                    var mastery = this.masteries[answer["Mastery"]];
                                    this.examsByStudent.get(student).get(exam).set(question, {
                                        "Answer": answer["Answer"],
                                        "Mastery": mastery
                                    });
                                }
                            }
                        } catch (err) {
                            _didIteratorError6 = true;
                            _iteratorError6 = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion6 && _iterator6.return) {
                                    _iterator6.return();
                                }
                            } finally {
                                if (_didIteratorError6) {
                                    throw _iteratorError6;
                                }
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError5 = true;
                    _iteratorError5 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
                            _iterator5.return();
                        }
                    } finally {
                        if (_didIteratorError5) {
                            throw _iteratorError5;
                        }
                    }
                }
            }
        }
    }, {
        key: "getAcademicYears",
        value: function getAcademicYears() {
            return Array.from(this.academicYears.values());
        }
    }, {
        key: "getAcademicYear",
        value: function getAcademicYear(academicYearId) {
            return this.academicYears.get(academicYearId);
        }
    }, {
        key: "getSchools",
        value: function getSchools() {
            return this.schools.values();
        }
    }, {
        key: "getSchool",
        value: function getSchool(schoolId) {
            return this.schools.get(schoolId);
        }
    }, {
        key: "getClasses",
        value: function getClasses() {
            return this.classes.values();
        }
    }, {
        key: "getClass",
        value: function getClass(classId) {
            return this.classes.get(classId);
        }
    }, {
        key: "getTeachers",
        value: function getTeachers() {
            return this.teachers.values();
        }
    }, {
        key: "getTeacher",
        value: function getTeacher(teacherId) {
            return this.teachers.get(teacherId);
        }
    }, {
        key: "getStudents",
        value: function getStudents() {
            return this.students.values();
        }
    }, {
        key: "getStudent",
        value: function getStudent(studentId) {
            return this.students.get(studentId);
        }
    }, {
        key: "getSubjects",
        value: function getSubjects() {
            return this.subjects.values();
        }
    }, {
        key: "getSubject",
        value: function getSubject(subjectId) {
            return this.subjects.get(subjectId);
        }
    }, {
        key: "getQuestion",
        value: function getQuestion(subject, questionId) {
            if (subject.Id in this.questions) {
                if (questionId in this.questions[subject.Id]) {
                    return _Question.Question.toJSON(this, questionId, this.questions[subject.Id][questionId]);
                }
            }
        }
    }, {
        key: "getExams",
        value: function getExams() {
            return this.exams.values();
        }
    }, {
        key: "getExam",
        value: function getExam(examId) {
            return this.exams.get(examId);
        }
    }, {
        key: "getContacts",
        value: function getContacts(entityName, entityId) {
            var contacts = [];
            var _iteratorNormalCompletion7 = true;
            var _didIteratorError7 = false;
            var _iteratorError7 = undefined;

            try {
                for (var _iterator7 = this.contacts[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                    var _ref13 = _step7.value;

                    var _ref14 = _slicedToArray(_ref13, 2);

                    var id = _ref14[0];
                    var json = _ref14[1];

                    if (json["EntityName"] == entityName && json["EntityId"] == entityId) {
                        contacts.push(_Contact.Contact.fromJSON(this, id, json));
                    }
                }
            } catch (err) {
                _didIteratorError7 = true;
                _iteratorError7 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion7 && _iterator7.return) {
                        _iterator7.return();
                    }
                } finally {
                    if (_didIteratorError7) {
                        throw _iteratorError7;
                    }
                }
            }

            return contacts;
        }
    }, {
        key: "updateContact",
        value: function updateContact(contact) {
            this.contacts[contact.Id] = contact.toJSON();
        }

        // AcademicYear related

    }, {
        key: "getPrevAcademicYear",
        value: function getPrevAcademicYear(academicYear) {
            // Return an instance of AcademicYear object
            // TODO: Implement it
            var academicYears = this.getAcademicYears();
            var index = academicYears.indexOf(academicYear);
            if (index != 0) {
                return academicYears[index - 1];
            }
        }
    }, {
        key: "getSchoolsForAcademicYear",
        value: function getSchoolsForAcademicYear(academicYear) {
            return this.schoolsByAcademicYear.get(academicYear.Id);
        }
    }, {
        key: "addAcademicYear",
        value: function addAcademicYear(academicYear) {
            var newId = this.getUniqueId();
            academicYear.setId(newId);
            this.academicyears.set(newId, academicYear);
        }
    }, {
        key: "addSchool",
        value: function addSchool(school) {
            var newId = this.getUniqueId();
            school.setId(newId);
            this.schools.set(newId, school);
        }
    }, {
        key: "addSchoolToAcademicYear",
        value: function addSchoolToAcademicYear(academicYear, school) {
            if (academicYear.Id in this.academicYears) {
                if (school.Id in this.schools) {
                    if (!(academicYear.Id in this.schoolsByAcademicYear)) {
                        this.schoolsByAcademicYear.set(academicYear.Id, []);
                    }
                    this.schoolsByAcademicYear.get(academicYear.Id).push(school);
                }
            }
        }

        // School related methods

    }, {
        key: "getContactsForSchool",
        value: function getContactsForSchool(school) {
            return this.schoolContacts.get(school);
        }
    }, {
        key: "getAcademicYearsForSchool",
        value: function getAcademicYearsForSchool(school) {
            var academicYears = [];
            for (var _ref15 in this.schoolsByAcademicYear) {
                var _ref16 = _slicedToArray(_ref15, 2);

                var academicYearId = _ref16[0];
                var schools = _ref16[1];

                if (school in schools) {
                    academicYears.push(this.getAcademicYear(academicYearId));
                }
            }
            return academicYears;
        }
    }, {
        key: "getCurrentAcademicYear",
        value: function getCurrentAcademicYear(school) {
            // Get the currently running academic year
            var academicYears = this.getAcademicYearsForSchool(school);
            // May be order them by start dates
            return academicYears[academicYears.length - 1];
        }
    }, {
        key: "getClassesForAcademicYear",
        value: function getClassesForAcademicYear(academicYear, school) {
            // Get all classes for the given school in the given academic year
            var allClasses = [];
            if (academicYear.Id in this.schoolsByAcademicYear) {
                for (var academicYearSchool in this.schoolsByAcademicYear.get(academicYear.Id)) {
                    if (school.Id == academicYearSchool.Id) {
                        var _iteratorNormalCompletion8 = true;
                        var _didIteratorError8 = false;
                        var _iteratorError8 = undefined;

                        try {
                            for (var _iterator8 = this.classes[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                                var _ref17 = _step8.value;

                                var _ref18 = _slicedToArray(_ref17, 2);

                                var classId = _ref18[0];
                                var cls = _ref18[1];

                                if (cls.AcademicYear == academicYear && cls.School == school) {
                                    allClasses.push(cls);
                                }
                            }
                        } catch (err) {
                            _didIteratorError8 = true;
                            _iteratorError8 = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion8 && _iterator8.return) {
                                    _iterator8.return();
                                }
                            } finally {
                                if (_didIteratorError8) {
                                    throw _iteratorError8;
                                }
                            }
                        }
                    }
                }
            }
            return allClasses;
        }
    }, {
        key: "getTeachersForAcademicYear",
        value: function getTeachersForAcademicYear(academicYear, school) {
            // Get all teachers in this school for the given academic year
            var allTeachers = [];
            for (var _ref19 in this.teachersBySchool) {
                var _ref20 = _slicedToArray(_ref19, 2);

                var _school2 = _ref20[0];
                var mappings = _ref20[1];

                for (var mapping in mappings) {
                    var teacher = mapping["Teacher"];
                    var startDate = this.toDate(mapping["StartDate"]);
                    var endDate = this.toDate(mapping["EndDate"]);
                    if (endDate == null) {
                        if (startDate < academicYear.StartDate) {
                            allTeachers.push(teacher);
                        }
                    } else {
                        if (endDate > academicYear.StartDate) {
                            allTeachers.push(teacher);
                        }
                    }
                }
            }
            return allTeachers;
        }
    }, {
        key: "addTeacher",
        value: function addTeacher(teacher) {
            var newId = this.getUniqueId();
            teacher.setId(newId);
            this.teachers.set(newId, teacher.toJSON());
        }
    }, {
        key: "addTeacherToSchool",
        value: function addTeacherToSchool(school, teacher) {
            if (!(school in this.teachersBySchool)) {
                this.teachersBySchool.set(school, []);
            }
            this.teachersBySchool[school].push({
                "Teacher": teacher,
                "StartDate": new Date(),
                "EndDate": null });
        }
    }, {
        key: "removeTeacherFromSchool",
        value: function removeTeacherFromSchool(school, teacher) {
            if (school in this.teachersBySchool && teacher in this.teachersBySchool.get(school)) {
                var _iteratorNormalCompletion9 = true;
                var _didIteratorError9 = false;
                var _iteratorError9 = undefined;

                try {
                    for (var _iterator9 = this.teachersBySchool[school][Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                        var mapping = _step9.value;

                        if (mapping["Teacher"] == teacher) {
                            mapping["EndDate"] = new Date();
                        }
                    }
                } catch (err) {
                    _didIteratorError9 = true;
                    _iteratorError9 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion9 && _iterator9.return) {
                            _iterator9.return();
                        }
                    } finally {
                        if (_didIteratorError9) {
                            throw _iteratorError9;
                        }
                    }
                }

                var academicYear = this.getCurrentAcademicYear();
                var classes = this.getClassesForAcademicYear(academicYear, school);
                for (var cls in classes) {
                    this.removeTeacherFromClass(cls, teacher);
                    this.removeTeacherFromSubjects(cls, teacher);
                }
            }
        }
    }, {
        key: "removeTeacherFromClass",
        value: function removeTeacherFromClass(cls, teacher) {
            var _iteratorNormalCompletion10 = true;
            var _didIteratorError10 = false;
            var _iteratorError10 = undefined;

            try {
                for (var _iterator10 = this.teachersByClass.get(cls)[Symbol.iterator](), _step10; !(_iteratorNormalCompletion10 = (_step10 = _iterator10.next()).done); _iteratorNormalCompletion10 = true) {
                    var mapping = _step10.value;

                    if (mapping["Teacher"] == teacher) {
                        mapping["EndDate"] = new Date();
                    }
                }
            } catch (err) {
                _didIteratorError10 = true;
                _iteratorError10 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion10 && _iterator10.return) {
                        _iterator10.return();
                    }
                } finally {
                    if (_didIteratorError10) {
                        throw _iteratorError10;
                    }
                }
            }
        }
    }, {
        key: "removeTeacherFromSubjects",
        value: function removeTeacherFromSubjects(cls, teacher) {
            var _iteratorNormalCompletion11 = true;
            var _didIteratorError11 = false;
            var _iteratorError11 = undefined;

            try {
                for (var _iterator11 = this.teachersByClassBySubject.get(cls)[Symbol.iterator](), _step11; !(_iteratorNormalCompletion11 = (_step11 = _iterator11.next()).done); _iteratorNormalCompletion11 = true) {
                    var _ref21 = _step11.value;

                    var _ref22 = _slicedToArray(_ref21, 2);

                    var subject = _ref22[0];
                    var mappings = _ref22[1];
                    var _iteratorNormalCompletion12 = true;
                    var _didIteratorError12 = false;
                    var _iteratorError12 = undefined;

                    try {
                        for (var _iterator12 = mappings[Symbol.iterator](), _step12; !(_iteratorNormalCompletion12 = (_step12 = _iterator12.next()).done); _iteratorNormalCompletion12 = true) {
                            var mapping = _step12.value;

                            if (mapping["Teacher"] == teacher) {
                                mapping["EndDate"] = new Date();
                            }
                        }
                    } catch (err) {
                        _didIteratorError12 = true;
                        _iteratorError12 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion12 && _iterator12.return) {
                                _iterator12.return();
                            }
                        } finally {
                            if (_didIteratorError12) {
                                throw _iteratorError12;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError11 = true;
                _iteratorError11 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion11 && _iterator11.return) {
                        _iterator11.return();
                    }
                } finally {
                    if (_didIteratorError11) {
                        throw _iteratorError11;
                    }
                }
            }
        }
    }, {
        key: "addClass",
        value: function addClass(cls) {
            var newId = this.getUniqueId();
            cls.setId(newId);
            this.classes.set(newId, cls.toJSON());
        }
    }, {
        key: "getClassesForSchool",
        value: function getClassesForSchool(school) {
            var allClasses = [];
            var _iteratorNormalCompletion13 = true;
            var _didIteratorError13 = false;
            var _iteratorError13 = undefined;

            try {
                for (var _iterator13 = this.classes[Symbol.iterator](), _step13; !(_iteratorNormalCompletion13 = (_step13 = _iterator13.next()).done); _iteratorNormalCompletion13 = true) {
                    var _ref23 = _step13.value;

                    var _ref24 = _slicedToArray(_ref23, 2);

                    var clsId = _ref24[0];
                    var cls = _ref24[1];

                    if (cls.AcademicYear == school.AcademicYear && cls.School == school) {
                        allClasses.push(cls);
                    }
                }
            } catch (err) {
                _didIteratorError13 = true;
                _iteratorError13 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion13 && _iterator13.return) {
                        _iterator13.return();
                    }
                } finally {
                    if (_didIteratorError13) {
                        throw _iteratorError13;
                    }
                }
            }

            return allClasses;
        }
    }, {
        key: "addAdminTeacher",
        value: function addAdminTeacher(cls, teacher) {
            if (cls.Id in this.classes) {
                if (!(cls in this.teachersByClass)) {
                    this.teachersByClass.set(cls, []);
                }
                this.teachersByClass.get(cls.Id).push({
                    "Teacher": teacher,
                    "StartDate": new Date(),
                    "EndDate": null
                });
            }
        }
    }, {
        key: "getSchoolContacts",
        value: function getSchoolContacts(school) {
            this.schoolContacts.get(school);
        }
    }, {
        key: "addStudent",
        value: function addStudent(student) {
            var studentId = this.getUniqueId();
            student.setId(studentId);
            this.students.set(studentId, student);
        }
    }, {
        key: "addStudentToSchool",
        value: function addStudentToSchool(school, student) {
            if (school.Id in this.schools) {
                if (!(school in this.studentsBySchool)) {
                    this.studentsBySchool.set(school, []);
                }
                this.studentsBySchool.get(school).push({
                    "Student": student,
                    "StartDate": new Date(),
                    "EndDate": null
                });
            }
        }
    }, {
        key: "removeStudentFromSchool",
        value: function removeStudentFromSchool(school, student) {
            if (school.Id in this.schools) {
                for (var mapping in this.studentsBySchool) {
                    if (mapping["Student"] == student.Id) {
                        mapping["EndDate"] = new Date();
                    }
                }
                for (var cls in this.getClassesForSchool(school)) {
                    var _iteratorNormalCompletion14 = true;
                    var _didIteratorError14 = false;
                    var _iteratorError14 = undefined;

                    try {
                        for (var _iterator14 = this.studentsByClass.get(cls)[Symbol.iterator](), _step14; !(_iteratorNormalCompletion14 = (_step14 = _iterator14.next()).done); _iteratorNormalCompletion14 = true) {
                            var mappings = _step14.value;

                            for (var _mapping in mappings) {
                                if (_mapping["Student"] == student) {
                                    _mapping["EndDate"] = new Date();
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError14 = true;
                        _iteratorError14 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion14 && _iterator14.return) {
                                _iterator14.return();
                            }
                        } finally {
                            if (_didIteratorError14) {
                                throw _iteratorError14;
                            }
                        }
                    }

                    var _iteratorNormalCompletion15 = true;
                    var _didIteratorError15 = false;
                    var _iteratorError15 = undefined;

                    try {
                        for (var _iterator15 = this.subjectsByClassByStudent.get(cls)[Symbol.iterator](), _step15; !(_iteratorNormalCompletion15 = (_step15 = _iterator15.next()).done); _iteratorNormalCompletion15 = true) {
                            var _ref25 = _step15.value;

                            var _ref26 = _slicedToArray(_ref25, 2);

                            var subject = _ref26[0];
                            var students = _ref26[1];

                            var studentIndex = students.indexOf(student.Id);
                            if (studentIndex >= 0) {
                                this.subjectsByClassByStudent.get(cls).set(subject, students.splice(studentIndex, 0));
                            }
                        }
                    } catch (err) {
                        _didIteratorError15 = true;
                        _iteratorError15 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion15 && _iterator15.return) {
                                _iterator15.return();
                            }
                        } finally {
                            if (_didIteratorError15) {
                                throw _iteratorError15;
                            }
                        }
                    }
                }
            }
        }

        // Class related methods

    }, {
        key: "updateClass",
        value: function updateClass(cls) {
            // Nothing to update
        }
    }, {
        key: "getSubjectsForClass",
        value: function getSubjectsForClass(cls) {
            // Get subjects for the given class
            if (cls in this.subjectsByClass) {
                return this.subjectsByClass.get(cls);
            }
        }
    }, {
        key: "getTeachersBySubjectForClass",
        value: function getTeachersBySubjectForClass(cls) {
            // Get all teachers for the given class
            var subjectTeachers = new Map();
            if (cls in this.teachersByClassBySubject) {
                var _iteratorNormalCompletion16 = true;
                var _didIteratorError16 = false;
                var _iteratorError16 = undefined;

                try {
                    for (var _iterator16 = this.teachersByClassBySubject.get(cls)[Symbol.iterator](), _step16; !(_iteratorNormalCompletion16 = (_step16 = _iterator16.next()).done); _iteratorNormalCompletion16 = true) {
                        var _ref27 = _step16.value;

                        var _ref28 = _slicedToArray(_ref27, 2);

                        var subject = _ref28[0];
                        var mappings = _ref28[1];

                        subjectTeachers.set(subject, []);
                        for (var mapping in mappings) {
                            if (mapping["EndDate"] == null) {
                                subjectTeachers.get(subject).push(mapping["Teacher"]);
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError16 = true;
                    _iteratorError16 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion16 && _iterator16.return) {
                            _iterator16.return();
                        }
                    } finally {
                        if (_didIteratorError16) {
                            throw _iteratorError16;
                        }
                    }
                }
            }
            return subjectTeachers;
        }
    }, {
        key: "getStudentsFromClass",
        value: function getStudentsFromClass(cls) {
            // Get all students for the given class
            var allStudents = [];
            if (cls in this.studentsByClass) {
                for (var mapping in this.studentsByClass.get(cls)) {
                    if (mapping["EndDate"] == null) {
                        allStudents.push(mapping["Student"]);
                    }
                }
            }
            return allStudents;
        }
    }, {
        key: "getAdminTeacher",
        value: function getAdminTeacher(cls) {
            if (cls in this.teachersByClass) {
                for (var mapping in this.teachersByClass.get(cls)) {
                    if (mapping["EndDate"] == null) {
                        return mapping["Student"];
                    }
                }
            }
        }
    }, {
        key: "getStudentsBySubject",
        value: function getStudentsBySubject(cls, subject) {
            var subjectStudents = new Map();
            if (cls in this.subjectsByClassByStudent) {
                var _iteratorNormalCompletion17 = true;
                var _didIteratorError17 = false;
                var _iteratorError17 = undefined;

                try {
                    for (var _iterator17 = this.subjectsByClassByStudent.get(cls)[Symbol.iterator](), _step17; !(_iteratorNormalCompletion17 = (_step17 = _iterator17.next()).done); _iteratorNormalCompletion17 = true) {
                        var _ref29 = _step17.value;

                        var _ref30 = _slicedToArray(_ref29, 2);

                        var _subject = _ref30[0];
                        var mappings = _ref30[1];

                        subjectStudents.set(_subject, []);
                        for (var mapping in mappings) {
                            if (mapping["EndDate"] == null) {
                                subjectStudents.get(_subject).push(mapping["Student"]);
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError17 = true;
                    _iteratorError17 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion17 && _iterator17.return) {
                            _iterator17.return();
                        }
                    } finally {
                        if (_didIteratorError17) {
                            throw _iteratorError17;
                        }
                    }
                }
            }
            return subjectStudents;
        }
    }, {
        key: "addSubject",
        value: function addSubject(subject) {
            var newId = this.getUniqueId();
            subject.setId(newId);
            this.subjects.set(newId, subject);
        }
    }, {
        key: "updateSubject",
        value: function updateSubject(subject) {
            // Nothing to update here
        }
    }, {
        key: "removeSubject",
        value: function removeSubject(subject) {
            // lets get to this later as we need to check questions and exams
        }
    }, {
        key: "addSubjectToClass",
        value: function addSubjectToClass(cls, subject) {
            if (!(cls in this.subjectsByClass)) {
                this.subjectsByClass.set(cls, []);
            }
            if (subject in this.subjectsByClass.get(cls)) {
                throw "Subject is already part of the class";
            }
            this.subjectsByClass.get(cls).push(subject);
        }
    }, {
        key: "removeSubjectFromClass",
        value: function removeSubjectFromClass(cls, subject) {
            // Lets get to this later
        }
    }, {
        key: "addTeacherToSubject",
        value: function addTeacherToSubject(cls, teacher, subject) {
            if (!(cls in this.teachersByClassBySubject)) {
                this.teachersByClassBySubject.set(cls, new Map());
            }
            if (!(subject in this.teachersByClassBySubject.get(cls))) {
                this.teachersByClassBySubject.get(cls).set(subject, []);
            }
            for (var mapping in this.teachersByClassBySubject.get(cls).get(subject)) {
                if (mapping["EndDate"] == null) {
                    mapping["EndDate"] = new Date();
                }
            }
            this.teachersByClassBySubject.get(cls).get(subject).push({
                "Teacher": teacher,
                "StartDate": new Date(),
                "EndDate": null
            });
        }
    }, {
        key: "removeTeacherForSubject",
        value: function removeTeacherForSubject(cls, teacher, subject) {
            if (!(cls in this.teachersByClassBySubject)) {
                throw "Unknown class";
            }
            if (!(subject in this.teachersByClassBySubject.get(cls))) {
                throw "Subject is not part of the class";
            }
            for (var mapping in this.teachersByClassBySubject.get(cls).get(subject)) {
                if (mapping["EndDate"] == null) {
                    mapping["EndDate"] = new Date();
                }
            }
        }
    }, {
        key: "addStudentToClass",
        value: function addStudentToClass(cls, student) {
            if (!(cls in this.studentsByClass)) {
                this.studentsByClass.set(cls, []);
            }
            this.studentsByClass.get(cls).push({
                "Student": student,
                "StartDate": new Date(),
                "EndDate": null
            });
        }
    }, {
        key: "removeStudentFromClass",
        value: function removeStudentFromClass(cls, student) {
            if (!(cls in this.studentsByClass)) {
                throw "Unknown class";
            }
            for (var mapping in this.studentsByClass.get(cls)) {
                if (mapping["Student"] == student) {
                    mapping["EndDate"] = new Date();
                }
            }
        }
    }, {
        key: "addStudentToSubject",
        value: function addStudentToSubject(cls, student, subject) {
            if (!(cls in this.subjectsByClassByStudent)) {
                this.subjectsByClassByStudent.set(cls, new Map());
            }
            if (!(subject in this.subjectsByClassByStudent.get(cls))) {
                this.subjectsByClassByStudent.get(cls).set(subject, []);
            }
            this.subjectsByClassByStudent.get(cls).get(subject).push({
                "Student": student,
                "StartDate": new Date(),
                "EndDate": null
            });
        }
    }, {
        key: "removeStudentFromSubject",
        value: function removeStudentFromSubject(cls, student, subject) {
            if (!(cls in this.subjectsByClassByStudent)) {
                throw "Unknown class";
            }
            if (!(subject in this.subjectsByClassByStudent.get(cls))) {
                throw "Subject is not part of the class";
            }
            for (var mapping in this.subjectsByClassByStudent.get(cls)) {
                if (mapping["Student"] == student) {
                    mapping["EndDate"] = new Date();
                }
            }
        }

        // Student related methods

    }, {
        key: "getStudentContacts",
        value: function getStudentContacts(student) {
            return this.studentContacts.get(student);
        }
    }, {
        key: "addStudentContact",
        value: function addStudentContact(student, contact) {
            contact.setId(this.getUniqueId());
            if (!(student in this.studentContacts)) {
                this.studentContacts.set(student, []);
            }
            this.studentContacts.get(student).push(contact);
        }
    }, {
        key: "getSchoolForStudent",
        value: function getSchoolForStudent(academicYear, student) {
            var _iteratorNormalCompletion18 = true;
            var _didIteratorError18 = false;
            var _iteratorError18 = undefined;

            try {
                for (var _iterator18 = this.studentsBySchool[Symbol.iterator](), _step18; !(_iteratorNormalCompletion18 = (_step18 = _iterator18.next()).done); _iteratorNormalCompletion18 = true) {
                    var _ref31 = _step18.value;

                    var _ref32 = _slicedToArray(_ref31, 2);

                    var school = _ref32[0];
                    var mappings = _ref32[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == student) {
                            var endDate = this.toDate(mapping["EndDate"]);
                            if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                return school;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError18 = true;
                _iteratorError18 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion18 && _iterator18.return) {
                        _iterator18.return();
                    }
                } finally {
                    if (_didIteratorError18) {
                        throw _iteratorError18;
                    }
                }
            }
        }
    }, {
        key: "getSchoolsForStudent",
        value: function getSchoolsForStudent(student) {
            var allSchools = [];
            var _iteratorNormalCompletion19 = true;
            var _didIteratorError19 = false;
            var _iteratorError19 = undefined;

            try {
                for (var _iterator19 = this.studentsBySchool[Symbol.iterator](), _step19; !(_iteratorNormalCompletion19 = (_step19 = _iterator19.next()).done); _iteratorNormalCompletion19 = true) {
                    var _ref33 = _step19.value;

                    var _ref34 = _slicedToArray(_ref33, 2);

                    var school = _ref34[0];
                    var mappings = _ref34[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == student) {
                            allSchools.push(school);
                        }
                    }
                }
            } catch (err) {
                _didIteratorError19 = true;
                _iteratorError19 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion19 && _iterator19.return) {
                        _iterator19.return();
                    }
                } finally {
                    if (_didIteratorError19) {
                        throw _iteratorError19;
                    }
                }
            }

            return allSchools;
        }
    }, {
        key: "getClassForStudent",
        value: function getClassForStudent(academicYear, student) {
            var _iteratorNormalCompletion20 = true;
            var _didIteratorError20 = false;
            var _iteratorError20 = undefined;

            try {
                for (var _iterator20 = this.studentsByClass[Symbol.iterator](), _step20; !(_iteratorNormalCompletion20 = (_step20 = _iterator20.next()).done); _iteratorNormalCompletion20 = true) {
                    var _ref35 = _step20.value;

                    var _ref36 = _slicedToArray(_ref35, 2);

                    var cls = _ref36[0];
                    var mappings = _ref36[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == student) {
                            var endDate = this.toDate(mapping["EndDate"]);
                            if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                return cls;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError20 = true;
                _iteratorError20 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion20 && _iterator20.return) {
                        _iterator20.return();
                    }
                } finally {
                    if (_didIteratorError20) {
                        throw _iteratorError20;
                    }
                }
            }
        }
    }, {
        key: "getClassesForStudent",
        value: function getClassesForStudent(school, student) {
            var allClasses = [];
            var _iteratorNormalCompletion21 = true;
            var _didIteratorError21 = false;
            var _iteratorError21 = undefined;

            try {
                for (var _iterator21 = this.studentsByClass[Symbol.iterator](), _step21; !(_iteratorNormalCompletion21 = (_step21 = _iterator21.next()).done); _iteratorNormalCompletion21 = true) {
                    var _ref37 = _step21.value;

                    var _ref38 = _slicedToArray(_ref37, 2);

                    var cls = _ref38[0];
                    var mappings = _ref38[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == student) {
                            allClasses.push(cls);
                        }
                    }
                }
            } catch (err) {
                _didIteratorError21 = true;
                _iteratorError21 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion21 && _iterator21.return) {
                        _iterator21.return();
                    }
                } finally {
                    if (_didIteratorError21) {
                        throw _iteratorError21;
                    }
                }
            }

            return allClasses;
        }
    }, {
        key: "getSubjectsForStudent",
        value: function getSubjectsForStudent(cls, student) {
            //  Get Subjects from the class for the given student
            var allSubjects = [];
            if (cls in this.subjectsByClassByStudent) {
                for (var _ref39 in this.subjectsByClassByStudent.get(cls)) {
                    var _ref40 = _slicedToArray(_ref39, 2);

                    var subject = _ref40[0];
                    var mappings = _ref40[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == student && mapping["EndDate"] == null) {
                            allSubjects.push(subject);
                        }
                    }
                }
            }
            return allSubjects;
        }
    }, {
        key: "getExamResultsForStudent",
        value: function getExamResultsForStudent(cls, subject, student) {
            // Return all exams for the given student in the given academic year for the given subject
            var examResults = {};
            for (var exam in this.getExamsForSubject(cls, subject)) {
                examResults[exam] = this.getExamResults(student, exam);
            }
            return examResults;
        }

        // Teacher related methods

    }, {
        key: "getTeacherContacts",
        value: function getTeacherContacts(teacher) {
            return this.teacherContacts.get(teacher);
        }
    }, {
        key: "addTeacherContact",
        value: function addTeacherContact(teacher, contact) {
            contact.setId(this.getUniqueId());
            if (!(teacher in this.teacherContacts)) {
                this.teacherContacts.set(teacher, []);
            }
            this.teacherContacts.get(teacher).push(contact);
        }
    }, {
        key: "getSchoolForTeacher",
        value: function getSchoolForTeacher(academicYear, teacher) {
            var _iteratorNormalCompletion22 = true;
            var _didIteratorError22 = false;
            var _iteratorError22 = undefined;

            try {
                for (var _iterator22 = this.teachersBySchool[Symbol.iterator](), _step22; !(_iteratorNormalCompletion22 = (_step22 = _iterator22.next()).done); _iteratorNormalCompletion22 = true) {
                    var _ref41 = _step22.value;

                    var _ref42 = _slicedToArray(_ref41, 2);

                    var school = _ref42[0];
                    var mappings = _ref42[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == teacher) {
                            var endDate = this.toDate(mapping["EndDate"]);
                            if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                return school;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError22 = true;
                _iteratorError22 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion22 && _iterator22.return) {
                        _iterator22.return();
                    }
                } finally {
                    if (_didIteratorError22) {
                        throw _iteratorError22;
                    }
                }
            }
        }
    }, {
        key: "getSchoolsForTeacher",
        value: function getSchoolsForTeacher(teacher) {
            var allSchools = [];
            var _iteratorNormalCompletion23 = true;
            var _didIteratorError23 = false;
            var _iteratorError23 = undefined;

            try {
                for (var _iterator23 = this.teachersBySchool[Symbol.iterator](), _step23; !(_iteratorNormalCompletion23 = (_step23 = _iterator23.next()).done); _iteratorNormalCompletion23 = true) {
                    var _ref43 = _step23.value;

                    var _ref44 = _slicedToArray(_ref43, 2);

                    var school = _ref44[0];
                    var mappings = _ref44[1];

                    for (var mapping in mappings) {
                        if (mapping["Teacher"] == teacher) {
                            allSchools.push(school);
                        }
                    }
                }
            } catch (err) {
                _didIteratorError23 = true;
                _iteratorError23 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion23 && _iterator23.return) {
                        _iterator23.return();
                    }
                } finally {
                    if (_didIteratorError23) {
                        throw _iteratorError23;
                    }
                }
            }

            return allSchools;
        }
    }, {
        key: "getClassesForTeacher",
        value: function getClassesForTeacher(academicYear, school, teacher) {
            var allClasses = [];
            var _iteratorNormalCompletion24 = true;
            var _didIteratorError24 = false;
            var _iteratorError24 = undefined;

            try {
                for (var _iterator24 = this.teachersByClass[Symbol.iterator](), _step24; !(_iteratorNormalCompletion24 = (_step24 = _iterator24.next()).done); _iteratorNormalCompletion24 = true) {
                    var _ref45 = _step24.value;

                    var _ref46 = _slicedToArray(_ref45, 2);

                    var cls = _ref46[0];
                    var mappings = _ref46[1];

                    for (var mapping in mappings) {
                        var endDate = mapping["EndDate"];
                        if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                            allClasses.push(cls);
                        }
                    }
                }
            } catch (err) {
                _didIteratorError24 = true;
                _iteratorError24 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion24 && _iterator24.return) {
                        _iterator24.return();
                    }
                } finally {
                    if (_didIteratorError24) {
                        throw _iteratorError24;
                    }
                }
            }

            return allClasses;
        }
    }, {
        key: "getSubjectsForTeacher",
        value: function getSubjectsForTeacher(teacher) {
            var allSubjects = {};
            var _iteratorNormalCompletion25 = true;
            var _didIteratorError25 = false;
            var _iteratorError25 = undefined;

            try {
                for (var _iterator25 = this.teachersByClassBySubject[Symbol.iterator](), _step25; !(_iteratorNormalCompletion25 = (_step25 = _iterator25.next()).done); _iteratorNormalCompletion25 = true) {
                    var _ref47 = _step25.value;

                    var _ref48 = _slicedToArray(_ref47, 2);

                    var cls = _ref48[0];
                    var subjects = _ref48[1];
                    var _iteratorNormalCompletion26 = true;
                    var _didIteratorError26 = false;
                    var _iteratorError26 = undefined;

                    try {
                        for (var _iterator26 = subjects[Symbol.iterator](), _step26; !(_iteratorNormalCompletion26 = (_step26 = _iterator26.next()).done); _iteratorNormalCompletion26 = true) {
                            var _ref49 = _step26.value;

                            var _ref50 = _slicedToArray(_ref49, 2);

                            var subject = _ref50[0];
                            var mappings = _ref50[1];

                            for (var mapping in mappings) {
                                if (mapping["Teacher"] == teacher && mapping["EndDate"] == null) {
                                    if (!(cls in allSubjects)) {
                                        allSubjects.set(cls, []);
                                    }
                                    allSubjects[cls].push(subject);
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError26 = true;
                        _iteratorError26 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion26 && _iterator26.return) {
                                _iterator26.return();
                            }
                        } finally {
                            if (_didIteratorError26) {
                                throw _iteratorError26;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError25 = true;
                _iteratorError25 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion25 && _iterator25.return) {
                        _iterator25.return();
                    }
                } finally {
                    if (_didIteratorError25) {
                        throw _iteratorError25;
                    }
                }
            }

            return allSubjects;
        }

        // Subject Related

    }, {
        key: "getQuestions",
        value: function getQuestions(subject) {
            // Return a list of Question objects
            if (subject in this.questionsBySubject) {
                return this.questionsBySubject.get(subject);
            }
        }
    }, {
        key: "addQuestion",
        value: function addQuestion(question, subject) {
            var questionId = this.getUniqueId();
            question.setId(questionId);
            if (!(subject in this.questionsBySubject)) {
                this.questionsBySubject.set(subject, []);
            }
            this.questionsBySubject.get(subject).push(question);
        }
    }, {
        key: "removeQuestion",
        value: function removeQuestion(question, subject) {
            if (question.Id in this.questions) {
                this.questions.delete(question.Id);
            }
            if (subject in this.questionsBySubject) {
                var subjectQuestions = this.questionsBySubject.get(subject);
                var questionIndex = subjectQuestions.indexOf(question);
                if (questionIndex >= 0) {
                    subjectQuestions.splice(questionIndex, 1);
                }
            }
        }
    }, {
        key: "updateQuestion",
        value: function updateQuestion(question, subject) {}
    }, {
        key: "addExam",
        value: function addExam(exam) {
            var examId = this.getUniqueId();
            exam.setId(examId);
            this.exams[examId] = exam;
            if (!(exam.Class in this.examsByClassBySubject)) {
                this.examsByClassBySubject.set(exam.Class, new Map());
            }
            if (!(exam.Subject in this.examsByClassBySubject.get(exam.Class))) {
                this.examsByClassBySubject.get(exam.Class).set(exam.Subject, []);
            }
            this.examsByClassBySubject.get(exam.Class).get(exam.Subject).push(exam);
        }
    }, {
        key: "removeExam",
        value: function removeExam(exam) {
            // lets get to this later
        }
    }, {
        key: "updateExam",
        value: function updateExam(exam) {}
    }, {
        key: "getExamsForSubject",
        value: function getExamsForSubject(cls, subject) {
            if (cls in this.examsByClassBySubject) {
                if (subject in this.examsByClassBySubject.get(cls)) {
                    return this.examsByClassBySubject.get(cls).get(subject);
                }
            }
        }

        // Question Related

    }, {
        key: "addMastery",
        value: function addMastery(mastery, question) {
            if (!(question in this.masteriesByQuestion)) {
                this.masteriesByQuestion.set(question, []);
            }
            var masteryId = this.getUniqueId();
            mastery.setId(masteryId);
            this.masteriesByQuestion.get(question).push(mastery);
        }
    }, {
        key: "removeMastery",
        value: function removeMastery(mastery, question) {
            if (mastery.Id in this.masteries) {
                this.masteries.delete(mastery.Id);
            }
            if (question in this.masteriesByQuestion) {
                var allMastery = this.masteriesByQuestion.get(question);
                var masteryIndex = allMastery.indexOf(mastery);
                if (masteryIndex >= 0) {
                    allMastery.splice(masteryIndex, 1);
                }
            }
        }
    }, {
        key: "getMasteryLevels",
        value: function getMasteryLevels(question) {
            return this.masteriesByQuestion.get(question);
        }

        // Mastery related

    }, {
        key: "updateMastery",
        value: function updateMastery(mastery) {}

        // Exam related

    }, {
        key: "addQuestionToExam",
        value: function addQuestionToExam(exam, question) {
            if (!(exam in this.examQuestions)) {
                this.examQuestions.set(exam, []);
            }
            this.examQuestions.get(exam).push(question);
        }
    }, {
        key: "removeQuestionFromExam",
        value: function removeQuestionFromExam(exam, question) {
            if (exam in this.examQuestions) {
                var questionIndex = this.examQuestions.get(exam).indexOf(question);
                if (questionIndex >= 0) {
                    this.examQuestions.get(exam).splice(questionIndex, 1);
                }
            }
        }
    }, {
        key: "getExamQuestions",
        value: function getExamQuestions(exam) {
            return this.examQuestions.get(exam);
        }
    }, {
        key: "getExamResults",
        value: function getExamResults(student, exam) {
            if (student in this.examsByStudent) {
                if (exam in this.examsByStudent.get(student)) {
                    return this.examsByStudent.get(student).get(exam);
                }
            }
        }
    }, {
        key: "addExamAnswer",
        value: function addExamAnswer(exam, student, question, answer, mastery) {
            if (!(student in this.examsByStudent[exam.Id])) {
                this.examsByStudent.set(student, new Map());
            }
            if (!(exam in this.examsByStudent.get(student))) {
                this.examsByStudent.get(student).set(exam, new Map());
            }
            this.examsByStudent.get(student).get(exam).set(question, {
                "Answer": answer,
                "Mastery": mastery
            });
        }
    }, {
        key: "updateExamAnswer",
        value: function updateExamAnswer(exam, student, question, answer, mastery) {
            if (exam in this.examsByStudent) {
                if (student in this.examsByStudent.get(exam)) {
                    this.examsByStudent.get(exam).get(student).set(question, {
                        "Answer": answer,
                        "Mastery": mastery.Id
                    });
                }
            }
        }
    }]);

    return MockDataSource;
}();
//# sourceMappingURL=MockDataSource2.js.map