"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _School = require("../Model/School.js");

var _AcademicYear = require("../Model/AcademicYear.js");

var _Address = require("../Model/Address.js");

var _Class = require("../Model/Class.js");

var _Contact = require("../Model/Contact.js");

var _Exam = require("../Model/Exam.js");

var _ExamResult = require("../Model/ExamResult.js");

var _Mastery = require("../Model/Mastery.js");

var _Question = require("../Model/Question.js");

var _Student = require("../Model/Student.js");

var _Subject = require("../Model/Subject.js");

var _Teacher = require("../Model/Teacher.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MockDataSource = function () {
    function MockDataSource() {
        _classCallCheck(this, MockDataSource);

        this.academicYears = new Array();
        this.schools = new Array();
        this.subjects = new Array();
        this.studentsBySchool = new Map();
        this.schoolToAY = new Map();
        this.ayToSchool = new Map();
        this.teachersBySchool = new Map();
        this.classesbyAcademicYear = new Map();
        this.subjectsByClasses = new Map();
        this.adminTeachers = new Map();
        this.teachersBySubject = new Map();
        this.studentsBySubject = new Map();
        this.studentsByClass = new Map();
        this.contactsByStudent = new Map();
        this.contactsByTeacher = new Map();
        this.contactsBySchool = new Map();
        this.initializeAYs();
        this.initializeSchool();
    }

    _createClass(MockDataSource, [{
        key: "initializeAYs",
        value: function initializeAYs() {
            this.ay2016 = new _AcademicYear.AcademicYear(this, "2015-2016", new Date(2015, 6, 1), new Date(2016, 3, 31), "acad1");
            this.ay2017 = new _AcademicYear.AcademicYear(this, "2016-2017", new Date(2016, 6, 1), new Date(2017, 3, 31), "acad2");
            this.academicYears = [this.ay2016, this.ay2017];
            this.classesbyAcademicYear.set(this.ay2016, new Map());
            this.classesbyAcademicYear.set(this.ay2017, new Map());
        }
    }, {
        key: "initializeSubjects",
        value: function initializeSubjects() {
            this.sub1 = new _Subject.Subject(this, "Maths 1A", "Maths", true, "sub1");
            this.sub2 = new _Subject.Subject(this, "Hindi 1A", "Hindi", false, "sub2");
            this.sub3 = new _Subject.Subject(this, "French 1A", "French", false, "sub3");
            this.sub4 = new _Subject.Subject(this, "Maths 2A", "Maths", true, "sub4");
            this.sub5 = new _Subject.Subject(this, "Science 2A", "Science", true, "sub5");
            this.subjects = [this.sub1, this.sub2, this.sub3, this.sub4, this.sub5];
        }
    }, {
        key: "initializeSchool",
        value: function initializeSchool() {
            var school = new _School.School(this, "School2", null, null, "s2");
            this.schoolToAY.set(school, [this.ay2016, this.ay2017]);
            this.ayToSchool.set(this.ay2016, [school]);
            this.ayToSchool.set(this.ay2017, [school]);
            this.schools.push(school);

            var teacher1 = new _Teacher.Teacher(this, "T1", "t1", "E001", "M", new Date(1981, 1, 1), null, null, "teacher1");
            var teacher2 = new _Teacher.Teacher(this, "T2", "t2", "E002", "M", new Date(1981, 1, 1), null, null, "teacher1");
            var teacher3 = new _Teacher.Teacher(this, "T3", "t3", "E003", "M", new Date(1981, 1, 1), null, null, "teacher1");
            var teacher4 = new _Teacher.Teacher(this, "T4", "t4", "E004", "M", new Date(1981, 1, 1), null, null, "teacher1");
            this.teachers = [teacher1, teacher2, teacher3, teacher4];
            this.teachersBySchool.set(school, [new Map([["Teacher", teacher1], ["StartDate", new Date(2016, 4, 1)], ["EndDate", new Date(2016, 7, 10)]]), new Map([["Teacher", teacher2], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]]), new Map([["Teacher", teacher3], ["StartDate", new Date(2016, 7, 11)], ["EndDate", null]]), new Map([["Teacher", teacher4], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]])]);

            var cls1 = new _Class.Class(this, "1A", "1", "A", "T4I-002", "English", this.ay2016, school);
            var cls2 = new _Class.Class(this, "1A", "1", "A", "T4I-003", "English", this.ay2017, school);
            var cls3 = new _Class.Class(this, "2A", "2", "A", "T4I-002", "English", this.ay2017, school);
            this.classesbyAcademicYear.get(this.ay2016).set(school, [cls1]);
            this.classesbyAcademicYear.get(this.ay2017).set(school, [cls2, cls3]);

            this.adminTeachers.set(cls1, [new Map([["Teacher", teacher1], ["StartDate", new Date(2016, 4, 1)], ["EndDate", new Date(2016, 7, 10)]]), new Map([["Teacher", teacher3], ["StartDate", new Date(2016, 7, 11)], ["EndDate", null]])]);
            this.adminTeachers.set(cls2, [new Map([["Teacher", teacher3], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]);
            this.adminTeachers.set(cls3, [new Map([["Teacher", teacher2], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]);

            this.subjectsByClasses.set(cls1, [this.sub1, this.sub2, this.sub3]);
            this.subjectsByClasses.set(cls2, [this.sub1, this.sub2, this.sub3]);
            this.subjectsByClasses.set(cls3, [this.sub4, this.sub5]);

            this.teachersBySubject.set(cls1, new Map([[this.sub1, [new Map([["Teacher", teacher1], ["StartDate", new Date(2016, 4, 1)], ["EndDate", new Date(2016, 7, 10)]]), new Map([["Teacher", teacher3], ["StartDate", new Date(2016, 7, 11)], ["EndDate", null]])]], [this.sub2, [new Map([["Teacher", teacher2], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]])]], [this.sub3, [new Map([["Teacher", teacher4], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]])]]]));
            this.teachersBySubject.set(cls2, new Map([[this.sub1, [new Map([["Teacher", teacher3], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]], [this.sub2, [new Map([["Teacher", teacher2], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]], [this.sub3, [new Map([["Teacher", teacher3], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]]]));
            this.teachersBySubject.set(cls3, new Map([[this.sub4, [new Map([["Teacher", teacher2], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]], [this.sub5, [new Map([["Teacher", teacher4], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]]]));

            var student1 = new _Student.Student(this, "S1", "s1", "S001", "M", new Date(2009, 1, 1), null, null, "123", "1233", "S1@T4I.com", "S001");
            var student2 = new _Student.Student(this, "S2", "s2", "S002", "M", new Date(2009, 1, 1), null, null, "124", "1244", "S2@T4I.com", "S002");
            var student3 = new _Student.Student(this, "S3", "s3", "S003", "F", new Date(2009, 1, 1), null, null, "125", "1255", "S3@T4I.com", "S003");
            var student4 = new _Student.Student(this, "S4", "s4", "S004", "F", new Date(2010, 1, 1), null, null, "126", "1266", "S4@T4I.com", "S004");
            var student5 = new _Student.Student(this, "S5", "s5", "S005", "M", new Date(2010, 1, 1), null, null, "127", "1277", "S5@T4I.com", "S005");
            this.studentsBySchool = new Map([[school, [new Map([["Student", student1], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]]), new Map([["Student", student2], ["StartDate", new Date(2016, 4, 1)], ["EndDate", new Date(2016, 8, 15)]]), new Map([["Student", student3], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]]), new Map([["Student", student4], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]]), new Map([["Student", student5], ["StartDate", new Date(2017, 4, 10)], ["EndDate", null]])]]]);

            this.studentsByClass = new Map([[cls1, [new Map([["Student", student1], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]]), new Map([["Student", student2], ["StartDate", new Date(2016, 4, 1)], ["EndDate", new Date(2016, 8, 15)]]), new Map([["Student", student3], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]])]], [cls2, [new Map([["Student", student4], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]]), new Map([["Student", student5], ["StartDate", new Date(2017, 4, 10)], ["EndDate", null]])]], [cls3, [new Map([["Student", student1], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]]), new Map([["Student", student3], ["StartDate", new Date(2017, 4, 1)], ["EndDate", null]])]]]);

            this.studentsBySubject = new Map([[cls1, new Map([[this.sub2, [student1]], [this.sub3, [student3]]])], [cls2, new Map([[this.sub2, [student4]], [this.sub3, [student5]]])]]);
        }

        // Academic Year related methods

    }, {
        key: "getAcademicYears",
        value: function getAcademicYears() {
            return this.academicYears;
        }
    }, {
        key: "getCurrentAcademicYear",
        value: function getCurrentAcademicYear(school) {
            // Get the currently running academic year
            if (school in this.schoolToAY) {
                var ays = this.schoolToAY[school];
                return ays[ays.length - 1];
            }
        }
    }, {
        key: "getPrevAcademicYear",
        value: function getPrevAcademicYear(acadYear) {
            // Return an instance of AcademicYear object
            if (acadYear in this.academicYears) {
                var index = this.academicYears.indexOf(acadYear);
                if (index != 0) {
                    return this.academicYears[index - 1];
                }
            }
        }
    }, {
        key: "getSchools",
        value: function getSchools(academicYear) {
            return this.ayToSchool.get(academicYear);
        }
    }, {
        key: "addAcademicYear",
        value: function addAcademicYear(academicYear) {
            this.academicYears.push(academicYear);
            academicYear.setId(this.academicYears.length);
            this.ayToSchool.set(academicYear, []);
            this.classesbyAcademicYear.set(academicYear, new Map());
        }

        // School related methods

    }, {
        key: "getClassesForAcademicYear",
        value: function getClassesForAcademicYear(academicYear, school) {
            // Get all classes for the given school in the given academic year
            if (academicYear in this.classesbyAcademicYear) {
                if (school in this.classesbyAcademicYear[academicYear]) {
                    return this.classesbyAcademicYear[academicYear][school];
                }
            }
        }
    }, {
        key: "getTeachersForAcademicYear",
        value: function getTeachersForAcademicYear(academicYear, school) {
            // Get all teachers in this school for the given academic year
            var teachers = [];
            if (school in this.teachersBySchool) {
                for (var i = 0; i < this.teachersBySchool[school].length; i++) {
                    var teacher = this.teachersBySchool[school][i];
                    var startDate = teacher.get("StartDate");
                    var endDate = teacher.get("EndDate");
                    if (endDate == null) {
                        if (startDate < academicYear.StartDate) {
                            teachers.push(teacher);
                        }
                    } else {
                        if (endDate > academicYear.StartDate) {
                            teachers.push(teacher);
                        }
                    }
                }
            }
            return teachers;
        }
    }, {
        key: "addSchool",
        value: function addSchool(academicYear, school) {
            this.schools.push(school);
            this.schoolToAY.set(school, [academicYear]);
            if (!this.ayToSchool.has(academicYear)) {
                this.ayToSchool.set(academicYear, []);
            }
            this.ayToSchool.get(academicYear).push(school);
            this.teachersBySchool.set(school, []);
        }
    }, {
        key: "addClassToSchool",
        value: function addClassToSchool(academicYear, school, cls) {
            this.classesbyAcademicYear.get(academicYear).get(school).push(cls);
        }
    }, {
        key: "addTeacher",
        value: function addTeacher(teacher) {
            this.teachers.push(teacher);
            teacher.setId(this.teachers.length);
        }
    }, {
        key: "addTeacherToSchool",
        value: function addTeacherToSchool(school, teacher) {
            if (!(school in this.teachersBySchool)) {
                this.teachersBySchool.set(school, []);
            }
            this.teachersBySchool.get(school).push(new Map([["Teacher", teacher1], ["StartDate", new Date(2016, 4, 1)], ["EndDate", null]]));
        }
    }, {
        key: "updateTeacherEndDate",
        value: function updateTeacherEndDate(mapping, teacher) {
            if (mapping["Teacher"] == teacher) {
                mapping["EndDate"] = new Date();
            }
        }
    }, {
        key: "removeTeacher",
        value: function removeTeacher(school, teacher) {
            var _this = this;

            if (school in this.teachersBySchool) {
                this.teachersBySchool[school].forEach(function (mapping) {
                    return _this.updateTeacherEndDate(mapping, teacher);
                });
                this.adminTeachers.forEach(function (cls, adminTeachers) {
                    return adminTeachers.forEach(function (mapping) {
                        return _this.updateTeacherEndDate(mapping, teacher);
                    });
                });
                this.teachersBySubject.forEach(function (cls, subjects) {
                    return subjects.forEach(function (subject, teachers) {
                        return teachers.forEach(function (mapping) {
                            return _this.updateTeacherEndDate(mapping, teacher);
                        });
                    });
                });
            }
        }

        // Class related methods

    }, {
        key: "addClass",
        value: function addClass(cls) {
            if (!(cls.School in this.classesbyAcademicYear[cls.AcademicYear])) {
                this.classesbyAcademicYear[cls.AcademicYear].set(cls.School, []);
            }
            if (!(cls in this.classesbyAcademicYear[cls.AcademicYear][cls.School])) {
                this.classesbyAcademicYear[cls.AcademicYear][cls.School].push(cls);
            }
            cls.setId(cls.AcademicYear.Name + cls.School.Name + cls.Name);
        }
    }, {
        key: "updateClass",
        value: function updateClass(cls) {
            // Nothing to update
        }
    }, {
        key: "getSubjectsForClass",
        value: function getSubjectsForClass(cls) {
            // Get subjects for the given class
            return this.subjectsByClasses.get(cls);
        }
    }, {
        key: "getTeachersForClass",
        value: function getTeachersForClass(cls) {
            // Get all teachers for the given class
            var teachers = [];
            if (cls in this.adminTeachers) {
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = this.adminTeachers[cls][Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var teacher = _step.value;

                        if (teacher["EndDate"] == null) {
                            teachers.push(teacher);
                        }
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }
            if (cls in this.teachersBySubject) {
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = this.teachersBySubject[cls][Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var _ref = _step2.value;

                        var _ref2 = _slicedToArray(_ref, 2);

                        var subject = _ref2[0];
                        var subjectTeachers = _ref2[1];
                        var _iteratorNormalCompletion3 = true;
                        var _didIteratorError3 = false;
                        var _iteratorError3 = undefined;

                        try {
                            for (var _iterator3 = subjectTeachers[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                                var teacher = _step3.value;

                                if (teacher["EndDate"] == null) {
                                    teachers.push(teacher);
                                }
                            }
                        } catch (err) {
                            _didIteratorError3 = true;
                            _iteratorError3 = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                    _iterator3.return();
                                }
                            } finally {
                                if (_didIteratorError3) {
                                    throw _iteratorError3;
                                }
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            }
            return teachers;
        }
    }, {
        key: "getStudentsFromClass",
        value: function getStudentsFromClass(cls) {
            // Get all students for the given class
            if (cls in this.studentsByClass) {
                var students = [];
                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    for (var _iterator4 = this.studentsByClass[cls][Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        var mapping = _step4.value;

                        var endDate = mapping["EndDate"];
                        if (endDate == null) {
                            students.push(mapping["Student"]);
                        }
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }

                return students;
            }
        }
    }, {
        key: "getAdminTeacher",
        value: function getAdminTeacher(cls) {
            if (cls in this.adminTeachers) {
                var _iteratorNormalCompletion5 = true;
                var _didIteratorError5 = false;
                var _iteratorError5 = undefined;

                try {
                    for (var _iterator5 = this.adminTeachers[cls][Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                        var teacher = _step5.value;

                        if (teacher["EndDate"] == null) {
                            return teacher;
                        }
                    }
                } catch (err) {
                    _didIteratorError5 = true;
                    _iteratorError5 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
                            _iterator5.return();
                        }
                    } finally {
                        if (_didIteratorError5) {
                            throw _iteratorError5;
                        }
                    }
                }
            }
        }
    }, {
        key: "getStudentsBySubject",
        value: function getStudentsBySubject(cls, students, subject) {
            if (cls in this.studentsBySubject) {
                if (subject.IsMandatory) {
                    return this.getStudentsFromClass(cls);
                }
                var mappings = this.studentsBySubject[cls];
                return mappings.get(subject);
            }
        }
    }, {
        key: "addAdminTeacher",
        value: function addAdminTeacher(cls, teacher) {
            if (!(cls in this.adminTeachers)) {
                this.adminTeachers.set(cls, []);
            }
            var teachers = this.adminTeachers[cls];
            teachers[teachers.length - 1]["EndDate"] = new Date();
            teachers.push(teacher);
        }
    }, {
        key: "addSubject",
        value: function addSubject(subject) {
            this.subjects.push(subject);
            subject.setId(this.subjects.length);
        }
    }, {
        key: "updateSubject",
        value: function updateSubject(subject) {
            // Nothing to update here
        }
    }, {
        key: "removeSubject",
        value: function removeSubject(subject) {
            // lets get to this later
        }
    }, {
        key: "addSubjectToClass",
        value: function addSubjectToClass(cls, subject) {
            // add to t4i.class_subjects
            if (!(cls in this.subjectsByClasses)) {
                this.subjectsByClasses[cls] = [];
            }
            this.subjectsByClasses[cls].push(subject);
        }
    }, {
        key: "removeSubjectFromClass",
        value: function removeSubjectFromClass(cls, subject) {
            // Remove from t4i.class_subjects
            // If no other class is using it then remove from t4i.subjects
            if (cls in this.subjectsByClasses) {
                var subjects = this.subjectsByClasses[cls];
                if (subject in subjects) {
                    this.subjectsByClasses[cls] = subjects.filter(function (item) {
                        return item != subject;
                    });
                }
            }
        }
    }, {
        key: "addTeacherToSubject",
        value: function addTeacherToSubject(cls, teacher, subject) {
            if (!(cls in this.teachersBySubject)) {
                this.teachersBySubject.set(cls, new Map());
            }
            var subjectMapping = this.teachersBySubject[cls];
            if (!(subject in subjectMapping)) {
                subjectMapping.set(subject, []);
            }
            var teachers = subjectMapping[subject];
            if (teachers.length > 1) {
                teachers[teachers.length - 1]["EndDate"] = new Date();
            }
            teachers.push(new Map([["Teacher", teacher], ["StartDate", new Date()]]));
        }
    }, {
        key: "removeTeacherForSubject",
        value: function removeTeacherForSubject(cls, teacher, subject) {
            if (cls in this.teachersBySubject) {
                var subjectMapping = this.teachersBySubject[cls];
                if (subject in subjectMapping) {
                    var teachers = subjectMapping[subject];
                    if (teachers.length > 1 && teachers[teachers.length]["Teacher"] == teacher) {
                        teachers[teachers.length - 1]["EndDate"] = new Date();
                    }
                }
            }
        }
    }, {
        key: "addStudentToClass",
        value: function addStudentToClass(cls, student) {
            if (!(cls in this.studentsByClass)) {
                this.studentsByClass.set(cls, []);
            }
            this.studentsByClass[cls].push(new Map([["Student", student], ["StartDate", new Date()], ["EndDate", null]]));
        }
    }, {
        key: "removeStudentFromClass",
        value: function removeStudentFromClass(cls, student) {
            if (cls in this.studentsByClass) {
                var _iteratorNormalCompletion6 = true;
                var _didIteratorError6 = false;
                var _iteratorError6 = undefined;

                try {
                    for (var _iterator6 = this.studentsByClass[cls][Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                        var mapping = _step6.value;

                        if (mapping["Student"] == student) {
                            mapping["EndDate"] = new Date();
                        }
                    }
                } catch (err) {
                    _didIteratorError6 = true;
                    _iteratorError6 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion6 && _iterator6.return) {
                            _iterator6.return();
                        }
                    } finally {
                        if (_didIteratorError6) {
                            throw _iteratorError6;
                        }
                    }
                }
            }
        }
    }, {
        key: "addStudentToSubject",
        value: function addStudentToSubject(cls, student, subject) {
            // add to t4i.class_student_subjects
            if (!subject.IsMandatory) {
                if (!(cls in this.studentsBySubject)) {
                    this.studentsBySubject.set(cls, []);
                }
                if (!(subject in this.studentsBySubject[cls])) {
                    this.studentsBySubject[cls].set(subject, []);
                }
                this.studentsBySubject[cls][subject].push(student);
            }
        }
    }, {
        key: "removeStudentFromSubject",
        value: function removeStudentFromSubject(cls, student, subject) {}
        // lets get here later


        // Student related methods

    }, {
        key: "addStudent",
        value: function addStudent(student) {
            if (!(student.School in this.studentsBySchool)) {
                this.studentsBySchool.set(student.School, []);
            }
            this.studentsBySchool[student.School].push(new Map([["Student", student], ["StartDate", new Date()], ["EndDate", null]]));
        }
    }, {
        key: "addContactToStudent",
        value: function addContactToStudent(student, contact) {
            // add to t4i.student_contacts
            if (!(student in this.contactsByStudent)) {
                this.contactsByStudent.set(student, []);
            }
            this.contactsByStudent[student].push(contact);
        }
    }, {
        key: "getSchoolsForStudent",
        value: function getSchoolsForStudent(academicYear, student) {
            var schools = [];
            var _iteratorNormalCompletion7 = true;
            var _didIteratorError7 = false;
            var _iteratorError7 = undefined;

            try {
                for (var _iterator7 = this.studentsBySchool[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                    var _ref3 = _step7.value;

                    var _ref4 = _slicedToArray(_ref3, 2);

                    var school = _ref4[0];
                    var mappings = _ref4[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == student) {
                            var endDate = mapping["EndDate"];
                            if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                schools.push(school);
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError7 = true;
                _iteratorError7 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion7 && _iterator7.return) {
                        _iterator7.return();
                    }
                } finally {
                    if (_didIteratorError7) {
                        throw _iteratorError7;
                    }
                }
            }

            return schools;
        }
    }, {
        key: "getClassesForStudent",
        value: function getClassesForStudent(academicYear, student) {
            var classes = [];
            var _iteratorNormalCompletion8 = true;
            var _didIteratorError8 = false;
            var _iteratorError8 = undefined;

            try {
                for (var _iterator8 = this.studentsByClass[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                    var _ref5 = _step8.value;

                    var _ref6 = _slicedToArray(_ref5, 2);

                    var cls = _ref6[0];
                    var mappings = _ref6[1];

                    for (var mapping in mappings) {
                        if (mapping["Student"] == student) {
                            var endDate = mapping["EndDate"];
                            if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                classes.push(cls);
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError8 = true;
                _iteratorError8 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion8 && _iterator8.return) {
                        _iterator8.return();
                    }
                } finally {
                    if (_didIteratorError8) {
                        throw _iteratorError8;
                    }
                }
            }

            return classes;
        }
    }, {
        key: "getSubjectsForStudent",
        value: function getSubjectsForStudent(cls, student) {
            //  Get Subjects from the class for the given student
            if (cls in this.studentsBySubject) {
                return this.studentsBySubject[cls].get(student);
            }
        }
    }, {
        key: "getExamsForStudent",
        value: function getExamsForStudent(acadYear, cls, student) {
            // Return all exams for the given student in the given academic year
        }
    }, {
        key: "getSubjectExamsForStudent",
        value: function getSubjectExamsForStudent(acadYear, cls, student, subject) {}
        // Return all exams for the given student in the given academic year for the given subject


        // Contact related methods

    }, {
        key: "getContacts",
        value: function getContacts(entity, contact) {
            // Read from t4i.contacts
        }
    }, {
        key: "addContact",
        value: function addContact(entity, contact) {
            // Add to t4i.contacts
        }
    }, {
        key: "updateContact",
        value: function updateContact(entity, contact) {}
        // Nothing to update here


        // Exam related methods

    }, {
        key: "addQuestion",
        value: function addQuestion(question) {
            // Add to t4i.questions
        }
    }, {
        key: "updateQuestion",
        value: function updateQuestion(question) {
            // Update t4i.questions
        }
    }, {
        key: "removeQuestion",
        value: function removeQuestion(question) {
            // Remove from t4i.questions
        }
    }, {
        key: "getQuestions",
        value: function getQuestions(subject) {
            // Return a list of Question objects
        }
    }, {
        key: "addMastery",
        value: function addMastery(mastery) {
            // Add to t4i.question_mastery
        }
    }, {
        key: "removeMastery",
        value: function removeMastery(mastery) {
            // Remove from t4i.question_mastery
        }
    }, {
        key: "updateMastery",
        value: function updateMastery(mastery) {
            // Update t4i.questions_mastery
        }
    }, {
        key: "getMastery",
        value: function getMastery(question) {
            // Return a list of Mastery objects from t4i.questions_mastery
        }
    }, {
        key: "addExam",
        value: function addExam(cls, exam) {
            // Add to t4i.exams
        }
    }, {
        key: "removeExam",
        value: function removeExam(exam) {
            // Remove from t4i.exams
        }
    }, {
        key: "updateExam",
        value: function updateExam(exam) {
            // Update t4i.exams
        }
    }, {
        key: "getExamsForSubject",
        value: function getExamsForSubject(cls, subject) {
            //
        }
    }, {
        key: "addQuestionToExam",
        value: function addQuestionToExam(exam, question) {
            // Add to t4i.exam_questions
        }
    }, {
        key: "removeQuestionFromExam",
        value: function removeQuestionFromExam(exam, question) {
            // Remove from t4i.exam_questions
        }
    }, {
        key: "getExamQuestions",
        value: function getExamQuestions(exam) {
            // Read from t4i.exam_questions
        }
    }, {
        key: "getExamResults",
        value: function getExamResults(student, exam) {
            // Return an instance of ExamResult object
        }
    }, {
        key: "addExamAnswer",
        value: function addExamAnswer(exam, student, question, answer, mastery) {
            // Add to t4i.exam_results
        }
    }, {
        key: "updateExamAnswer",
        value: function updateExamAnswer(exam, student, question, answer, mastery) {
            // Update t4i.exam_results
        }
    }]);

    return MockDataSource;
}();

module.exports = {
    MockDataSource: MockDataSource
};
//# sourceMappingURL=MockDataSourceOld.js.map