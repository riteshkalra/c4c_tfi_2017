"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MockDataSource = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _School = require("../Model/School.js");

var _AcademicYear = require("../Model/AcademicYear.js");

var _Address = require("../Model/Address.js");

var _Class = require("../Model/Class.js");

var _Contact = require("../Model/Contact.js");

var _Exam = require("../Model/Exam.js");

var _ExamResult = require("../Model/ExamResult.js");

var _Mastery = require("../Model/Mastery.js");

var _Question = require("../Model/Question.js");

var _Student = require("../Model/Student.js");

var _Subject = require("../Model/Subject.js");

var _Teacher = require("../Model/Teacher.js");

var _Term = require("../Model/Term.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MockDataSource = exports.MockDataSource = function () {
    function MockDataSource() {
        _classCallCheck(this, MockDataSource);

        this.initializeAcademicYears();
        this.initializeSchools();
        this.initializeClasses();
        this.initializeStudents();
        this.initializeStudentsBySchool();
        this.initializeStudentsByClass();
        this.initializeTeachers();
        this.initializeTeachersBySchool();
        this.initializeTeachersByClass();
        this.initializeSubjects();
        this.initializeSubjectsByClass();
        this.initializeTeachersBySubject();
        this.initializeSubjectsByStudent();
        this.initializeQuestions();
        this.initializeMasteryLevels();
        this.initializeExams();
        this.initializeExamQuestions();
        this.initializeExamResults();
    }

    _createClass(MockDataSource, [{
        key: "toDate",
        value: function toDate(val) {
            return val != null ? new Date(val) : null;
        }
    }, {
        key: "getUniqueId",
        value: function getUniqueId() {
            return Math.random().toString(36).substr(2, 16);
        }
    }, {
        key: "initializeAcademicYears",
        value: function initializeAcademicYears() {
            this.academicYears = new Map();
            var academicYearsObj = require("./Mock/academicyears.json");
            for (var academicYearId in academicYearsObj) {
                var mapping = academicYearsObj[academicYearId];
                this.academicYears.set(academicYearId, _AcademicYear.AcademicYear.fromJSON(this, academicYearId, mapping));
            }
        }
    }, {
        key: "initializeContacts",
        value: function initializeContacts(entityName, entityId) {
            var contacts = [];
            var jsonObj = require("./Mock/academicyears.json");
            for (var contactId in jsonObj) {
                var mapping = jsonObj[contactId];
                if (mapping["EntityName"] == entityName && mapping["EntityId"] == entityId) {
                    contacts.push(_Contact.Contact.fromJSON(this, contactId, mapping));
                }
            }
            return contacts;
        }
    }, {
        key: "initializeSchools",
        value: function initializeSchools() {
            this.schools = new Map();
            this.schoolContacts = new Map();
            this.holidaysBySchool = new Map();
            var jsonObj = require("./Mock/schools.json");
            for (var schoolId in jsonObj) {
                var mapping = jsonObj[schoolId];
                var school = _School.School.fromJSON(this, schoolId, mapping);
                this.schools.set(schoolId, school);
                this.schoolContacts.set(school.Id, this.initializeContacts("School", schoolId));
                var holidays = [];
                this.holidaysBySchool.set(school, holidays);
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = (mapping["Holidays"] || [])[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var date = _step.value;

                        holidays.push(this.toDate(date));
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }

            this.schoolsByAcademicYear = new Map();
            jsonObj = require("./Mock/academicyear_schools.json");
            for (var academicYearId in jsonObj) {
                for (var i = 0; i < jsonObj[academicYearId].length; i++) {
                    var _schoolId = jsonObj[academicYearId][i];
                    var academicYear = this.academicYears.get(academicYearId);
                    var _school = this.schools.get(_schoolId);
                    if (!this.schoolsByAcademicYear.has(academicYearId)) {
                        this.schoolsByAcademicYear.set(academicYear.Id, []);
                    }
                    this.schoolsByAcademicYear.get(academicYear.Id).push(_school.Id);
                }
            }
        }
    }, {
        key: "initializeClasses",
        value: function initializeClasses() {
            this.classes = new Map();
            this.termsByClass = new Map();
            var jsonObj = require("./Mock/classes.json");
            for (var classId in jsonObj) {
                var mapping = jsonObj[classId];
                var cls = _Class.Class.fromJSON(this, classId, mapping);
                this.classes.set(classId, cls);
                var terms = [];
                this.termsByClass.set(cls, terms);
                for (var term in cls["Terms"]) {
                    terms.push(_Term.Term.fromJSON(term));
                }
            }
        }
    }, {
        key: "initializeStudents",
        value: function initializeStudents() {
            var jsonObj = require("./Mock/students.json");
            this.students = new Map();
            this.studentContacts = new Map();
            for (var studentId in jsonObj) {
                var mapping = jsonObj[studentId];
                var student = _Student.Student.fromJSON(this, studentId, mapping);
                this.students.set(studentId, student);
                this.studentContacts.set(student.Id, this.initializeContacts("Student", studentId));
            }
        }
    }, {
        key: "initializeStudentsBySchool",
        value: function initializeStudentsBySchool() {
            var jsonObj = require("./Mock/school_students.json");
            this.studentsBySchool = new Map();
            for (var schoolId in jsonObj) {
                var mappings = jsonObj[schoolId];
                if (this.schools.has(schoolId)) {
                    var school = this.schools.get(schoolId);
                    if (!this.studentsBySchool.has(schoolId)) {
                        this.studentsBySchool.set(schoolId, []);
                    }
                    var _iteratorNormalCompletion2 = true;
                    var _didIteratorError2 = false;
                    var _iteratorError2 = undefined;

                    try {
                        for (var _iterator2 = mappings[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            var mapping = _step2.value;

                            var studentId = mapping["Student"];
                            if (this.students.has(studentId)) {
                                this.studentsBySchool.get(schoolId).push({
                                    "Student": this.students.get(studentId),
                                    "StartDate": this.toDate(mapping["StartDate"]),
                                    "EndDate": this.toDate(mapping["EndDate"])
                                });
                            }
                        }
                    } catch (err) {
                        _didIteratorError2 = true;
                        _iteratorError2 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }
                        } finally {
                            if (_didIteratorError2) {
                                throw _iteratorError2;
                            }
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeStudentsByClass",
        value: function initializeStudentsByClass() {
            var jsonObj = require("./Mock/class_students.json");
            this.studentsByClass = new Map();
            for (var classId in jsonObj) {
                var mappings = jsonObj[classId];
                if (!this.classes.has(classId)) continue;
                var cls = this.classes.get(classId);
                if (!this.studentsByClass.has(cls.Id)) {
                    this.studentsByClass.set(cls.Id, []);
                }
                for (var studentId in mappings) {
                    var mapping = mappings[studentId];
                    if (this.students.has(studentId)) {
                        this.studentsByClass.get(cls.Id).push({
                            "Student": this.students.get(studentId),
                            "StartDate": this.toDate(mapping["StartDate"]),
                            "EndDate": this.toDate(mapping["EndDate"])
                        });
                    }
                }
            }
        }
    }, {
        key: "initializeTeachers",
        value: function initializeTeachers() {
            var jsonObj = require("./Mock/teachers.json");
            this.teachers = new Map();
            this.teacherContacts = new Map();
            for (var teacherId in jsonObj) {
                var mapping = jsonObj[teacherId];
                var teacher = _Teacher.Teacher.fromJSON(this, teacherId, mapping);
                this.teachers.set(teacherId, teacher);
                this.teacherContacts.set(teacherId, this.initializeContacts("Teacher", teacherId));
            }
        }
    }, {
        key: "initializeTeachersBySchool",
        value: function initializeTeachersBySchool() {
            var jsonObj = require("./Mock/school_teachers.json");
            this.teachersBySchool = new Map();
            for (var schoolId in jsonObj) {
                var mappings = jsonObj[schoolId];
                if (this.schools.has(schoolId)) {
                    var school = this.schools.get(schoolId);
                    if (!this.teachersBySchool.has(schoolId)) {
                        this.teachersBySchool.set(schoolId, []);
                    }
                    var _iteratorNormalCompletion3 = true;
                    var _didIteratorError3 = false;
                    var _iteratorError3 = undefined;

                    try {
                        for (var _iterator3 = mappings[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            var mapping = _step3.value;

                            var teacherId = mapping["Teacher"];
                            if (this.teachers.has(teacherId)) {
                                var teacher = this.teachers.get(teacherId);
                                this.teachersBySchool.get(schoolId).push({
                                    "Teacher": this.teachers.get(teacherId),
                                    "StartDate": this.toDate(mapping["StartDate"]),
                                    "EndDate": this.toDate(mapping["EndDate"])
                                });
                            }
                        }
                    } catch (err) {
                        _didIteratorError3 = true;
                        _iteratorError3 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                _iterator3.return();
                            }
                        } finally {
                            if (_didIteratorError3) {
                                throw _iteratorError3;
                            }
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeTeachersByClass",
        value: function initializeTeachersByClass() {
            var jsonObj = require("./Mock/class_teachers.json");
            this.teachersByClass = new Map();
            for (var classId in jsonObj) {
                var mappings = jsonObj[classId];
                if (!this.classes.has(classId)) continue;
                var cls = this.classes.get(classId);
                if (!this.teachersByClass.has(classId)) {
                    this.teachersByClass.set(classId, []);
                }
                var _iteratorNormalCompletion4 = true;
                var _didIteratorError4 = false;
                var _iteratorError4 = undefined;

                try {
                    for (var _iterator4 = mappings[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                        var mapping = _step4.value;

                        var teacherId = mapping["Teacher"];
                        if (this.teachers.has(teacherId)) {
                            this.teachersByClass.get(classId).push({
                                "Teacher": this.teachers.get(teacherId),
                                "StartDate": this.toDate(mapping["StartDate"]),
                                "EndDate": this.toDate(mapping["EndDate"])
                            });
                        }
                    }
                } catch (err) {
                    _didIteratorError4 = true;
                    _iteratorError4 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion4 && _iterator4.return) {
                            _iterator4.return();
                        }
                    } finally {
                        if (_didIteratorError4) {
                            throw _iteratorError4;
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeSubjects",
        value: function initializeSubjects() {
            var jsonObj = require("./Mock/subjects.json");
            this.subjects = new Map();
            for (var subjectId in jsonObj) {
                var mapping = jsonObj[subjectId];
                this.subjects.set(subjectId, _Subject.Subject.fromJSON(this, subjectId, mapping));
            }
        }
    }, {
        key: "initializeSubjectsByClass",
        value: function initializeSubjectsByClass() {
            var jsonObj = require("./Mock/class_subjects.json");
            this.subjectsByClass = new Map();
            for (var classId in jsonObj) {
                var subjects = jsonObj[classId];
                if (!this.classes.has(classId)) continue;
                var cls = this.classes.get(classId);
                if (!this.subjectsByClass.has(classId)) {
                    this.subjectsByClass.set(classId, []);
                }
                var _iteratorNormalCompletion5 = true;
                var _didIteratorError5 = false;
                var _iteratorError5 = undefined;

                try {
                    for (var _iterator5 = subjects[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                        var subjectId = _step5.value;

                        if (!this.subjects.has(subjectId)) continue;
                        var subject = this.subjects.get(subjectId);
                        this.subjectsByClass.get(classId).push(subject.Id);
                    }
                } catch (err) {
                    _didIteratorError5 = true;
                    _iteratorError5 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion5 && _iterator5.return) {
                            _iterator5.return();
                        }
                    } finally {
                        if (_didIteratorError5) {
                            throw _iteratorError5;
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeTeachersBySubject",
        value: function initializeTeachersBySubject() {
            var jsonObj = require("./Mock/class_subject_teachers.json");
            this.teachersByClassBySubject = new Map();
            for (var classId in jsonObj) {
                var subjects = jsonObj[classId];
                if (!this.classes.has(classId)) continue;
                var cls = this.classes.get(classId);
                if (!this.teachersByClassBySubject.has(classId)) {
                    this.teachersByClassBySubject.set(classId, new Map());
                }
                for (var subjectId in subjects) {
                    var mappings = subjects[subjectId];
                    if (!this.subjects.has(subjectId)) continue;
                    var subject = this.subjects.get(subjectId);
                    if (!this.teachersByClassBySubject.get(classId).has(subjectId)) {
                        this.teachersByClassBySubject.get(classId).set(subjectId, []);
                    }
                    var _iteratorNormalCompletion6 = true;
                    var _didIteratorError6 = false;
                    var _iteratorError6 = undefined;

                    try {
                        for (var _iterator6 = mappings[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
                            var mapping = _step6.value;

                            var teacherId = mapping["Teacher"];
                            if (this.teachers.has(teacherId)) {
                                this.teachersByClassBySubject.get(classId).get(subjectId).push({
                                    "Teacher": this.teachers.get(teacherId),
                                    "StartDate": this.toDate(mapping["StartDate"]),
                                    "EndDate": this.toDate(mapping["EndDate"])
                                });
                            }
                        }
                    } catch (err) {
                        _didIteratorError6 = true;
                        _iteratorError6 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion6 && _iterator6.return) {
                                _iterator6.return();
                            }
                        } finally {
                            if (_didIteratorError6) {
                                throw _iteratorError6;
                            }
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeSubjectsByStudent",
        value: function initializeSubjectsByStudent() {
            var jsonObj = require("./Mock/class_student_subjects.json");
            this.subjectsByClassByStudent = new Map();
            for (var classId in jsonObj) {
                if (!this.classes.has(classId)) continue;
                var cls = this.classes.get(classId);
                if (!this.subjectsByClassByStudent.has(classId)) {
                    this.subjectsByClassByStudent.set(classId, new Map());
                }
                var subjects = jsonObj[classId];
                for (var subjectId in subjects) {
                    var studentIds = subjects[subjectId];
                    if (!this.subjects.has(subjectId)) continue;
                    var subject = this.subjects.get(subjectId);
                    if (!this.subjectsByClassByStudent.get(classId).has(subjectId)) {
                        this.subjectsByClassByStudent.get(classId).set(subjectId, []);
                    }
                    var _iteratorNormalCompletion7 = true;
                    var _didIteratorError7 = false;
                    var _iteratorError7 = undefined;

                    try {
                        for (var _iterator7 = studentIds[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
                            var studentId = _step7.value;

                            if (this.students.has(studentId)) {
                                this.subjectsByClassByStudent.get(classId).get(subjectId).push(this.students.get(studentId));
                            }
                        }
                    } catch (err) {
                        _didIteratorError7 = true;
                        _iteratorError7 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion7 && _iterator7.return) {
                                _iterator7.return();
                            }
                        } finally {
                            if (_didIteratorError7) {
                                throw _iteratorError7;
                            }
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeQuestions",
        value: function initializeQuestions() {
            var jsonObj = require("./Mock/questions.json");
            this.questions = new Map();
            this.questionsBySubject = new Map();
            for (var subjectId in jsonObj) {
                var mappings = jsonObj[subjectId];
                if (this.subjects.has(subjectId)) {
                    var subject = this.subjects.get(subjectId);
                    if (!this.questionsBySubject.has(subjectId)) {
                        this.questionsBySubject.set(subjectId, []);
                    }
                    for (var questionId in mappings) {
                        var mapping = mappings[questionId];
                        var question = _Question.Question.fromJSON(this, questionId, mapping);
                        this.questions.set(questionId, question);
                        this.questionsBySubject.get(subjectId).push(question);
                    }
                }
            }
        }
    }, {
        key: "initializeMasteryLevels",
        value: function initializeMasteryLevels() {
            var jsonObj = require("./Mock/question_masteries.json");
            this.masteries = new Map();
            this.masteriesByQuestion = new Map();
            for (var questionId in jsonObj) {
                var mappings = jsonObj[questionId];
                if (this.questions.has(questionId)) {
                    var question = this.questions.get(questionId);
                    if (!this.masteriesByQuestion.has(questionId)) {
                        this.masteriesByQuestion.set(questionId, []);
                    }
                    for (var masteryId in mappings) {
                        var mapping = mappings[masteryId];
                        var _mastery = _Mastery.MasteryLevel.fromJSON(this, masteryId, mapping);
                        this.masteries.set(masteryId, _mastery);
                        this.masteriesByQuestion.get(questionId).push(_mastery);
                    }
                }
            }
        }
    }, {
        key: "initializeExams",
        value: function initializeExams() {
            var jsonObj = require("./Mock/exams.json");
            this.exams = new Map();
            this.examsByClassBySubject = new Map();
            for (var examId in jsonObj) {
                var mapping = jsonObj[examId];
                var clsId = mapping["Class"];
                var subjectId = mapping["Subject"];
                if (this.classes.has(clsId) && this.subjects.has(subjectId)) {
                    var cls = this.classes.get(clsId);
                    var subject = this.subjects.get(subjectId);
                    if (!this.examsByClassBySubject.has(clsId)) {
                        this.examsByClassBySubject.set(clsId, new Map());
                    }
                    if (!this.examsByClassBySubject.get(clsId).has(subjectId)) {
                        this.examsByClassBySubject.get(clsId).set(subjectId, []);
                    }
                    var exam = _Exam.Exam.fromJSON(this, examId, mapping);
                    this.exams.set(examId, exam);
                    this.examsByClassBySubject.get(clsId).get(subjectId).push(exam.Id);
                }
            }
        }
    }, {
        key: "initializeExamQuestions",
        value: function initializeExamQuestions() {
            var jsonObj = require("./Mock/exam_questions.json");
            this.examQuestions = new Map();
            for (var examId in jsonObj) {
                var questionIds = jsonObj[examId];
                if (this.exams.has(examId)) {
                    var exam = this.exams.get(examId);
                    if (!this.examQuestions.has(examId)) {
                        this.examQuestions.set(examId, []);
                    }
                    var _iteratorNormalCompletion8 = true;
                    var _didIteratorError8 = false;
                    var _iteratorError8 = undefined;

                    try {
                        for (var _iterator8 = questionIds[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                            var questionId = _step8.value;

                            if (this.questions.has(questionId)) {
                                this.examQuestions.get(examId).push(questionId);
                            }
                        }
                    } catch (err) {
                        _didIteratorError8 = true;
                        _iteratorError8 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion8 && _iterator8.return) {
                                _iterator8.return();
                            }
                        } finally {
                            if (_didIteratorError8) {
                                throw _iteratorError8;
                            }
                        }
                    }
                }
            }
        }
    }, {
        key: "initializeExamResults",
        value: function initializeExamResults() {
            var jsonObj = require("./Mock/exam_answers.json");
            this.examsByStudent = new Map();
            for (var examId in jsonObj) {
                var mappings = jsonObj[examId];
                if (!this.exams.has(examId)) continue;
                var exam = this.exams.get(examId);
                for (var studentId in mappings) {
                    if (!this.students.has(studentId)) continue;
                    var answers = mappings[studentId];
                    var student = this.students[studentId];
                    if (!this.examsByStudent.has(examId)) {
                        this.examsByStudent.set(examId, new Map());
                    }
                    if (!this.examsByStudent.get(examId).has(studentId)) {
                        this.examsByStudent.get(examId).set(studentId, new Map());
                    }
                    for (var questionId in answers) {
                        if (!this.questions.has(questionId)) continue;
                        var mapping = answers[questionId];
                        var masteryId = mapping["Mastery"];
                        var question = this.questions.get(questionId);
                        if (this.masteries.has(masteryId)) {
                            this.examsByStudent.get(examId).get(studentId).set(question, {
                                "Answer": mapping["Answer"],
                                "Mastery": this.masteries.get(masteryId)
                            });
                        }
                    }
                }
            }
        }
    }, {
        key: "getAcademicYears",
        value: function getAcademicYears() {
            return Array.from(this.academicYears.values());
        }
    }, {
        key: "getAcademicYear",
        value: function getAcademicYear(academicYearId) {
            return this.academicYears.get(academicYearId);
        }
    }, {
        key: "getSchools",
        value: function getSchools() {
            return this.schools.values();
        }
    }, {
        key: "getSchool",
        value: function getSchool(schoolId) {
            return this.schools.get(schoolId);
        }
    }, {
        key: "getClasses",
        value: function getClasses() {
            return this.classes.values();
        }
    }, {
        key: "getClass",
        value: function getClass(classId) {
            return this.classes.get(classId);
        }
    }, {
        key: "getTeachers",
        value: function getTeachers() {
            return this.teachers.values();
        }
    }, {
        key: "getTeacher",
        value: function getTeacher(teacherId) {
            return this.teachers.get(teacherId);
        }
    }, {
        key: "getStudents",
        value: function getStudents() {
            return this.students.values();
        }
    }, {
        key: "getStudent",
        value: function getStudent(studentId) {
            return this.students.get(studentId);
        }
    }, {
        key: "getSubjects",
        value: function getSubjects() {
            return this.subjects.values();
        }
    }, {
        key: "getSubject",
        value: function getSubject(subjectId) {
            return this.subjects.get(subjectId);
        }
    }, {
        key: "getQuestion",
        value: function getQuestion(questionId) {
            return this.questions.get(questionId);
        }
    }, {
        key: "getExams",
        value: function getExams() {
            return this.exams.values();
        }
    }, {
        key: "getExam",
        value: function getExam(examId) {
            return this.exams.get(examId);
        }

        // AcademicYear related

    }, {
        key: "getCurrentAcademicYear",
        value: function getCurrentAcademicYear() {
            // Get the currently running academic year
            var academicYears = Array.from(this.academicYears.values());
            // May be order them by start dates
            return academicYears[academicYears.length - 1];
        }
    }, {
        key: "getPreviousAcademicYear",
        value: function getPreviousAcademicYear(academicYear) {
            // Return an instance of AcademicYear object
            var academicYears = this.getAcademicYears();
            for (var index = 0; index < academicYears.length; index++) {
                if (academicYears[index].Id == academicYear.Id) {
                    return academicYears[index - 1];
                }
            }
        }
    }, {
        key: "getSchoolsForAcademicYear",
        value: function getSchoolsForAcademicYear(academicYear) {
            var schools = [];
            if (this.schoolsByAcademicYear.has(academicYear.Id)) {
                var schoolIds = this.schoolsByAcademicYear.get(academicYear.Id);
                for (var i = 0; i < schoolIds.length; i++) {
                    var schoolId = schoolIds[i];
                    schools.push(this.schools.get(schoolId));
                }
            }
            return schools;
        }
    }, {
        key: "addAcademicYear",
        value: function addAcademicYear(academicYear) {
            var newId = this.getUniqueId();
            academicYear.setId(newId);
            this.academicYears.set(newId, academicYear);
        }
    }, {
        key: "addSchool",
        value: function addSchool(school) {
            var newId = this.getUniqueId();
            school.setId(newId);
            this.schools.set(newId, school);
        }
    }, {
        key: "addSchoolToAcademicYear",
        value: function addSchoolToAcademicYear(academicYear, school) {
            if (this.academicYears.has(academicYear.Id)) {
                if (this.schools.has(school.Id)) {
                    if (!this.schoolsByAcademicYear.has(academicYear.Id)) {
                        this.schoolsByAcademicYear.set(academicYear.Id, []);
                    }
                    this.schoolsByAcademicYear.get(academicYear.Id).push(school.Id);
                }
            }
        }

        // School related methods

    }, {
        key: "getContactsForSchool",
        value: function getContactsForSchool(school) {
            return this.schoolContacts.get(school.Id);
        }
    }, {
        key: "addSchoolContact",
        value: function addSchoolContact(school, contact) {
            contact.setId(this.getUniqueId());
            if (!this.schoolContacts.has(school.Id)) {
                this.schoolContacts.set(school.Id, []);
            }
            this.schoolContacts.get(school.Id).push(contact);
        }
    }, {
        key: "getHolidays",
        value: function getHolidays(school) {
            return this.holidaysBySchool.get(school);
        }
    }, {
        key: "addHoliday",
        value: function addHoliday(school, date) {
            if (!this.holidaysBySchool.has(school)) {
                this.holidaysBySchool.set(school, []);
            }
            if (!this.holidaysBySchool.get(school).includes(date)) {
                this.holidaysBySchool.get(school).push(date);
            }
        }
    }, {
        key: "getAcademicYearsForSchool",
        value: function getAcademicYearsForSchool(school) {
            var academicYears = [];
            var _iteratorNormalCompletion9 = true;
            var _didIteratorError9 = false;
            var _iteratorError9 = undefined;

            try {
                for (var _iterator9 = this.schoolsByAcademicYear[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                    var _ref = _step9.value;

                    var _ref2 = _slicedToArray(_ref, 2);

                    var academicYearId = _ref2[0];
                    var schoolIds = _ref2[1];

                    if (schoolIds.includes(school.Id)) {
                        academicYears.push(this.getAcademicYear(academicYearId));
                    }
                }
            } catch (err) {
                _didIteratorError9 = true;
                _iteratorError9 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion9 && _iterator9.return) {
                        _iterator9.return();
                    }
                } finally {
                    if (_didIteratorError9) {
                        throw _iteratorError9;
                    }
                }
            }

            return academicYears;
        }
    }, {
        key: "getCurrentAcademicYearForSchool",
        value: function getCurrentAcademicYearForSchool(school) {
            // Get the currently running academic year
            var academicYears = this.getAcademicYearsForSchool(school);
            // May be order them by start dates
            return academicYears[academicYears.length - 1];
        }
    }, {
        key: "getClassesForAcademicYear",
        value: function getClassesForAcademicYear(academicYear, school) {
            // Get all classes for the given school in the given academic year
            var allClasses = [];
            if (this.schoolsByAcademicYear.has(academicYear.Id)) {
                var _iteratorNormalCompletion10 = true;
                var _didIteratorError10 = false;
                var _iteratorError10 = undefined;

                try {
                    for (var _iterator10 = this.schoolsByAcademicYear.get(academicYear.Id)[Symbol.iterator](), _step10; !(_iteratorNormalCompletion10 = (_step10 = _iterator10.next()).done); _iteratorNormalCompletion10 = true) {
                        var academicYearSchoolId = _step10.value;

                        if (school.Id == academicYearSchoolId) {
                            var _iteratorNormalCompletion11 = true;
                            var _didIteratorError11 = false;
                            var _iteratorError11 = undefined;

                            try {
                                for (var _iterator11 = this.classes[Symbol.iterator](), _step11; !(_iteratorNormalCompletion11 = (_step11 = _iterator11.next()).done); _iteratorNormalCompletion11 = true) {
                                    var _ref3 = _step11.value;

                                    var _ref4 = _slicedToArray(_ref3, 2);

                                    var classId = _ref4[0];
                                    var cls = _ref4[1];

                                    if (cls.AcademicYear == academicYear && cls.School == school) {
                                        allClasses.push(cls);
                                    }
                                }
                            } catch (err) {
                                _didIteratorError11 = true;
                                _iteratorError11 = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion11 && _iterator11.return) {
                                        _iterator11.return();
                                    }
                                } finally {
                                    if (_didIteratorError11) {
                                        throw _iteratorError11;
                                    }
                                }
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError10 = true;
                    _iteratorError10 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion10 && _iterator10.return) {
                            _iterator10.return();
                        }
                    } finally {
                        if (_didIteratorError10) {
                            throw _iteratorError10;
                        }
                    }
                }
            }
            return allClasses;
        }
    }, {
        key: "getTeachersForAcademicYear",
        value: function getTeachersForAcademicYear(academicYear, school) {
            // Get all teachers in this school for the given academic year
            var allTeachers = [];
            var _iteratorNormalCompletion12 = true;
            var _didIteratorError12 = false;
            var _iteratorError12 = undefined;

            try {
                for (var _iterator12 = this.teachersBySchool[Symbol.iterator](), _step12; !(_iteratorNormalCompletion12 = (_step12 = _iterator12.next()).done); _iteratorNormalCompletion12 = true) {
                    var _ref5 = _step12.value;

                    var _ref6 = _slicedToArray(_ref5, 2);

                    var schoolId = _ref6[0];
                    var mappings = _ref6[1];

                    if (school.Id != schoolId) continue;
                    var _iteratorNormalCompletion13 = true;
                    var _didIteratorError13 = false;
                    var _iteratorError13 = undefined;

                    try {
                        for (var _iterator13 = mappings[Symbol.iterator](), _step13; !(_iteratorNormalCompletion13 = (_step13 = _iterator13.next()).done); _iteratorNormalCompletion13 = true) {
                            var mapping = _step13.value;

                            var teacher = mapping["Teacher"];
                            var startDate = this.toDate(mapping["StartDate"]);
                            var endDate = this.toDate(mapping["EndDate"]);
                            if (endDate == null) {
                                if (startDate <= academicYear.EndDate) {
                                    allTeachers.push(teacher);
                                }
                            } else {
                                if (endDate > academicYear.StartDate) {
                                    allTeachers.push(teacher);
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError13 = true;
                        _iteratorError13 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion13 && _iterator13.return) {
                                _iterator13.return();
                            }
                        } finally {
                            if (_didIteratorError13) {
                                throw _iteratorError13;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError12 = true;
                _iteratorError12 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion12 && _iterator12.return) {
                        _iterator12.return();
                    }
                } finally {
                    if (_didIteratorError12) {
                        throw _iteratorError12;
                    }
                }
            }

            return allTeachers;
        }
    }, {
        key: "getStudentsForAcademicYear",
        value: function getStudentsForAcademicYear(academicYear, school) {
            var allStudents = [];
            var _iteratorNormalCompletion14 = true;
            var _didIteratorError14 = false;
            var _iteratorError14 = undefined;

            try {
                for (var _iterator14 = this.studentsBySchool[Symbol.iterator](), _step14; !(_iteratorNormalCompletion14 = (_step14 = _iterator14.next()).done); _iteratorNormalCompletion14 = true) {
                    var _ref7 = _step14.value;

                    var _ref8 = _slicedToArray(_ref7, 2);

                    var schoolId = _ref8[0];
                    var mappings = _ref8[1];

                    if (school.Id != schoolId) continue;
                    var _iteratorNormalCompletion15 = true;
                    var _didIteratorError15 = false;
                    var _iteratorError15 = undefined;

                    try {
                        for (var _iterator15 = mappings[Symbol.iterator](), _step15; !(_iteratorNormalCompletion15 = (_step15 = _iterator15.next()).done); _iteratorNormalCompletion15 = true) {
                            var mapping = _step15.value;

                            var student = mapping["Student"];
                            var startDate = this.toDate(mapping["StartDate"]);
                            var endDate = this.toDate(mapping["EndDate"]);
                            if (endDate == null) {
                                if (startDate <= academicYear.EndDate) {
                                    allStudents.push(student);
                                }
                            } else {
                                if (endDate > academicYear.StartDate) {
                                    allStudents.push(student);
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError15 = true;
                        _iteratorError15 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion15 && _iterator15.return) {
                                _iterator15.return();
                            }
                        } finally {
                            if (_didIteratorError15) {
                                throw _iteratorError15;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError14 = true;
                _iteratorError14 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion14 && _iterator14.return) {
                        _iterator14.return();
                    }
                } finally {
                    if (_didIteratorError14) {
                        throw _iteratorError14;
                    }
                }
            }

            return allStudents;
        }
    }, {
        key: "addTeacher",
        value: function addTeacher(teacher) {
            var newId = this.getUniqueId();
            teacher.setId(newId);
            this.teachers.set(newId, teacher.toJSON());
        }
    }, {
        key: "addTeacherToSchool",
        value: function addTeacherToSchool(school, teacher) {
            if (!this.teachersBySchool.has(school.Id)) {
                this.teachersBySchool.set(school.Id, []);
            }
            this.teachersBySchool.get(school.Id).push({
                "Teacher": teacher,
                "StartDate": new Date(),
                "EndDate": null });
        }
    }, {
        key: "removeTeacherFromSchool",
        value: function removeTeacherFromSchool(school, teacher, reason) {
            if (this.teachersBySchool.has(school.Id)) {
                var _iteratorNormalCompletion16 = true;
                var _didIteratorError16 = false;
                var _iteratorError16 = undefined;

                try {
                    for (var _iterator16 = this.teachersBySchool[school.Id][Symbol.iterator](), _step16; !(_iteratorNormalCompletion16 = (_step16 = _iterator16.next()).done); _iteratorNormalCompletion16 = true) {
                        var mapping = _step16.value;

                        if (mapping["Teacher"] == teacher) {
                            mapping["EndDate"] = new Date();
                        }
                    }
                } catch (err) {
                    _didIteratorError16 = true;
                    _iteratorError16 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion16 && _iterator16.return) {
                            _iterator16.return();
                        }
                    } finally {
                        if (_didIteratorError16) {
                            throw _iteratorError16;
                        }
                    }
                }

                var academicYear = this.getCurrentAcademicYearForSchool(school);
                var classes = this.getClassesForAcademicYear(academicYear, school);
                for (var cls in classes) {
                    this.removeTeacherFromClass(cls, teacher);
                    this.removeTeacherFromSubjects(cls, teacher);
                }
            }
        }
    }, {
        key: "removeTeacherFromClass",
        value: function removeTeacherFromClass(cls, teacher) {
            var _iteratorNormalCompletion17 = true;
            var _didIteratorError17 = false;
            var _iteratorError17 = undefined;

            try {
                for (var _iterator17 = this.teachersByClass.get(cls)[Symbol.iterator](), _step17; !(_iteratorNormalCompletion17 = (_step17 = _iterator17.next()).done); _iteratorNormalCompletion17 = true) {
                    var mapping = _step17.value;

                    if (mapping["Teacher"] == teacher) {
                        mapping["EndDate"] = new Date();
                    }
                }
            } catch (err) {
                _didIteratorError17 = true;
                _iteratorError17 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion17 && _iterator17.return) {
                        _iterator17.return();
                    }
                } finally {
                    if (_didIteratorError17) {
                        throw _iteratorError17;
                    }
                }
            }
        }
    }, {
        key: "removeTeacherFromSubjects",
        value: function removeTeacherFromSubjects(cls, teacher) {
            var _iteratorNormalCompletion18 = true;
            var _didIteratorError18 = false;
            var _iteratorError18 = undefined;

            try {
                for (var _iterator18 = this.teachersByClassBySubject.get(cls.Id)[Symbol.iterator](), _step18; !(_iteratorNormalCompletion18 = (_step18 = _iterator18.next()).done); _iteratorNormalCompletion18 = true) {
                    var _ref9 = _step18.value;

                    var _ref10 = _slicedToArray(_ref9, 2);

                    var subjectId = _ref10[0];
                    var mappings = _ref10[1];
                    var _iteratorNormalCompletion19 = true;
                    var _didIteratorError19 = false;
                    var _iteratorError19 = undefined;

                    try {
                        for (var _iterator19 = mappings[Symbol.iterator](), _step19; !(_iteratorNormalCompletion19 = (_step19 = _iterator19.next()).done); _iteratorNormalCompletion19 = true) {
                            var mapping = _step19.value;

                            if (mapping["Teacher"] == teacher) {
                                mapping["EndDate"] = new Date();
                            }
                        }
                    } catch (err) {
                        _didIteratorError19 = true;
                        _iteratorError19 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion19 && _iterator19.return) {
                                _iterator19.return();
                            }
                        } finally {
                            if (_didIteratorError19) {
                                throw _iteratorError19;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError18 = true;
                _iteratorError18 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion18 && _iterator18.return) {
                        _iterator18.return();
                    }
                } finally {
                    if (_didIteratorError18) {
                        throw _iteratorError18;
                    }
                }
            }
        }
    }, {
        key: "addClass",
        value: function addClass(cls) {
            var newId = this.getUniqueId();
            cls.setId(newId);
            this.classes.set(newId, cls);
        }
    }, {
        key: "getClassesForSchool",
        value: function getClassesForSchool(school) {
            var allClasses = [];
            var _iteratorNormalCompletion20 = true;
            var _didIteratorError20 = false;
            var _iteratorError20 = undefined;

            try {
                for (var _iterator20 = this.classes[Symbol.iterator](), _step20; !(_iteratorNormalCompletion20 = (_step20 = _iterator20.next()).done); _iteratorNormalCompletion20 = true) {
                    var _ref11 = _step20.value;

                    var _ref12 = _slicedToArray(_ref11, 2);

                    var clsId = _ref12[0];
                    var cls = _ref12[1];

                    if (cls.AcademicYear == school.AcademicYear && cls.School == school) {
                        allClasses.push(cls);
                    }
                }
            } catch (err) {
                _didIteratorError20 = true;
                _iteratorError20 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion20 && _iterator20.return) {
                        _iterator20.return();
                    }
                } finally {
                    if (_didIteratorError20) {
                        throw _iteratorError20;
                    }
                }
            }

            return allClasses;
        }
    }, {
        key: "addAdminTeacher",
        value: function addAdminTeacher(cls, teacher) {
            if (this.classes.has(cls.Id)) {
                if (!this.teachersByClass.has(cls.Id)) {
                    this.teachersByClass.set(cls.Id, []);
                }
                this.teachersByClass.get(cls.Id).push({
                    "Teacher": teacher,
                    "StartDate": new Date(),
                    "EndDate": null
                });
            }
        }
    }, {
        key: "getSchoolContacts",
        value: function getSchoolContacts(school) {
            return this.schoolContacts.get(school.Id) || [];
        }
    }, {
        key: "addStudent",
        value: function addStudent(student) {
            var studentId = this.getUniqueId();
            student.setId(studentId);
            this.students.set(studentId, student);
        }
    }, {
        key: "addStudentToSchool",
        value: function addStudentToSchool(school, student) {
            if (this.schools.has(school.Id)) {
                if (!this.studentsBySchool.has(school.Id)) {
                    this.studentsBySchool.set(school.Id, []);
                }
                this.studentsBySchool.get(school.Id).push({
                    "Student": student,
                    "StartDate": new Date(),
                    "EndDate": null
                });
            }
        }
    }, {
        key: "removeStudentFromSchool",
        value: function removeStudentFromSchool(school, student, reason) {
            if (this.schools.has(school.Id)) {
                var _iteratorNormalCompletion21 = true;
                var _didIteratorError21 = false;
                var _iteratorError21 = undefined;

                try {
                    for (var _iterator21 = this.studentsBySchool[Symbol.iterator](), _step21; !(_iteratorNormalCompletion21 = (_step21 = _iterator21.next()).done); _iteratorNormalCompletion21 = true) {
                        var _ref13 = _step21.value;

                        var _ref14 = _slicedToArray(_ref13, 2);

                        var schoolId = _ref14[0];
                        var mappings = _ref14[1];
                        var _iteratorNormalCompletion25 = true;
                        var _didIteratorError25 = false;
                        var _iteratorError25 = undefined;

                        try {
                            for (var _iterator25 = mappings[Symbol.iterator](), _step25; !(_iteratorNormalCompletion25 = (_step25 = _iterator25.next()).done); _iteratorNormalCompletion25 = true) {
                                var _mapping = _step25.value;

                                if (_mapping["Student"] == student.Id) {
                                    _mapping["EndDate"] = new Date();
                                }
                            }
                        } catch (err) {
                            _didIteratorError25 = true;
                            _iteratorError25 = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion25 && _iterator25.return) {
                                    _iterator25.return();
                                }
                            } finally {
                                if (_didIteratorError25) {
                                    throw _iteratorError25;
                                }
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError21 = true;
                    _iteratorError21 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion21 && _iterator21.return) {
                            _iterator21.return();
                        }
                    } finally {
                        if (_didIteratorError21) {
                            throw _iteratorError21;
                        }
                    }
                }

                for (var cls in this.getClassesForSchool(school)) {
                    var _iteratorNormalCompletion22 = true;
                    var _didIteratorError22 = false;
                    var _iteratorError22 = undefined;

                    try {
                        for (var _iterator22 = this.studentsByClass.get(cls.Id)[Symbol.iterator](), _step22; !(_iteratorNormalCompletion22 = (_step22 = _iterator22.next()).done); _iteratorNormalCompletion22 = true) {
                            var _mappings = _step22.value;
                            var _iteratorNormalCompletion24 = true;
                            var _didIteratorError24 = false;
                            var _iteratorError24 = undefined;

                            try {
                                for (var _iterator24 = _mappings[Symbol.iterator](), _step24; !(_iteratorNormalCompletion24 = (_step24 = _iterator24.next()).done); _iteratorNormalCompletion24 = true) {
                                    var mapping = _step24.value;

                                    if (mapping["Student"] == student) {
                                        mapping["EndDate"] = new Date();
                                    }
                                }
                            } catch (err) {
                                _didIteratorError24 = true;
                                _iteratorError24 = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion24 && _iterator24.return) {
                                        _iterator24.return();
                                    }
                                } finally {
                                    if (_didIteratorError24) {
                                        throw _iteratorError24;
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError22 = true;
                        _iteratorError22 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion22 && _iterator22.return) {
                                _iterator22.return();
                            }
                        } finally {
                            if (_didIteratorError22) {
                                throw _iteratorError22;
                            }
                        }
                    }

                    var _iteratorNormalCompletion23 = true;
                    var _didIteratorError23 = false;
                    var _iteratorError23 = undefined;

                    try {
                        for (var _iterator23 = this.subjectsByClassByStudent.get(cls.Id)[Symbol.iterator](), _step23; !(_iteratorNormalCompletion23 = (_step23 = _iterator23.next()).done); _iteratorNormalCompletion23 = true) {
                            var _ref15 = _step23.value;

                            var _ref16 = _slicedToArray(_ref15, 2);

                            var subjectId = _ref16[0];
                            var studentIds = _ref16[1];

                            var studentIndex = studentIds.indexOf(student.Id);
                            if (studentIndex >= 0) {
                                this.subjectsByClassByStudent.get(cls.Id).set(subjectId, studentIds.splice(studentIndex, 0));
                            }
                        }
                    } catch (err) {
                        _didIteratorError23 = true;
                        _iteratorError23 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion23 && _iterator23.return) {
                                _iterator23.return();
                            }
                        } finally {
                            if (_didIteratorError23) {
                                throw _iteratorError23;
                            }
                        }
                    }
                }
            }
        }

        // Class related methods

    }, {
        key: "updateClass",
        value: function updateClass(cls) {
            // Nothing to update
        }
    }, {
        key: "getTerms",
        value: function getTerms(cls) {
            return this.termsByClass.get(cls);
        }
    }, {
        key: "addTerm",
        value: function addTerm(cls, term) {
            if (!this.termsByClass.has(cls)) {
                this.termsByClass.set(cls, []);
            }
            this.termsByClass.get(cls).push(term);
        }
    }, {
        key: "getSubjectsForClass",
        value: function getSubjectsForClass(cls) {
            // Get subjects for the given class
            if (this.subjectsByClass.has(cls.Id)) {
                var subjects = [];
                var _iteratorNormalCompletion26 = true;
                var _didIteratorError26 = false;
                var _iteratorError26 = undefined;

                try {
                    for (var _iterator26 = this.subjectsByClass.get(cls.Id)[Symbol.iterator](), _step26; !(_iteratorNormalCompletion26 = (_step26 = _iterator26.next()).done); _iteratorNormalCompletion26 = true) {
                        var subjectId = _step26.value;

                        subjects.push(this.subjects.get(subjectId));
                    }
                } catch (err) {
                    _didIteratorError26 = true;
                    _iteratorError26 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion26 && _iterator26.return) {
                            _iterator26.return();
                        }
                    } finally {
                        if (_didIteratorError26) {
                            throw _iteratorError26;
                        }
                    }
                }

                return subjects;
            }
        }
    }, {
        key: "getTeachersBySubjectForClass",
        value: function getTeachersBySubjectForClass(cls) {
            // Get all teachers for the given class
            var subjectTeachers = new Map();
            if (this.teachersByClassBySubject.has(cls.Id)) {
                var _iteratorNormalCompletion27 = true;
                var _didIteratorError27 = false;
                var _iteratorError27 = undefined;

                try {
                    for (var _iterator27 = this.teachersByClassBySubject.get(cls.Id)[Symbol.iterator](), _step27; !(_iteratorNormalCompletion27 = (_step27 = _iterator27.next()).done); _iteratorNormalCompletion27 = true) {
                        var _ref17 = _step27.value;

                        var _ref18 = _slicedToArray(_ref17, 2);

                        var subjectId = _ref18[0];
                        var mappings = _ref18[1];

                        var subject = this.subjects.get(subjectId);
                        var _iteratorNormalCompletion28 = true;
                        var _didIteratorError28 = false;
                        var _iteratorError28 = undefined;

                        try {
                            for (var _iterator28 = mappings[Symbol.iterator](), _step28; !(_iteratorNormalCompletion28 = (_step28 = _iterator28.next()).done); _iteratorNormalCompletion28 = true) {
                                var mapping = _step28.value;

                                if (mapping["EndDate"] == null) {
                                    subjectTeachers.set(subject, mapping["Teacher"]);
                                }
                            }
                        } catch (err) {
                            _didIteratorError28 = true;
                            _iteratorError28 = err;
                        } finally {
                            try {
                                if (!_iteratorNormalCompletion28 && _iterator28.return) {
                                    _iterator28.return();
                                }
                            } finally {
                                if (_didIteratorError28) {
                                    throw _iteratorError28;
                                }
                            }
                        }
                    }
                } catch (err) {
                    _didIteratorError27 = true;
                    _iteratorError27 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion27 && _iterator27.return) {
                            _iterator27.return();
                        }
                    } finally {
                        if (_didIteratorError27) {
                            throw _iteratorError27;
                        }
                    }
                }
            }
            return subjectTeachers;
        }
    }, {
        key: "getStudentsFromClass",
        value: function getStudentsFromClass(cls) {
            // Get all students for the given class
            var allStudents = [];
            if (this.studentsByClass.has(cls.Id)) {
                var _iteratorNormalCompletion29 = true;
                var _didIteratorError29 = false;
                var _iteratorError29 = undefined;

                try {
                    for (var _iterator29 = this.studentsByClass.get(cls.Id)[Symbol.iterator](), _step29; !(_iteratorNormalCompletion29 = (_step29 = _iterator29.next()).done); _iteratorNormalCompletion29 = true) {
                        var mapping = _step29.value;

                        var endDate = mapping["EndDate"];
                        if (endDate == null || endDate <= cls.AcademicYear.EndDate) {
                            allStudents.push(mapping["Student"]);
                        }
                    }
                } catch (err) {
                    _didIteratorError29 = true;
                    _iteratorError29 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion29 && _iterator29.return) {
                            _iterator29.return();
                        }
                    } finally {
                        if (_didIteratorError29) {
                            throw _iteratorError29;
                        }
                    }
                }
            }
            return allStudents;
        }
    }, {
        key: "getAdminTeacher",
        value: function getAdminTeacher(cls) {
            if (this.teachersByClass.has(cls.Id)) {
                var _iteratorNormalCompletion30 = true;
                var _didIteratorError30 = false;
                var _iteratorError30 = undefined;

                try {
                    for (var _iterator30 = this.teachersByClass.get(cls.Id)[Symbol.iterator](), _step30; !(_iteratorNormalCompletion30 = (_step30 = _iterator30.next()).done); _iteratorNormalCompletion30 = true) {
                        var mapping = _step30.value;

                        if (mapping["EndDate"] == null) {
                            return mapping["Teacher"];
                        }
                    }
                } catch (err) {
                    _didIteratorError30 = true;
                    _iteratorError30 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion30 && _iterator30.return) {
                            _iterator30.return();
                        }
                    } finally {
                        if (_didIteratorError30) {
                            throw _iteratorError30;
                        }
                    }
                }
            }
        }
    }, {
        key: "getStudentsBySubject",
        value: function getStudentsBySubject(cls, subject) {
            if (this.subjectsByClassByStudent.has(cls.Id)) {
                if (this.subjectsByClassByStudent.get(cls.Id).has(subject.Id)) {
                    return this.subjectsByClassByStudent.get(cls.Id).get(subject.Id);
                }
            }
            return [];
        }
    }, {
        key: "addSubject",
        value: function addSubject(subject) {
            var newId = this.getUniqueId();
            subject.setId(newId);
            this.subjects.set(newId, subject);
        }
    }, {
        key: "updateSubject",
        value: function updateSubject(subject) {
            // Nothing to update here
        }
    }, {
        key: "removeSubject",
        value: function removeSubject(subject) {
            // lets get to this later as we need to check questions and exams
        }
    }, {
        key: "addSubjectToClass",
        value: function addSubjectToClass(cls, subject) {
            if (!this.subjectsByClass.has(cls.Id)) {
                this.subjectsByClass.set(cls.Id, []);
            }
            if (this.subjectsByClass.get(cls.Id).includes(subject.Id)) {
                throw "Subject is already part of the class";
            }
            this.subjectsByClass.get(cls.Id).push(subject.Id);
        }
    }, {
        key: "removeSubjectFromClass",
        value: function removeSubjectFromClass(cls, subject) {
            // Lets get to this later
        }
    }, {
        key: "addTeacherToSubject",
        value: function addTeacherToSubject(cls, teacher, subject) {
            if (!this.teachersByClassBySubject.has(cls.Id)) {
                this.teachersByClassBySubject.set(cls.Id, new Map());
            }
            if (!this.teachersByClassBySubject.get(cls.Id).has(subject.Id)) {
                this.teachersByClassBySubject.get(cls.Id).set(subject.Id, []);
            }
            var _iteratorNormalCompletion31 = true;
            var _didIteratorError31 = false;
            var _iteratorError31 = undefined;

            try {
                for (var _iterator31 = this.teachersByClassBySubject.get(cls.Id).get(subject.Id)[Symbol.iterator](), _step31; !(_iteratorNormalCompletion31 = (_step31 = _iterator31.next()).done); _iteratorNormalCompletion31 = true) {
                    var mapping = _step31.value;

                    if (mapping["EndDate"] == null) {
                        mapping["EndDate"] = new Date();
                    }
                }
            } catch (err) {
                _didIteratorError31 = true;
                _iteratorError31 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion31 && _iterator31.return) {
                        _iterator31.return();
                    }
                } finally {
                    if (_didIteratorError31) {
                        throw _iteratorError31;
                    }
                }
            }

            this.teachersByClassBySubject.get(cls.Id).get(subject.Id).push({
                "Teacher": teacher,
                "StartDate": new Date(),
                "EndDate": null
            });
        }
    }, {
        key: "removeTeacherForSubject",
        value: function removeTeacherForSubject(cls, teacher, subject) {
            if (!this.teachersByClassBySubject.has(cls.Id)) {
                throw "Unknown class";
            }
            if (!this.teachersByClassBySubject.get(cls.Id).has(subject.Id)) {
                throw "Subject is not part of the class";
            }
            var _iteratorNormalCompletion32 = true;
            var _didIteratorError32 = false;
            var _iteratorError32 = undefined;

            try {
                for (var _iterator32 = this.teachersByClassBySubject.get(cls.Id).get(subject.Id)[Symbol.iterator](), _step32; !(_iteratorNormalCompletion32 = (_step32 = _iterator32.next()).done); _iteratorNormalCompletion32 = true) {
                    var mapping = _step32.value;

                    if (mapping["EndDate"] == null) {
                        mapping["EndDate"] = new Date();
                    }
                }
            } catch (err) {
                _didIteratorError32 = true;
                _iteratorError32 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion32 && _iterator32.return) {
                        _iterator32.return();
                    }
                } finally {
                    if (_didIteratorError32) {
                        throw _iteratorError32;
                    }
                }
            }
        }
    }, {
        key: "addStudentToClass",
        value: function addStudentToClass(cls, student) {
            if (!this.studentsByClass.has(cls.Id)) {
                this.studentsByClass.set(cls.Id, []);
            }
            this.studentsByClass.get(cls.Id).push({
                "Student": student,
                "StartDate": new Date(),
                "EndDate": null
            });
        }
    }, {
        key: "removeStudentFromClass",
        value: function removeStudentFromClass(cls, student) {
            if (!this.studentsByClass.has(cls.Id)) {
                throw "Unknown class";
            }
            var _iteratorNormalCompletion33 = true;
            var _didIteratorError33 = false;
            var _iteratorError33 = undefined;

            try {
                for (var _iterator33 = this.studentsByClass.get(cls.Id)[Symbol.iterator](), _step33; !(_iteratorNormalCompletion33 = (_step33 = _iterator33.next()).done); _iteratorNormalCompletion33 = true) {
                    var mapping = _step33.value;

                    if (mapping["Student"] == student) {
                        mapping["EndDate"] = new Date();
                    }
                }
            } catch (err) {
                _didIteratorError33 = true;
                _iteratorError33 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion33 && _iterator33.return) {
                        _iterator33.return();
                    }
                } finally {
                    if (_didIteratorError33) {
                        throw _iteratorError33;
                    }
                }
            }
        }
    }, {
        key: "addStudentToSubject",
        value: function addStudentToSubject(cls, student, subject) {
            if (!this.subjectsByClassByStudent.has(cls.Id)) {
                this.subjectsByClassByStudent.set(cls.Id, new Map());
            }
            if (!this.subjectsByClassByStudent.get(cls.Id).has(subject.Id)) {
                this.subjectsByClassByStudent.get(cls.Id).set(subject.Id, []);
            }
            this.subjectsByClassByStudent.get(cls.Id).get(subject.Id).push({
                "Student": student,
                "StartDate": new Date(),
                "EndDate": null
            });
        }
    }, {
        key: "removeStudentFromSubject",
        value: function removeStudentFromSubject(cls, student, subject) {
            if (!this.subjectsByClassByStudent.has(cls.Id)) {
                throw "Unknown class";
            }
            if (!this.subjectsByClassByStudent.get(cls.Id).has(subject.Id)) {
                throw "Subject is not part of the class";
            }
            var _iteratorNormalCompletion34 = true;
            var _didIteratorError34 = false;
            var _iteratorError34 = undefined;

            try {
                for (var _iterator34 = this.subjectsByClassByStudent.get(cls.Id).get(subject.Id)[Symbol.iterator](), _step34; !(_iteratorNormalCompletion34 = (_step34 = _iterator34.next()).done); _iteratorNormalCompletion34 = true) {
                    var mapping = _step34.value;

                    if (mapping["Student"] == student) {
                        mapping["EndDate"] = new Date();
                    }
                }
            } catch (err) {
                _didIteratorError34 = true;
                _iteratorError34 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion34 && _iterator34.return) {
                        _iterator34.return();
                    }
                } finally {
                    if (_didIteratorError34) {
                        throw _iteratorError34;
                    }
                }
            }
        }

        // Student related methods

    }, {
        key: "getStudentContacts",
        value: function getStudentContacts(student) {
            return this.studentContacts.get(student.Id);
        }
    }, {
        key: "addStudentContact",
        value: function addStudentContact(student, contact) {
            contact.setId(this.getUniqueId());
            if (!this.studentContacts.has(student.Id)) {
                this.studentContacts.set(student.Id, []);
            }
            this.studentContacts.get(student.Id).push(contact);
        }
    }, {
        key: "getSchoolForStudent",
        value: function getSchoolForStudent(academicYear, student) {
            var _iteratorNormalCompletion35 = true;
            var _didIteratorError35 = false;
            var _iteratorError35 = undefined;

            try {
                for (var _iterator35 = this.studentsBySchool[Symbol.iterator](), _step35; !(_iteratorNormalCompletion35 = (_step35 = _iterator35.next()).done); _iteratorNormalCompletion35 = true) {
                    var _ref19 = _step35.value;

                    var _ref20 = _slicedToArray(_ref19, 2);

                    var schoolId = _ref20[0];
                    var mappings = _ref20[1];
                    var _iteratorNormalCompletion36 = true;
                    var _didIteratorError36 = false;
                    var _iteratorError36 = undefined;

                    try {
                        for (var _iterator36 = mappings[Symbol.iterator](), _step36; !(_iteratorNormalCompletion36 = (_step36 = _iterator36.next()).done); _iteratorNormalCompletion36 = true) {
                            var mapping = _step36.value;

                            if (mapping["Student"] == student) {
                                var endDate = this.toDate(mapping["EndDate"]);
                                if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                    return this.schools.get(schoolId);
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError36 = true;
                        _iteratorError36 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion36 && _iterator36.return) {
                                _iterator36.return();
                            }
                        } finally {
                            if (_didIteratorError36) {
                                throw _iteratorError36;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError35 = true;
                _iteratorError35 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion35 && _iterator35.return) {
                        _iterator35.return();
                    }
                } finally {
                    if (_didIteratorError35) {
                        throw _iteratorError35;
                    }
                }
            }
        }
    }, {
        key: "getSchoolsForStudent",
        value: function getSchoolsForStudent(student) {
            var allSchools = [];
            var _iteratorNormalCompletion37 = true;
            var _didIteratorError37 = false;
            var _iteratorError37 = undefined;

            try {
                for (var _iterator37 = this.studentsBySchool[Symbol.iterator](), _step37; !(_iteratorNormalCompletion37 = (_step37 = _iterator37.next()).done); _iteratorNormalCompletion37 = true) {
                    var _ref21 = _step37.value;

                    var _ref22 = _slicedToArray(_ref21, 2);

                    var schoolId = _ref22[0];
                    var mappings = _ref22[1];
                    var _iteratorNormalCompletion38 = true;
                    var _didIteratorError38 = false;
                    var _iteratorError38 = undefined;

                    try {
                        for (var _iterator38 = mappings[Symbol.iterator](), _step38; !(_iteratorNormalCompletion38 = (_step38 = _iterator38.next()).done); _iteratorNormalCompletion38 = true) {
                            var mapping = _step38.value;

                            if (mapping["Student"] == student) {
                                allSchools.push(this.schools.get(schoolId));
                            }
                        }
                    } catch (err) {
                        _didIteratorError38 = true;
                        _iteratorError38 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion38 && _iterator38.return) {
                                _iterator38.return();
                            }
                        } finally {
                            if (_didIteratorError38) {
                                throw _iteratorError38;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError37 = true;
                _iteratorError37 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion37 && _iterator37.return) {
                        _iterator37.return();
                    }
                } finally {
                    if (_didIteratorError37) {
                        throw _iteratorError37;
                    }
                }
            }

            return allSchools;
        }
    }, {
        key: "getClassForStudent",
        value: function getClassForStudent(academicYear, student) {
            var _iteratorNormalCompletion39 = true;
            var _didIteratorError39 = false;
            var _iteratorError39 = undefined;

            try {
                for (var _iterator39 = this.studentsByClass[Symbol.iterator](), _step39; !(_iteratorNormalCompletion39 = (_step39 = _iterator39.next()).done); _iteratorNormalCompletion39 = true) {
                    var _ref23 = _step39.value;

                    var _ref24 = _slicedToArray(_ref23, 2);

                    var clsId = _ref24[0];
                    var mappings = _ref24[1];
                    var _iteratorNormalCompletion40 = true;
                    var _didIteratorError40 = false;
                    var _iteratorError40 = undefined;

                    try {
                        for (var _iterator40 = mappings[Symbol.iterator](), _step40; !(_iteratorNormalCompletion40 = (_step40 = _iterator40.next()).done); _iteratorNormalCompletion40 = true) {
                            var mapping = _step40.value;

                            if (mapping["Student"] == student) {
                                var endDate = this.toDate(mapping["EndDate"]);
                                if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                    return this.classes.get(clsId);
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError40 = true;
                        _iteratorError40 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion40 && _iterator40.return) {
                                _iterator40.return();
                            }
                        } finally {
                            if (_didIteratorError40) {
                                throw _iteratorError40;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError39 = true;
                _iteratorError39 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion39 && _iterator39.return) {
                        _iterator39.return();
                    }
                } finally {
                    if (_didIteratorError39) {
                        throw _iteratorError39;
                    }
                }
            }
        }
    }, {
        key: "getClassesForStudent",
        value: function getClassesForStudent(school, student) {
            var allClasses = [];
            var _iteratorNormalCompletion41 = true;
            var _didIteratorError41 = false;
            var _iteratorError41 = undefined;

            try {
                for (var _iterator41 = this.studentsByClass[Symbol.iterator](), _step41; !(_iteratorNormalCompletion41 = (_step41 = _iterator41.next()).done); _iteratorNormalCompletion41 = true) {
                    var _ref25 = _step41.value;

                    var _ref26 = _slicedToArray(_ref25, 2);

                    var clsId = _ref26[0];
                    var mappings = _ref26[1];
                    var _iteratorNormalCompletion42 = true;
                    var _didIteratorError42 = false;
                    var _iteratorError42 = undefined;

                    try {
                        for (var _iterator42 = mappings[Symbol.iterator](), _step42; !(_iteratorNormalCompletion42 = (_step42 = _iterator42.next()).done); _iteratorNormalCompletion42 = true) {
                            var mapping = _step42.value;

                            if (mapping["Student"] == student) {
                                allClasses.push(this.classes.get(clsId));
                            }
                        }
                    } catch (err) {
                        _didIteratorError42 = true;
                        _iteratorError42 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion42 && _iterator42.return) {
                                _iterator42.return();
                            }
                        } finally {
                            if (_didIteratorError42) {
                                throw _iteratorError42;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError41 = true;
                _iteratorError41 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion41 && _iterator41.return) {
                        _iterator41.return();
                    }
                } finally {
                    if (_didIteratorError41) {
                        throw _iteratorError41;
                    }
                }
            }

            return allClasses;
        }
    }, {
        key: "getSubjectsForStudent",
        value: function getSubjectsForStudent(cls, student) {
            //  Get Subjects from the class for the given student
            var allSubjects = [];
            if (this.subjectsByClassByStudent.has(cls.Id)) {
                for (var _ref27 in this.subjectsByClassByStudent.get(cls.Id)) {
                    var _ref28 = _slicedToArray(_ref27, 2);

                    var subjectId = _ref28[0];
                    var mappings = _ref28[1];
                    var _iteratorNormalCompletion43 = true;
                    var _didIteratorError43 = false;
                    var _iteratorError43 = undefined;

                    try {
                        for (var _iterator43 = mappings[Symbol.iterator](), _step43; !(_iteratorNormalCompletion43 = (_step43 = _iterator43.next()).done); _iteratorNormalCompletion43 = true) {
                            var mapping = _step43.value;

                            if (mapping["Student"] == student && mapping["EndDate"] == null) {
                                allSubjects.push(this.subjects.get(subjectId));
                            }
                        }
                    } catch (err) {
                        _didIteratorError43 = true;
                        _iteratorError43 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion43 && _iterator43.return) {
                                _iterator43.return();
                            }
                        } finally {
                            if (_didIteratorError43) {
                                throw _iteratorError43;
                            }
                        }
                    }
                }
            }
            return allSubjects;
        }
    }, {
        key: "getExamResultsForStudent",
        value: function getExamResultsForStudent(cls, subject, student) {
            // Return all exams for the given student in the given academic year for the given subject
            var examResults = {};
            for (var exam in this.getExamsForSubject(cls, subject)) {
                examResults[exam] = this.getExamResults(student, exam);
            }
            return examResults;
        }

        // Teacher related methods

    }, {
        key: "getTeacherContacts",
        value: function getTeacherContacts(teacher) {
            return this.teacherContacts.get(teacher);
        }
    }, {
        key: "addTeacherContact",
        value: function addTeacherContact(teacher, contact) {
            contact.setId(this.getUniqueId());
            if (!this.teacherContacts.has(teacher.Id)) {
                this.teacherContacts.set(teacher.Id, []);
            }
            this.teacherContacts.get(teacher.Id).push(contact);
        }
    }, {
        key: "getSchoolForTeacher",
        value: function getSchoolForTeacher(academicYear, teacher) {
            var _iteratorNormalCompletion44 = true;
            var _didIteratorError44 = false;
            var _iteratorError44 = undefined;

            try {
                for (var _iterator44 = this.teachersBySchool[Symbol.iterator](), _step44; !(_iteratorNormalCompletion44 = (_step44 = _iterator44.next()).done); _iteratorNormalCompletion44 = true) {
                    var _ref29 = _step44.value;

                    var _ref30 = _slicedToArray(_ref29, 2);

                    var schoolId = _ref30[0];
                    var mappings = _ref30[1];
                    var _iteratorNormalCompletion45 = true;
                    var _didIteratorError45 = false;
                    var _iteratorError45 = undefined;

                    try {
                        for (var _iterator45 = mappings[Symbol.iterator](), _step45; !(_iteratorNormalCompletion45 = (_step45 = _iterator45.next()).done); _iteratorNormalCompletion45 = true) {
                            var mapping = _step45.value;

                            if (mapping["Teacher"] == teacher) {
                                var endDate = this.toDate(mapping["EndDate"]);
                                if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                    return this.schools.get(schoolId);
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError45 = true;
                        _iteratorError45 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion45 && _iterator45.return) {
                                _iterator45.return();
                            }
                        } finally {
                            if (_didIteratorError45) {
                                throw _iteratorError45;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError44 = true;
                _iteratorError44 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion44 && _iterator44.return) {
                        _iterator44.return();
                    }
                } finally {
                    if (_didIteratorError44) {
                        throw _iteratorError44;
                    }
                }
            }
        }
    }, {
        key: "getSchoolsForTeacher",
        value: function getSchoolsForTeacher(teacher) {
            var allSchools = [];
            var _iteratorNormalCompletion46 = true;
            var _didIteratorError46 = false;
            var _iteratorError46 = undefined;

            try {
                for (var _iterator46 = this.teachersBySchool[Symbol.iterator](), _step46; !(_iteratorNormalCompletion46 = (_step46 = _iterator46.next()).done); _iteratorNormalCompletion46 = true) {
                    var _ref31 = _step46.value;

                    var _ref32 = _slicedToArray(_ref31, 2);

                    var schoolId = _ref32[0];
                    var mappings = _ref32[1];
                    var _iteratorNormalCompletion47 = true;
                    var _didIteratorError47 = false;
                    var _iteratorError47 = undefined;

                    try {
                        for (var _iterator47 = mappings[Symbol.iterator](), _step47; !(_iteratorNormalCompletion47 = (_step47 = _iterator47.next()).done); _iteratorNormalCompletion47 = true) {
                            var mapping = _step47.value;

                            if (mapping["Teacher"] == teacher) {
                                allSchools.push(this.schools.get(schoolId));
                            }
                        }
                    } catch (err) {
                        _didIteratorError47 = true;
                        _iteratorError47 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion47 && _iterator47.return) {
                                _iterator47.return();
                            }
                        } finally {
                            if (_didIteratorError47) {
                                throw _iteratorError47;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError46 = true;
                _iteratorError46 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion46 && _iterator46.return) {
                        _iterator46.return();
                    }
                } finally {
                    if (_didIteratorError46) {
                        throw _iteratorError46;
                    }
                }
            }

            return allSchools;
        }
    }, {
        key: "getClassesForTeacher",
        value: function getClassesForTeacher(academicYear, school, teacher) {
            var allClasses = [];
            var _iteratorNormalCompletion48 = true;
            var _didIteratorError48 = false;
            var _iteratorError48 = undefined;

            try {
                for (var _iterator48 = this.teachersByClass[Symbol.iterator](), _step48; !(_iteratorNormalCompletion48 = (_step48 = _iterator48.next()).done); _iteratorNormalCompletion48 = true) {
                    var _ref33 = _step48.value;

                    var _ref34 = _slicedToArray(_ref33, 2);

                    var clsId = _ref34[0];
                    var mappings = _ref34[1];
                    var _iteratorNormalCompletion49 = true;
                    var _didIteratorError49 = false;
                    var _iteratorError49 = undefined;

                    try {
                        for (var _iterator49 = mappings[Symbol.iterator](), _step49; !(_iteratorNormalCompletion49 = (_step49 = _iterator49.next()).done); _iteratorNormalCompletion49 = true) {
                            var mapping = _step49.value;

                            var endDate = mapping["EndDate"];
                            if (endDate == null || endDate >= academicYear.StartDate && endDate <= academicYear.EndDate) {
                                allClasses.push(this.classes.get(clsId));
                            }
                        }
                    } catch (err) {
                        _didIteratorError49 = true;
                        _iteratorError49 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion49 && _iterator49.return) {
                                _iterator49.return();
                            }
                        } finally {
                            if (_didIteratorError49) {
                                throw _iteratorError49;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError48 = true;
                _iteratorError48 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion48 && _iterator48.return) {
                        _iterator48.return();
                    }
                } finally {
                    if (_didIteratorError48) {
                        throw _iteratorError48;
                    }
                }
            }

            return allClasses;
        }
    }, {
        key: "getSubjectsForTeacher",
        value: function getSubjectsForTeacher(teacher) {
            var allSubjects = {};
            var _iteratorNormalCompletion50 = true;
            var _didIteratorError50 = false;
            var _iteratorError50 = undefined;

            try {
                for (var _iterator50 = this.teachersByClassBySubject[Symbol.iterator](), _step50; !(_iteratorNormalCompletion50 = (_step50 = _iterator50.next()).done); _iteratorNormalCompletion50 = true) {
                    var _ref35 = _step50.value;

                    var _ref36 = _slicedToArray(_ref35, 2);

                    var clsId = _ref36[0];
                    var subjects = _ref36[1];

                    var cls = this.classes.get(clsId);
                    var _iteratorNormalCompletion51 = true;
                    var _didIteratorError51 = false;
                    var _iteratorError51 = undefined;

                    try {
                        for (var _iterator51 = subjects[Symbol.iterator](), _step51; !(_iteratorNormalCompletion51 = (_step51 = _iterator51.next()).done); _iteratorNormalCompletion51 = true) {
                            var _ref37 = _step51.value;

                            var _ref38 = _slicedToArray(_ref37, 2);

                            var subjectId = _ref38[0];
                            var mappings = _ref38[1];

                            var subject = this.subjects.get(subjectId);
                            var _iteratorNormalCompletion52 = true;
                            var _didIteratorError52 = false;
                            var _iteratorError52 = undefined;

                            try {
                                for (var _iterator52 = mappings[Symbol.iterator](), _step52; !(_iteratorNormalCompletion52 = (_step52 = _iterator52.next()).done); _iteratorNormalCompletion52 = true) {
                                    var mapping = _step52.value;

                                    if (mapping["Teacher"] == teacher && mapping["EndDate"] == null) {
                                        if (!allSubjects.has(cls)) {
                                            allSubjects.set(cls, []);
                                        }
                                        allSubjects[cls].push(subject);
                                    }
                                }
                            } catch (err) {
                                _didIteratorError52 = true;
                                _iteratorError52 = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion52 && _iterator52.return) {
                                        _iterator52.return();
                                    }
                                } finally {
                                    if (_didIteratorError52) {
                                        throw _iteratorError52;
                                    }
                                }
                            }
                        }
                    } catch (err) {
                        _didIteratorError51 = true;
                        _iteratorError51 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion51 && _iterator51.return) {
                                _iterator51.return();
                            }
                        } finally {
                            if (_didIteratorError51) {
                                throw _iteratorError51;
                            }
                        }
                    }
                }
            } catch (err) {
                _didIteratorError50 = true;
                _iteratorError50 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion50 && _iterator50.return) {
                        _iterator50.return();
                    }
                } finally {
                    if (_didIteratorError50) {
                        throw _iteratorError50;
                    }
                }
            }

            return allSubjects;
        }

        // Subject Related

    }, {
        key: "getQuestions",
        value: function getQuestions(subject) {
            // Return a list of Question objects
            if (this.questionsBySubject.has(subject.Id)) {
                return this.questionsBySubject.get(subject.Id);
            }
        }
    }, {
        key: "addQuestion",
        value: function addQuestion(question, subject) {
            var questionId = this.getUniqueId();
            question.setId(questionId);
            if (!this.questionsBySubject.has(subject.Id)) {
                this.questionsBySubject.set(subject.Id, []);
            }
            this.questionsBySubject.get(subject.Id).push(questionId);
        }
    }, {
        key: "removeQuestion",
        value: function removeQuestion(question, subject) {
            if (this.questions.has(question.Id)) {
                this.questions.delete(question.Id);
            }
            if (this.questionsBySubject.has(subject.Id)) {
                var subjectQuestionIds = this.questionsBySubject.get(subject.Id);
                var questionIndex = subjectQuestionIds.indexOf(question.Id);
                if (questionIndex >= 0) {
                    subjectQuestionIds.splice(questionIndex, 1);
                }
            }
        }
    }, {
        key: "updateQuestion",
        value: function updateQuestion(question, subject) {}
    }, {
        key: "addExam",
        value: function addExam(exam) {
            var examId = this.getUniqueId();
            exam.setId(examId);
            this.exams[examId] = exam;
            if (!this.examsByClassBySubject.has(exam.Class.Id)) {
                this.examsByClassBySubject.set(exam.Class.Id, new Map());
            }
            if (!this.examsByClassBySubject.get(exam.Class.Id).has(exam.Subject.Id)) {
                this.examsByClassBySubject.get(exam.Class.Id).set(exam.Subject.Id, []);
            }
            this.examsByClassBySubject.get(exam.Class.Id).get(exam.Subject.Id).push(exam.Id);
        }
    }, {
        key: "removeExam",
        value: function removeExam(exam) {
            // lets get to this later
        }
    }, {
        key: "updateExam",
        value: function updateExam(exam) {}
    }, {
        key: "getExamsForSubject",
        value: function getExamsForSubject(cls, subject) {
            var exams = [];
            if (this.examsByClassBySubject.has(cls.Id)) {
                if (this.examsByClassBySubject.get(cls.Id).has(subject.Id)) {
                    var _iteratorNormalCompletion53 = true;
                    var _didIteratorError53 = false;
                    var _iteratorError53 = undefined;

                    try {
                        for (var _iterator53 = this.examsByClassBySubject.get(cls.Id).get(subject.Id)[Symbol.iterator](), _step53; !(_iteratorNormalCompletion53 = (_step53 = _iterator53.next()).done); _iteratorNormalCompletion53 = true) {
                            var examId = _step53.value;

                            exams.push(this.exams.get(examId));
                        }
                    } catch (err) {
                        _didIteratorError53 = true;
                        _iteratorError53 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion53 && _iterator53.return) {
                                _iterator53.return();
                            }
                        } finally {
                            if (_didIteratorError53) {
                                throw _iteratorError53;
                            }
                        }
                    }
                }
            }
            return exams;
        }

        // Question Related

    }, {
        key: "addMasteryLevel",
        value: function addMasteryLevel(masteryLevel, question) {
            if (!this.masteriesByQuestion.has(question.Id)) {
                this.masteriesByQuestion.set(question.Id, []);
            }
            var masteryId = this.getUniqueId();
            masteryLevel.setId(masteryId);
            this.masteriesByQuestion.get(question.Id).push(masteryLevel.Id);
        }
    }, {
        key: "removeMasteryLevel",
        value: function removeMasteryLevel(masteryLevel, question) {
            if (this.masteries.has(masteryLevel.Id)) {
                this.masteries.delete(mastery.Id);
            }
            if (this.masteriesByQuestion.has(question.Id)) {
                var allMasteryIds = this.masteriesByQuestion.get(question.Id);
                var masteryIndex = allMasteryIds.indexOf(masteryLevel.Id);
                if (masteryIndex >= 0) {
                    allMasteryIds.splice(masteryIndex, 1);
                }
            }
        }
    }, {
        key: "getMasteryLevel",
        value: function getMasteryLevel(masteryId) {
            return this.masteries.get(masteryId);
        }
    }, {
        key: "getMasteryLevels",
        value: function getMasteryLevels(question) {
            return this.masteriesByQuestion.get(question.Id);
        }

        // Mastery related

    }, {
        key: "updateMasteryLevel",
        value: function updateMasteryLevel(mastery) {}

        // Exam related

    }, {
        key: "addQuestionToExam",
        value: function addQuestionToExam(exam, question) {
            if (!this.examQuestions.has(exam.Id)) {
                this.examQuestions.set(exam.Id, []);
            }
            this.examQuestions.get(exam.Id).push(question.Id);
        }
    }, {
        key: "removeQuestionFromExam",
        value: function removeQuestionFromExam(exam, question) {
            if (this.examQuestions.has(exam.Id)) {
                var questionIndex = this.examQuestions.get(exam.Id.Id).indexOf(question);
                if (questionIndex >= 0) {
                    this.examQuestions.get(exam.Id).splice(questionIndex, 1);
                }
            }
        }
    }, {
        key: "getExamQuestions",
        value: function getExamQuestions(exam) {
            var questions = [];
            var _iteratorNormalCompletion54 = true;
            var _didIteratorError54 = false;
            var _iteratorError54 = undefined;

            try {
                for (var _iterator54 = this.examQuestions.get(exam.Id)[Symbol.iterator](), _step54; !(_iteratorNormalCompletion54 = (_step54 = _iterator54.next()).done); _iteratorNormalCompletion54 = true) {
                    var questionId = _step54.value;

                    questions.push(this.questions.get(questionId));
                }
            } catch (err) {
                _didIteratorError54 = true;
                _iteratorError54 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion54 && _iterator54.return) {
                        _iterator54.return();
                    }
                } finally {
                    if (_didIteratorError54) {
                        throw _iteratorError54;
                    }
                }
            }

            return questions;
        }
    }, {
        key: "getExamResults",
        value: function getExamResults(exam, student) {
            if (this.examsByStudent.has(exam.Id)) {
                if (this.examsByStudent.get(exam.Id).has(student.Id)) {
                    return this.examsByStudent.get(exam.Id).get(student.Id);
                }
            }
        }
    }, {
        key: "addExamAnswer",
        value: function addExamAnswer(exam, student, question, answer, mastery) {
            if (!this.examsByStudent.has(exam.Id)) {
                this.examsByStudent.set(exam.Id, new Map());
            }
            if (!this.examsByStudent.get(exam.Id).has(student.Id)) {
                this.examsByStudent.get(exam.Id).set(student.Id, new Map());
            }
            this.examsByStudent.get(exam.Id).get(student.Id).set(question, {
                "Answer": answer,
                "Mastery": mastery
            });
        }
    }, {
        key: "updateExamAnswer",
        value: function updateExamAnswer(exam, student, question, answer, mastery) {
            if (this.examsByStudent.has(exam.Id)) {
                if (this.examsByStudent.get(exam.Id).has(student.Id)) {
                    this.examsByStudent.get(exam.Id).get(student.Id).set(question, {
                        "Answer": answer,
                        "Mastery": mastery.Id
                    });
                }
            }
        }
    }]);

    return MockDataSource;
}();
//# sourceMappingURL=MockDataSource.js.map