"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SQLDataSource = function () {
    function SQLDataSource() {
        _classCallCheck(this, SQLDataSource);
    }

    // Academic Year related methods

    _createClass(SQLDataSource, [{
        key: "getAcademicYears",
        value: function getAcademicYears() {
            // Get all academic years
        }
    }, {
        key: "getCurrentAcademicYear",
        value: function getCurrentAcademicYear() {
            // Get the currently running academic year
        }
    }, {
        key: "getPrevAcademicYear",
        value: function getPrevAcademicYear(acadYear) {
            // Return an instance of AcademicYear object
        }
    }, {
        key: "getSchools",
        value: function getSchools(acadYear) {}
    }, {
        key: "addAcademicYear",
        value: function addAcademicYear(acadYear) {}
        // add to t4i.academicyears


        // School related methos


    }, {
        key: "getClassesForAcademicYear",
        value: function getClassesForAcademicYear(acadYear, school) {
            // Get all classes for the given school in the given academic year
        }
    }, {
        key: "getTeachersForAcademicYear",
        value: function getTeachersForAcademicYear(acadYear, school) {
            // Get all teachers in this school for the given academic year
        }
    }, {
        key: "addSchool",
        value: function addSchool(school) {
            // add to t4i.schools
        }
    }, {
        key: "addClassToSchool",
        value: function addClassToSchool(acadYear, school, cls) {
            // add to t4i.school_classes
        }
    }, {
        key: "addTeacher",
        value: function addTeacher(teacher) {
            // add to t4i.teachers
        }
    }, {
        key: "addTeacherToSchool",
        value: function addTeacherToSchool(school, teacher) {
            // add to t4i.school_teachers
        }
    }, {
        key: "removeTeacherFromSchool",
        value: function removeTeacherFromSchool(acadYear, school, teacher) {}
        // update t4i.school_teachers
        // update t4i.class_teachers
        // update t4i.class_subject_teachers


        // Class related methods

    }, {
        key: "addClass",
        value: function addClass(cls) {
            // add to t4i.classes
        }
    }, {
        key: "updateClass",
        value: function updateClass(cls) {
            // update t4i.classes
        }
    }, {
        key: "getSubjectsForClass",
        value: function getSubjectsForClass(cls) {
            // Get subjects for the given class
        }
    }, {
        key: "getTeachersForClass",
        value: function getTeachersForClass(cls) {
            // Get all teachers for the given class
        }
    }, {
        key: "getStudentsFromClass",
        value: function getStudentsFromClass(cls) {
            // Get all students for the given class
        }
    }, {
        key: "getAdminTeacher",
        value: function getAdminTeacher(cls) {
            // Get admin teacher for class
        }
    }, {
        key: "getStudentsBySubject",
        value: function getStudentsBySubject(cls, students, subject) {
            studentIds = []; // Get this fro DB
            studentsBySubject = [];
            for (i = 0; i < students.length; i++) {
                student = students[i];
                if (studentIds.indexOf(student.Id) != -1) {
                    studentsBySubject.push(student);
                }
            }
            return studentsBySubject;
        }
    }, {
        key: "addAdminTeacher",
        value: function addAdminTeacher(acadYear, school, cls, teacher) {
            // add to t4i.class_teachers
        }
    }, {
        key: "addSubject",
        value: function addSubject(subject) {
            // add to t4i.subjects
        }
    }, {
        key: "updateSubject",
        value: function updateSubject(subject) {
            // Update t4i.subjects
        }
    }, {
        key: "removeSubject",
        value: function removeSubject(subject) {
            // Remove from t4i.subjects
        }
    }, {
        key: "addSubjectToClass",
        value: function addSubjectToClass(cls, subject, isMandatory) {
            // add to t4i.class_subjects
        }
    }, {
        key: "removeSubjectFromClass",
        value: function removeSubjectFromClass(cls, subject) {
            // Remove from t4i.class_subjects
            // If no other class is using it then remove from t4i.subjects
        }
    }, {
        key: "addTeacherToSubject",
        value: function addTeacherToSubject(cls, teacher, subject) {
            // add to t4i.class_subject_teachers
        }
    }, {
        key: "removeTeacherForSubject",
        value: function removeTeacherForSubject(cls, teacher, subject) {
            // remove from t4i.class_subject_teachers
        }
    }, {
        key: "addStudentToClass",
        value: function addStudentToClass(cls, student) {
            // add to t4i.class_student_subjects
        }
    }, {
        key: "removeStudentFromClass",
        value: function removeStudentFromClass(cls, student) {
            // update t4i.class_students
        }
    }, {
        key: "addStudentToSubject",
        value: function addStudentToSubject(cls, student, subject) {
            // add to t4i.class_student_subjects
        }
    }, {
        key: "removeStudentFromSubject",
        value: function removeStudentFromSubject(cls, student, subject) {}
        // remove from t4i.class_student_subjects


        // Student related methods

    }, {
        key: "addStudent",
        value: function addStudent(student) {
            // add to t4i.students
        }
    }, {
        key: "addContactsToStudent",
        value: function addContactsToStudent(student, contacts) {
            // add to t4i.student_contacts
        }
    }, {
        key: "getSchoolForStudent",
        value: function getSchoolForStudent(acadYear, student) {
            // Get the current school for the given student
        }
    }, {
        key: "getClassForStudent",
        value: function getClassForStudent(acadYear, school, student) {
            // Get the current class for the student
        }
    }, {
        key: "getSubjectsForStudent",
        value: function getSubjectsForStudent(acadYear, school, cls, student) {
            //  Get Subjects from the class for the given student
        }
    }, {
        key: "getExamsForStudent",
        value: function getExamsForStudent(acadYear, cls, student) {
            // Return all exams for the given student in the given academic year
        }
    }, {
        key: "getSubjectExamsForStudent",
        value: function getSubjectExamsForStudent(acadYear, cls, student, subject) {}
        // Return all exams for the given student in the given academic year for the given subject


        // Contact related methods

    }, {
        key: "getContacts",
        value: function getContacts(entity, contact) {
            // Read from t4i.contacts
        }
    }, {
        key: "addContact",
        value: function addContact(entity, contact) {
            // Add to t4i.contacts
        }
    }, {
        key: "updateContact",
        value: function updateContact(entity, contact) {}
        // Update t4i.contacts


        // Exam related methods

    }, {
        key: "addQuestion",
        value: function addQuestion(question) {
            // Add to t4i.questions
        }
    }, {
        key: "updateQuestion",
        value: function updateQuestion(question) {
            // Update t4i.questions
        }
    }, {
        key: "removeQuestion",
        value: function removeQuestion(question) {
            // Remove from t4i.questions
        }
    }, {
        key: "getQuestions",
        value: function getQuestions(subject) {
            // Return a list of Question objects
        }
    }, {
        key: "addMastery",
        value: function addMastery(mastery) {
            // Add to t4i.question_mastery
        }
    }, {
        key: "removeMastery",
        value: function removeMastery(mastery) {
            // Remove from t4i.question_mastery
        }
    }, {
        key: "updateMastery",
        value: function updateMastery(mastery) {
            // Update t4i.questions_mastery
        }
    }, {
        key: "getMastery",
        value: function getMastery(question) {
            // Return a list of Mastery objects from t4i.questions_mastery
        }
    }, {
        key: "addExam",
        value: function addExam(cls, exam) {
            // Add to t4i.exams
        }
    }, {
        key: "removeExam",
        value: function removeExam(exam) {
            // Remove from t4i.exams
        }
    }, {
        key: "updateExam",
        value: function updateExam(exam) {
            // Update t4i.exams
        }
    }, {
        key: "getExamsForSubject",
        value: function getExamsForSubject(cls, subject) {
            //
        }
    }, {
        key: "addQuestionToExam",
        value: function addQuestionToExam(exam, question) {
            // Add to t4i.exam_questions
        }
    }, {
        key: "removeQuestionFromExam",
        value: function removeQuestionFromExam(exam, question) {
            // Remove from t4i.exam_questions
        }
    }, {
        key: "getExamQuestions",
        value: function getExamQuestions(exam) {
            // Read from t4i.exam_questions
        }
    }, {
        key: "getExamResults",
        value: function getExamResults(student, exam) {
            // Return an instance of ExamResult object
        }
    }, {
        key: "addExamAnswer",
        value: function addExamAnswer(exam, student, question, answer, mastery) {
            // Add to t4i.exam_results
        }
    }, {
        key: "updateExamAnswer",
        value: function updateExamAnswer(exam, student, question, answer, mastery) {
            // Update t4i.exam_results
        }
    }]);

    return SQLDataSource;
}();
//# sourceMappingURL=SQLDataSource.js.map