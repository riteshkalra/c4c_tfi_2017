"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Term = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _School = require("./School.js");

var _Address = require("./Address.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Term = exports.Term = function (_Entity) {
    _inherits(Term, _Entity);

    function Term(dataSource, name, startDate, endDate, cls) {
        var id = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

        _classCallCheck(this, Term);

        var _this = _possibleConstructorReturn(this, (Term.__proto__ || Object.getPrototypeOf(Term)).call(this, dataSource, id));

        _this.name = name;
        _this.startDate = startDate;
        _this.endDate = endDate;
        _this.cls = cls;
        return _this;
    }

    _createClass(Term, [{
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "StartDate": this.startDate.toString(),
                "EndDate": this.endDate.toString()
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "StartDate",
        get: function get() {
            return this.startDate;
        }
    }, {
        key: "EndDate",
        get: function get() {
            return this.endDate;
        }
    }, {
        key: "Class",
        get: function get() {
            return this.cls;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new Term(dataSource, json["Name"], new Date(json["StartDate"]), new Date(json["EndDate"]), id);
        }
    }]);

    return Term;
}(_Entity2.Entity);
//# sourceMappingURL=Term.js.map