"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Teacher = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Contact = require("./Contact.js");

var _Address = require("./Address.js");

var _Image = require("./Image.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Teacher = exports.Teacher = function (_Entity) {
    _inherits(Teacher, _Entity);

    function Teacher(dataSource, firstName, lastName, empId, gender, dob, address, image, phone1, phone2, email) {
        var id = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : null;

        _classCallCheck(this, Teacher);

        var _this = _possibleConstructorReturn(this, (Teacher.__proto__ || Object.getPrototypeOf(Teacher)).call(this, dataSource, id));

        _this.firstName = firstName;
        _this.lastName = lastName;
        _this.empId = empId;
        _this.gender = gender;
        _this.dob = dob;
        _this.image = image;
        _this.address = address;
        _this.phone1 = phone1;
        _this.phone2 = phone2;
        _this.email = email;
        _this.contacts = null;
        _this.acadYear = null;
        _this.classes = null;
        _this.subjects = null;
        return _this;
    }

    _createClass(Teacher, [{
        key: "addContact",
        value: function addContact(firstName, lastName, relationship, phone, mobile, fax, email) {
            var contact = new _Contact.Contact(firstName, lastName, relationship, phone, mobile, fax, email);
            this.dataSource.addContact(this, contact);
            this.contacts = null;
        }
    }, {
        key: "updateContact",
        value: function updateContact(contact) {
            this.dataSource.updateContact(this, contact);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "FirstName": this.firstName,
                "LastName": this.lastName,
                "EmpID": this.empId,
                "Gender": this.gender,
                "DOB": this.dob.toString(),
                "Address": this.address != null ? this.address.toJSON() : null,
                "Image": this.image != null ? this.image.toJSON() : null,
                "Phone1": this.phone1,
                "Phone2": this.phone2,
                "Email": this.email
            };
        }
    }, {
        key: "FirstName",
        get: function get() {
            return this.firstName;
        }
    }, {
        key: "LastName",
        get: function get() {
            return this.lastName;
        }
    }, {
        key: "Name",
        get: function get() {
            return this.FirstName + " " + this.LastName;
        }
    }, {
        key: "EmployeeID",
        get: function get() {
            return this.empId;
        }
    }, {
        key: "Gender",
        get: function get() {
            return this.gender;
        }
    }, {
        key: "DOB",
        get: function get() {
            return this.dob;
        }
    }, {
        key: "Image",
        get: function get() {
            return this.image;
        }
    }, {
        key: "Phone1",
        get: function get() {
            return this.phone1;
        }
    }, {
        key: "Phone2",
        get: function get() {
            return this.phone2;
        }
    }, {
        key: "Email",
        get: function get() {
            return this.email;
        }
    }, {
        key: "Address",
        get: function get() {
            return this.address;
        }
    }, {
        key: "Contact",
        get: function get() {
            if (this.contacts == null) {
                this.contacts = this.dataSource.getTeacherContacts(this);
            }
            return this.contacts;
        }
    }, {
        key: "School",
        get: function get() {
            if (this.school == null) {
                this.school = this.dataSource.getSchoolForTeacher(this);
            }
            return this.school;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            return this.School.AcademicYear;
        }
    }, {
        key: "Classes",
        get: function get() {
            // Classes where this teacher is the admin
            if (this.cls == null) {
                this.cls = this.dataSource.getClassesForTeacher(this.AcademicYear, this.School, this);
            }
            return this.cls;
        }
    }, {
        key: "SubjectsByClasses",
        get: function get() {
            // All subjects handled by this teacher in various classes
            if (this.subjects == null) {
                this.subjects = this.dataSource.getSubjectsForTeacher(this.AcademicYear, this.School, this.Classes, this);
            }
            return this.subjects;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new Teacher(dataSource, json["FirstName"], json["LastName"], json["EmpID"], json["Gender"], new Date(json["DOB"]), _Address.Address.fromJSON(json["Address"]), _Image.Image.fromJSON(json["Image"]), json["Phone1"], json["Phone2"], json["Email"], id);
        }
    }]);

    return Teacher;
}(_Entity2.Entity);
//# sourceMappingURL=Teacher.js.map