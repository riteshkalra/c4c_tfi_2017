"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Student = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Contact = require("./Contact.js");

var _Address = require("./Address");

var _Image = require("./Image");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Student = exports.Student = function (_Entity) {
    _inherits(Student, _Entity);

    function Student(dataSource, firstName, lastName, rollNo, gender, dob, address, image, phone1, phone2, email) {
        var id = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : null;

        _classCallCheck(this, Student);

        var _this = _possibleConstructorReturn(this, (Student.__proto__ || Object.getPrototypeOf(Student)).call(this, dataSource, id));

        _this.firstName = firstName;
        _this.lastName = lastName;
        _this.rollNo = rollNo;
        _this.gender = gender;
        _this.dob = dob;
        _this.image = image;
        _this.address = address;
        _this.phone1 = phone1;
        _this.phone2 = phone2;
        _this.email = email;
        _this.contacts = null;
        _this.school = null;
        _this.cls = null;
        _this.subjects = null;
        _this.exams = {};
        return _this;
    }

    _createClass(Student, [{
        key: "getExamResultsForSubject",
        value: function getExamResultsForSubject(subject) {
            if (!(subject.Name in this.exams)) {
                this.exams[subject] = this.dataSource.getExamResultsForStudent(this.Class, subject, this);
            }
            return this.exams[subject];
        }
    }, {
        key: "addContact",
        value: function addContact(firstName, lastName, relationship, phone, mobile, fax, email) {
            var contact = new _Contact.Contact(firstName, lastName, relationship, phone, mobile, fax, email);
            this.dataSource.addStudentContact(this, contact);
            this.contacts = null;
        }
    }, {
        key: "updateContact",
        value: function updateContact(contact) {
            this.dataSource.updateContact(this, contact);
        }
    }, {
        key: "getSchools",
        value: function getSchools() {
            return this.getSchoolsForStudent(this);
        }
    }, {
        key: "getClassesFromSchool",
        value: function getClassesFromSchool(school) {
            return this.getClassesForStudent(school, this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "FirstName": this.firstName,
                "LastName": this.lastName,
                "RollNo": this.rollNo,
                "Gender": this.gender,
                "DOB": this.dob.toString(),
                "Address": this.Address != null ? this.address.toJSON() : null,
                "Image": this.Image != null ? this.image.toJSON() : null,
                "Phone1": this.phone1,
                "Phone2": this.phone2,
                "Email": this.email
            };
        }
    }, {
        key: "FirstName",
        get: function get() {
            return this.firstName;
        }
    }, {
        key: "LastName",
        get: function get() {
            return this.lastName;
        }
    }, {
        key: "Name",
        get: function get() {
            return this.firstName + " " + this.lastName;
        }
    }, {
        key: "RollNumber",
        get: function get() {
            return this.rollNo;
        }
    }, {
        key: "Gender",
        get: function get() {
            return this.gender;
        }
    }, {
        key: "DOB",
        get: function get() {
            return this.dob;
        }
    }, {
        key: "Image",
        get: function get() {
            return this.image;
        }
    }, {
        key: "Address",
        get: function get() {
            return this.address;
        }
    }, {
        key: "Phone1",
        get: function get() {
            return this.phone1;
        }
    }, {
        key: "Phone2",
        get: function get() {
            return this.phone2;
        }
    }, {
        key: "Email",
        get: function get() {
            return this.email;
        }
    }, {
        key: "Contacts",
        get: function get() {
            if (this.contacts == null) {
                this.contacts = this.dataSource.getStudentContacts(this);
            }
            return this.contacts;
        }
    }, {
        key: "School",
        get: function get() {
            // Current school for this student
            if (this.school == null) {
                this.school = this.dataSource.getSchoolForStudent(this);
            }
            return this.school;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            return this.School.AcademicYear;
        }
    }, {
        key: "Class",
        get: function get() {
            // Current class for this student
            if (this.cls == null) {
                this.cls = this.dataSource.getClassForStudent(this.AcademicYear, this.School, this);
            }
            return this.cls;
        }
    }, {
        key: "Subjects",
        get: function get() {
            // Subjects from the current class
            if (this.subjects == null) {
                this.subjects = this.dataSource.getSubjectsForStudent(this.Class, this);
            }
            return this.subjects;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new Student(dataSource, json["FirstName"], json["LastName"], json["EmpID"], json["Gender"], new Date(json["DOB"]), _Address.Address.fromJSON(json["Address"]), _Image.Image.fromJSON(json["Image"]), json["Phone1"], json["Phone2"], json["Email"], id);
        }
    }]);

    return Student;
}(_Entity2.Entity);
//# sourceMappingURL=Student.js.map