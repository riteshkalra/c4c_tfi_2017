"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Class = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Student = require("./Student.js");

var _Subject = require("./Subject.js");

var _Teacher = require("./Teacher.js");

var _Exam = require("./Exam.js");

var _Term = require("./Term.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Class = exports.Class = function (_Entity) {
    _inherits(Class, _Entity);

    function Class(dataSource, name, grade, section, code, teachingMedium, academicYear, school) {
        var id = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : null;

        _classCallCheck(this, Class);

        var _this = _possibleConstructorReturn(this, (Class.__proto__ || Object.getPrototypeOf(Class)).call(this, dataSource, id));

        _this.name = name;
        _this.grade = grade;
        _this.section = section;
        _this.code = code;
        _this.teachingMedium = teachingMedium;
        _this.school = school;
        _this.academicYear = academicYear;
        _this.subjects = null;
        _this.adminTeacher = null;
        _this.teachers = null;
        _this.students = null;
        _this.studentsByOptionalSubject = null;
        _this.examsBySubject = null;
        return _this;
    }

    _createClass(Class, [{
        key: "addTerm",
        value: function addTerm(name, startDate, endDate) {
            var term = new _Term.Term(name, startDate, endDate);
            this.dataSource.addTerm(cls, term);
            this.terms = null;
        }
    }, {
        key: "addAdminTeacher",
        value: function addAdminTeacher(teacher) {
            this.dataSource.addAdminTeacher(this, teacher);
            this.adminTeacher = teacher;
        }
    }, {
        key: "addSubject",
        value: function addSubject(name, category, isMandatory) {
            var subject = new _Subject.Subject(this.dataSource, name, category, isMandatory);
            this.dataSource.addSubject(subject);
            this.addExistingSubject(subject);
        }
    }, {
        key: "addExistingSubject",
        value: function addExistingSubject(subject) {
            this.dataSource.addSubjectToClass(this, subject);
            this.subjects = null;
        }
    }, {
        key: "removeSubject",
        value: function removeSubject(subject) {
            this.dataSource.removeSubjectFromClass(this, subject);
            this.subjects = null;
        }
    }, {
        key: "addTeacherForSubject",
        value: function addTeacherForSubject(teacher, subject) {
            this.dataSource.addTeacherToSubject(this, teacher, subject);
            this.teachers = null;
        }
    }, {
        key: "removeTeacherForSubject",
        value: function removeTeacherForSubject(teacher, subject) {
            this.dataSource.removeTeacherForSubject(this, teacher, subject);
            this.teachers = null;
        }
    }, {
        key: "addStudent",
        value: function addStudent(firstName, lastName, rollNo, gender, dob, image, building, street, city, districy, state, country, postal, phone1, phone2, email) {
            // Add a new student to this school in a class
            var address = new Address(building, street, city, districy, state, country, postal);
            var student = new _Student.Student(this.dataSource, firstName, lastName, rollNo, gender, dob, image, address, phone1, phone2, email);
            this.dataSource.addStudent(student);
            return this.addExistingStudent(student);
        }
    }, {
        key: "addExistingStudent",
        value: function addExistingStudent(student) {
            this.dataSource.addStudentToClass(this, student);
            this.students = null;
            return student;
        }
    }, {
        key: "removeStudent",
        value: function removeStudent(student) {
            // Student leaves the school, so update the end date
            this.dataSource.removeStudentFromClass(this, student);
            this.students = null;
        }
    }, {
        key: "addStudentToSubject",
        value: function addStudentToSubject(student, subject) {
            if (!subject.IsMandatory) {
                this.dataSource.addStudentToSubject(this, subject, student);
                this.studentsByOptionalSubject = null;
            }
        }
    }, {
        key: "removeStudentFromSubject",
        value: function removeStudentFromSubject(student, subject) {
            if (!subject.IsMandatory) {
                this.dataSource.removeStudentFromSubject(this, subject, student);
                this.studentsByOptionalSubject = null;
            }
        }
    }, {
        key: "getStudentsBySubject",
        value: function getStudentsBySubject(subject) {
            if (!subject.IsMandatory) {
                return this.StudentsByOptionalSubjects[subject];
            } else {
                return this.Students;
            }
        }
    }, {
        key: "addExam",
        value: function addExam(name, date, passMarks, passGrades, subject) {
            var exam = new _Exam.Exam(name, date, passMarks, passGrades, this, subject);
            this.dataSource.addExam(this, exam);
        }
    }, {
        key: "removeExam",
        value: function removeExam(exam) {
            this.dataSource.removeExam(exam);
        }
    }, {
        key: "getExamsForSubject",
        value: function getExamsForSubject(subject) {
            return this.dataSource.getExamsForSubject(this, subject);
        }
    }, {
        key: "getExamResults",
        value: function getExamResults(student, exam) {
            return this.dataSource.getExamResults(student, exam);
        }
    }, {
        key: "update",
        value: function update() {
            this.dataSource.updateClass(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Grade": this.grade,
                "Section": this.section,
                "Code": this.code,
                "TeachingMedium": this.teachingMedium,
                "AcademicYear": this.academicYear.Id,
                "School": this.school.Id
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Grade",
        get: function get() {
            return this.grade;
        }
    }, {
        key: "Section",
        get: function get() {
            return this.section;
        }
    }, {
        key: "Code",
        get: function get() {
            return this.code;
        }
    }, {
        key: "TeachingMedium",
        get: function get() {
            return this.teachingMedium;
        }
    }, {
        key: "School",
        get: function get() {
            return this.school;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            return this.academicYear;
        }
    }, {
        key: "Terms",
        get: function get() {
            if (this.terms == null) {
                this.terms = this.dataSource.getTerms(this);
            }
            return this.terms;
        }
    }, {
        key: "Subjects",
        get: function get() {
            if (this.subjects == null) {
                this.subjects = this.dataSource.getSubjectsForClass(this);
            }
            return this.subjects;
        }
    }, {
        key: "TeachersBySubject",
        get: function get() {
            if (this.teachers == null) {
                this.teachers = this.dataSource.getTeachersBySubjectForClass(this);
            }
            return this.teachers;
        }
    }, {
        key: "Students",
        get: function get() {
            if (this.students == null) {
                this.students = this.dataSource.getStudentsFromClass(this);
            }
            return this.students;
        }
    }, {
        key: "AdminTeacher",
        get: function get() {
            if (this.adminTeacher == null) {
                this.adminTeacher = this.dataSource.getAdminTeacher(this);
            }
            return this.adminTeacher;
        }
    }, {
        key: "StudentsByOptionalSubjects",
        get: function get() {
            if (this.studentsByOptionalSubject == null) {
                this.studentsByOptionalSubject = new Map();
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = this.Subjects[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var subject = _step.value;

                        if (subject.IsMandatory) continue;
                        this.studentsByOptionalSubject.set(subject, this.dataSource.getStudentsBySubject(this, subject));
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }
            return this.studentsByOptionalSubject;
        }
    }, {
        key: "ExamsBySubject",
        get: function get() {
            if (this.examsBySubject == null) {
                this.examsBySubject = new Map();
                var _iteratorNormalCompletion2 = true;
                var _didIteratorError2 = false;
                var _iteratorError2 = undefined;

                try {
                    for (var _iterator2 = this.Subjects[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                        var subject = _step2.value;

                        this.examsBySubject.set(subject, this.dataSource.getExamsForSubject(this, subject));
                    }
                } catch (err) {
                    _didIteratorError2 = true;
                    _iteratorError2 = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion2 && _iterator2.return) {
                            _iterator2.return();
                        }
                    } finally {
                        if (_didIteratorError2) {
                            throw _iteratorError2;
                        }
                    }
                }
            }
            return this.examsBySubject;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new Class(dataSource, json["Name"], json["Grade"], json["Section"], json["Code"], json["TeachingMedium"], dataSource.getAcademicYear(json["AcademicYear"]), dataSource.getSchool(json["School"]), id);
        }
    }]);

    return Class;
}(_Entity2.Entity);
//# sourceMappingURL=Class.js.map