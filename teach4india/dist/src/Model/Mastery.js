"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MasteryLevel = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MasteryLevel = exports.MasteryLevel = function (_Entity) {
    _inherits(MasteryLevel, _Entity);

    function MasteryLevel(dataSource, question, mastery, mark, grade) {
        var id = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

        _classCallCheck(this, MasteryLevel);

        var _this = _possibleConstructorReturn(this, (MasteryLevel.__proto__ || Object.getPrototypeOf(MasteryLevel)).call(this, dataSource, id));

        _this.question = question;
        _this.mastery = mastery;
        _this.mark = mark;
        _this.grade = grade;
        return _this;
    }

    _createClass(MasteryLevel, [{
        key: "update",
        value: function update() {
            this.dataSource.updateMasteryLevel(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Question": this.Question.Id,
                "Mastery": this.mastery,
                "Mark": this.Mark,
                "Grade": this.Grade
            };
        }
    }, {
        key: "Question",
        get: function get() {
            return this.question;
        }
    }, {
        key: "Mastery",
        get: function get() {
            return this.mastery;
        }
    }, {
        key: "Mark",
        get: function get() {
            return this.mark;
        }
    }, {
        key: "Grade",
        get: function get() {
            return this.grade;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new MasteryLevel(dataSource, dataSource.getQuestion(json["Question"]), json["Mastery"], json["Mark"], json["Grade"], id);
        }
    }]);

    return MasteryLevel;
}(_Entity2.Entity);
//# sourceMappingURL=Mastery.js.map