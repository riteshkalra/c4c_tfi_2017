"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Question = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Mastery = require("./Mastery.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Question = exports.Question = function (_Entity) {
    _inherits(Question, _Entity);

    function Question(dataSource, description, concept, qtype, difficultyLevel, options, answer, subject) {
        var id = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : null;

        _classCallCheck(this, Question);

        var _this = _possibleConstructorReturn(this, (Question.__proto__ || Object.getPrototypeOf(Question)).call(this, dataSource, id));

        _this.description = description;
        _this.concept = concept;
        _this.qtype = qtype;
        _this.difficultyLevel = difficultyLevel;
        _this.options = options;
        _this.answer = answer;
        _this.subject = subject;
        _this.masteryLevels = null;
        return _this;
    }

    _createClass(Question, [{
        key: "addMasteryLevel",
        value: function addMasteryLevel(mastery, mark, grade) {
            var masteryLevel = new _Mastery.MasteryLevel(this.dataSource, this, mastery, mark, grade);
            this.dataSource.addMasteryLevel(masteryLevel);
            this.masteryLevels = null;
        }
    }, {
        key: "removeMasteryLevel",
        value: function removeMasteryLevel(masteryLevel) {
            this.dataSource.removeMasteryLevel(masteryLevel);
            this.masteryLevels = null;
        }
    }, {
        key: "update",
        value: function update() {
            this.dataSource.updateQuestion(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Description": this.description,
                "Concept": this.concept,
                "Type": this.qtype,
                "DifficultyLevel": this.difficultyLevel,
                "Options": this.options,
                "Answer": this.answer,
                "Subject": this.subject.Id
            };
        }
    }, {
        key: "Description",
        get: function get() {
            return this.description;
        }
    }, {
        key: "Concept",
        get: function get() {
            return this.concept;
        }
    }, {
        key: "Type",
        get: function get() {
            return this.qtype;
        }
    }, {
        key: "DifficultyLevel",
        get: function get() {
            return this.difficultyLevel;
        }
    }, {
        key: "Options",
        get: function get() {
            return this.options;
        }
    }, {
        key: "Answer",
        get: function get() {
            return this.answer;
        }
    }, {
        key: "Subject",
        get: function get() {
            return this.subject;
        }
    }, {
        key: "MasteryLevels",
        get: function get() {
            if (this.masteryLevels == null) {
                this.masteryLevels = this.dataSource.getMasteryLevels(this);
            }
            return this.masteryLevels;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new Question(dataSource, json["Description"], json["Concept"], json["Type"], json["DifficultyLevel"], json["Options"], json["Answer"], dataSource.getSubject(json["Subject"]), id);
        }
    }]);

    return Question;
}(_Entity2.Entity);
//# sourceMappingURL=Question.js.map