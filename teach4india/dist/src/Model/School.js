"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.School = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Address = require("./Address.js");

var _Image = require("./Image.js");

var _Teacher = require("./Teacher.js");

var _Student = require("./Student.js");

var _Class = require("./Class.js");

var _Contact = require("./Contact.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var School = exports.School = function (_Entity) {
    _inherits(School, _Entity);

    function School(dataSource, name, registrationData, address, image, phone1, phone2, fax, email) {
        var id = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : null;

        _classCallCheck(this, School);

        var _this = _possibleConstructorReturn(this, (School.__proto__ || Object.getPrototypeOf(School)).call(this, dataSource, id));

        _this.name = name;
        _this.registrationData = registrationData;
        _this.address = address;
        _this.image = image;
        _this.phone1 = phone1;
        _this.phone2 = phone2;
        _this.fax = fax;
        _this.email = email;
        _this.contacts = null;
        _this.academicYear = null;
        _this.classes = null;
        _this.teachers = null;
        _this.students = null;
        return _this;
    }

    _createClass(School, [{
        key: "addTeacher",
        value: function addTeacher(firstName, lastName, empId, gender, dob, image, building, street, city, district, state, country, postal, phone1, phone2, email) {
            // Add a new teacher to this school
            var address = new _Address.Address(building, street, city, district, state, country, postal);
            var teacher = new _Teacher.Teacher(this.dataSource, firstName, lastName, empId, gender, dob, address, image, phone1, phone2, email);
            this.dataSource.addTeacher(teacher);
            this.dataSource.addTeacherToSchool(this, teacher);
            this.teachers = null;
            return teacher;
        }
    }, {
        key: "removeTeacher",
        value: function removeTeacher(teacher, reason) {
            this.dataSource.removeTeacherFromSchool(this, teacher, reason);
            this.teachers = null;
        }
    }, {
        key: "addStudent",
        value: function addStudent(firstName, lastName, rollNo, gender, dob, image, building, street, city, district, state, country, postal, phone1, phone2, email) {
            var address = new _Address.Address(building, street, city, district, state, country, postal);
            var student = new _Student.Student(this.dataSource, firstName, lastName, rollNo, gender, dob, address, image, phone1, phone2, email);
            this.dataSource.addStudent(student);
            this.dataSource.addStudentToSchool(this, student);
            this.students = null;
            return student;
        }
    }, {
        key: "removeStudent",
        value: function removeStudent(student, reason) {
            this.dataSource.removeStudentFromSchool(this, student, reason);
            this.students = null;
        }
    }, {
        key: "addClass",
        value: function addClass(name, grade, section, code, teachingMedium) {
            var academicYear = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

            if (academicYear == null) {
                academicYear = this.AcademicYear;
            }
            var cls = new _Class.Class(this.dataSource, name, grade, section, code, teachingMedium, academicYear, this);
            this.dataSource.addClass(cls);
            this.classes = null;
            return cls;
        }
    }, {
        key: "getPrevClass",
        value: function getPrevClass(cls) {
            for (var i = 0; i < this.Classes.length; i++) {
                var curCls = this.Classes[i];
                if (curCls.Grade == cls.Grade - 1 && curCls.Section == cls.Section) {
                    return curCls;
                }
            }
            return null;
        }
    }, {
        key: "getPreviousAcademicYear",
        value: function getPreviousAcademicYear(academicYear) {
            return this.dataSource.getPreviousAcademicYear(academicYear);
        }
    }, {
        key: "getClassesFromPrevAcademicYear",
        value: function getClassesFromPrevAcademicYear(academicYear) {
            var prevAcademicYear = this.getPreviousAcademicYear(academicYear);
            return this.dataSource.getClassesForAcademicYear(prevAcademicYear, this);
        }
    }, {
        key: "addAcademicYear",
        value: function addAcademicYear(academicYear) {
            this.dataSource.addSchoolToAcademicYear(academicYear, this);
            var prevClasses = this.getClassesFromPrevAcademicYear(academicYear);
            // Clone classes from previous academic year
            if (prevClasses != null) {
                for (var i = 0; i < prevClasses.length; i++) {
                    var cls = prevClasses[i];
                    // Add a new class for this year
                    var newCls = this.addClass(cls.name, cls.grade, cls.section, cls.code, cls.teachingMedium, academicYear, this);
                    // Map the same admin teacher from last year
                    this.dataSource.addAdminTeacher(newCls, cls.AdminTeacher);
                    // Copy all subjects from the old class
                    for (var j = 0; j < cls.Subjects.length; j++) {
                        var subject = cls.Subjects[j];
                        newCls.addExistingSubject(subject);
                        newCls.addTeacherForSubject(cls.TeachersBySubject.get(subject), subject);
                    }
                    // Students have to be promoted individually
                }
            }
        }
    }, {
        key: "setAcademicYear",
        value: function setAcademicYear(academicYear) {
            this.academicYear = academicYear;
            this.classes = null;
            this.teachers = null;
            this.students = null;
        }
    }, {
        key: "addContact",
        value: function addContact(firstName, lastName, relationship, phone, mobile, fax, email) {
            var contact = new _Contact.Contact(this.dataSource, firstName, lastName, relationship, phone, mobile, fax, email, "School", this.Id);
            this.dataSource.addSchoolContact(this, contact);
            this.contacts = null;
            return contact;
        }
    }, {
        key: "addHoliday",
        value: function addHoliday(date) {
            this.dataSource.addHoliday(this, date);
            this.holidays = null;
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "RegistrationData": this.registrationData,
                "Address": this.Address != null ? this.Address.toJSON() : null,
                "Image": this.Image != null ? this.Image.toJSON() : null,
                "Phone1": this.Phone1,
                "Phone2": this.Phone2,
                "Fax": this.Fax,
                "Email": this.Email
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "RegistrationData",
        get: function get() {
            return this.registrationData;
        }
    }, {
        key: "Address",
        get: function get() {
            return this.address;
        }
    }, {
        key: "Image",
        get: function get() {
            return this.image;
        }
    }, {
        key: "Phone1",
        get: function get() {
            return this.phone1;
        }
    }, {
        key: "Phone2",
        get: function get() {
            return this.phone2;
        }
    }, {
        key: "Fax",
        get: function get() {
            return this.fax;
        }
    }, {
        key: "Email",
        get: function get() {
            return this.email;
        }
    }, {
        key: "Contacts",
        get: function get() {
            if (this.contacts == null) {
                this.contacts = this.dataSource.getSchoolContacts(this);
            }
            return this.contacts;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            if (this.academicYear == null) {
                this.academicYear = this.dataSource.getCurrentAcademicYearForSchool(this);
            }
            return this.academicYear;
        }
    }, {
        key: "Classes",
        get: function get() {
            if (this.classes == null) {
                this.classes = this.dataSource.getClassesForAcademicYear(this.AcademicYear, this);
            }
            return this.classes;
        }
    }, {
        key: "Teachers",
        get: function get() {
            if (this.teachers == null) {
                this.teachers = this.dataSource.getTeachersForAcademicYear(this.AcademicYear, this);
            }
            return this.teachers;
        }
    }, {
        key: "Students",
        get: function get() {
            if (this.students == null) {
                this.students = this.dataSource.getStudentsForAcademicYear(this.AcademicYear, this);
            }
            return this.students;
        }
    }, {
        key: "Holidays",
        get: function get() {
            if (this.holidays == null) {
                this.holidays = this.dataSource.getHolidays(this);
            }
            return this.holidays;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            if (json == null) return null;
            return new School(dataSource, json["Name"], json["RegistrationData"], _Address.Address.fromJSON(json["Address"]), _Image.Image.fromJSON(json["Image"]), json["Phone1"], json["Phone2"], json["Fax"], json["Email"], id);
        }
    }]);

    return School;
}(_Entity2.Entity);
//# sourceMappingURL=School.js.map