"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Exam = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity");

var _ExamResult = require("./ExamResult");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Exam = exports.Exam = function (_Entity) {
    _inherits(Exam, _Entity);

    function Exam(dataSource, name, date, passMarks, passGrades, cls, subject) {
        var id = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : null;

        _classCallCheck(this, Exam);

        var _this = _possibleConstructorReturn(this, (Exam.__proto__ || Object.getPrototypeOf(Exam)).call(this, dataSource, id));

        _this.name = name;
        _this.date = date;
        _this.passMarks = passMarks;
        _this.passGrades = passGrades;
        _this.cls = cls;
        _this.subject = subject;
        _this.questions = null;
        _this.results = null;
        return _this;
    }

    _createClass(Exam, [{
        key: "addQuestion",
        value: function addQuestion(question) {
            this.dataSource.addQuestionToExam(this, question);
            this.questions = null;
        }
    }, {
        key: "removeQuestion",
        value: function removeQuestion(question) {
            this.dataSource.removeQuestionFromExam(this, question);
            this.questions = null;
        }
    }, {
        key: "update",
        value: function update() {
            this.dataSource.updateExam(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Date": this.date.toString(),
                "PassMarks": this.passMarks,
                "PassGrades": this.passGrades,
                "Class": this.cls.Id,
                "Subject": this.Subject.Id
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Date",
        get: function get() {
            return this.date;
        }
    }, {
        key: "PassMarks",
        get: function get() {
            return this.passMarks;
        }
    }, {
        key: "PassGrades",
        get: function get() {
            return this.passGrades;
        }
    }, {
        key: "Class",
        get: function get() {
            return this.cls;
        }
    }, {
        key: "Subject",
        get: function get() {
            return this.subject;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            return this.Class.AcademicYear;
        }
    }, {
        key: "Questions",
        get: function get() {
            if (this.questions == null) {
                this.questions = this.dataSource.getExamQuestions(this);
            }
            return this.questions;
        }
    }, {
        key: "Results",
        get: function get() {
            if (this.results == null) {
                this.results = new Map();
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = this.Class.Students[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var student = _step.value;

                        var result = new _ExamResult.ExamResult(this.dataSource, this, student);
                        this.results.set(student, result);
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }
            return this.results;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new Exam(dataSource, json["Name"], new Date(json["Date"]), json["PassMarks"], json["PassGrades"], dataSource.getClass(json["Class"]), dataSource.getSubject(json["Subject"]), id);
        }
    }]);

    return Exam;
}(_Entity2.Entity);
//# sourceMappingURL=Exam.js.map