"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.AcademicYear = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _School = require("./School.js");

var _Address = require("./Address.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AcademicYear = exports.AcademicYear = function (_Entity) {
    _inherits(AcademicYear, _Entity);

    function AcademicYear(dataSource, name, board, startDate, endDate) {
        var id = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

        _classCallCheck(this, AcademicYear);

        var _this = _possibleConstructorReturn(this, (AcademicYear.__proto__ || Object.getPrototypeOf(AcademicYear)).call(this, dataSource, id));

        _this.name = name;
        _this.board = board;
        _this.startDate = startDate;
        _this.endDate = endDate;
        _this.schools = null;
        return _this;
    }

    _createClass(AcademicYear, [{
        key: "addSchool",
        value: function addSchool(name, registrationData, image, building, street, city, district, state, country, postal, phone1, phone2, fax, email) {
            var address = new _Address.Address(building, street, city, district, state, country, postal);
            var school = new _School.School(this.dataSource, name, registrationData, address, image, phone1, phone2, fax, email);
            this.dataSource.addSchool(school);
            this.dataSource.addSchoolToAcademicYear(this, school);
            return school;
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Board": this.board,
                "StartDate": this.startDate.toString(),
                "EndDate": this.endDate.toString()
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Board",
        get: function get() {
            return this.board;
        }
    }, {
        key: "StartDate",
        get: function get() {
            return this.startDate;
        }
    }, {
        key: "EndDate",
        get: function get() {
            return this.endDate;
        }
    }, {
        key: "Schools",
        get: function get() {
            if (this.schools == null) {
                this.schools = this.dataSource.getSchoolsForAcademicYear(this);
            }
            return this.schools;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, id, json) {
            return new AcademicYear(dataSource, json["Name"], json["Board"], new Date(json["StartDate"]), json["EndDate"] != null ? new Date(json["EndDate"]) : null, id);
        }
    }]);

    return AcademicYear;
}(_Entity2.Entity);
//# sourceMappingURL=AcademicYear.js.map