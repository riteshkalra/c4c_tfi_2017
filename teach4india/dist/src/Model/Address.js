"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Address = exports.Address = function () {
    function Address(building, street, city, district, state, country, postal) {
        _classCallCheck(this, Address);

        this.building = building;
        this.street = street;
        this.city = city;
        this.district = district;
        this.state = state;
        this.country = country;
        this.postal = postal;
    }

    _createClass(Address, [{
        key: "toJSON",
        value: function toJSON() {
            return {
                "BuildingName": this.building,
                "StreetName": this.street,
                "City": this.city,
                "District": this.district,
                "State": this.state,
                "Country": this.country,
                "Postal": this.postal
            };
        }
    }, {
        key: "BuildingName",
        get: function get() {
            return this.building;
        }
    }, {
        key: "StreetName",
        get: function get() {
            return this.street;
        }
    }, {
        key: "City",
        get: function get() {
            return this.city;
        }
    }, {
        key: "District",
        get: function get() {
            return this.district;
        }
    }, {
        key: "State",
        get: function get() {
            return this.state;
        }
    }, {
        key: "Country",
        get: function get() {
            return this.country;
        }
    }, {
        key: "Postal",
        get: function get() {
            return this.postal;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataSource, json) {
            if (json == null) {
                return null;
            }
            return new Address(json["BuildingName"], json["StreetName"], json["City"], json["District"], json["State"], json["Country"], json["Postal"]);
        }
    }]);

    return Address;
}();
//# sourceMappingURL=Address.js.map