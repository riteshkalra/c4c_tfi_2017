"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Image = exports.Image = function () {
    function Image(name, contents) {
        _classCallCheck(this, Image);

        this.name = name;
        this.contents = contents;
    }

    _createClass(Image, [{
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Contents": this.contents
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Contents",
        get: function get() {
            return this.contents;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(json) {
            if (json == null) return null;
            return new Image(json["Name"], json["Contents"]);
        }
    }]);

    return Image;
}();
//# sourceMappingURL=Image.js.map