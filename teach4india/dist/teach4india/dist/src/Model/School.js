"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.School = undefined;

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

var _Entity2 = require("./Entity.js");

var _Address = require("./Address.js");

var _Image = require("./Image.js");

var _Teacher = require("./Teacher.js");

var _Class = require("./Class.js");

var _Contact = require("./Contact.js");

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _possibleConstructorReturn(self, call) {
    if (!self) {
        throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var School = exports.School = function (_Entity) {
    _inherits(School, _Entity);

    function School(dataService, name, address, image) {
        var id = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;

        _classCallCheck(this, School);

        var _this = _possibleConstructorReturn(this, (School.__proto__ || Object.getPrototypeOf(School)).call(this, dataService, id));

        _this.name = name;
        _this.address = address;
        _this.image = image;
        _this.contacts = null;
        _this.academicYear = null;
        _this.classes = null;
        _this.teachers = null;
        return _this;
    }

    _createClass(School, [{
        key: "addTeacher",
        value: function addTeacher(firstName, lastName, empId, gender, dob, image, building, street, city, districy, state, country, postal, phone1, phone2, email) {
            // Add a new teacher to this school
            var address = new _Address.Address(building, street, city, districy, state, country, postal);
            var teacher = new _Teacher.Teacher(this.dataService, firstName, lastName, empId, gender, dob, address, image);
            this.dataService.addTeacher(teacher);
            this.dataService.addTeacherToSchool(this, teacher);
            this.teachers = null;
            return teacher;
        }
    }, {
        key: "removeTeacher",
        value: function removeTeacher(teacher) {
            // Teacher leaves the school, so update the end date
            this.dataService.removeTeacherFromSchool(this, teacher);
            this.teachers = null;
        }
    }, {
        key: "addClass",
        value: function addClass(name, grade, section, code, teachingMedium) {
            var academicYear = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : null;

            if (academicYear == null) {
                academicYear = this.AcademicYear;
            }
            var cls = new _Class.Class(this.dataService, name, grade, section, code, teachingMedium, academicYear, this);
            this.dataService.addClass(cls);
            this.classes = null;
            return cls;
        }
    }, {
        key: "getPrevClass",
        value: function getPrevClass(cls) {
            for (var i = 0; i < this.Classes.length; i++) {
                var curCls = this.Classes[i];
                if (this.Classes[i].Grade == cls.Grade - 1 && this.Classes[i].Section == cls.Section) {
                    return this.Classes[i];
                }
            }
            return null;
        }
    }, {
        key: "getPreviousAcademicYear",
        value: function getPreviousAcademicYear(academicYear) {
            return this.dataService.getPreviousAcademicYear(academicYear);
        }
    }, {
        key: "getClassesFromPrevAcademicYear",
        value: function getClassesFromPrevAcademicYear(academicYear) {
            var prevAcademicYear = this.getPrevAcademicYear(academicYear);
            return this.dataService.getClassesForAcademicYear(prevAcademicYear, this);
        }
    }, {
        key: "addAcademicYear",
        value: function addAcademicYear(academicYear) {
            this.dataService.addSchoolToAcademicYear(academicYear, this);
            var prevClasses = this.getClassesFromPrevAcademicYear(academicYear);
            // Clone classes from previous academic year
            if (prevClasses != null) {
                for (var cls in this.classes) {
                    // Add a new class for this year
                    var newCls = this.addClass(cls.name, cls.grade, cls.section, cls.code, cls.teachingMedium, academicYear, this);
                    // Map the same admin teacher from last year
                    this.dataService.addAdminTeacher(newCls, cls.AdminTeacher);
                    // Copy all subjects from the old class
                    for (j = 0; j < cls.Subjects.length; j++) {
                        var subject = cls.Subjects[j];
                        var newSubject = newCls.addSubject(subject.Name, subject.Category, subject.IsMandatory);
                        newCls.addTeacherForSubject(cls.Teachers[subject], newSubject);
                    }
                    // Copy all students from previous grade and section
                    var prevCls = this.getPrevClass(cls);
                    for (j = 0; j < prevCls.Students.length; j++) {
                        newCls.addExistingStudent(prevCls.Students[j]);
                    }
                    // Students by optional subjects has to be done manually
                }
            }
        }
    }, {
        key: "setAcademicYear",
        value: function setAcademicYear(academicYear) {
            this.academicYear = academicYear;
            this.classes = null;
            this.teachers = null;
        }
    }, {
        key: "addContact",
        value: function addContact(firstName, lastName, relationship, phone, mobile, fax, email) {
            var contact = new _Contact.Contact(firstName, lastName, relationship, phone, mobile, fax, email, "School", this.Id);
            this.dataService.addContact(contact);
            this.contacts = null;
        }
    }, {
        key: "updateContact",
        value: function updateContact(contact) {
            this.dataService.updateContact(this, contact);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Address": this.Address.toJSON(),
                "Image": this.Image.toJSON()
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Address",
        get: function get() {
            return this.address;
        }
    }, {
        key: "Image",
        get: function get() {
            return this.image;
        }
    }, {
        key: "Contacts",
        get: function get() {
            if (this.contacts == null) {
                this.contacts = this.dataService.getSchoolContacts(this);
            }
            return this.contacts;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            if (this.academicYear == null) {
                this.academicYear = this.dataService.getCurrentAcademicYear(this);
            }
            return this.academicYear;
        }
    }, {
        key: "Classes",
        get: function get() {
            if (this.classes == null) {
                this.classes = this.dataService.getClassesForAcademicYear(this.AcademicYear, this);
            }
            return this.classes;
        }
    }, {
        key: "Teachers",
        get: function get() {
            if (this.teachers == null) {
                this.teachers = this.dataService.getTeachersForAcademicYear(this.academicYear, this);
            }
            return null;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new School(dataService, json["Name"], _Address.Address.fromJSON(json["Address"]), _Image.Image.fromJSON(json["Image"]), id);
        }
    }]);

    return School;
}(_Entity2.Entity);
//# sourceMappingURL=School.js.map
//# sourceMappingURL=School.js.map