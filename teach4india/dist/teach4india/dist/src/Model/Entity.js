"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var Entity = exports.Entity = function () {
    function Entity(dataService) {
        var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        _classCallCheck(this, Entity);

        this.dataService = dataService;
        this.id = id;
        this.name = null;
    }

    _createClass(Entity, [{
        key: "setId",
        value: function setId(newId) {
            if (id == null) {
                id = newId;
            }
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {};
        }
    }, {
        key: "Id",
        get: function get() {
            return this.id;
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }]);

    return Entity;
}();
//# sourceMappingURL=Entity.js.map
//# sourceMappingURL=Entity.js.map