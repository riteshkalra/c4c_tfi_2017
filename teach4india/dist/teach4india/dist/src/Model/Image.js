"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var Image = exports.Image = function () {
    function Image(dataService, id, name, contents) {
        _classCallCheck(this, Image);

        this.dataService = dataService;
        this.id = id;
        this.name = name;
        this.contents = contents;
    }

    _createClass(Image, [{
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Contents",
        get: function get() {
            return this.contents;
        }
    }]);

    return Image;
}();
//# sourceMappingURL=Image.js.map
//# sourceMappingURL=Image.js.map