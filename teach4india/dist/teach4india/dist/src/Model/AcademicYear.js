"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

var _Entity2 = require("./Entity.js");

var _School = require("./School.js");

var _Address = require("./Address.js");

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _possibleConstructorReturn(self, call) {
    if (!self) {
        throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

// Represents an academic year
var AcademicYear = function (_Entity) {
    _inherits(AcademicYear, _Entity);

    function AcademicYear(dataService, name, startDate, endDate) {
        var id = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;

        _classCallCheck(this, AcademicYear);

        var _this = _possibleConstructorReturn(this, (AcademicYear.__proto__ || Object.getPrototypeOf(AcademicYear)).call(this, dataService, id));

        _this.name = name;
        _this.startDate = startDate;
        _this.endDate = endDate;
        return _this;
    }

    _createClass(AcademicYear, [{
        key: "addSchool",
        value: function addSchool(name, image, building, street, city, districy, state, country, postal, phone1, phone2, fax, email) {
            var address = (0, _Address.Address)(building, street, city, districy, state, country, postal);
            var school = (0, _School.School)(name, address, image);
            this.dataService.addSchool(school);
            this.dataService.addSchoolToAcademicYear(this, school);
        }
    }, {
        key: "getSchools",
        value: function getSchools() {
            // Get all schools for this academic year
            this.dataService.getSchoolsForAcademicYear(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "StartDate": this.startDate,
                "EndDate": this.endDate
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "StartDate",
        get: function get() {
            return this.startDate;
        }
    }, {
        key: "EndDate",
        get: function get() {
            return this.endDate;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new AcademicYear(dataService, json["Name"], json["StartDate"], json["EndDate"], id);
        }
    }]);

    return AcademicYear;
}(_Entity2.Entity);

module.exports = {
    AcademicYear: AcademicYear
};
//# sourceMappingURL=AcademicYear.js.map
//# sourceMappingURL=AcademicYear.js.map