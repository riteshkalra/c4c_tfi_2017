"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Contact = undefined;

var _createClass = function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
}();

var _Entity2 = require("./Entity.js");

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

function _possibleConstructorReturn(self, call) {
    if (!self) {
        throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
        throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
    }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

var Contact = exports.Contact = function (_Entity) {
    _inherits(Contact, _Entity);

    function Contact(dataService, firstName, lastName, relationship, phone, mobile, fax, email, entityName, entityId) {
        var id = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : null;

        _classCallCheck(this, Contact);

        var _this = _possibleConstructorReturn(this, (Contact.__proto__ || Object.getPrototypeOf(Contact)).call(this, dataService, id));

        _this.firstName = firstName;
        _this.lastName = lastName;
        _this.relationship = relationship;
        _this.phone = phone;
        _this.mobile = mobile;
        _this.fax = fax;
        _this.email = email;
        _this.entityName = entityName;
        _this.entityId = entityId;
        return _this;
    }

    _createClass(Contact, [{
        key: "toJSON",
        value: function toJSON() {
            return {
                "FirstName": this.firstName,
                "LastName": this.lastName,
                "Relationship": this.relationship,
                "Phone": this.phone,
                "Mobile": this.mobile,
                "Fax": this.fax,
                "Email": this.email,
                "EntityName": this.entityName,
                "EntityId": this.entityId
            };
        }
    }, {
        key: "FirstName",
        get: function get() {
            return this.firstName;
        }
    }, {
        key: "LastName",
        get: function get() {
            return this.lastName;
        }
    }, {
        key: "Name",
        get: function get() {
            return this.firstName + this.lastName;
        }
    }, {
        key: "Relationship",
        get: function get() {
            return this.relationship;
        }
    }, {
        key: "Phone",
        get: function get() {
            return this.phone;
        }
    }, {
        key: "Mobile",
        get: function get() {
            return this.mobile;
        }
    }, {
        key: "Fax",
        get: function get() {
            return this.fax;
        }
    }, {
        key: "Email",
        get: function get() {
            return this.email;
        }
    }, {
        key: "Entity",
        get: function get() {
            return this.entityName;
        }
    }, {
        key: "EntityId",
        get: function get() {
            return this.entityId;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new Contact(dataService, json["FirstName"], json["LastName"], json["Relationship"], json["Phone"], json["Mobile"], json["Fax"], json["Email"], json["EntityName"], json["EntityId"], id);
        }
    }]);

    return Contact;
}(_Entity2.Entity);
//# sourceMappingURL=Contact.js.map
//# sourceMappingURL=Contact.js.map