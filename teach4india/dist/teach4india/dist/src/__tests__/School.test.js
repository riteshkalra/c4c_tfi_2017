"use strict";

var _School = require("../Model/School.js");

var _MockDataSource = require("../DataSource/MockDataSource.js");

describe('Student Tests', function () {
    beforeAll(function () {});
    it('Test Name', function () {
        var sch = new _School.School(null, 'School1', null, null, null);
        expect(sch.Name).toBe('School1');
        var ds = new _MockDataSource.MockDataSource();
        console.log(ds.getAcademicYears().length);
    });
});
//# sourceMappingURL=School.test.js.map
//# sourceMappingURL=School.test.js.map