"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Exam = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Exam = exports.Exam = function (_Entity) {
    _inherits(Exam, _Entity);

    function Exam(dataService, name, date, passMarks, cls, subject) {
        var id = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : null;

        _classCallCheck(this, Exam);

        var _this = _possibleConstructorReturn(this, (Exam.__proto__ || Object.getPrototypeOf(Exam)).call(this, dataService, id));

        _this.name = name;
        _this.date = date;
        _this.passMarks = passMarks;
        _this.cls = cls;
        _this.subject = subject;
        _this.questions = null;
        return _this;
    }

    _createClass(Exam, [{
        key: "addQuestion",
        value: function addQuestion(question) {
            this.dataService.addQuestionToExam(this, question);
            this.questions = null;
        }
    }, {
        key: "removeQuestion",
        value: function removeQuestion(question) {
            this.dataService.removeQuestionFromExam(this, question);
            this.questions = null;
        }
    }, {
        key: "getResults",
        value: function getResults(student) {
            return this.dataService.getExamResults(student, this);
        }
    }, {
        key: "update",
        value: function update() {
            this.dataService.updateExam(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Date": this.date,
                "PassMarks": this.passMarks,
                "Class": this.cls.Id,
                "Subject": this.Subject.Id
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Date",
        get: function get() {
            return this.date;
        }
    }, {
        key: "PassMarks",
        get: function get() {
            return this.passMarks;
        }
    }, {
        key: "Class",
        get: function get() {
            return this.cls;
        }
    }, {
        key: "Subject",
        get: function get() {
            return this.subject;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            return this.Class.AcademicYear;
        }
    }, {
        key: "Questions",
        get: function get() {
            if (this.questions == null) {
                this.questions = this.dataService.getExamQuestions(this);
            }
            return this.questions;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new Exam(dataService, json["Name"], json["Date"], json["PassMarks"], dataService.getClassById(json["Class"]), dataService.getSubjectById(json["Subject"]), id);
        }
    }]);

    return Exam;
}(_Entity2.Entity);
//# sourceMappingURL=Exam.js.map