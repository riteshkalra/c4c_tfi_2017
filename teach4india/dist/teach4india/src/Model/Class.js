"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Class = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Student = require("./Student.js");

var _Subject = require("./Subject.js");

var _Teacher = require("./Teacher.js");

var _Exam = require("./Exam.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Class = exports.Class = function (_Entity) {
    _inherits(Class, _Entity);

    function Class(dataService, name, grade, section, code, teachingMedium, academicYear, school) {
        var id = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : null;

        _classCallCheck(this, Class);

        var _this = _possibleConstructorReturn(this, (Class.__proto__ || Object.getPrototypeOf(Class)).call(this, dataService, id));

        _this.name = name;
        _this.grade = grade;
        _this.section = section;
        _this.code = code;
        _this.teachingMedium = teachingMedium;
        _this.school = school;
        _this.academicYear = academicYear;
        _this.subjects = null;
        _this.adminTeacher = null;
        _this.teachers = null;
        _this.students = null;
        _this.studentsByOptionalSubject = null;
        return _this;
    }

    _createClass(Class, [{
        key: "addAdminTeacher",
        value: function addAdminTeacher(teacher) {
            this.dataService.addAdminTeacher(this, teacher);
            this.adminTeacher = teacher;
        }
    }, {
        key: "addSubject",
        value: function addSubject(name, category, isMandatory) {
            var subject = new _Subject.Subject(this.dataService, name, category, isMandatory);
            this.dataService.addSubject(subject);
            this.addExistingSubject(subject);
        }
    }, {
        key: "addExistingSubject",
        value: function addExistingSubject(subject) {
            this.dataService.addSubjectToClass(this, subject);
            this.subjects = null;
        }
    }, {
        key: "removeSubject",
        value: function removeSubject(subject) {
            this.dataService.removeSubjectFromClass(this, subject);
            this.subjects = null;
        }
    }, {
        key: "addTeacherForSubject",
        value: function addTeacherForSubject(teacher, subject) {
            this.dataService.addTeacherToSubject(this, teacher, subject);
            this.teachers = null;
        }
    }, {
        key: "removeTeacherForSubject",
        value: function removeTeacherForSubject(teacher, subject) {
            this.dataService.removeTeacherForSubject(this, teacher, subject);
            this.teachers = null;
        }
    }, {
        key: "addStudent",
        value: function addStudent(firstName, lastName, rollNo, gender, dob, image, building, street, city, districy, state, country, postal, phone1, phone2, email) {
            // Add a new student to this school in a class
            var address = new Address(building, street, city, districy, state, country, postal);
            var student = new _Student.Student(this.dataService, firstName, lastName, rollNo, gender, dob, image, address, phone1, phone2, email);
            this.dataService.addStudent(student);
            return this.addExistingStudent(student);
        }
    }, {
        key: "addExistingStudent",
        value: function addExistingStudent(student) {
            this.dataService.addStudentToClass(this, student);
            this.students = null;
            return student;
        }
    }, {
        key: "removeStudent",
        value: function removeStudent(student) {
            // Student leaves the school, so update the end date
            this.dataService.removeStudentFromClass(this, student);
            this.students = null;
        }
    }, {
        key: "addStudentToSubject",
        value: function addStudentToSubject(student, subject) {
            if (!subject.IsMandatory) {
                this.dataService.addStudentToSubject(this, subject, student);
                this.studentsByOptionalSubject = null;
            }
        }
    }, {
        key: "removeStudentFromSubject",
        value: function removeStudentFromSubject(student, subject) {
            if (!subject.IsMandatory) {
                this.dataService.removeStudentFromSubject(this, subject, student);
                this.studentsByOptionalSubject = null;
            }
        }
    }, {
        key: "getStudentsBySubject",
        value: function getStudentsBySubject(subject) {
            if (!subject.IsMandatory) {
                return this.StudentsByOptionalSubjects[subject];
            } else {
                return this.Students;
            }
        }
    }, {
        key: "addExam",
        value: function addExam(name, date, passMarks, subject) {
            var exam = new _Exam.Exam(name, date, passMarks, subject);
            this.dataService.addExam(this, exam);
        }
    }, {
        key: "removeExam",
        value: function removeExam(exam) {
            this.dataService.removeExam(exam);
        }
    }, {
        key: "getExamsForSubject",
        value: function getExamsForSubject(subject) {
            return this.dataService.getExamsForSubject(this, subject);
        }
    }, {
        key: "getExamResults",
        value: function getExamResults(student, exam) {
            return this.dataService.getExamResults(student, exam);
        }
    }, {
        key: "update",
        value: function update() {
            this.dataService.updateClass(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Grade": this.grade,
                "Section": this.section,
                "Code": this.code,
                "TeachingMedium": this.teachingMedium,
                "AcademicYear": this.academicYear.Id,
                "School": this.school.Id
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Grade",
        get: function get() {
            return this.grade;
        }
    }, {
        key: "Section",
        get: function get() {
            return this.section;
        }
    }, {
        key: "Code",
        get: function get() {
            return this.code;
        }
    }, {
        key: "TeachingMedium",
        get: function get() {
            return this.teachingMedium;
        }
    }, {
        key: "School",
        get: function get() {
            return this.school;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            return this.academicYear;
        }
    }, {
        key: "Subjects",
        get: function get() {
            if (this.subjects == null) {
                this.subjects = this.dataService.getSubjectsForClass(this);
            }
            return this.subjects;
        }
    }, {
        key: "Teachers",
        get: function get() {
            if (this.teachers == null) {
                this.teachers = this.dataService.getTeachersForClass(this);
            }
            return this.teachers;
        }
    }, {
        key: "Students",
        get: function get() {
            if (this.students == null) {
                this.students = this.dataService.getStudentsFromClass(this);
            }
            return this.students;
        }
    }, {
        key: "AdminTeacher",
        get: function get() {
            if (this.adminTeacher == null) {
                this.adminTeacher = this.dataService.getAdminTeacher(this);
            }
            return this.adminTeacher;
        }
    }, {
        key: "StudentsByOptionalSubjects",
        get: function get() {
            if (this.studentsByOptionalSubject == null) {
                this.studentsByOptionalSubject = {};
                for (var i = 0; i < this.Subjects.length; i++) {
                    var subject = this.Subjects[i];
                    this.studentsByOptionalSubject[subject] = this.dataService.getStudentsBySubject(cls, this.Students, subject);
                }
            }
            return this.studentsByOptionalSubject;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new Class(dataService, json["Name"], json["Grade"], json["Section"], json["Code"], json["TeachingMedium"], dataService.getAcademicYear(json["AcademicYear"]), dataService.getSchool(json["School"]), id);
        }
    }]);

    return Class;
}(_Entity2.Entity);
//# sourceMappingURL=Class.js.map