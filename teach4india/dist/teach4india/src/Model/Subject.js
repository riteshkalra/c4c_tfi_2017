"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Subject = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Question = require("./Question.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Subject = exports.Subject = function (_Entity) {
    _inherits(Subject, _Entity);

    function Subject(dataService, name, category, isMandatory) {
        var id = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;

        _classCallCheck(this, Subject);

        var _this = _possibleConstructorReturn(this, (Subject.__proto__ || Object.getPrototypeOf(Subject)).call(this, dataService, id));

        _this.name = name;
        _this.category = category;
        _this.isMandatory = isMandatory;
        _this.questions = null;
        return _this;
    }

    _createClass(Subject, [{
        key: "addQuestion",
        value: function addQuestion(description, concept, qtype, difficultyLevel, options, answer) {
            var question = new _Question.Question(description, concept, qtype, difficultyLevel, options, answer, this);
            this.dataService.addQuestion(question);
            this.questions = null;
        }
    }, {
        key: "removeQuestion",
        value: function removeQuestion(question) {
            this.dataService.removeQuestion(question);
            this.questions = null;
        }
    }, {
        key: "update",
        value: function update() {
            this.dataService.updateSubject(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Name": this.name,
                "Category": this.category,
                "IsMandatory": this.isMandatory
            };
        }
    }, {
        key: "Name",
        get: function get() {
            return this.name;
        }
    }, {
        key: "Category",
        get: function get() {
            return this.category;
        }
    }, {
        key: "IsMandatory",
        get: function get() {
            return this.isMandatory;
        }
    }, {
        key: "Questions",
        get: function get() {
            if (this.questions == null) {
                this.questions = this.dataService.getQuestions(this);
            }
            return this.questions;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new Subject(dataService, json["Name"], json["Category"], json["IsMandatory"], id);
        }
    }]);

    return Subject;
}(_Entity2.Entity);
//# sourceMappingURL=Subject.js.map