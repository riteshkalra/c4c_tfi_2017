"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Teacher = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

var _Contact = require("./Contact.js");

var _Address = require("./Address.js");

var _Image = require("./Image.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Teacher = exports.Teacher = function (_Entity) {
    _inherits(Teacher, _Entity);

    function Teacher(dataService, firstName, lastName, empId, gender, dob, address, image) {
        var id = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : null;

        _classCallCheck(this, Teacher);

        var _this = _possibleConstructorReturn(this, (Teacher.__proto__ || Object.getPrototypeOf(Teacher)).call(this, dataService, id));

        _this.firstName = firstName;
        _this.lastName = lastName;
        _this.empId = empId;
        _this.gender = gender;
        _this.dob = dob;
        _this.image = image;
        _this.address = address;
        _this.contacts = null;
        _this.acadYear = null;
        _this.classes = null;
        _this.subjects = null;
        return _this;
    }

    _createClass(Teacher, [{
        key: "addContact",
        value: function addContact(firstName, lastName, relationship, phone, mobile, fax, email) {
            var contact = new _Contact.Contact(firstName, lastName, relationship, phone, mobile, fax, email);
            this.dataService.addContact(this, contact);
            this.contacts = null;
        }
    }, {
        key: "updateContact",
        value: function updateContact(contact) {
            this.dataService.updateContact(this, contact);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "FirstName": this.firstName,
                "LastName": this.lastName,
                "EmpID": this.empId,
                "Gender": this.gender,
                "DOB": this.dob,
                "Address": this.address.toJSON(),
                "Image": this.image.toJSON(),
                "Phone1": this.phone1,
                "Phone2": this.phone2,
                "Email": this.email
            };
        }
    }, {
        key: "FirstName",
        get: function get() {
            return this.firstName;
        }
    }, {
        key: "LastName",
        get: function get() {
            return this.lastName;
        }
    }, {
        key: "EmployeeID",
        get: function get() {
            return this.empId;
        }
    }, {
        key: "Gender",
        get: function get() {
            return this.gender;
        }
    }, {
        key: "DOB",
        get: function get() {
            return this.dob;
        }
    }, {
        key: "Image",
        get: function get() {
            return this.image;
        }
    }, {
        key: "Address",
        get: function get() {
            return this.address;
        }
    }, {
        key: "Contact",
        get: function get() {
            if (this.contacts == null) {
                this.contacts = this.dataService.getContacts(this);
            }
            return this.contacts;
        }
    }, {
        key: "School",
        get: function get() {
            if (this.school == null) {
                this.school = this.dataService.getSchoolForTeacher(this);
            }
            return this.school;
        }
    }, {
        key: "AcademicYear",
        get: function get() {
            return this.School.AcademicYear;
        }
    }, {
        key: "Classes",
        get: function get() {
            // Classes where this teacher is the admin
            if (this.cls == null) {
                this.cls = this.dataService.getClassesForTeacher(this.AcademicYear, this.School, this);
            }
            return this.cls;
        }
    }, {
        key: "SubjectsByClasses",
        get: function get() {
            // All subjects handled by this teacher in various classes
            if (this.subjects == null) {
                this.subjects = this.dataService.getSubjectsForTeacher(this.AcademicYear, this.School, this.Classes, this);
            }
            return this.subjects;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new Teacher(dataService, json["FirstName"], json["LastName"], json["EmpID"], json["Gender"], json["DOB"], _Address.Address.fromJSON(json["Address"]), _Image.Image.fromJSON(json["Image"]), json["Phone1"], json["Phone2"], json["Email"], id);
        }
    }]);

    return Teacher;
}(_Entity2.Entity);
//# sourceMappingURL=Teacher.js.map