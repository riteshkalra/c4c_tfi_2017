"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Mastery = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Mastery = exports.Mastery = function (_Entity) {
    _inherits(Mastery, _Entity);

    function Mastery(dataService, question, masteryLevel, mark) {
        var id = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;

        _classCallCheck(this, Mastery);

        var _this = _possibleConstructorReturn(this, (Mastery.__proto__ || Object.getPrototypeOf(Mastery)).call(this, dataService, id));

        _this.question = question;
        _this.masteryLevel = masteryLevel;
        _this.mark = mark;
        return _this;
    }

    _createClass(Mastery, [{
        key: "update",
        value: function update() {
            this.dataService.updateMastery(this);
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Question": this.Question.Id,
                "MasteryLevel": this.masteryLevel,
                "Mark": this.mark
            };
        }
    }, {
        key: "Question",
        get: function get() {
            return this.question;
        }
    }, {
        key: "MasteryLevel",
        get: function get() {
            return this.masteryLevel;
        }
    }, {
        key: "Mark",
        get: function get() {
            return this.mark;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new Mastery(dataService, dataService.getQuestionById(json["Question"]), json["MasteryLevel"], json["Mark"], id);
        }
    }]);

    return Mastery;
}(_Entity2.Entity);
//# sourceMappingURL=Mastery.js.map