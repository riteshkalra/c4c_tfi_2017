"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ExamResult = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Entity2 = require("./Entity");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ExamResult = exports.ExamResult = function (_Entity) {
    _inherits(ExamResult, _Entity);

    function ExamResult(dataService, exam, student) {
        var id = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

        _classCallCheck(this, ExamResult);

        var _this = _possibleConstructorReturn(this, (ExamResult.__proto__ || Object.getPrototypeOf(ExamResult)).call(this, dataService, id));

        _this.exam = exam;
        _this.student = student;
        _this.answers = null;
        return _this;
    }

    _createClass(ExamResult, [{
        key: "addAnswer",
        value: function addAnswer(question, answer, mastery) {
            this.dataService.addExamAnswer(this.Exam, this.Student, question, answer, mastery);
            this.answers = null;
        }
    }, {
        key: "updateAnswer",
        value: function updateAnswer(question, answer, mastery) {
            this.dataService.updateExamAnswer(this.Exam, this.Student, question, answer, mastery);
            this.answers = null;
        }
    }, {
        key: "toJSON",
        value: function toJSON() {
            return {
                "Exam": this.Exam.Id,
                "Student": this.Student.Id
            };
        }
    }, {
        key: "Exam",
        get: function get() {
            return this.exam;
        }
    }, {
        key: "Student",
        get: function get() {
            return this.student;
        }
    }, {
        key: "Answers",
        get: function get() {
            // Dictionary of question to answer
            if (this.answers == null) {
                //    this.answers = this.dataService.getExamResult(student, exam).answers;
            }
            return this.answers;
        }
    }, {
        key: "Mark",
        get: function get() {
            // Calculate mark based on mastery
            return null;
        }
    }], [{
        key: "fromJSON",
        value: function fromJSON(dataService, id, json) {
            return new ExamResult(dataService, dataService.getExamById(json["Exam"]), dataService.getStudentById(json["Student"]), id);
        }
    }]);

    return ExamResult;
}(_Entity2.Entity);
//# sourceMappingURL=ExamResult.js.map